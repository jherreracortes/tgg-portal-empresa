package cl.mobid;

import com.jcraft.jsch.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Properties;

public class GetFile {

	public GetFile() {
	}

	public static java.util.ArrayList<DataFile> isfile(String ip, String ruta, String file) {
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader buffer = null;
		ArrayList<DataFile> data = new ArrayList<DataFile>();
		try {
			MainValidator.toLog("[" + MainValidator.fecha_log(false) + "] EMPIEZA CONEXION " + ip + " " + ruta + file);
			session = jsch.getSession("procfile", ip);
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword("proc_tgg-file");
			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			MainValidator.toLog("Is connected to IP:" + channel.isConnected());
			sftpChannel.cd(ruta);
			InputStream is = sftpChannel.get(file);
			buffer = new BufferedReader(new InputStreamReader(is));
			String getLine = "";
			while ((getLine = buffer.readLine()) != null) {
				DataFile df = new DataFile();
				String[] split = getLine.split(";",3);
				df.setMsisdn(split[0]);
				df.setSender(split[1]);
				df.setTexto(split[2]);
				data.add(df);
			}
			sftpChannel.exit();
			session.disconnect();
			MainValidator.toLog("[" + MainValidator.fecha_log(false) + "] Line: " + data.size());
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = MainValidator.exceptionToString(e);
			MainValidator.toLog(sb.toString());
		} finally {
			try {
				if (buffer != null)
					buffer.close();
			} catch (Exception e) {
				StringBuilder sb = new StringBuilder();
				sb = MainValidator.exceptionToString(e);
				MainValidator.toLog(sb.toString());
			}
		}
		MainValidator.toLog("OUT");
		return data;
	}

}
