package cl.mobid;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;

public class MainValidator {
	static ArrayList<TGG_Country> tgg;
	public static String BASE_DATOS = "wsa_telcel_la";
	
	public MainValidator() {
	}

	public static void main(String[] args) {
		tgg = new ArrayList<TGG_Country>();
		ArrayList<HashMap<String, String>> d = DBAccess.getPaisData();
		for (HashMap<String, String> hm : d) {
			TGG_Country tc = new TGG_Country();
			tc.setId_country((String) hm.get("id_country"));
			tc.setLargo((String) hm.get("length"));
			tc.setTime_gmt((String) hm.get("time_gmt"));
			tc.setName((String) hm.get("name"));
			tc.setCode((String) hm.get("code"));
			tc.setTime_offset((String) hm.get("time_offset"));
			if ((((String) hm.get("prefix")).length() == 0) || (hm.get("prefix") == null)) {
				tc.setPrefix("NO");
			} else {
				tc.setPrefix((String) hm.get("prefix"));
			}
			tgg.add(tc);
		}
		int inter = Integer.parseInt("5000");
		try {
			Timer timer = new Timer();
			timer.schedule(new java.util.TimerTask() {
				public void run() {
					MainValidator.get();
					MainValidator.toLog("Esperando archivos globales agendados");
				}
			}, 0L, inter);
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = MainValidator.exceptionToString(e);
			MainValidator.toLog(sb.toString());
		}
	}

	public static void get() {
		GetData g = new GetData();
		int malos = 0;
		try {
			if (g.isData()) {
				ArrayList<DataFile> validador = GetFile.isfile(g.getIp_server(), g.getRuta_archivo(), g.getFile());
				TGG_Country tg = null;
				for (int i = 0; i < validador.size(); i++) {
					DataFile df = (DataFile) validador.get(i);
					for (int m = 0; m < tgg.size(); m++) {
						tg = (TGG_Country) tgg.get(m);
						MainValidator.toLog("prefix " + tg.getPrefix());
						if (df.getMsisdn().startsWith(tg.getPrefix())) {
							//String MENSAJE_LOG="Id_pais " + tg.getId_country() + " SENDER " + df.getSender();
							String MENSAJE_LOG="Validacion de archivo global agendado: Id_pais " + tg.getId_country() + " SENDER " + df.getSender()+"";
							MainValidator.toLog(MENSAJE_LOG);
							m = tgg.size();
						}
					}
					if (!tg.getLargo().equals(String.valueOf(df.getMsisdn().length()))) {
						malos++;
						MainValidator.toLog("Validacion: Numero Telefono Incorrecto " + df.getMsisdn() + " Mensaje " + df.getTexto() + " ID Process " + g.getId_process());	
						DBAccess.insert_error(df.getMsisdn(), df.getTexto(), "Numero incorrecto", g.getId_process());
					} else if ((df.getTexto().length() < 0) && (df.getTexto().length() > 161)) {
						malos++;
						MainValidator.toLog(" Validacion: Mensaje archivo del numero telefono " + df.getMsisdn());
						MainValidator.toLog(" Mensaje" + df.getTexto() + " Texto incorrecto  ID Process " + g.getId_process());
						DBAccess.insert_error(df.getMsisdn(), df.getTexto(), "Texto incorrecto", g.getId_process());
					}
				}
				DBAccess.update_process(g.getId(), malos);
			}
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = MainValidator.exceptionToString(e);
			MainValidator.toLog(sb.toString());
		}
	}

	public static String fecha_log(boolean d) {
		Date date = new Date();
		String finalDate = new String();
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(date);
		calendario.add(5, 0);
		String formatFecha;
		if (d) {
			formatFecha = "yyyyMMdd";
		} else {
			formatFecha = "yyyy-MM-dd HH:mm:ss.SSS";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formatFecha);
		finalDate = sdf.format(calendario.getTime());
		return finalDate;
	}

	public static boolean validateMSISDN(String msisdn, String len) {
		boolean resp = false;
		if (msisdn.length() == Integer.parseInt(len)) {
			resp = true;
		} else {
			resp = false;
		}
		return resp;
	}

	public static String changeHour(String fecha, String time_offset) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String fecha_respuesta = null;
		Date d = new Date();
		try {
			d = df.parse(fecha);
			Calendar gc = new java.util.GregorianCalendar();
			gc.setTime(d);
			gc.add(10, Integer.parseInt(time_offset));
			Date d2 = gc.getTime();
			fecha_respuesta = df.format(d2);
		} catch (java.text.ParseException e) {
			StringBuilder sb = new StringBuilder();
			sb = MainValidator.exceptionToString(e);
			MainValidator.toLog(sb.toString());
		}
		return fecha_respuesta;
	}

	public static synchronized void toLog(String mess) {
		try {
			String timestamp = fecha_log(false);
			String eol = System.getProperty("line.separator");
			String date = fecha_log(true);
			FileWriter out = new FileWriter("/home/appmobid/Apps/Logs/Validador." + date + ".log", true);
			out.write("[" + timestamp + "] " + mess + eol);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static StringBuilder exceptionToString(Exception e) {
		StringBuilder sb = new StringBuilder();
		StackTraceElement[] listStackTrace = e.getStackTrace();
		for (StackTraceElement element : listStackTrace) {
			sb.append("ERROR: ");
			sb.append(element.toString());
			sb.append("\n");
		}
		return sb;
	}
}
