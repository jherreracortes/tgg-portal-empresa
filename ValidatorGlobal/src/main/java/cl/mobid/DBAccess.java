package cl.mobid;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sql.DataSource;


public class DBAccess {


	public DBAccess() {

	}

	private static ArrayList<HashMap<String, String>> getMysqlResponse(String sql, boolean data) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(MainValidator.BASE_DATOS);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			if (!data) {
				while (rs.next()) {
					HashMap<String, String> hm = new HashMap<String, String>();

					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						String colname = rsmd.getColumnName(i);
						hm.put(colname, rs.getString(colname));
					}
					arrayList.add(hm);
				}
			} else {
				HashMap<String, String> hm = new HashMap<String, String>();

				while (rs.next()) {
					hm.put(rs.getString(rsmd.getColumnName(2)), rs.getString(rsmd.getColumnName(1)));
				}
				arrayList.add(hm);
			}
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			sb = MainValidator.exceptionToString(e);
			MainValidator.toLog(sb.toString());
			return null;
		} finally {
			cerrarConexion(con,stmt,rs);
		}
		return arrayList;
	}

	private static void cerrarConexion(Connection con, Statement stmt, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			sb = MainValidator.exceptionToString(e);
			MainValidator.toLog(sb.toString());
		}
	}

	private static void exec(String sql) {
		Connection con = null;
		Statement stmt = null;
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(MainValidator.BASE_DATOS);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			stmt.execute(sql);
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			sb = MainValidator.exceptionToString(e);
			MainValidator.toLog(sb.toString());
		} finally {
			cerrarConexion(con, stmt, null);
		}
	}

	public static HashMap<String, String> getDataFile() {
		String sql = "SELECT  * FROM file_process WHERE stage='VALIDATE' AND dispatch_time > DATE_ADD(now(),INTERVAL 25 MINUTE) ORDER BY created_time DESC LIMIT 1;";
		ArrayList<HashMap<String, String>> bal = getMysqlResponse(sql, false);
		if (bal.size() == 1) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}

	public static ArrayList<HashMap<String, String>> getPaisData() {
		String sql = "SELECT * FROM tgg_country WHERE id_country > 0";
		ArrayList<HashMap<String, String>> resp = getMysqlResponse(sql, false);
		return resp;
	}

	public static void insert_error(String msisdn, String text, String error, String id) {
		String sql = "INSERT INTO error_messages_globales( msisdn, text, error,input_process) VALUES ('" + msisdn
				+ "','" + text + "','" + error + "'," + id + " );";
		exec(sql);
	}

	public static void update_process(String id_file, int cant) {
		String sql = "UPDATE file_process SET bad_messages=" + cant + ", stage='GLOBAL' WHERE id_process=" + id_file
				+ ";";
		exec(sql);
	}
}
