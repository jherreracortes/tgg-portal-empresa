package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class ConsolReport implements Serializable {

    private static final long serialVersionUID = 1L;

    private String report_date;
    private int count;
    private int pushed;
    private int confirmed;
    private int failed;
    private int nopushed;
    private int queued;
    private int canceled;

    public String getReport_date() {
	return report_date;
    }

    public void setReport_date(String report_date) {
	this.report_date = report_date;
    }

    public int getCount() {
	return count;
    }

    public void setCount(int count) {
	this.count = count;
    }

    public int getPushed() {
	return pushed;
    }

    public void setPushed(int pushed) {
	this.pushed = pushed;
    }

    public int getConfirmed() {
	return confirmed;
    }

    public void setConfirmed(int confirmed) {
	this.confirmed = confirmed;
    }

    public int getFailed() {
	return failed;
    }

    public void setFailed(int failed) {
	this.failed = failed;
    }

    public int getNopushed() {
	return nopushed;
    }

    public void setNopushed(int nopushed) {
	this.nopushed = nopushed;
    }

    public int getQueued() {
	return queued;
    }

    public void setQueued(int queued) {
	this.queued = queued;
    }

    public int getCanceled() {
	return canceled;
    }

    public void setCanceled(int canceled) {
	this.canceled = canceled;
    }
}
