package cl.mobid.wsa.telcel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cl.mobid.wsa.telcel.bean.CostCenter;
import cl.mobid.wsa.telcel.mapper.CostCenterMapper;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CostCenterService {

    @Autowired
    private CostCenterMapper costCenterMapper;

    public int createCenter(CostCenter costCenter) {
	return costCenterMapper.createCenter(costCenter);
    }
    
    public int countCenters(int idCompany, String searchName) {
	return costCenterMapper.countCenters(idCompany, searchName);
    }

    public List<CostCenter> selectCenters(int limit, int offset, int idCompany,
	    String searchName) {
	return costCenterMapper.selectCenters(limit, offset, idCompany, searchName);
    }

    public CostCenter selectByIdCenter(int idCenter) {
	return costCenterMapper.selectByIdCenter(idCenter);
    }

    public int updateCenter(CostCenter costCenter) {
	return costCenterMapper.updateCenter(costCenter);
    }

    public int setEnabled(int idCenter, int enabled) {
	return costCenterMapper.setEnabled(idCenter, enabled);
    }
}
