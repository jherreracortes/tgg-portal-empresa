package cl.mobid.wsa.telcel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cl.mobid.wsa.telcel.bean.Group;
import cl.mobid.wsa.telcel.bean.GroupProcess;
import cl.mobid.wsa.telcel.bean.Mobile;
import cl.mobid.wsa.telcel.mapper.GroupMapper;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class GroupService {

    @Autowired
    private GroupMapper groupMapper;

    public int createGroup(Group group) {
	return groupMapper.createGroup(group);
    }

    public int assignGroup(String msisdn, int idGroup, int createdBy) {
	return groupMapper.assignGroup(msisdn, idGroup, createdBy);
    }

    public int createGroupProcess(GroupProcess groupProcess) {
	return groupMapper.createGroupProcess(groupProcess);
    }

    public int countGroups(int idCompany, String searchName) {
	return groupMapper.countGroups(idCompany, searchName);
    }

    public List<Group> selectGroups(int limit, int offset, int idCompany,
	    String searchName, int searchCountry, String orderBy, String order,
	    int enabled) {
	return groupMapper.selectGroups(limit, offset, idCompany, searchName,
		searchCountry, orderBy, order, enabled);
    }

    public int countMobiles(int idGroup, String searchMsisdn) {
	return groupMapper.countMobiles(idGroup, searchMsisdn);
    }

    public List<Mobile> selectMobiles(int limit, int offset, int idGroup,
	    String searchMsisdn) {
	return groupMapper.selectMobiles(limit, offset, idGroup, searchMsisdn);
    }

    public Group selectByIdGroup(int idGroup) {
	return groupMapper.selectByIdGroup(idGroup);
    }

    public int updateGroup(Group group) {
	return groupMapper.updateGroup(group);
    }

    public int setEnabled(int idGroup, int enabled) {
	return groupMapper.setEnabled(idGroup, enabled);
    }

    public int removeGroup(int idGroup, int idMobile) {
	return groupMapper.removeGroup(idGroup, idMobile);
    }
}
