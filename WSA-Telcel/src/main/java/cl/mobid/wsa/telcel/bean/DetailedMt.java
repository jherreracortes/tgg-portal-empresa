package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DetailedMt implements Serializable {

    private static final long serialVersionUID = 1L;

    private String msisdn;
    private String sender;
    private String stage;
    private String created_time;
    private String dispatch_time;
    private String result_time;
    private String text;
    private String content;
    private String input_mode;
    private String input_type;
    private String full_name;
    private String ccost;

    public String getMsisdn() {
	return msisdn;
    }

    public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
    }

    public String getSender() {
	return sender;
    }

    public void setSender(String sender) {
	this.sender = sender;
    }

    public String getStage() {
	return stage;
    }

    public void setStage(String stage) {
	this.stage = stage;
    }

    public String getCreated_time() {
	return created_time;
    }

    public void setCreated_time(String created_time) {
	this.created_time = created_time;
    }

    public String getDispatch_time() {
	return dispatch_time;
    }

    public void setDispatch_time(String dispatch_time) {
	this.dispatch_time = dispatch_time;
    }

    public String getResult_time() {
	return result_time;
    }

    public void setResult_time(String result_time) {
	this.result_time = result_time;
    }

    public String getText() {
	return text;
    }

    public void setText(String text) {
	this.text = text;
    }

    public String getContent() {
	return content;
    }

    public void setContent(String content) {
	this.content = content;
    }

    public String getInput_mode() {
	return input_mode;
    }

    public void setInput_mode(String input_mode) {
	this.input_mode = input_mode;
    }

    public String getInput_type() {
	return input_type;
    }

    public void setInput_type(String input_type) {
	this.input_type = input_type;
    }

    public String getFull_name() {
	return full_name;
    }

    public void setFull_name(String full_name) {
	this.full_name = full_name;
    }

    public String getCcost() {
	return ccost;
    }

    public void setCcost(String ccost) {
	this.ccost = ccost;
    }
}
