package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;

    private String company_status;
    private String type;
    private int id_country;
    private String country_shortname;

    public Country() {
    }

    public Country(String company_status, String type, int id_country,
	    String country_shortname) {
	setCompany_status(company_status);
	setType(type);
	setId_country(id_country);
	setCountry_shortname(country_shortname);
    }

    public String getCompany_status() {
	return company_status;
    }

    public void setCompany_status(String company_status) {
	this.company_status = company_status;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public int getId_country() {
	return id_country;
    }

    public void setId_country(int id_country) {
	this.id_country = id_country;
    }

    public String getCountry_shortname() {
	return country_shortname;
    }

    public void setCountry_shortname(String country_shortname) {
	this.country_shortname = country_shortname;
    }
}
