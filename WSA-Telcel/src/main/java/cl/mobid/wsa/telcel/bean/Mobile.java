package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Mobile implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id_mobile;
    private String msisdn;
    private String created_time;
    private String created_by;
    
    public int getId_mobile() {
	return id_mobile;
    }
    
    public void setId_mobile(int id_mobile) {
	this.id_mobile = id_mobile;
    }

    public String getMsisdn() {
	return msisdn;
    }

    public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
    }

    public String getCreated_time() {
	return created_time;
    }

    public void setCreated_time(String created_time) {
	this.created_time = created_time;
    }

    public String getCreated_by() {
	return created_by;
    }

    public void setCreated_by(String created_by) {
	this.created_by = created_by;
    }
}
