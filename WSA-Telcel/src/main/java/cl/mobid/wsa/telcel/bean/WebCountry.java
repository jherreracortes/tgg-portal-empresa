package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class WebCountry implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id;
    private String country;
    private String code;
    private String time_offset;
    private String time_gmt;
    private String pref;
    private String max;

    public int getId() {
	return id;
    }

    public void setId(int id) {
	this.id = id;
    }

    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getTime_offset() {
	return time_offset;
    }

    public void setTime_offset(String time_offset) {
	this.time_offset = time_offset;
    }

    public String getTime_gmt() {
	return time_gmt;
    }

    public void setTime_gmt(String time_gmt) {
	this.time_gmt = time_gmt;
    }

    public String getPref() {
	return pref;
    }

    public void setPref(String pref) {
	this.pref = pref;
    }

    public String getMax() {
	return max;
    }

    public void setMax(String max) {
	this.max = max;
    }

}
