package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
public class CostCenter implements Serializable{

    private static final long serialVersionUID = 1L;

    private int id_center;
    private int id_company;
    private String code;
    private String name;
    private int enabled;
    private String created_time;
    private int created_by;
    private String username;
    
    public int getId_center() {
	return id_center;
    }
    
    public void setId_center(int id_center) {
	this.id_center = id_center;
    }

    public int getId_company() {
	return id_company;
    }

    public void setId_company(int id_company) {
	this.id_company = id_company;
    }

    public String getCode() {
	return code;
    }

    public void setCode(String code) {
	this.code = code;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public int getEnabled() {
	return enabled;
    }

    public void setEnabled(int enabled) {
	this.enabled = enabled;
    }

    public String getCreated_time() {
	return created_time;
    }

    public void setCreated_time(String created_time) {
	this.created_time = created_time;
    }

    public int getCreated_by() {
	return created_by;
    }

    public void setCreated_by(int created_by) {
	this.created_by = created_by;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }
}
