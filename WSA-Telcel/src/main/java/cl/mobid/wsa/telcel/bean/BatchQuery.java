package cl.mobid.wsa.telcel.bean;

public class BatchQuery {
    
    private int id;
    private int id_company;
    private int id_user;
    private int id_country;
    private int id_group;
    private String file_original_name;
    private String file_internal_name;
    private String insert_time;
    private String status;
    private String info;
    private String sent_time;
    private String process_time;
    private String expired_time;
    private String mode;
    private int rows_total;
    private int rows_ok;
    private int rows_unknown;
    private int rows_timeout;

    private int error_code;
    private String error_text;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id= id;
    }

    public int getId_company() {
        return id_company;
    }

    public void setId_company(int id_company) {
        this.id_company = id_company;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_country() {
	return id_country;
    }

    public void setId_country(int id_country) {
	this.id_country = id_country;
    }

    public int getId_group() {
	return id_group;
    }

    public void setId_group(int id_group) {
	this.id_group = id_group;
    }

    public String getFile_original_name() {
	return file_original_name;
    }

    public void setFile_original_name(String file_original_name) {
	this.file_original_name = file_original_name;
    }

    public String getFile_internal_name() {
	return file_internal_name;
    }

    public void setFile_internal_name(String file_internal_name) {
	this.file_internal_name = file_internal_name;
    }

    public String getInsert_time() {
	return insert_time;
    }

    public void setInsert_time(String insert_time) {
	this.insert_time = insert_time;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getInfo() {
	return info;
    }

    public void setInfo(String info) {
	this.info = info;
    }

    public String getSent_time() {
	return sent_time;
    }

    public void setSent_time(String sent_time) {
	this.sent_time = sent_time;
    }

    public String getProcess_time() {
	return process_time;
    }

    public void setProcess_time(String process_time) {
	this.process_time = process_time;
    }

    public String getExpired_time() {
	return expired_time;
    }

    public void setExpired_time(String expired_time) {
	this.expired_time = expired_time;
    }

    public String getMode() {
	return mode;
    }

    public void setMode(String mode) {
	this.mode = mode;
    }

    public int getRows_total() {
	return rows_total;
    }

    public void setRows_total(int rows_total) {
	this.rows_total = rows_total;
    }

    public int getRows_ok() {
	return rows_ok;
    }

    public void setRows_ok(int rows_ok) {
	this.rows_ok = rows_ok;
    }

    public int getRows_unknown() {
	return rows_unknown;
    }

    public void setRows_unknown(int rows_unknown) {
	this.rows_unknown = rows_unknown;
    }

    public int getRows_timeout() {
	return rows_timeout;
    }

    public void setRows_timeout(int rows_timeout) {
	this.rows_timeout = rows_timeout;
    }

    public int getError_code() {
	return error_code;
    }

    public void setError_code(int error_code) {
	this.error_code = error_code;
    }

    public String getError_text() {
	return error_text;
    }

    public void setError_text(String error_text) {
	this.error_text = error_text;
    }

}
