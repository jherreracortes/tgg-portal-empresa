package cl.mobid.wsa.telcel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cl.mobid.wsa.telcel.bean.ConsolReport;
import cl.mobid.wsa.telcel.bean.ConsolidatedMo;
import cl.mobid.wsa.telcel.bean.DetailedMo;
import cl.mobid.wsa.telcel.bean.DetailedMt;
import cl.mobid.wsa.telcel.bean.FileMessage;
import cl.mobid.wsa.telcel.bean.GroupMessage;
import cl.mobid.wsa.telcel.bean.InputProcess;
import cl.mobid.wsa.telcel.bean.SingleMessage;

public interface MessageMapper {

    public int createInputProcess(InputProcess process);

    public int createSingleMessage(SingleMessage message);

    public int createGroupMessage(@Param("process") InputProcess process,
	    @Param("message") GroupMessage message);

    public int createFileMessage(@Param("process") InputProcess process,
	    @Param("message") FileMessage message, @Param("stage") String stage);

    public List<ConsolReport> consolidatedReport(
	    @Param("idCompany") int idCompany,
	    @Param("timeGMT") String timeGMT,
	    @Param("startDate") String startDate,
	    @Param("endDate") String endDate,
	    @Param("searchMsisdn") String searchMsisdn,
	    @Param("searchSender") String searchSender,
	    @Param("searchStage") String searchStage,
	    @Param("searchType") String searchType,
	    @Param("searchCCost") int searchCCost,
	    @Param("searchUser") int searchUser, @Param("order") String order);

    public void detailedReport(@Param("idCompany") int idCompany,
	    @Param("timeGMT") String timeGMT,
	    @Param("fileName") String fileName,
	    @Param("startDate") String startDate,
	    @Param("endDate") String endDate,
	    @Param("searchMsisdn") String searchMsisdn,
	    @Param("searchSender") String searchSender,
	    @Param("searchStage") String searchStage,
	    @Param("searchType") String searchType,
	    @Param("searchCCost") int searchCCost,
	    @Param("searchUser") int searchUser, @Param("order") String order,
	    @Param("language") String language);

    public List<DetailedMt> detailedReportLimit(
	    @Param("idCompany") int idCompany,
	    @Param("timeGMT") String timeGMT,
	    @Param("startDate") String startDate,
	    @Param("endDate") String endDate,
	    @Param("searchMsisdn") String searchMsisdn,
	    @Param("searchSender") String searchSender,
	    @Param("searchStage") String searchStage,
	    @Param("searchType") String searchType,
	    @Param("searchCCost") int searchCCost,
	    @Param("searchUser") int searchUser, @Param("order") String order,
	    @Param("language") String language);

    public List<ConsolidatedMo> consolidatedMo(
	    @Param("idCompany") int idCompany,
	    @Param("timeGMT") String timeGMT,
	    @Param("startDate") String startDate,
	    @Param("endDate") String endDate,
	    @Param("searchMsisdn") String searchMsisdn,
	    @Param("searchSender") String searchSender,
	    @Param("searchStage") String searchStage,
	    @Param("order") String order);

    public void detailedMo(@Param("idCompany") int idCompany,
	    @Param("timeGMT") String timeGMT,
	    @Param("fileName") String fileName,
	    @Param("startDate") String startDate,
	    @Param("endDate") String endDate,
	    @Param("searchMsisdn") String searchMsisdn,
	    @Param("searchSender") String searchSender,
	    @Param("searchStage") String searchStage,
	    @Param("order") String order,
	    @Param("language") String language);

    public List<DetailedMo> detailedMoLimit(@Param("idCompany") int idCompany,
	    @Param("timeGMT") String timeGMT,
	    @Param("startDate") String startDate,
	    @Param("endDate") String endDate,
	    @Param("searchMsisdn") String searchMsisdn,
	    @Param("searchSender") String searchSender,
	    @Param("searchStage") String searchStage,
	    @Param("order") String order);

    public String convertDispatchTime(@Param("idCountry") int idCountry,
	    @Param("dispatchTime") String dispatchTime);

    public String selectGMT(int idCountry);
}
