package cl.mobid.wsa.telcel.service;

import java.io.File;
import java.util.List;
import java.util.HashMap;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import cl.mobid.wsa.telcel.bean.SingleQuery;
import cl.mobid.wsa.telcel.bean.BatchQuery;
import cl.mobid.wsa.telcel.bean.Bitacora;
import cl.mobid.wsa.telcel.mapper.QueryOperatorMapper;

//---------------

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;

import org.apache.http.client.methods.HttpPost; 
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
//import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import javax.servlet.ServletContext;

//---------------

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class QueryOperatorService
{

    @Autowired
    private ServletContext sCtx;

    @Autowired
    private QueryOperatorMapper queryOperatorMapper;

    public int createSingleQuery(SingleQuery singleQuery)
    {
	return queryOperatorMapper.createSingleQuery(singleQuery);
    }

    public int updateSingleQuery(SingleQuery singleQuery)
    {
	return queryOperatorMapper.updateSingleQuery(singleQuery);
    }

    public int consumeSingleQuery(SingleQuery singleQuery)
    {

	int res;

	try
	{
		// Create a StringEntity for the SOAP XML.
		String body = "";
		body += "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:com=\"http://common.tiaxa.com/\">";
		body +=    "<soapenv:Header/>";
		body +=    "<soapenv:Body>";
		body +=       "<com:sendOperatorQuery>";
		body +=          "<requestId>" + singleQuery.getId() + "</requestId>";
		body +=          "<Id_company>" + singleQuery.getId_company() + "</Id_company>";
		body +=          "<msisdn>" + singleQuery.getMsisdn() + "</msisdn>";
		body +=       "</com:sendOperatorQuery>";
		body +=    "</soapenv:Body>";
		body += "</soapenv:Envelope>";

		System.out.println("==== Request ============================");
		System.out.println(body);
		System.out.println("=========================================");

		StringEntity stringEntity = new StringEntity(body, "UTF-8");
		stringEntity.setChunked(true);

		// Request parameters and other properties.
		HttpPost httpPost = new HttpPost(sCtx.getInitParameter("QueryOperator_WS_URL"));
		httpPost.setEntity(stringEntity);
		httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");
		httpPost.addHeader("Authorization", sCtx.getInitParameter("QueryOperator_WS_AUTH"));
		httpPost.addHeader("SOAPAction", "http://ws.tiaxa.com/sendOperatorQuery");

		// Execute and get the response.
		HttpClient httpClient = HttpClientBuilder.create().build();
//		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response = httpClient.execute(httpPost);
		HttpEntity entity = response.getEntity();

		String strResponse = null;
		if (entity != null)
		{
			strResponse = EntityUtils.toString(entity);
		}

		System.out.println("==== Response ===========================");
		System.out.println(strResponse);
		System.out.println("=========================================");
		
		int idxIni = strResponse.indexOf("<resultCode>");
		int idxFin = strResponse.indexOf("</resultCode>");
		if (idxIni >= 0 && idxFin >= 0 && idxIni < idxFin)
		{
			String result = strResponse.split("<resultCode>")[1].split("</resultCode>")[0];
			if (result == null)
			{
				res = -30;
			}
			else
			{
				singleQuery.setResult(result.trim());
				singleQuery.setOperator(queryOperatorMapper.selectOperatorName(result.trim()));
				res = 0;
			}
		}
		else
		{
			res = -20;
		}
             
	}
	catch (Exception e)
	{
		System.out.println("ERROR");
		System.out.println(e.getMessage());
		res = -10;
	}
	finally {
		
	}

	return res;
    }

    public int createBatchQuery(BatchQuery batchQuery)
    {
	return queryOperatorMapper.createBatchQuery(batchQuery);
    }

    private String formatDatetime(String tm)
    {
	return tm.substring(0,4) + "-" + tm.substring(4,6) + "-" + tm.substring(6,8) + " " + tm.substring(8,10) + ":" + tm.substring(10,12) + ":" + tm.substring(12,14);
    }

    private synchronized String getTimeMark()
    {
	// Sincronizamos y esperamos 2 segundos para asegurarnos que no se creen
	// archivos con el mismo nombre y se sobreescriban.
	try
	{
		Thread.sleep(2 * 1000);
	}
	catch (Exception e)
	{
	}

	return queryOperatorMapper.selectFormatedNow();
    }

    public HashMap<String,Object> createQueryFile(int idGroup, int idCompany)
    {
        String timeMark = getTimeMark();
	String fileName = String.format("COPFILE_%06d_%s_Carga.txt", idCompany, timeMark);

	try
	{
	    Path file = Paths.get(sCtx.getInitParameter("QueryOperator_FILE_IN_PATH") + "/" + fileName);
	    List<String> list = queryOperatorMapper.selectMobiles(idGroup);
	    Files.write(file, list, Charset.forName("UTF-8"), StandardOpenOption.CREATE_NEW);

            HashMap<String,Object> result = new HashMap<String,Object>();
	    result.put("FileName", fileName);
	    result.put("InsertTime", formatDatetime(timeMark));
	    result.put("RowsTotal", String.valueOf(list.size()) );
	    return result;
	}
	catch (Exception e)
	{
	    System.out.println("Error at createQueryFile: " + e.getMessage());	
	    e.printStackTrace();
	    return null;
	}
    }

    public HashMap<String,String> createQueryFile(MultipartFile upload, int idCompany)
    {
        String timeMark = getTimeMark();
	String fileName = String.format("COPFILE_%06d_%s_Carga.txt", idCompany, timeMark);

	try
	{
	    Path file = Paths.get(sCtx.getInitParameter("QueryOperator_FILE_IN_PATH") + "/" + fileName);
	    Files.copy(upload.getInputStream(), file);

            HashMap<String,String> result = new HashMap<String,String>();
	    result.put("FileName", fileName);
	    result.put("InsertTime", formatDatetime(timeMark));
	    return result;
	}
	catch (Exception e)
	{
	    System.out.println("Error at createQueryFile: " + e.getMessage());	
	    e.printStackTrace();
	    return null;
	}
    }

    public File getResultQueryFile(String fileName)
    {
        return new File(sCtx.getInitParameter("QueryOperator_FILE_OUT_PATH") + "/" + fileName);
    }

    public List<Bitacora> getBitacora(String startDate, String endDate, String archivo, Integer idCompany, String order)
    {
        return queryOperatorMapper.selectBitacora(startDate, endDate, archivo, idCompany, order);
    }

}
