package cl.mobid.wsa.telcel.csv;

import cl.mobid.wsa.telcel.bean.TrafficDateOut;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class TggDateEntryConverter implements CSVEntryConverter<TrafficDateOut> {
    
//    @Override
    public String[] convertEntry(TrafficDateOut traffic) {
	String[] columns = new String[19];

	columns[0] = traffic.getDate();
	columns[1] = String.valueOf(traffic.getId_company());
	columns[2] = traffic.getCompany_type();
	columns[3] = traffic.getCompany_name();
	columns[4] = String.valueOf(traffic.getService_id());
	columns[5] = traffic.getService_type();
	columns[6] = traffic.getService_tag();
	columns[7] = traffic.getService_name();
	columns[8] = traffic.getServ_country_name();
	columns[9] = traffic.getChannel_in();
	columns[10] = String.valueOf(traffic.getMo_ok());
	columns[11] = String.valueOf(traffic.getMo_nook());
	columns[12] = String.valueOf(traffic.getMo_total());
	columns[13] = String.valueOf(traffic.getMt_ok());
	columns[14] = String.valueOf(traffic.getMt_nook());
	columns[15] = String.valueOf(traffic.getMt_total());
	columns[16] = String.valueOf(traffic.getTotal_ok());
	columns[17] = String.valueOf(traffic.getTotal_nook());
	columns[18] = String.valueOf(traffic.getTotal());
	
	return columns;
    }
}
