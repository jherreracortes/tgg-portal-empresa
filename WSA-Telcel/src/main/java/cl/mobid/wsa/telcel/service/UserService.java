package cl.mobid.wsa.telcel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cl.mobid.wsa.telcel.bean.User;
import cl.mobid.wsa.telcel.mapper.UserMapper;
import cl.mobid.wsa.telcel.oauth.OauthUser;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public int createrUser(User user) {
	return userMapper.createUser(user);
    }

    public int createUserRole(User user) {
	return userMapper.createUserRole(user.getId_user(), user.getId_role());
    }

    public int countUsers(OauthUser user, String username, int searchCompany) {
	if (searchCompany > 0) {
	    user.setId_company(searchCompany);
	}

	return userMapper.countUsers(user.getId_company(),
		username);
    }

    public List<User> selectUsers(int limit, int offset, OauthUser user,
	    String searchName, int searchCompany) {
	if (searchCompany > 0) {
	    user.setId_company(searchCompany);
	}
	
	return userMapper.selectUsers(limit, offset, user.getId_user(),
		user.getId_company(), searchName);
    }

    public User selectUserByIdUser(int idUser) {
	return userMapper.selectByIdUser(idUser);
    }

    public String selectPasswordByIdUser(int idUser) {
	return userMapper.selectPassword(idUser);
    }

    public boolean hasPermission(int idRole, String title) {
	return (userMapper.hasPermission(idRole, title) >= 1);
    }

    public int updateUser(User user) {
	return userMapper.updateUser(user);
    }

    public int updateUserRole(User user) {
	return userMapper.updateUserRole(user.getId_user(), user.getId_role());
    }

    public int updateUserPassword(int idUser, String password) {
	return userMapper.updateUserPassword(idUser, password);
    }

    public int setEnabled(int idUser, int modifiedBy, int enabled) {
	return userMapper.setEnabled(idUser, modifiedBy, enabled);
    }

    public int callCreateUser(User user) {
	return userMapper.callCreateUser(user);
    }
}
