package cl.mobid.wsa.telcel.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cl.mobid.wsa.telcel.bean.ConsolidatedProcess;
import cl.mobid.wsa.telcel.bean.InputProcess;
import cl.mobid.wsa.telcel.oauth.OauthUser;
import cl.mobid.wsa.telcel.service.ProcessService;

@RestController
@RequestMapping("/api/process")
public class ProcessController {

    @Autowired
    private ProcessService processService;

    @RequestMapping(value = "/consolidated", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object consolidatedProcess(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "searchstage", required = false) String searchStage,
            @RequestParam(value = "searchccost", required = false, defaultValue = "0") int searchCCost) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (startDate != null && endDate != null && idCountry != -1) {
	    List<ConsolidatedProcess> processList = processService
		    .getConsolidatedProcess(oauthUser.getId_company(),
			    processService.getGMT(idCountry), startDate, endDate, searchCCost);

	    if (searchStage != null) {
		for (int i = 0; i < processList.size(); i++) {
		    int total = 0;

		    if ((total = processService.getByStage(oauthUser
			    .getId_company(), processList.get(i).getPid(),
			    searchStage)) > 0) {
			processList.get(i).setTotal(total);
		    } else {
			processList.remove(i);
		    }
		}
	    }

	    return processList;
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/wb/consolidated", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object consolidatedWBProcess(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "searchtype", required = false) String searchType,
	    @RequestParam(value = "searchaction", required = false) String searchAction) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (startDate != null && endDate != null && idCountry != -1) {

	    if (searchType != null) {
		if (searchType.toLowerCase().trim().equals("wl")) {
		    searchType = "('WL_ADD', 'WL_DEL')";
		} else {
		    searchType = "('BL_ADD', 'BL_DEL')";
		}
	    } else {
		searchType = "('WL_ADD', 'WL_DEL', 'BL_ADD', 'BL_DEL')";
	    }

	    List<ConsolidatedProcess> processList = processService
		    .getConsolidatedWBProcess(oauthUser.getId_company(),
			    processService.getGMT(idCountry), startDate,
			    endDate, searchType);

	    List<ConsolidatedProcess> actionList = new ArrayList<ConsolidatedProcess>();
	    if (searchAction != null) {
		for (ConsolidatedProcess process : processList) {
		    if (process.getInput_mode().toLowerCase()
			    .contains(searchAction.toLowerCase())) {
			actionList.add(process);
		    }
		}
		/*
		 * for (int i = 0; i < processList.size(); i++) { if
		 * (!processList.get(i).getInput_mode().toLowerCase()
		 * .contains(searchAction.toLowerCase())) {
		 * processList.remove(i); } }
		 */
	    } else {
		actionList = processList;
	    }

	    List<ConsolidatedProcess> resultList = new ArrayList<ConsolidatedProcess>();
	    for (ConsolidatedProcess process : actionList) {
		int total = 0;
		if ((total = processService.getWBErrors(process.getPid())) > 0) {
		    process.setTotal(total);
		    resultList.add(process);
		}
	    }

	    return resultList;
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.GET)
    public Object getCancelProcess(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry) {
	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	return processService.getCancelProcess(oauthUser.getId_company(),
		processService.getGMT(idCountry));
    }

    @RequestMapping(value = "/cancel/{idProcess}", method = RequestMethod.GET)
    public Object getCancelProcessById(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @PathVariable int idProcess) {
	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	ConsolidatedProcess consolidatedProcess;
	if ((consolidatedProcess = processService.getCancelProcessById(
		oauthUser.getId_company(), idProcess,
		processService.getGMT(idCountry))) != null) {
	    return consolidatedProcess;
	} else {
	    return new ResponseEntity<String>("process not found",
		    HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(value = "/detailed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object detailedeport(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "id_process", defaultValue = "0") int idProcess,
	    @RequestParam(value = "searchstage", required = false) String searchStage,
            @RequestParam(value = "searchccost", required = false, defaultValue = "0") int searchCCost) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	String Path = "/home/filesWSA/";
	String fileName = "process_" + oauthUser.getId_user() + "_"
		+ System.currentTimeMillis();

	if (idCountry != -1 && idProcess != 0) {
	    processService.getDetailedProcess(oauthUser.getId_company(),
		    idProcess, processService.getGMT(idCountry), Path
			    + fileName + ".csv", searchStage, searchCCost, oauthUser
			    .getLanguage().toUpperCase().trim());

	    return new ResponseEntity<String>(fileName, HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/detailed/cancel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object detailedeport(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "id_process", defaultValue = "0") int idProcess) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	String Path = "/home/filesWSA/";
	String fileName = "error_" + oauthUser.getId_user() + "_"
		+ System.currentTimeMillis();

	if (idProcess > 0 && idCountry > -1) {
	    processService.getDetailedErrors(idProcess,
		    processService.getGMT(idCountry), Path + fileName + ".csv",
		    oauthUser.getLanguage().toUpperCase().trim());

	    return new ResponseEntity<String>(fileName, HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/wb/detailed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object detailedWBReport(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "id_process", defaultValue = "0") int idProcess,
	    @RequestParam(value = "type") String type,
	    @RequestParam(value = "searchstage", required = false) String searchStage) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	String Path = "/home/filesWSA/";
	String fileName = "process_" + oauthUser.getId_user() + "_"
		+ System.currentTimeMillis();

	if (idCountry != -1 && idProcess != 0 && type != null) {

	    if (type.toLowerCase().trim().equals("wl_add")) {
		type = "addToWListService";
	    } else if (type.toLowerCase().trim().equals("wl_del")) {
		type = "delFromWListService";
	    } else if (type.toLowerCase().trim().equals("bl_add")) {
		type = "addToServRestricted";
	    } else if (type.toLowerCase().trim().equals("bl_del")) {
		type = "delFromServRestricted";
	    }

	    processService.getDetailedWBProcess(oauthUser.getId_company(),
		    idProcess, processService.getGMT(idCountry), type, Path
			    + fileName + ".csv", searchStage, oauthUser
			    .getLanguage().toUpperCase().trim());

	    return new ResponseEntity<String>(fileName, HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/download/{fileName}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getFile(@PathVariable String fileName,
	    HttpServletResponse response) {

	final HttpHeaders headers = new HttpHeaders();

	File file = new File("/home/filesWSA/" + fileName + ".csv");

	if (file.exists()) {
	    InputStream inputStream = null;
	    OutputStream outputStream = null;

	    try {
		inputStream = new FileInputStream(file);
	    } catch (FileNotFoundException e) {

		e.printStackTrace();
	    }

	    response.setContentType("text/csv");
	    response.setHeader("Content-Disposition", "attachment; filename=\""
		    + file.getName() + "\"");
	    Long fileSize = file.length();
	    response.setContentLength(fileSize.intValue());

	    try {
		outputStream = response.getOutputStream();
	    } catch (IOException e) {
		e.printStackTrace();
	    }

	    byte[] buffer = new byte[1024];
	    int read = 0;

	    try {
		while ((read = inputStream.read(buffer)) != -1) {
		    outputStream.write(buffer, 0, read);
		}

		outputStream.flush();
		outputStream.close();
		inputStream.close();
	    } catch (IOException e) {

		e.printStackTrace();
	    }
	}

	headers.setContentType(MediaType.TEXT_PLAIN);
	return new ResponseEntity<byte[]>("file not found".getBytes(), headers,
		HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> setCanceled(
	    @RequestParam(value = "id_process") int idProcess) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	InputProcess process;
	if ((process = processService.getProcessById(oauthUser.getId_company(),
		idProcess)) != null) {
	    if (processService.cancelMessage(oauthUser, process) == 1) {
		return new ResponseEntity<String>("message canceled ok",
			HttpStatus.OK);
	    }
	} else {
	    return new ResponseEntity<String>("process not found",
		    HttpStatus.NOT_FOUND);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
