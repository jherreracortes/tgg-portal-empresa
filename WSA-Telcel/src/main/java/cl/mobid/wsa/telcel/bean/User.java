package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id_user;
    private int id_company;
    private String full_name;
    private String username;
    private String password;
    private String language;
    private int enabled;
    @JsonInclude(Include.NON_DEFAULT)
    private int modified_by;
    @JsonInclude(Include.NON_DEFAULT)
    private int id_role;
    private String title;
    private int id_country;

    public int getId_user() {
	return id_user;
    }

    public void setId_user(int id_user) {
	this.id_user = id_user;
    }

    public int getId_company() {
	return id_company;
    }

    public void setId_company(int id_company) {
	this.id_company = id_company;
    }

    public String getFull_name() {
	return full_name;
    }

    public void setFull_name(String full_name) {
	this.full_name = full_name;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getLanguage() {
	return language;
    }

    public void setLanguage(String language) {
	this.language = language;
    }

    public int getEnabled() {
	return enabled;
    }

    public void setEnabled(int enabled) {
	this.enabled = enabled;
    }

    public int getModified_by() {
	return modified_by;
    }

    public void setModified_by(int modified_by) {
	this.modified_by = modified_by;
    }
    
    public int getId_role() {
	return id_role;
    }

    public void setId_role(int id_role) {
	this.id_role = id_role;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public int getId_country() {
	return id_country;
    }

    public void setId_country(int id_country) {
	this.id_country = id_country;
    }
}
