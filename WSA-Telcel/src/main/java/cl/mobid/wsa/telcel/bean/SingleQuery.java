package cl.mobid.wsa.telcel.bean;

public class SingleQuery {
    
    private int id;
    private String query_time;
    private int id_company;
    private int id_user;
    private int id_country;
    private String msisdn;
    private String result;
    private String operator;
    private int error_code;
    private String error_text;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id= id;
    }

    public String getQuery_time() {
	return query_time;
    }

    public void setQuery_time(String query_time) {
	this.query_time = query_time;
    }

    public int getId_company() {
        return id_company;
    }

    public void setId_company(int id_company) {
        this.id_company = id_company;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_country() {
	return id_country;
    }

    public void setId_country(int id_country) {
	this.id_country = id_country;
    }

    public String getMsisdn() {
	return msisdn;
    }

    public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
    }

    public String getResult() {
	return result;
    }

    public void setResult(String result) {
	this.result = result;
    }

    public String getOperator() {
	return operator;
    }

    public void setOperator(String operator) {
	this.operator = operator;
    }

    public int getError_code() {
	return error_code;
    }

    public void setError_code(int error_code) {
	this.error_code = error_code;
    }

    public String getError_text() {
	return error_text;
    }

    public void setError_text(String error_text) {
	this.error_text = error_text;
    }

}
