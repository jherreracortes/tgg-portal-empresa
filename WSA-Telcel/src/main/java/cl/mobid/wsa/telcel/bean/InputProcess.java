package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class InputProcess implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private int id_process;
    private int id_company;
    private String input_mode;
    private String created_time;
    private int created_by;

    // MHL - 2017-05-02
    private int id_center;
    
    public int getId_process() {
	return id_process;
    }
    
    public void setId_process(int id_process) {
	this.id_process = id_process;
    }

    public int getId_company() {
	return id_company;
    }

    public void setId_company(int id_company) {
	this.id_company = id_company;
    }

    public String getInput_mode() {
	return input_mode;
    }

    public void setInput_mode(String input_mode) {
	this.input_mode = input_mode;
    }

    public String getCreated_time() {
	return created_time;
    }

    public void setCreated_time(String created_time) {
	this.created_time = created_time;
    }

    public int getCreated_by() {
	return created_by;
    }

    public void setCreated_by(int created_by) {
	this.created_by = created_by;
    }

    // MHL - 2017-05-02

    public int getId_center() {
	return id_center;
    }
    
    public void setId_center(int id_center) {
	this.id_center = id_center;
    }

}
