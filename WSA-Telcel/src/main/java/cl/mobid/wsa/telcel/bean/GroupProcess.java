package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class GroupProcess implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id_process;
    private String ip_server;
    private String file_path;
    private String file_name;
    private int id_group;
    private int created_by;

    public int getId_process() {
	return id_process;
    }

    public void setId_process(int id_process) {
	this.id_process = id_process;
    }

    public String getIp_server() {
	return ip_server;
    }

    public void setIp_server(String ip_server) {
	this.ip_server = ip_server;
    }

    public String getFile_path() {
	return file_path;
    }

    public void setFile_path(String file_path) {
	this.file_path = file_path;
    }

    public String getFile_name() {
	return file_name;
    }

    public void setFile_name(String file_name) {
	this.file_name = file_name;
    }

    public int getId_group() {
	return id_group;
    }

    public void setId_group(int id_group) {
	this.id_group = id_group;
    }

    public int getCreated_by() {
	return created_by;
    }

    public void setCreated_by(int created_by) {
	this.created_by = created_by;
    }
}
