package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

public class WhiteListProcess implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private int id;
    private String ip_server; 
    private String file_path; 
    private String file_name; 
    private String sender; 
    private int input_process;
    private String type; 
    private int total; 
    private String created_time;
    private int id_country;
    
    public int getId() {
	return id;
    }
    
    public void setId(int id) {
	this.id = id;
    }

    public String getIp_server() {
	return ip_server;
    }

    public void setIp_server(String ip_server) {
	this.ip_server = ip_server;
    }

    public String getFile_path() {
	return file_path;
    }

    public void setFile_path(String file_path) {
	this.file_path = file_path;
    }

    public String getFile_name() {
	return file_name;
    }

    public void setFile_name(String file_name) {
	this.file_name = file_name;
    }

    public String getSender() {
	return sender;
    }

    public void setSender(String sender) {
	this.sender = sender;
    }

    public int getInput_process() {
	return input_process;
    }

    public void setInput_process(int input_process) {
	this.input_process = input_process;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public int getTotal() {
	return total;
    }

    public void setTotal(int total) {
	this.total = total;
    }

    public String getCreated_time() {
	return created_time;
    }

    public void setCreated_time(String created_time) {
	this.created_time = created_time;
    }

    public int getId_country() {
	return id_country;
    }

    public void setId_country(int id_country) {
	this.id_country = id_country;
    }
}
