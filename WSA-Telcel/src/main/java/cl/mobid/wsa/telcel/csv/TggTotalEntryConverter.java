package cl.mobid.wsa.telcel.csv;

import cl.mobid.wsa.telcel.bean.TrafficTotalOut;

import com.googlecode.jcsv.writer.CSVEntryConverter;

public class TggTotalEntryConverter implements
	CSVEntryConverter<TrafficTotalOut> {

//    @Override
    public String[] convertEntry(TrafficTotalOut traffic) {
	String[] columns = new String[18];

	columns[0] = traffic.getDate();
	columns[1] = String.valueOf(traffic.getId_company());
	columns[2] = traffic.getCompany_name();
	columns[3] = traffic.getCompany_type();
	columns[4] = String.valueOf(traffic.getService_id());
	columns[5] = traffic.getService_tag();
	columns[6] = traffic.getService_name();
	columns[7] = traffic.getService_type();
	columns[8] = traffic.getServ_country_name();
	columns[9] = String.valueOf(traffic.getTotal_mo_ok());
	columns[10] = String.valueOf(traffic.getTotal_dr_bill_ok());
	columns[11] = String.valueOf(traffic.getTotal_mt_ok());
	columns[12] = String.valueOf(traffic.getTotal_bill_ok());
	columns[13] = String.valueOf(traffic.getTotal_dr_no_bill_ok());
	columns[14] = String.valueOf(traffic.getTotal_ok());
	columns[15] = String.valueOf(traffic.getTotal_mo_nook());
	columns[16] = String.valueOf(traffic.getTotal_mt_nook());
	columns[17] = String.valueOf(traffic.getTotal_nook());

	return columns;
    }
}
