package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ConsolidatedProcess implements Serializable {

    private static final long serialVersionUID = 1L;

    private int pid;
    private String created_time;
    private String file_name;
    private String input_mode;
    private String dispatch_time;
    private int total;
    private String sender;
    private String country_name;
    private int bad_messages;
    private int canceleable;
    private String ccost;

    public int getPid() {
	return pid;
    }

    public void setPid(int pid) {
	this.pid = pid;
    }

    public String getCreated_time() {
	return created_time;
    }

    public void setCreated_time(String created_time) {
	this.created_time = created_time;
    }

    public String getFile_name() {
	return file_name;
    }

    public void setFile_name(String file_name) {
	this.file_name = file_name;
    }

    public String getInput_mode() {
	return input_mode;
    }

    public void setInput_mode(String input_mode) {
	this.input_mode = input_mode;
    }

    public String getDispatch_time() {
	return dispatch_time;
    }

    public void setDispatch_time(String dispatch_time) {
	this.dispatch_time = dispatch_time;
    }

    public int getTotal() {
	return total;
    }

    public void setTotal(int total) {
	this.total = total;
    }
    
    public String getSender() {
	return sender;
    }

    public void setSender(String sender) {
	this.sender = sender;
    }
    
    public String getCountry_name() {
	return country_name;
    }

    public void setCountry_name(String country_name) {
	this.country_name = country_name;
    }

    public int getBad_messages() {
	return bad_messages;
    }

    public void setBad_messages(int bad_messages) {
	this.bad_messages = bad_messages;
    }

    public int getCanceleable() {
	return canceleable;
    }

    public void setCanceleable(int canceleable) {
	this.canceleable = canceleable;
    }

    public String getCcost() {
	return ccost;
    }

    public void setCcost(String ccost) {
	this.ccost = ccost;
    }

}
