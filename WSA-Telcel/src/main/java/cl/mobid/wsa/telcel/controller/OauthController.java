package cl.mobid.wsa.telcel.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/oauth")
public class OauthController {

    @Autowired
    private DefaultTokenServices tokenServices;

    @RequestMapping(value = "/revoke", method = RequestMethod.GET)
    public ResponseEntity<String> revokeToken(
	    @RequestParam("access_token") String tokenValue) {

	tokenServices.revokeToken(tokenValue);

	return new ResponseEntity<String>(tokenValue, HttpStatus.OK);
    }

    @RequestMapping(value = "/check", method = RequestMethod.GET)
    public ResponseEntity<Boolean> checkToken(
	    @RequestParam(value = "username", required = false) String username) {

	List<OAuth2AccessToken> userTokens = null;

	if (username == null) {
	    return new ResponseEntity<Boolean>(HttpStatus.NOT_FOUND);
	} else {
	    userTokens = new ArrayList<OAuth2AccessToken>(
		    tokenServices.findTokensByUserName(username.trim()));
	    
	    if (!userTokens.isEmpty()) {
		if (userTokens.get(0).getExpiresIn() > 60) {
		    return new ResponseEntity<Boolean>(true, HttpStatus.OK);
		}
	    }
	    
	}

	return new ResponseEntity<Boolean>(false, HttpStatus.OK);
    }
}
