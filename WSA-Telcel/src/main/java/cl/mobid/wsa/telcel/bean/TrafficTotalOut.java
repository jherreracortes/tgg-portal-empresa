package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TrafficTotalOut implements Serializable {

    private static final long serialVersionUID = 1L;

    private int error_code;
    private String error_msg;
    private String date;
    private int id_company;
    private String company_name;
    private String company_type;
    private String comp_country_name;
    private int service_id;
    private String service_tag;
    private String service_name;
    private String service_type;
    private String serv_country_name;
    private int total_mo_ok;
    private int total_dr_bill_ok;
    private int total_mt_ok;
    private int total_bill_ok;
    private int total_dr_no_bill_ok;
    private int total_ok;
    private int total_mo_nook;
    private int total_mt_nook;
    private int total_nook;
    
    public int getError_code() {
	return error_code;
    }
    
    public void setError_code(int error_code) {
	this.error_code = error_code;
    }

    public String getError_msg() {
	return error_msg;
    }

    public void setError_msg(String error_msg) {
	this.error_msg = error_msg;
    }

    public String getDate() {
	return date;
    }

    public void setDate(String date) {
	this.date = date;
    }

    public int getId_company() {
	return id_company;
    }

    public void setId_company(int id_company) {
	this.id_company = id_company;
    }

    public String getCompany_name() {
	return company_name;
    }

    public void setCompany_name(String company_name) {
	this.company_name = company_name;
    }

    public String getCompany_type() {
	return company_type;
    }

    public void setCompany_type(String company_type) {
	this.company_type = company_type;
    }

    public String getComp_country_name() {
	return comp_country_name;
    }

    public void setComp_country_name(String comp_country_name) {
	this.comp_country_name = comp_country_name;
    }

    public int getService_id() {
	return service_id;
    }

    public void setService_id(int service_id) {
	this.service_id = service_id;
    }

    public String getService_tag() {
	return service_tag;
    }

    public void setService_tag(String service_tag) {
	this.service_tag = service_tag;
    }

    public String getService_name() {
	return service_name;
    }

    public void setService_name(String service_name) {
	this.service_name = service_name;
    }

    public String getService_type() {
	return service_type;
    }

    public void setService_type(String service_type) {
	this.service_type = service_type;
    }

    public String getServ_country_name() {
	return serv_country_name;
    }

    public void setServ_country_name(String serv_country_name) {
	this.serv_country_name = serv_country_name;
    }

    public int getTotal_mo_ok() {
	return total_mo_ok;
    }

    public void setTotal_mo_ok(int total_mo_ok) {
	this.total_mo_ok = total_mo_ok;
    }

    public int getTotal_dr_bill_ok() {
	return total_dr_bill_ok;
    }

    public void setTotal_dr_bill_ok(int total_dr_bill_ok) {
	this.total_dr_bill_ok = total_dr_bill_ok;
    }

    public int getTotal_mt_ok() {
	return total_mt_ok;
    }

    public void setTotal_mt_ok(int total_mt_ok) {
	this.total_mt_ok = total_mt_ok;
    }

    public int getTotal_bill_ok() {
	return total_bill_ok;
    }

    public void setTotal_bill_ok(int total_bill_ok) {
	this.total_bill_ok = total_bill_ok;
    }

    public int getTotal_dr_no_bill_ok() {
	return total_dr_no_bill_ok;
    }

    public void setTotal_dr_no_bill_ok(int total_dr_no_bill_ok) {
	this.total_dr_no_bill_ok = total_dr_no_bill_ok;
    }

    public int getTotal_ok() {
	return total_ok;
    }

    public void setTotal_ok(int total_ok) {
	this.total_ok = total_ok;
    }

    public int getTotal_mo_nook() {
	return total_mo_nook;
    }

    public void setTotal_mo_nook(int total_mo_nook) {
	this.total_mo_nook = total_mo_nook;
    }

    public int getTotal_mt_nook() {
	return total_mt_nook;
    }

    public void setTotal_mt_nook(int total_mt_nook) {
	this.total_mt_nook = total_mt_nook;
    }

    public int getTotal_nook() {
	return total_nook;
    }

    public void setTotal_nook(int total_nook) {
	this.total_nook = total_nook;
    }
}
