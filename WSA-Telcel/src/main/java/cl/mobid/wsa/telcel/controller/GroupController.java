package cl.mobid.wsa.telcel.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;

import cl.mobid.wsa.telcel.bean.Group;
import cl.mobid.wsa.telcel.bean.GroupProcess;
import cl.mobid.wsa.telcel.oauth.OauthUser;
import cl.mobid.wsa.telcel.service.GroupService;

@RestController
@RequestMapping("/api/group")
public class GroupController {

    @Autowired
    private GroupService groupService;

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createGroup(@RequestBody Group group) {
	HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
	String message = "";

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (groupWasUpdate(group)) {
	    status = HttpStatus.OK;
	    message = "group was updated";
	} else if (groupCanBeCreated(group)) {
	    group.setId_company(oauthUser.getId_company());
	    group.setCreated_by(oauthUser.getId_user());
	    if (groupService.createGroup(group) == 1) {
		status = HttpStatus.CREATED;
		message = "group was created";
	    }
	} else {
	    status = HttpStatus.NOT_ACCEPTABLE;
	    message = "The information provided is not sufficient to perform either an UPDATE or an INSERT";
	}

	return new ResponseEntity<String>(message, status);
    }

    @RequestMapping(value = "/assign", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> assignGroup(@RequestBody ObjectNode data) {
	HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
	String message = "";

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();
	
	int id_group;
	String msisdn;

	if (data.get("id_group") != null && data.get("msisdn") != null) {
	    id_group = data.get("id_group").asInt();
	    msisdn = data.get("msisdn").asText().trim();

	    int result = groupService.assignGroup(msisdn, id_group, oauthUser.getId_user());

	    if (result == 200 || result == 201) {
		status = HttpStatus.OK;
		message = "mobile added succesfully";
	    } else if (result == 1062) {
		status = HttpStatus.CONFLICT;
		message = "mobile exists in group";
	    }
	} else {
	    status = HttpStatus.NOT_ACCEPTABLE;
	    message = "invalid json";
	}

	return new ResponseEntity<String>(message, status);
    }

    @RequestMapping(value = "/file", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> setFile(HttpServletRequest request,
	    @RequestBody ObjectNode data) {
	HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
	String message = "";

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();
	
	int id_group;
	String filename;

	if (data.get("id_group") != null && data.get("filename") != null) {
	    id_group = data.get("id_group").asInt();
	    filename = data.get("filename").asText().trim();

	    int index = filename.lastIndexOf("/") + 1;
	    GroupProcess groupProcess = new GroupProcess();

	    groupProcess.setIp_server(request.getRemoteAddr());
	    groupProcess.setFile_path(filename.substring(0, index));
	    groupProcess
		    .setFile_name(filename.substring(index, filename.length()));
	    groupProcess.setId_group(id_group);
	    groupProcess.setCreated_by(oauthUser.getId_user());

	    if (groupService.createGroupProcess(groupProcess) == 1) {
		status = HttpStatus.OK;
		message = "file marked for upload";
	    }
	}

	return new ResponseEntity<String>(message, status);
    }

    @RequestMapping(value = "/remove", method = RequestMethod.DELETE)
    public ResponseEntity<String> removeGroup(
	    @RequestParam(value = "id_group") int idGroup,
	    @RequestParam(value = "id_mobile") int idMobile) {
	HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
	String message = "";

	if (mobileWasRemoved(idGroup, idMobile)) {
	    status = HttpStatus.OK;
	    message = "mobile removed succesfully";
	}

	return new ResponseEntity<String>(message, status);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getGroups(
	    @RequestParam(value = "offset", defaultValue = "-1") int offset,
	    @RequestParam(value = "limit", defaultValue = "-1") int limit,
	    @RequestParam(value = "searchname", required = false) String searchName,
	    @RequestParam(value = "searchcountry", defaultValue = "-1", required = false) int searchCountry,
	    @RequestParam(value = "field", required = false) String orderBy,
	    @RequestParam(value = "order", required = false) String order,
	    @RequestParam(value = "enabled", defaultValue = "-1", required = false) int enabled) {
	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (offset == -1 && limit == -1)
	    return String.valueOf(groupService
		    .countGroups(oauthUser.getId_company(), searchName));

	if (orderBy != null) {
	    if (orderBy.toLowerCase().equals("name")) {
		orderBy = "G.name";
	    } else if (orderBy.toLowerCase().equals("created_time")) {
		orderBy = "G.created_time";
	    }
	}

	return groupService.selectGroups(limit, offset,
		oauthUser.getId_company(), searchName, searchCountry, orderBy,
		order, enabled);
    }

    @RequestMapping(value = "/{idGroup}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Group getGroupById(@PathVariable int idGroup) {
	return groupService.selectByIdGroup(idGroup);
    }

    @RequestMapping(value = "/mobile/{idGroup}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getMobiles(
	    @RequestParam(value = "offset", defaultValue = "-1") int offset,
	    @RequestParam(value = "limit", defaultValue = "-1") int limit,
	    @RequestParam(value = "searchmsisdn", required = false) String searchMsisdn,
	    @PathVariable int idGroup) {

	if (offset == -1 && limit == -1)
	    return String
		    .valueOf(groupService.countMobiles(idGroup, searchMsisdn));

	return groupService.selectMobiles(limit, offset, idGroup, searchMsisdn);
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> setEnabled(
	    @RequestParam(value = "id_group") int idGroup,
	    @RequestParam(value = "enabled") int enabled) {
	HttpStatus status;
	String message = "";

	if (groupWasDisabled(idGroup, enabled)) {
	    status = HttpStatus.OK;
	    message = "enabled was updated";
	} else {
	    status = HttpStatus.NOT_FOUND;
	}

	return new ResponseEntity<String>(message, status);
    }

    private boolean groupCanBeCreated(Group group) {
	return group.getName() != null;
    }

    private boolean groupWasUpdate(Group group) {
	return groupService.updateGroup(group) == 1;
    }

    private boolean groupWasDisabled(int idGroup, int enabled) {
	return groupService.setEnabled(idGroup, enabled) == 1;
    }

    private boolean mobileWasRemoved(int idGroup, int idMobile) {
	return groupService.removeGroup(idGroup, idMobile) == 1;
    }
}
