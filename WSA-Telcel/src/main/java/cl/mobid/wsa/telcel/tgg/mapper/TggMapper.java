package cl.mobid.wsa.telcel.tgg.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import cl.mobid.wsa.telcel.bean.Company;
import cl.mobid.wsa.telcel.bean.Country;
import cl.mobid.wsa.telcel.bean.Service;
import cl.mobid.wsa.telcel.bean.TrafficDateOut;
import cl.mobid.wsa.telcel.bean.TrafficTotalOut;
import cl.mobid.wsa.telcel.bean.WhiteList;

public interface TggMapper {

    public Company selectCompany(int idCompany);

    public Country selectCountry(int idCompany);

    public String selectWord(@Param("idLanguage") int idLanguage,
	    @Param("searchWord") String searchWord);

    public List<Service> selectServices(@Param("idCompany") int idCompany,
	    @Param("searchCountry") int searchCountry);

    public Service selectServiceById(@Param("idCompany") int idCompany, @Param("idService") int idService);

    public int countServiceRestricted(int idService);

    public List<String> selectServiceRestricted(@Param("limit") int limit,
	    @Param("offset") int offset, @Param("idService") int idService);

    public int countWhiteLists(int idCompany);

    public List<WhiteList> selectWhiteLists(@Param("limit") int limit,
	    @Param("offset") int offset, @Param("idCompany") int idCompany);

    public List<Service> selectWhiteListById(@Param("idCompany") int idCompany,
	    @Param("idList") int idList);

    public int countBlackLists(int idCompany);

    public List<Service> selectBlackLists(@Param("limit") int limit,
	    @Param("offset") int offset, @Param("idCompany") int idCompany);

    public List<TrafficDateOut> selectDateTraffic(
	    @Param("startDate") Date startDate, @Param("endDate") Date endDate,
	    @Param("idCountry") Integer idCountry,
	    @Param("idCompany") Integer idCompany,
	    @Param("idService") Integer idService,
	    @Param("channel") String channel, @Param("type") String type,
	    @Param("language") Integer language);

    public List<TrafficTotalOut> selectTotalTraffic(
	    @Param("startDate") Date startDate, @Param("endDate") Date endDate,
	    @Param("idCountry") Integer idCountry,
	    @Param("idCompany") Integer idCompany,
	    @Param("idService") Integer idService,
	    @Param("language") Integer language);

    public int validateWhiteList(@Param("msisdn") String msisdn,
	    @Param("idService") int idService);

    public int validateBlackList(@Param("msisdn") String msisdn,
	    @Param("idService") int idService);

    public int validateBlackListService(@Param("msisdn") String msisdn,
	    @Param("idService") int idService);
}
