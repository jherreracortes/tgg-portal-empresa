package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

public class SingleMessage extends Message implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private int id_mt;
    private int lap;
    private String msisdn;
    private String content;

    // MHL - 2017-05-02
    private int id_center;
    
    public int getId_mt() {
	return id_mt;
    }
    
    public void setId_mt(int id_mt) {
	this.id_mt = id_mt;
    }

    public int getLap() {
	return lap;
    }

    public void setLap(int lap) {
	this.lap = lap;
    }
    
    public String getMsisdn() {
  	return msisdn;
    }
    
    public void setMsisdn(String msisdn) {
  	this.msisdn = msisdn;
    }

    public String getContent() {
   	return content;
    }

    public void setContent(String content) {
  	this.content = content;
    }

    // MHL - 2017-05-02

    public int getId_center() {
	return id_center;
    }

    public void setId_center(int id_center) {
	this.id_center = id_center;
    }

}
