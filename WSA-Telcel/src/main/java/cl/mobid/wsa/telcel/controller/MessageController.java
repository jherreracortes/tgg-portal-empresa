package cl.mobid.wsa.telcel.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cl.mobid.wsa.telcel.bean.FileMessage;
import cl.mobid.wsa.telcel.bean.GroupMessage;
import cl.mobid.wsa.telcel.bean.InputProcess;
import cl.mobid.wsa.telcel.bean.SingleMessage;
import cl.mobid.wsa.telcel.oauth.OauthUser;
import cl.mobid.wsa.telcel.service.GroupService;
import cl.mobid.wsa.telcel.service.MessageService;

@RestController
@RequestMapping("/api/message")
public class MessageController {

    @Autowired
    private MessageService messageService;
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/single", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> SingleMessage(
	    @RequestBody SingleMessage singleMessage) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	InputProcess process = new InputProcess();
	process.setId_company(oauthUser.getId_company());
	process.setCreated_by(oauthUser.getId_user());

	if (singleMessage.getDispatch_time() != null) {
	    process.setInput_mode("SNG_SCH");

	    String pDispatchTime = null;
	    if ((pDispatchTime = checkDispatchTime(
		    singleMessage.getId_country(),
		    singleMessage.getDispatch_time())) != null) {
		singleMessage.setDispatch_time(pDispatchTime);
	    } else {
		return new ResponseEntity<String>("wrong dispatch time",
			HttpStatus.NOT_ACCEPTABLE);
	    }
	} else {
	    process.setInput_mode("SNG_IMM");
	}

	singleMessage.setId_company(oauthUser.getId_company());

        // MHL - 2017-05-02
        process.setId_center(singleMessage.getId_center());

	if (messageService.createInputProcess(process) > 0) {
	    singleMessage.setInput_process(process.getId_process());
	    if (messageService.createSingleMessage(singleMessage) > 0) {
		return new ResponseEntity<String>(
			"message created succesfully", HttpStatus.OK);
	    }
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/group", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> groupMessage(
	    @RequestBody GroupMessage groupMessage) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	InputProcess process = new InputProcess();
	process.setId_company(oauthUser.getId_company());
	process.setCreated_by(oauthUser.getId_user());

	if (groupMessage.getDispatch_time() != null) {
	    process.setInput_mode("GRP_SCH");

	    String pDispatchTime = null;
	    if ((pDispatchTime = checkDispatchTime(
		    groupMessage.getId_country(),
		    groupMessage.getDispatch_time())) != null) {
		groupMessage.setDispatch_time(pDispatchTime);
	    } else {
		return new ResponseEntity<String>("wrong dispatch time",
			HttpStatus.NOT_ACCEPTABLE);
	    }
	} else {
	    process.setInput_mode("GRP_IMM");
	}

        // MHL - 2017-05-02
        process.setId_center(groupMessage.getId_center());

	groupMessage.setId_company(oauthUser.getId_company());
	groupMessage.setTotal(groupService.countMobiles(
		groupMessage.getId_group(), null));

	if (messageService.createGroupMessage(process, groupMessage) > 0) {
	    return new ResponseEntity<String>("message created succesfully",
		    HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/file", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> FileMessage(HttpServletRequest request,
	    @RequestBody FileMessage fileMessage) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	InputProcess process = new InputProcess();
	process.setId_company(oauthUser.getId_company());
	process.setCreated_by(oauthUser.getId_user());

	if (fileMessage.getDispatch_time() != null) {
	    process.setInput_mode("FILE_SCH");

	    String pDispatchTime = null;
	    if ((pDispatchTime = checkDispatchTime(fileMessage.getId_country(),
		    fileMessage.getDispatch_time())) != null) {
		fileMessage.setDispatch_time(pDispatchTime);
	    } else {
		return new ResponseEntity<String>("wrong dispatch time",
			HttpStatus.NOT_ACCEPTABLE);
	    }
	} else {
	    process.setInput_mode("FILE_IMM");
	}

        // MHL - 2017-05-02
        process.setId_center(fileMessage.getId_center());

	int index = fileMessage.getFile_name().lastIndexOf("/") + 1;
	fileMessage
		.setFile_path(fileMessage.getFile_name().substring(0, index));
	fileMessage.setFile_name(fileMessage.getFile_name().substring(index,
		fileMessage.getFile_name().length()));
	fileMessage.setIp_server(request.getRemoteAddr());

	if (messageService.createFileMessage(process, fileMessage, "QUEUED") > 0) {
	    return new ResponseEntity<String>("message created succesfully",
		    HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/file/global", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> FileGlobal(HttpServletRequest request,
	    @RequestBody FileMessage fileMessage) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	InputProcess process = new InputProcess();
	process.setId_company(oauthUser.getId_company());
	process.setCreated_by(oauthUser.getId_user());

	String status = "";
	if (fileMessage.getDispatch_time() != null) {
	    process.setInput_mode("FILE_SCH");
	    status = "VALIDATE";

	    String pDispatchTime = null;
	    if ((pDispatchTime = checkDispatchTime(fileMessage.getId_country(),
		    fileMessage.getDispatch_time())) != null) {
		fileMessage.setDispatch_time(pDispatchTime);
	    } else {
		return new ResponseEntity<String>("wrong dispatch time",
			HttpStatus.NOT_ACCEPTABLE);
	    }
	} else {
	    process.setInput_mode("FILE_IMM");
	    status = "GLOBAL";
	}

        // MHL - 2017-05-02
        process.setId_center(fileMessage.getId_center());

	int index = fileMessage.getFile_name().lastIndexOf("/") + 1;
	fileMessage
		.setFile_path(fileMessage.getFile_name().substring(0, index));
	fileMessage.setFile_name(fileMessage.getFile_name().substring(index,
		fileMessage.getFile_name().length()));
	fileMessage.setIp_server(request.getRemoteAddr());

	if (messageService.createFileMessage(process, fileMessage, status) > 0) {
	    return new ResponseEntity<String>("message created succesfully",
		    HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/consolidated", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object consolidatedReport(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "order") String order,
	    @RequestParam(value = "searchmsisdn", required = false) String searchMsisdn,
	    @RequestParam(value = "searchsender", required = false) String searchSender,
	    @RequestParam(value = "searchstage", required = false) String searchStage,
	    @RequestParam(value = "searchtype", required = false) String searchType,
	    @RequestParam(value = "searchccost", required = false, defaultValue = "0") int searchCCost,
	    @RequestParam(value = "searchuser", required = false, defaultValue = "0") int searchUser) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (startDate != null && endDate != null && idCountry != -1
		&& order != null) {

	    if (searchType != null) {
		if (searchType.toLowerCase().equals("sng")) {
		    searchType = "'SNG_IMM','SNG_SCH'";
		} else if (searchType.toLowerCase().equals("grp")) {
		    searchType = "'GRP_IMM','GRP_SCH'";
		} else {
		    searchType = "'FILE_IMM','FILE_SCH'";
		}
	    }

	    return messageService.getConsolidatedReport(
		    oauthUser.getId_company(),
		    messageService.getGMT(idCountry), startDate, endDate,
		    searchMsisdn, searchSender, searchStage, searchType,
		    searchCCost, searchUser, order);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/detailed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object detailedeport(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "order") String order,
	    @RequestParam(value = "searchmsisdn", required = false) String searchMsisdn,
	    @RequestParam(value = "searchsender", required = false) String searchSender,
	    @RequestParam(value = "searchstage", required = false) String searchStage,
	    @RequestParam(value = "searchtype", required = false) String searchType,
	    @RequestParam(value = "searchccost", required = false, defaultValue = "0") int searchCCost,
	    @RequestParam(value = "searchuser", required = false, defaultValue = "0") int searchUser) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	String Path = "/home/filesWSA/";
	String fileName = oauthUser.getId_user() + "_"
		+ System.currentTimeMillis();

	if (startDate != null && endDate != null && idCountry != -1
		&& order != null) {

	    if (searchType != null) {
		if (searchType.toLowerCase().equals("sng")) {
		    searchType = "'SNG_IMM','SNG_SCH'";
		} else if (searchType.toLowerCase().equals("grp")) {
		    searchType = "'GRP_IMM','GRP_SCH'";
		} else {
		    searchType = "'FILE_IMM','FILE_SCH'";
		}
	    }

	    messageService.getDetailedReport(oauthUser.getId_company(),
		    messageService.getGMT(idCountry), Path + fileName + ".csv",
		    startDate, endDate, searchMsisdn, searchSender,
		    searchStage, searchType, searchCCost, searchUser, order, oauthUser
			    .getLanguage().toUpperCase().trim());

	    return new ResponseEntity<String>(fileName, HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/detailed/mt", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object detailedLimit(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "order") String order,
	    @RequestParam(value = "searchmsisdn", required = false) String searchMsisdn,
	    @RequestParam(value = "searchsender", required = false) String searchSender,
	    @RequestParam(value = "searchstage", required = false) String searchStage,
	    @RequestParam(value = "searchtype", required = false) String searchType,
	    @RequestParam(value = "searchccost", required = false, defaultValue = "0") int searchCCost,
	    @RequestParam(value = "searchuser", required = false, defaultValue = "0") int searchUser) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (startDate != null && endDate != null && idCountry != -1
		&& order != null) {

	    if (searchType != null) {
		if (searchType.toLowerCase().equals("sng")) {
		    searchType = "'SNG_IMM','SNG_SCH'";
		} else if (searchType.toLowerCase().equals("grp")) {
		    searchType = "'GRP_IMM','GRP_SCH'";
		} else {
		    searchType = "'FILE_IMM','FILE_SCH'";
		}
	    }

	    return messageService.getDetailedReportLimit(
		    oauthUser.getId_company(),
		    messageService.getGMT(idCountry), startDate, endDate,
		    searchMsisdn, searchSender, searchStage, searchType,
		    searchCCost, searchUser, order, oauthUser.getLanguage().toUpperCase()
			    .trim());
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/download/{fileName}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getFile(@PathVariable String fileName,
	    HttpServletResponse response) {

	final HttpHeaders headers = new HttpHeaders();

	File file = new File("/home/filesWSA/" + fileName + ".csv");

	if (file.exists()) {
	    InputStream inputStream = null;
	    OutputStream outputStream = null;

	    try {
		inputStream = new FileInputStream(file);
	    } catch (FileNotFoundException e) {

		e.printStackTrace();
	    }

	    response.setContentType("text/csv");
	    response.setHeader("Content-Disposition", "attachment; filename=\""
		    + file.getName() + "\"");
	    Long fileSize = file.length();
	    response.setContentLength(fileSize.intValue());

	    try {
		outputStream = response.getOutputStream();
	    } catch (IOException e) {
		e.printStackTrace();
	    }

	    byte[] buffer = new byte[1024];
	    int read = 0;

	    try {
		while ((read = inputStream.read(buffer)) != -1) {
		    outputStream.write(buffer, 0, read);
		}

		outputStream.flush();
		outputStream.close();
		inputStream.close();
	    } catch (IOException e) {

		e.printStackTrace();
	    }
	}

	headers.setContentType(MediaType.TEXT_PLAIN);
	return new ResponseEntity<byte[]>("file not found".getBytes(), headers,
		HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/consolidated/mo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object consolidatedMo(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "order") String order,
	    @RequestParam(value = "searchmsisdn", required = false) String searchMsisdn,
	    @RequestParam(value = "searchsender", required = false) String searchSender,
	    @RequestParam(value = "searchstage", required = false) String searchStage) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (startDate != null && endDate != null && idCountry != -1
		&& order != null) {
	    return messageService.getConsolidatedMo(oauthUser.getId_company(),
		    messageService.getGMT(idCountry), startDate, endDate,
		    searchMsisdn, searchSender, searchStage, order);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/detailed/mo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object detailedMo(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "order") String order,
	    @RequestParam(value = "searchmsisdn", required = false) String searchMsisdn,
	    @RequestParam(value = "searchsender", required = false) String searchSender,
	    @RequestParam(value = "searchstage", required = false) String searchStage) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	String Path = "/home/filesWSA/";
	String fileName = "mo_" + oauthUser.getId_user() + "_"
		+ System.currentTimeMillis();

	if (startDate != null && endDate != null && idCountry != -1
		&& order != null) {
	    messageService.getDetailedMo(oauthUser.getId_company(),
		    messageService.getGMT(idCountry), Path + fileName + ".csv",
		    startDate, endDate, searchMsisdn, searchSender,
		    searchStage, order, oauthUser.getLanguage().toUpperCase()
			    .trim());

	    return new ResponseEntity<String>(fileName, HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/detailed/mo/limit", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object detailedMoLimit(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "order") String order,
	    @RequestParam(value = "searchmsisdn", required = false) String searchMsisdn,
	    @RequestParam(value = "searchsender", required = false) String searchSender,
	    @RequestParam(value = "searchstage", required = false) String searchStage) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (startDate != null && endDate != null && idCountry != -1
		&& order != null) {
	    return messageService.getDetailedMoLimit(oauthUser.getId_company(),
		    messageService.getGMT(idCountry), startDate, endDate,
		    searchMsisdn, searchSender, searchStage, order);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/download/mo/{fileName}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getFileMo(@PathVariable String fileName,
	    HttpServletResponse response) {

	final HttpHeaders headers = new HttpHeaders();

	File file = new File("/home/filesWSA/" + fileName + ".csv");

	if (file.exists()) {
	    InputStream inputStream = null;
	    OutputStream outputStream = null;

	    try {
		inputStream = new FileInputStream(file);
	    } catch (FileNotFoundException e) {

		e.printStackTrace();
	    }

	    response.setContentType("text/csv");
	    response.setHeader("Content-Disposition", "attachment; filename=\""
		    + file.getName() + "\"");
	    Long fileSize = file.length();
	    response.setContentLength(fileSize.intValue());

	    try {
		outputStream = response.getOutputStream();
	    } catch (IOException e) {
		e.printStackTrace();
	    }

	    byte[] buffer = new byte[1024];
	    int read = 0;

	    try {
		while ((read = inputStream.read(buffer)) != -1) {
		    outputStream.write(buffer, 0, read);
		}

		outputStream.flush();
		outputStream.close();
		inputStream.close();
	    } catch (IOException e) {

		e.printStackTrace();
	    }
	}

	headers.setContentType(MediaType.TEXT_PLAIN);
	return new ResponseEntity<byte[]>("file not found".getBytes(), headers,
		HttpStatus.NOT_FOUND);
    }

    private String checkDispatchTime(int idCountry, String dispatchTime) {
	return messageService.convertDispatchTime(idCountry, dispatchTime);
    }
}

