package cl.mobid.wsa.telcel.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import cl.mobid.wsa.telcel.bean.ConsolidatedProcess;
import cl.mobid.wsa.telcel.bean.InputProcess;
import cl.mobid.wsa.telcel.bean.WebCountry;
import cl.mobid.wsa.telcel.bean.WhiteListProcess;

public interface ProcessMapper {

    public int createWBProcess(@Param("process") InputProcess process,
	    @Param("whitelist") WhiteListProcess whitelist);

    public List<WebCountry> selectCountry();
    
    public List<ConsolidatedProcess> consolidatedProcess(
	    @Param("idCompany") int idCompany,
	    @Param("timeGMT") String timeGMT,
	    @Param("startDate") String startDate,
	    @Param("endDate") String endDate,
            @Param("searchCCost") int searchCCost);

    public List<ConsolidatedProcess> consolidatedWBProcess(
	    @Param("idCompany") int idCompany,
	    @Param("timeGMT") String timeGMT,
	    @Param("startDate") String startDate,
	    @Param("endDate") String endDate,
	    @Param("searchType") String searchType);

    public List<ConsolidatedProcess> selectCancelProcess(
	    @Param("idCompany") int idCompany, @Param("timeGMT") String timeGMT);

    public ConsolidatedProcess selectCancelProcessById(
	    @Param("idCompany") int idCompany,
	    @Param("idProcess") int idProcess, @Param("timeGMT") String timeGMT);

    public InputProcess selectProcessById(@Param("idCompany") int idCompany,
	    @Param("idProcess") int idProcess);

    public int countByStage(@Param("idCompany") int idCompany,
	    @Param("idProcess") int idProcess,
	    @Param("searchStage") String searchStage);

    public int countWBErrors(int idProcess);

    public void detailedProcess(@Param("idCompany") int idCompany,
	    @Param("idProcess") int idProcess,
	    @Param("timeGMT") String timeGMT,
	    @Param("fileName") String fileName,
	    @Param("searchStage") String searchStage,
            @Param("searchCCost") int searchCCost,
	    @Param("language") String language);

    public void detailedWBProcess(@Param("idCompany") int idCompany,
	    @Param("idProcess") int idProcess,
	    @Param("timeGMT") String timeGMT, @Param("type") String type,
	    @Param("fileName") String fileName,
	    @Param("searchStage") String searchStage,
	    @Param("language") String language);

    public void detailedErrors(@Param("idProcess") int idProcess,
	    @Param("timeGMT") String timeGMT,
	    @Param("fileName") String fileName,
	    @Param("language") String language);

    public int cancelMessage(@Param("idCompany") int idCompany,
	    @Param("createdBy") int createdBy,
	    @Param("inputProcess") int inputProcess,
	    @Param("inputMode") String inputMode);

    public String selectGMT(int idCountry);

    public int selectIdProc(int idCountry);

    public ArrayList<HashMap<String, Object>> selectProcParams(int idProc);

    public String selectResponse(@Param("code") int code,
	    @Param("type") String type, @Param("language") String language);
    
    public String selectRestrictedHeader(String language);
    
    public String selectDateTrafficHeader(String language);
    
    public String selectTotalTrafficHeader(String language);
}
