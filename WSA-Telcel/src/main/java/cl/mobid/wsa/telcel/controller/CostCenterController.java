package cl.mobid.wsa.telcel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.mobid.wsa.telcel.bean.CostCenter;
import cl.mobid.wsa.telcel.oauth.OauthUser;
import cl.mobid.wsa.telcel.service.CostCenterService;

@RestController
@RequestMapping("/api/center")
public class CostCenterController {

    @Autowired
    private CostCenterService costCenterService;

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createGroup(@RequestBody CostCenter costCenter) {
	HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
	String message = "";

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (centerWasUpdate(costCenter)) {
	    status = HttpStatus.OK;
	    message = "center was updated";
	} else if (centerCanBeCreated(costCenter)) {
	    costCenter.setId_company(oauthUser.getId_company());
	    costCenter.setCreated_by(oauthUser.getId_user());

	    int result = 0;
	    if ((result = costCenterService.createCenter(costCenter)) == 201) {
		status = HttpStatus.CREATED;
		message = "center was created";
	    } else if (result == 1062) {
		status = HttpStatus.CONFLICT;
		message = "duplicate code for center";
	    }
	} else {
	    status = HttpStatus.NOT_ACCEPTABLE;
	    message = "The information provided is not sufficient to perform either an UPDATE or an INSERT";
	}

	return new ResponseEntity<String>(message, status);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getGroups(
	    @RequestParam(value = "offset", defaultValue = "-1") int offset,
	    @RequestParam(value = "limit", defaultValue = "-1") int limit,
	    @RequestParam(value = "searchname", required = false) String searchName) {
	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (offset == -1 && limit == -1)
	    return String.valueOf(costCenterService.countCenters(
		    oauthUser.getId_company(), searchName));

	return costCenterService.selectCenters(limit, offset,
		oauthUser.getId_company(), searchName);
    }

    @RequestMapping(value = "/{idCenter}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CostCenter getGroupById(@PathVariable int idCenter) {
	return costCenterService.selectByIdCenter(idCenter);
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> setEnabled(
	    @RequestParam(value = "id_center") int idCenter,
	    @RequestParam(value = "enabled") int enabled) {
	HttpStatus status;
	String message = "";

	if (centerWasDisabled(idCenter, enabled)) {
	    status = HttpStatus.OK;
	    message = "enabled was updated";
	} else {
	    status = HttpStatus.NOT_FOUND;
	}

	return new ResponseEntity<String>(message, status);
    }

    private boolean centerCanBeCreated(CostCenter costCenter) {
	return costCenter.getCode() != null && costCenter.getName() != null;
    }

    private boolean centerWasUpdate(CostCenter costCenter) {
	return costCenterService.updateCenter(costCenter) == 1;
    }

    private boolean centerWasDisabled(int idCenter, int enabled) {
	return costCenterService.setEnabled(idCenter, enabled) == 1;
    }
}
