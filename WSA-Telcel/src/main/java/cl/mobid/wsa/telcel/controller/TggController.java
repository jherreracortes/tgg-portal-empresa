package cl.mobid.wsa.telcel.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.googlecode.jcsv.writer.CSVWriter;
import com.googlecode.jcsv.writer.internal.CSVWriterBuilder;

import cl.mobid.wsa.telcel.bean.Company;
import cl.mobid.wsa.telcel.bean.InputProcess;
import cl.mobid.wsa.telcel.bean.Service;
import cl.mobid.wsa.telcel.bean.TrafficDateOut;
import cl.mobid.wsa.telcel.bean.TrafficTotalOut;
import cl.mobid.wsa.telcel.bean.WebCountry;
import cl.mobid.wsa.telcel.bean.WhiteListProcess;
import cl.mobid.wsa.telcel.bean.SingleQuery;
import cl.mobid.wsa.telcel.bean.BatchQuery;

import cl.mobid.wsa.telcel.csv.TggDateEntryConverter;
import cl.mobid.wsa.telcel.csv.TggRestrictedConverter;
import cl.mobid.wsa.telcel.csv.TggTotalEntryConverter;
import cl.mobid.wsa.telcel.oauth.OauthUser;
import cl.mobid.wsa.telcel.service.ProcessService;
import cl.mobid.wsa.telcel.service.TggService;
import cl.mobid.wsa.telcel.service.QueryOperatorService;
import cl.mobid.wsa.telcel.soap.TggSoap;

@RestController
@RequestMapping("/api/tgg")
public class TggController {

    @Autowired
    private TggService tggService;
    @Autowired
    private ProcessService processService;
    @Autowired
    private QueryOperatorService queryOperatorService;

    @RequestMapping(value = "/company", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Company getCompany() {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	int idLanguage = getIdLanguage(oauthUser.getLanguage());
	Company company = tggService.getCompany(oauthUser.getId_company());

	company.setType(tggService.getTranslation(idLanguage, company.getType()));
	company.setAllow_exceed(tggService.getTranslation(idLanguage,
		company.getAllow_exceed()));
	company.setCtrl_msg_limit(tggService.getTranslation(idLanguage,
		company.getCtrl_msg_limit()));
	company.setCountry_name(tggService.getTranslation(idLanguage,
		company.getCountry_name()));
	return company;
    }
    
    @RequestMapping(value = "/company/{idCompany}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Company getCompanyById(@PathVariable int idCompany) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	int idLanguage = getIdLanguage(oauthUser.getLanguage());
	Company company = tggService.getCompany(idCompany);

	company.setType(tggService.getTranslation(idLanguage, company.getType()));
	company.setAllow_exceed(tggService.getTranslation(idLanguage,
		company.getAllow_exceed()));
	company.setCtrl_msg_limit(tggService.getTranslation(idLanguage,
		company.getCtrl_msg_limit()));
	company.setCountry_name(tggService.getTranslation(idLanguage,
		company.getCountry_name()));

	return company;
    }

    @RequestMapping(value ="/country", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<WebCountry> getCountry() {
	return processService.getCountry();
    }
    
    @RequestMapping(value = "/service", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Service> getServices(
	    @RequestParam(value = "searchcountry", defaultValue = "-1", required = false) int searchCountry) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	List<Service> services = tggService.getServices(
		oauthUser.getId_company(), searchCountry);

	int idLanguage = getIdLanguage(oauthUser.getLanguage());

	for (int i = 0; i < services.size(); i++) {
	    Service service = services.get(i);

	    services.get(i).setService_type(
		    tggService.getTranslation(idLanguage,
			    service.getService_type()));

	    services.get(i).setService_status_description(
		    tggService.getTranslation(idLanguage,
			    service.getService_status()));

	    services.get(i).setCountry_name(
		    tggService.getTranslation(idLanguage,
			    service.getCountry_name()));

	    services.get(i).setCtrl_msg_limit(
		    tggService.getTranslation(idLanguage,
			    service.getCtrl_msg_limit()));
	}

	return services;
    }

    @RequestMapping(value = "/service/{idService}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Service getServiceById(@PathVariable int idService) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	int idLanguage = getIdLanguage(oauthUser.getLanguage());

	Service service = tggService.getServiceById(oauthUser.getId_company(),
		idService);

	service.setService_type(tggService.getTranslation(idLanguage,
		service.getService_type()));
	service.setService_status(tggService.getTranslation(idLanguage,
		service.getService_status()));

	service.setMulti_operador(tggService.getTranslation(idLanguage,
		service.getMulti_operador()));
	service.setReceive_mo(tggService.getTranslation(idLanguage,
		service.getReceive_mo()));

	service.setCtrl_msg_limit(tggService.getTranslation(idLanguage,
		service.getCtrl_msg_limit()));
	service.setCtrl_time(tggService.getTranslation(idLanguage,
		service.getCtrl_time()));

	service.setCheck_whitelist(tggService.getTranslation(idLanguage,
		service.getCheck_whitelist()));
	service.setCheck_blacklist(tggService.getTranslation(idLanguage,
		service.getCheck_blacklist()));
	service.setCtrl_msg_day(tggService.getTranslation(idLanguage,
		service.getCtrl_msg_day()));

	service.setNotify_optout(tggService.getTranslation(idLanguage,
		service.getNotify_optout()));
	service.setNotify_others(tggService.getTranslation(idLanguage,
		service.getNotify_others()));

	service.setInvitation(tggService.getTranslation(idLanguage,
		service.getInvitation()));

	service.setAllow_exceed(tggService.getTranslation(idLanguage,
		service.getAllow_exceed()));

	service.setDispatch_mo_flag(tggService.getTranslation(idLanguage,
		service.getDispatch_mo_flag()));
	service.setDispatch_dr_flag(tggService.getTranslation(idLanguage,
		service.getDispatch_dr_flag()));
	service.setUse_fake_sender(tggService.getTranslation(idLanguage,
		service.getUse_fake_sender()));
	service.setAsk_for_ack(tggService.getTranslation(idLanguage,
		service.getAsk_for_ack()));

	service.setEnable_sms_web(tggService.getTranslation(idLanguage,
		service.getEnable_sms_web()));
	service.setEnable_sms_ws(tggService.getTranslation(idLanguage,
		service.getEnable_sms_ws()));
	service.setEnable_sms_smpp(tggService.getTranslation(idLanguage,
		service.getEnable_sms_smpp()));

	service.setEnable_prov_web(tggService.getTranslation(idLanguage,
		service.getEnable_prov_web()));
	service.setEnable_prov_ws(tggService.getTranslation(idLanguage,
		service.getEnable_prov_ws()));

	service.setBill_ack(tggService.getTranslation(idLanguage,
		service.getBill_ack()));
	service.setCountry_name(tggService.getTranslation(idLanguage,
		service.getCountry_name()));

	return service;
    }

    @RequestMapping(value = "/service/restricted", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getServiceRestricted(
	    @RequestParam(value = "offset", defaultValue = "-1") int offset,
	    @RequestParam(value = "limit", defaultValue = "-1") int limit,
	    @RequestParam(value = "id_service") int idService) {

	/*
	 * OauthUser user = (OauthUser) SecurityContextHolder.getContext()
	 * .getAuthentication().getPrincipal();
	 */

	if (offset == -1 && limit == -1) {
	    return String.valueOf(tggService.countServiceRestricted(idService));
	}

	return tggService.getServiceRestricted(limit, offset, idService);
    }

    @RequestMapping(value = "/service/restricted/file", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getRestrictedFile(
	    @RequestParam(value = "id_service", defaultValue = "-1") Integer idService) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	String Path = "/home/filesWSA/";
	String fileName = "restricted_" + user.getId_user() + "_"
		+ System.currentTimeMillis();

	if (idService != -1) {

	    List<String> restricted = tggService.getServiceRestricted(-1, 0,
		    idService);

	    try {
		// Writer out = new FileWriter(Path + fileName + ".csv");
		PrintWriter out = new PrintWriter(Path + fileName + ".csv",
			"ISO-8859-1");

		CSVWriter<String> csvWriter = new CSVWriterBuilder<String>(out)
			.entryConverter(new TggRestrictedConverter()).build();

		out.write(processService.getRestrictedHeader(user.getLanguage()
			.toUpperCase().trim()));
		csvWriter.writeAll(restricted);

		csvWriter.flush();
		csvWriter.close();
		out.close();

		return new ResponseEntity<String>(fileName, HttpStatus.OK);
	    } catch (IOException e) {
		return new ResponseEntity<String>("",
			HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/whitelist/process", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> putWhiteListProcess(
	    HttpServletRequest request,
	    @RequestBody WhiteListProcess whiteListProcess) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	InputProcess process = new InputProcess();
	process.setId_company(user.getId_company());
	process.setCreated_by(user.getId_user());

	if (whiteListProcess.getType().equals("WL")) {
	    process.setInput_mode("WL_ADD");
	} else {
	    process.setInput_mode("WL_DEL");
	}

	int index = whiteListProcess.getFile_name().lastIndexOf("/") + 1;
	whiteListProcess.setFile_path(whiteListProcess.getFile_name()
		.substring(0, index));
	whiteListProcess.setFile_name(whiteListProcess.getFile_name()
		.substring(index, whiteListProcess.getFile_name().length()));
	whiteListProcess.setIp_server(request.getRemoteAddr());

	if (processService.createWhiteListProcess(process, whiteListProcess) == 1) {
	    return new ResponseEntity<String>("process created succesfully",
		    HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/whitelist/subscriber", method = RequestMethod.PUT)
    public Object addWhiteList(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "msisdn") String msisdn,
	    @RequestParam(value = "sender") String sender) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (idCountry != -1) {
	    TggSoap soapClient = new TggSoap(
		    processService.getProcParams(processService
			    .getIdProc(idCountry)));

	    return new ResponseEntity<Object>(processService.getResponse(
		    soapClient.addToWListService(msisdn, sender),
		    "addToWListService", user.getLanguage().toUpperCase()
			    .trim()), HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/whitelist/subscriber", method = RequestMethod.DELETE)
    public Object delWhiteList(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "msisdn") String msisdn,
	    @RequestParam(value = "sender") String sender) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (idCountry != -1) {
	    TggSoap soapClient = new TggSoap(
		    processService.getProcParams(processService
			    .getIdProc(idCountry)));

	    return new ResponseEntity<Object>(processService.getResponse(
		    soapClient.delFromWListService(msisdn, sender),
		    "delFromWListService", user.getLanguage().toUpperCase()
			    .trim()), HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/whitelist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getWhiteLists(
	    @RequestParam(value = "offset", defaultValue = "-1") int offset,
	    @RequestParam(value = "limit", defaultValue = "-1") int limit) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (offset == -1 && limit == -1) {
	    return String.valueOf(tggService.countWhiteLists(user
		    .getId_company()));
	}

	return tggService.getWhiteLists(limit, offset, user.getId_company());
    }

    @RequestMapping(value = "/whitelist/{idList}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Service> getWhiteListById(@PathVariable int idList) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	return tggService.getWhiteListById(user.getId_company(), idList);
    }

    @RequestMapping(value = "/whitelist/validate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public int validateWhiteList(@RequestParam(value = "msisdn") String msisdn,
	    @RequestParam(value = "id_service") int idService) {

	return tggService.validateWhiteList(msisdn, idService);
    }

    @RequestMapping(value = "/blacklist/process", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> putBlackListProcess(
	    HttpServletRequest request,
	    @RequestBody WhiteListProcess whiteListProcess) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	InputProcess process = new InputProcess();
	process.setId_company(user.getId_company());
	process.setCreated_by(user.getId_user());

	if (whiteListProcess.getType().equals("BL")) {
	    process.setInput_mode("BL_ADD");
	} else {
	    process.setInput_mode("BL_DEL");
	}

	int index = whiteListProcess.getFile_name().lastIndexOf("/") + 1;
	whiteListProcess.setFile_path(whiteListProcess.getFile_name()
		.substring(0, index));
	whiteListProcess.setFile_name(whiteListProcess.getFile_name()
		.substring(index, whiteListProcess.getFile_name().length()));
	whiteListProcess.setIp_server(request.getRemoteAddr());

	if (processService.createWhiteListProcess(process, whiteListProcess) == 1) {
	    return new ResponseEntity<String>("process created succesfully",
		    HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/blacklist/subscriber", method = RequestMethod.PUT)
    public Object addBlackList(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "msisdn") String msisdn,
	    @RequestParam(value = "sender") String sender) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (idCountry != -1) {
	    TggSoap soapClient = new TggSoap(
		    processService.getProcParams(processService
			    .getIdProc(idCountry)));

	    return new ResponseEntity<Object>(processService.getResponse(
		    soapClient.addToServRestricted(msisdn, sender),
		    "addToServRestricted", user.getLanguage().toUpperCase()
			    .trim()), HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/blacklist/subscriber", method = RequestMethod.DELETE)
    public Object delBlackList(
	    @RequestParam(value = "id_country", defaultValue = "-1") int idCountry,
	    @RequestParam(value = "msisdn") String msisdn,
	    @RequestParam(value = "sender") String sender) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (idCountry != -1) {
	    TggSoap soapClient = new TggSoap(
		    processService.getProcParams(processService
			    .getIdProc(idCountry)));

	    return new ResponseEntity<Object>(processService.getResponse(
		    soapClient.delFromServRestricted(msisdn, sender),
		    "delFromServRestricted", user.getLanguage().toUpperCase()
			    .trim()), HttpStatus.OK);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/blacklist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getBlackLists(
	    @RequestParam(value = "offset", defaultValue = "-1") int offset,
	    @RequestParam(value = "limit", defaultValue = "-1") int limit) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (offset == -1 && limit == -1) {
	    return String.valueOf(tggService.countBlackLists(user
		    .getId_company()));
	}

	return tggService.getBlackLists(limit, offset, user.getId_company());
    }

    @RequestMapping(value = "/blacklist/validate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HashMap<String, Integer> validateBlackList(
	    @RequestParam(value = "msisdn") String msisdn,
	    @RequestParam(value = "id_service") int idService) {

	return tggService.validateBlackList(msisdn, idService);
    }

    @RequestMapping(value = "/traffic/date", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getTrafficDate(
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "id_country", required = false) Integer idCountry,
	    @RequestParam(value = "id_service", required = false, defaultValue = "-1") Integer idService,
	    @RequestParam(value = "searchchannel", required = false) String channel,
	    @RequestParam(value = "searchtype", required = false) String type,
	    @RequestParam(value = "searchlanguage", required = false, defaultValue = "ES") String language) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (startDate != null && endDate != null) {
	    if (idService == -1) {
		idService = null;
	    }

	    int idLanguage = getIdLanguage(language);

	    return tggService.getTrafficDate(startDate, endDate, idCountry,
		    user.getId_company(), idService, channel, type, idLanguage);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/traffic/date/file", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getTrafficDateFile(
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "id_country", required = false) Integer idCountry,
	    @RequestParam(value = "id_service", required = false, defaultValue = "-1") Integer idService,
	    @RequestParam(value = "searchchannel", required = false) String channel,
	    @RequestParam(value = "searchtype", required = false) String type,
	    @RequestParam(value = "searchlanguage", required = false, defaultValue = "ES") String language) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	String Path = "/home/filesWSA/";
	String fileName = "tgg_" + user.getId_user() + "_"
		+ System.currentTimeMillis();

	if (startDate != null && endDate != null && idCountry != -1) {
	    if (idService == -1) {
		idService = null;
	    }

	    int idLanguage = getIdLanguage(language);

	    List<TrafficDateOut> traffic = tggService.getTrafficDate(startDate,
		    endDate, idCountry, user.getId_company(), idService,
		    channel, type, idLanguage);

	    try {
		PrintWriter out = new PrintWriter(Path + fileName + ".csv",
			"ISO-8859-1");

		CSVWriter<TrafficDateOut> csvWriter = new CSVWriterBuilder<TrafficDateOut>(
			out).entryConverter(new TggDateEntryConverter())
			.build();

		out.write(processService.getDateTrafficHeader(user
			.getLanguage().toUpperCase().trim()));
		csvWriter.writeAll(traffic);

		csvWriter.flush();
		csvWriter.close();
		out.close();

		return new ResponseEntity<String>(fileName, HttpStatus.OK);
	    } catch (IOException e) {
		return new ResponseEntity<String>("",
			HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/traffic/total", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getTrafficTotal(
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "id_country", required = false) Integer idCountry,
	    @RequestParam(value = "id_service", required = false, defaultValue = "-1") Integer idService,
	    @RequestParam(value = "searchlanguage", required = false, defaultValue = "ES") String language) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	if (startDate != null && endDate != null) {
	    if (idService == -1) {
		idService = null;
	    }

	    int idLanguage = getIdLanguage(language);

	    return tggService.getTrafficTotal(startDate, endDate, idCountry,
		    user.getId_company(), idService, idLanguage);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/traffic/total/file", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getTrafficTotalFile(
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "id_country", required = false) Integer idCountry,
	    @RequestParam(value = "id_service", required = false, defaultValue = "-1") Integer idService,
	    @RequestParam(value = "searchlanguage", required = false, defaultValue = "ES") String language) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	String Path = "/home/filesWSA/";
	String fileName = "tgg_" + user.getId_user() + "_"
		+ System.currentTimeMillis();

	if (startDate != null && endDate != null && idCountry != -1) {
	    if (idService == -1) {
		idService = null;
	    }

	    int idLanguage = getIdLanguage(language);

	    List<TrafficTotalOut> traffic = tggService.getTrafficTotal(
		    startDate, endDate, idCountry, user.getId_company(),
		    idService, idLanguage);

	    try {
		PrintWriter out = new PrintWriter(Path + fileName + ".csv",
			"ISO-8859-1");

		CSVWriter<TrafficTotalOut> csvWriter = new CSVWriterBuilder<TrafficTotalOut>(
			out).entryConverter(new TggTotalEntryConverter())
			.build();

		out.write(processService.getTotalTrafficHeader(user
			.getLanguage().toUpperCase().trim()));
		csvWriter.writeAll(traffic);

		csvWriter.flush();
		csvWriter.close();
		out.close();

		return new ResponseEntity<String>(fileName, HttpStatus.OK);
	    } catch (IOException e) {
		return new ResponseEntity<String>("",
			HttpStatus.INTERNAL_SERVER_ERROR);
	    }
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/traffic/download/{fileName}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getFile(@PathVariable String fileName,
	    HttpServletResponse response) {

	final HttpHeaders headers = new HttpHeaders();

	File file = new File("/home/filesWSA/" + fileName + ".csv");

	if (file.exists()) {
	    InputStream inputStream = null;
	    OutputStream outputStream = null;

	    try {
		inputStream = new FileInputStream(file);
	    } catch (FileNotFoundException e) {

		e.printStackTrace();
	    }

	    response.setContentType("text/csv");
	    response.setHeader("Content-Disposition", "attachment; filename=\""
		    + file.getName() + "\"");
	    Long fileSize = file.length();
	    response.setContentLength(fileSize.intValue());

	    try {
		outputStream = response.getOutputStream();
	    } catch (IOException e) {
		e.printStackTrace();
	    }

	    byte[] buffer = new byte[1024];
	    int read = 0;

	    try {
		while ((read = inputStream.read(buffer)) != -1) {
		    outputStream.write(buffer, 0, read);
		}

		outputStream.flush();
		outputStream.close();
		inputStream.close();
	    } catch (IOException e) {

		e.printStackTrace();
	    }
	}

	headers.setContentType(MediaType.TEXT_PLAIN);
	return new ResponseEntity<byte[]>("file not found".getBytes(), headers,
		HttpStatus.NOT_FOUND);
    }

    // Consulta Operadora

    @RequestMapping(value = "/singleQuery", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public SingleQuery getSingleQuery(
            @RequestParam(value = "id_country") int idCountry,
            @RequestParam(value = "msisdn") String msisdn ) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	SingleQuery singleQuery = new SingleQuery();

	singleQuery.setId_company(oauthUser.getId_company());
	singleQuery.setId_user(oauthUser.getId_user());
	singleQuery.setId_country(idCountry);
	singleQuery.setMsisdn(msisdn);

	// crear registro
        int res = queryOperatorService.createSingleQuery(singleQuery);
        if (res > 0)
	{
		// enviar request soap
		res = queryOperatorService.consumeSingleQuery(singleQuery);

		if (res == 0)
		{
			singleQuery.setError_code(0);
			singleQuery.setError_text("OK");
		}
		else
		{
			singleQuery.setError_code(-2);
			singleQuery.setError_text("Failed to get remote ws response.");
		}

		// modificar registro
	        queryOperatorService.updateSingleQuery(singleQuery);
	}
	else
	{
		singleQuery.setError_code(-1);
		singleQuery.setError_text("Failed to create single query register.");
	}

	return singleQuery;
    }

    @RequestMapping(value = "/listQuery", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BatchQuery getListQuery(
            @RequestParam(value = "id_country") int idCountry,
            @RequestParam(value = "id_group") int idGroup,
            @RequestParam(value = "name_group") String nameGroup) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	BatchQuery batchQuery = new BatchQuery();

	HashMap<String,Object> result = queryOperatorService.createQueryFile(idGroup, oauthUser.getId_company());
	if (result == null)
	{
		batchQuery.setError_code(-1);
		batchQuery.setError_text("Failed to create file from list.");
	}
	else
	{
		batchQuery.setId_company(oauthUser.getId_company());
		batchQuery.setId_user(oauthUser.getId_user());
		batchQuery.setId_country(idCountry);
		batchQuery.setId_group(idGroup);
		batchQuery.setFile_original_name(nameGroup);
		batchQuery.setFile_internal_name((String)result.get("FileName"));
		batchQuery.setInsert_time((String)result.get("InsertTime"));
		batchQuery.setStatus("QUEUED");
		batchQuery.setInfo("");
		batchQuery.setMode("LIST");
		batchQuery.setRows_total((Integer)result.get("RowsTotal"));
		batchQuery.setRows_ok(0);
		batchQuery.setRows_unknown(0);
		batchQuery.setRows_timeout(0);

		// crear registro
		int res = queryOperatorService.createBatchQuery(batchQuery);
		if (res > 0)
		{
			batchQuery.setError_code(0);
			batchQuery.setError_text("OK");
		}
		else
		{
			batchQuery.setError_code(-2);
			batchQuery.setError_text("Failed to create list query register.");
		}
	}

	return batchQuery;
    }

    @RequestMapping(value = "/fileQuery", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BatchQuery getFileQuery(
            @RequestParam(value = "id_country") int idCountry,
            @RequestParam(value = "filename") String fileName,
            @RequestParam(value = "filedata") MultipartFile fileData,
            @RequestParam(value = "rowscount") int rowsCount) {

	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	BatchQuery batchQuery = new BatchQuery();

	HashMap<String,String> result = queryOperatorService.createQueryFile(fileData, oauthUser.getId_company());
	if (result == null)
	{
		batchQuery.setError_code(-1);
		batchQuery.setError_text("Failed to create file from uploaded request.");
	}
	else
	{
		batchQuery.setId_company(oauthUser.getId_company());
		batchQuery.setId_user(oauthUser.getId_user());
		batchQuery.setId_country(idCountry);
		batchQuery.setId_group(0);
		batchQuery.setFile_original_name(fileName);
		batchQuery.setFile_internal_name((String)result.get("FileName"));
		batchQuery.setInsert_time((String)result.get("InsertTime"));
		batchQuery.setStatus("QUEUED");
		batchQuery.setInfo("");
		batchQuery.setMode("FILE");
		batchQuery.setRows_total(rowsCount);
		batchQuery.setRows_ok(0);
		batchQuery.setRows_unknown(0);
		batchQuery.setRows_timeout(0);

		// crear registro
		int res = queryOperatorService.createBatchQuery(batchQuery);
		if (res > 0)
		{
			batchQuery.setError_code(0);
			batchQuery.setError_text("OK");
		}
		else
		{
			batchQuery.setError_code(-2);
			batchQuery.setError_text("Failed to create file query register.");
		}
	}

	return batchQuery;
    }

    @RequestMapping(value = "/bitacora", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getBitacora(
	    @RequestParam(value = "start_date") String startDate,
	    @RequestParam(value = "end_date") String endDate,
	    @RequestParam(value = "archivo") String archivo,
	    @RequestParam(value = "order") String order) {

	OauthUser user = (OauthUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

	if (startDate != null && endDate != null)
        {
	    return queryOperatorService.getBitacora(startDate, endDate, archivo, user.getId_company(), order);
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(value = "/resultQuery", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getResultQuery(@RequestParam(value = "resultname") String resultName,
	    HttpServletResponse response) {

	final HttpHeaders headers = new HttpHeaders();

	File file = queryOperatorService.getResultQueryFile(resultName);

	if (file.exists())
	{
	    InputStream inputStream = null;
	    OutputStream outputStream = null;

	    try
	    {
		inputStream = new FileInputStream(file);
	    }
	    catch (FileNotFoundException e)
	    {
		e.printStackTrace();
	    }

	    response.setContentType("text/csv");
	    response.setHeader("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"");
	    Long fileSize = file.length();
	    response.setContentLength(fileSize.intValue());

	    try
	    {
		outputStream = response.getOutputStream();
	    }
	    catch (IOException e)
	    {
		e.printStackTrace();
	    }

	    byte[] buffer = new byte[1024];
	    int read = 0;

	    try
	    {
		while ((read = inputStream.read(buffer)) != -1)
		{
		    outputStream.write(buffer, 0, read);
		}

		outputStream.flush();
		outputStream.close();
		inputStream.close();
	    }
	    catch (IOException e)
	    {
		e.printStackTrace();
	    }
	}

	headers.setContentType(MediaType.TEXT_PLAIN);
	return new ResponseEntity<byte[]>("file not found".getBytes(), headers, HttpStatus.NOT_FOUND);
    }

    private int getIdLanguage(String code) {
	if (code.toLowerCase().trim().equals("es")) {
	    return 1;
	} else if (code.toLowerCase().trim().equals("en")) {
	    return 2;
	} else if (code.toLowerCase().trim().equals("pt")) {
	    return 3;
	}

	return -1;
    }
}
