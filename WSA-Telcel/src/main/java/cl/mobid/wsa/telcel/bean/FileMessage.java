package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

public class FileMessage extends Message implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private int id_process;
    private String ip_server;
    private String file_path;
    private String file_name;
    private int total;

    // MHL - 2017-05-02
    private int id_center;
    
    public int getId_process() {
	return id_process;
    }
    
    public void setId_process(int id_process) {
	this.id_process = id_process;
    }

    public String getIp_server() {
	return ip_server;
    }

    public void setIp_server(String ip_server) {
	this.ip_server = ip_server;
    }

    public String getFile_path() {
	return file_path;
    }

    public void setFile_path(String file_path) {
	this.file_path = file_path;
    }

    public String getFile_name() {
	return file_name;
    }

    public void setFile_name(String file_name) {
	this.file_name = file_name;
    }

    public int getTotal() {
	return total;
    }

    public void setTotal(int total) {
	this.total = total;
    }

    // MHL - 2017-05-02

    public int getId_center() {
	return id_center;
    }
    
    public void setId_center(int id_center) {
	this.id_center = id_center;
    }
}
