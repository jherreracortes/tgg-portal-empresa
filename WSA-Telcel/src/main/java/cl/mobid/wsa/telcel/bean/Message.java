package cl.mobid.wsa.telcel.bean;

public class Message {
    
    private String sender;
    private String stage;
    private int delivery_receipt;
    private String data_coding;
    private int id_company;
    private int id_country;
    private int input_process;
    private String created_time;
    private String dispatch_time;

    public String getSender() {
	return sender;
    }

    public void setSender(String sender) {
	this.sender = sender;
    }

    public String getStage() {
	return stage;
    }

    public void setStage(String stage) {
	this.stage = stage;
    }

    public int getDelivery_receipt() {
	return delivery_receipt;
    }

    public void setDelivery_receipt(int delivery_receipt) {
	this.delivery_receipt = delivery_receipt;
    }

    public String getData_coding() {
	return data_coding;
    }

    public void setData_coding(String data_coding) {
	this.data_coding = data_coding;
    }

    public int getId_company() {
	return id_company;
    }

    public void setId_company(int id_company) {
	this.id_company = id_company;
    }

    public int getId_country() {
	return id_country;
    }

    public void setId_country(int id_country) {
	this.id_country = id_country;
    }

    public int getInput_process() {
	return input_process;
    }

    public void setInput_process(int input_process) {
	this.input_process = input_process;
    }

    public String getCreated_time() {
	return created_time;
    }

    public void setCreated_time(String created_time) {
	this.created_time = created_time;
    }

    public String getDispatch_time() {
	return dispatch_time;
    }

    public void setDispatch_time(String dispatch_time) {
	this.dispatch_time = dispatch_time;
    }
}
