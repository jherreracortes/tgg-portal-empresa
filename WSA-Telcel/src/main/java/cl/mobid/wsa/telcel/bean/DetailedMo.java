package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class DetailedMo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String msisdn;
    private String sender;
    private String created_time;
    private String content;

    public String getMsisdn() {
	return msisdn;
    }

    public void setMsisdn(String msisdn) {
	this.msisdn = msisdn;
    }

    public String getSender() {
	return sender;
    }

    public void setSender(String sender) {
	this.sender = sender;
    }

    public String getCreated_time() {
	return created_time;
    }

    public void setCreated_time(String created_time) {
	this.created_time = created_time;
    }

    public String getContent() {
	return content;
    }

    public void setContent(String content) {
	this.content = content;
    }
}
