package cl.mobid.wsa.telcel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cl.mobid.wsa.telcel.bean.SingleQuery;
import cl.mobid.wsa.telcel.bean.BatchQuery;
import cl.mobid.wsa.telcel.bean.Bitacora;

public interface QueryOperatorMapper {

    public int createSingleQuery(SingleQuery singleQuery);

    public int updateSingleQuery(SingleQuery singleQuery);

    public int createBatchQuery(BatchQuery batchQuery);

    public List<String> selectMobiles(@Param("idGroup") int idGroup);

    public String selectFormatedNow();

    public String selectOperatorName(@Param("code") String code);

    public List<Bitacora> selectBitacora(@Param("startDate") String startDate,
                                         @Param("endDate") String endDate,
                                         @Param("archivo") String archivo, 
                                         @Param("idCompany") int idCompany,
                                         @Param("order") String order);

}

