package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

public class WhiteList implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id_list;
    private String list_name;
    private String list_status;
    private String control_lines;
    private String max_lines;
    private String current_lines;
    private int id_company;

    public int getId_list() {
	return id_list;
    }

    public void setId_list(int id_list) {
	this.id_list = id_list;
    }

    public String getList_name() {
	return list_name;
    }

    public void setList_name(String list_name) {
	this.list_name = list_name;
    }

    public String getList_status() {
	return list_status;
    }

    public void setList_status(String list_status) {
	this.list_status = list_status;
    }

    public String getControl_lines() {
	return control_lines;
    }

    public void setControl_lines(String control_lines) {
	this.control_lines = control_lines;
    }

    public String getMax_lines() {
	return max_lines;
    }

    public void setMax_lines(String max_lines) {
	this.max_lines = max_lines;
    }

    public String getCurrent_lines() {
	return current_lines;
    }

    public void setCurrent_lines(String current_lines) {
	this.current_lines = current_lines;
    }

    public int getId_company() {
	return id_company;
    }

    public void setId_company(int id_company) {
	this.id_company = id_company;
    }
}
