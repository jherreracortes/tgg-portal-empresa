package cl.mobid.wsa.telcel.oauth;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;

import cl.mobid.wsa.telcel.bean.Country;
import cl.mobid.wsa.telcel.bean.User;
import cl.mobid.wsa.telcel.mapper.UserMapper;
import cl.mobid.wsa.telcel.tgg.mapper.TggMapper;

public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserMapper userMapper;
    @Autowired
    TggMapper tggMapper;
    @Autowired
    private DefaultTokenServices tokenServices;

//    @Override
    public UserDetails loadUserByUsername(String username)
	    throws UsernameNotFoundException {

	List<OAuth2AccessToken> userTokens = null;
	userTokens = new ArrayList<OAuth2AccessToken>(
		tokenServices.findTokensByUserName(username.trim()));

	if (!userTokens.isEmpty()) {
	    tokenServices.revokeToken(userTokens.get(0).getValue());
	}

	User user = userMapper.selectByUsername(username);

	if (user == null) {
	    throw new UsernameNotFoundException("User not found...");
	}

	OauthUser oauthUser = new OauthUser(user);

	if (user.getTitle().toLowerCase().equals("mobid")) {
	    oauthUser.setCountry(new Country("ACTIVE", "ADMIN", 0, "gl"));

	} else {
	    oauthUser.setCountry(tggMapper.selectCountry(oauthUser.getId_company()));

	    if (oauthUser.getCountry().getCompany_status().toLowerCase().equals("canceled")) {
		oauthUser.setEnabled(0);
	    }
	}

	return oauthUser;
    }

}
