package cl.mobid.wsa.telcel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cl.mobid.wsa.telcel.bean.Group;
import cl.mobid.wsa.telcel.bean.GroupProcess;
import cl.mobid.wsa.telcel.bean.Mobile;

public interface GroupMapper {

    public int createGroup(Group group);

    public int assignGroup(@Param("msisdn") String msisdn,
	    @Param("idGroup") int idGroup,
	    @Param("createdBy") int createdBy);

    public int createGroupProcess(GroupProcess groupProcess);

    public int countGroups(@Param("idCompany") int idCompany,
	    @Param("searchName") String searchName);

    public List<Group> selectGroups(@Param("limit") int limit,
	    @Param("offset") int offset, @Param("idCompany") int idCompany,
	    @Param("searchName") String searchName,
	    @Param("searchCountry") int searchCountry,
	    @Param("orderBy") String orderBy, @Param("order") String order,
	    @Param("enabled") int enabled);

    public Group selectByIdGroup(int idGroup);

    public int countMobiles(@Param("idGroup") int idGroup,
	    @Param("searchMsisdn") String searchMsisdn);

    public List<Mobile> selectMobiles(@Param("limit") int limit,
	    @Param("offset") int offset, @Param("idGroup") int idGroup,
	    @Param("searchMsisdn") String searchMsisdn);

    public int updateGroup(Group group);

    public int setEnabled(@Param("idGroup") int idGroup,
	    @Param("enabled") int enabled);

    public int removeGroup(@Param("idGroup") int idGroup,
	    @Param("idMobile") int idMobile);
}
