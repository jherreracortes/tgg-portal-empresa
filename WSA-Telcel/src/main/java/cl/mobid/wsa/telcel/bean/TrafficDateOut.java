package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class TrafficDateOut implements Serializable {

    private static final long serialVersionUID = 1L;

    private int error_code; 
    private String error_msg;   
    private String date;   
    private int id_company;          
    private String company_name;          
    private String company_type;  
    private String comp_country_name;  
    private int service_id;  
    private String service_tag;           
    private String service_name;            
    private String service_type;  
    private String serv_country_name;  
    private String channel_in;  
    private int mo_ok;  
    private int mo_nook;  
    private int mo_total;  
    private int mt_ok;  
    private int mt_nook;  
    private int mt_total;  
    private int total_ok;  
    private int total_nook;  
    private int total;
    
    public int getError_code() {
	return error_code;
    }
    
    public void setError_code(int error_code) {
	this.error_code = error_code;
    }

    public String getError_msg() {
	return error_msg;
    }

    public void setError_msg(String error_msg) {
	this.error_msg = error_msg;
    }

    public String getDate() {
	return date;
    }

    public void setDate(String date) {
	this.date = date;
    }

    public int getId_company() {
	return id_company;
    }

    public void setId_company(int id_company) {
	this.id_company = id_company;
    }

    public String getCompany_name() {
	return company_name;
    }

    public void setCompany_name(String company_name) {
	this.company_name = company_name;
    }

    public String getCompany_type() {
	return company_type;
    }

    public void setCompany_type(String company_type) {
	this.company_type = company_type;
    }

    public String getComp_country_name() {
	return comp_country_name;
    }

    public void setComp_country_name(String comp_country_name) {
	this.comp_country_name = comp_country_name;
    }

    public int getService_id() {
	return service_id;
    }

    public void setService_id(int service_id) {
	this.service_id = service_id;
    }

    public String getService_tag() {
	return service_tag;
    }

    public void setService_tag(String service_tag) {
	this.service_tag = service_tag;
    }

    public String getService_name() {
	return service_name;
    }

    public void setService_name(String service_name) {
	this.service_name = service_name;
    }

    public String getService_type() {
	return service_type;
    }

    public void setService_type(String service_type) {
	this.service_type = service_type;
    }

    public String getServ_country_name() {
	return serv_country_name;
    }

    public void setServ_country_name(String serv_country_name) {
	this.serv_country_name = serv_country_name;
    }

    public String getChannel_in() {
	return channel_in;
    }

    public void setChannel_in(String channel_in) {
	this.channel_in = channel_in;
    }

    public int getMo_ok() {
	return mo_ok;
    }

    public void setMo_ok(int mo_ok) {
	this.mo_ok = mo_ok;
    }

    public int getMo_nook() {
	return mo_nook;
    }

    public void setMo_nook(int mo_nook) {
	this.mo_nook = mo_nook;
    }

    public int getMo_total() {
	return mo_total;
    }

    public void setMo_total(int mo_total) {
	this.mo_total = mo_total;
    }

    public int getMt_ok() {
	return mt_ok;
    }

    public void setMt_ok(int mt_ok) {
	this.mt_ok = mt_ok;
    }

    public int getMt_nook() {
	return mt_nook;
    }

    public void setMt_nook(int mt_nook) {
	this.mt_nook = mt_nook;
    }

    public int getMt_total() {
	return mt_total;
    }

    public void setMt_total(int mt_total) {
	this.mt_total = mt_total;
    }

    public int getTotal_ok() {
	return total_ok;
    }

    public void setTotal_ok(int total_ok) {
	this.total_ok = total_ok;
    }

    public int getTotal_nook() {
	return total_nook;
    }

    public void setTotal_nook(int total_nook) {
	this.total_nook = total_nook;
    }

    public int getTotal() {
	return total;
    }

    public void setTotal(int total) {
	this.total = total;
    }
}
