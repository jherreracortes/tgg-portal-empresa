package cl.mobid.wsa.telcel.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cl.mobid.wsa.telcel.bean.ConsolidatedProcess;
import cl.mobid.wsa.telcel.bean.InputProcess;
import cl.mobid.wsa.telcel.bean.WebCountry;
import cl.mobid.wsa.telcel.bean.WhiteListProcess;
import cl.mobid.wsa.telcel.mapper.ProcessMapper;
import cl.mobid.wsa.telcel.oauth.OauthUser;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ProcessService {

    @Autowired
    private ProcessMapper processMapper;

    public int createWhiteListProcess(InputProcess process,
	    WhiteListProcess whitelist) {
	return processMapper.createWBProcess(process, whitelist);
    }

    public List<WebCountry> getCountry() {
	return processMapper.selectCountry();
    }

    public List<ConsolidatedProcess> getConsolidatedProcess(int idCompany,
	    String timeGMT, String startDate, String endDate, int searchCCost) {
	return processMapper.consolidatedProcess(idCompany, timeGMT, startDate, endDate, searchCCost);
    }

    public List<ConsolidatedProcess> getConsolidatedWBProcess(int idCompany,
	    String timeGMT, String startDate, String endDate,
	    String searchType) {
	return processMapper.consolidatedWBProcess(idCompany, timeGMT,
		startDate, endDate, searchType);
    }

    public List<ConsolidatedProcess> getCancelProcess(int idCompany,
	    String timeGMT) {
	return processMapper.selectCancelProcess(idCompany, timeGMT);
    }

    public ConsolidatedProcess getCancelProcessById(int idCompany,
	    int idProcess, String timeGMT) {
	return processMapper.selectCancelProcessById(idCompany, idProcess,
		timeGMT);
    }

    public InputProcess getProcessById(int idCompany, int idProcess) {
	return processMapper.selectProcessById(idCompany, idProcess);
    }

    public int getWBErrors(int idProcess) {
	return processMapper.countWBErrors(idProcess);
    }

    public int getByStage(int idCompany, int idProcess, String searchStage) {
	return processMapper.countByStage(idCompany, idProcess, searchStage);
    }

    public void getDetailedProcess(int idCompany, int idProcess, String timeGMT,
	    String fileName, String searchStage, int searchCCost, String language) {
	processMapper.detailedProcess(idCompany, idProcess, timeGMT, fileName,
		searchStage, searchCCost, language);
    }

    public void getDetailedWBProcess(int idCompany, int idProcess,
	    String timeGMT, String type, String fileName, String searchStage,
	    String language) {
	processMapper.detailedWBProcess(idCompany, idProcess, timeGMT, type,
		fileName, searchStage, language);
    }

    public void getDetailedErrors(int idProcess, String timeGMT,
	    String fileName, String language) {
	processMapper.detailedErrors(idProcess, timeGMT, fileName, language);
    }

    public int cancelMessage(OauthUser user, InputProcess process) {
	return processMapper.cancelMessage(user.getId_company(),
		user.getId_user(), process.getId_process(),
		process.getInput_mode());
    }

    public String getGMT(int idCountry) {
	return processMapper.selectGMT(idCountry);
    }

    public int getIdProc(int idCountry) {
	return processMapper.selectIdProc(idCountry);
    }

    public ArrayList<HashMap<String, Object>> getProcParams(int idProc) {
	return processMapper.selectProcParams(idProc);
    }

    public String getResponse(int code, String type, String language) {
	return processMapper.selectResponse(code, type, language);
    }

    public String getRestrictedHeader(String language) {
	return processMapper.selectRestrictedHeader(language);
    }

    public String getDateTrafficHeader(String language) {
	return processMapper.selectDateTrafficHeader(language);
    }

    public String getTotalTrafficHeader(String language) {
	return processMapper.selectTotalTrafficHeader(language);
    }
}
