package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ConsolidatedMo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String report_date;
    private int count;

    public String getReport_date() {
	return report_date;
    }

    public void setReport_date(String report_date) {
	this.report_date = report_date;
    }

    public int getCount() {
	return count;
    }

    public void setCount(int count) {
	this.count = count;
    }
}
