package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

public class GroupMessage extends Message implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private int id_process;
    private int id_group;
    private String content;
    private int total;

    // MHL - 2017-05-02
    private int id_center;
    
    public int getId_process() {
	return id_process;
    }
    
    public void setId_process(int id_process) {
	this.id_process = id_process;
    }

    public int getId_group() {
	return id_group;
    }

    public void setId_group(int id_group) {
	this.id_group = id_group;
    }

    public String getContent() {
	return content;
    }

    public void setContent(String content) {
	this.content = content;
    }

    public int getTotal() {
	return total;
    }

    public void setTotal(int total) {
	this.total = total;
    }

    // MHL - 2017-05-02

    public int getId_center() {
	return id_center;
    }
    
    public void setId_center(int id_center) {
	this.id_center = id_center;
    }
}
