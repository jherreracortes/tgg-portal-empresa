package cl.mobid.wsa.telcel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cl.mobid.wsa.telcel.bean.CostCenter;

public interface CostCenterMapper {
    
    public int createCenter(CostCenter costCenter);

    public int countCenters(@Param("idCompany") int idCompany,
	    @Param("searchName") String searchName);

    public List<CostCenter> selectCenters(@Param("limit") int limit,
	    @Param("offset") int offset, @Param("idCompany") int idCompany,
	    @Param("searchName") String searchName);
    
    public CostCenter selectByIdCenter(int idCenter);

    public int updateCenter(CostCenter costCenter);

    public int setEnabled(@Param("idCenter") int idCenter,
	    @Param("enabled") int enabled);
}
