package cl.mobid.wsa.telcel.soap;

import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.springframework.util.Base64Utils;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TggSoap {

    HashMap<String, Object> pConfig;

    public TggSoap(ArrayList<HashMap<String, Object>> procParams) {
	pConfig = new HashMap<String, Object>();

	for (HashMap<String, Object> row : procParams) {
	    pConfig.put(row.get("tag_param").toString(), row.get("param")
		    .toString());
	}
    }

    public int addToWListService(String msisdn, String sender) {
	SOAPConnectionFactory soapClient;
	try {
	    soapClient = SOAPConnectionFactory.newInstance();
	    SOAPConnection soapConnection = soapClient.createConnection();

	    String action = "addToWListService";
	    String request = "addToWListServiceRequest";

	    String URL = pConfig.get("URL").toString() + "/" + action;

	    System.out.println("URL : " + URL);

	    SOAPMessage soapResponse = soapConnection.call(
		    createMessage(action, request, msisdn, sender), URL);

	    HashMap<String, Object> response = printResponse(soapResponse);

	    System.out.println(response);
	    soapConnection.close();

	    if (response.get("retCode") != null) {
		return Integer.parseInt(response.get("retCode").toString());
	    }
	} catch (Exception e) {
	    return 500;
	}

	return 500;
    }

    public int delFromWListService(String msisdn, String sender) {
	SOAPConnectionFactory soapClient;
	try {
	    soapClient = SOAPConnectionFactory.newInstance();
	    SOAPConnection soapConnection = soapClient.createConnection();

	    String action = "delFromWListService";
	    String request = "delFromWListServiceRequest";

	    String URL = pConfig.get("URL").toString() + "/" + action;

	    System.out.println("URL : " + URL);

	    SOAPMessage soapResponse = soapConnection.call(
		    createMessage(action, request, msisdn, sender), URL);

	    HashMap<String, Object> response = printResponse(soapResponse);

	    System.out.println(response);
	    soapConnection.close();

	    if (response.get("retCode") != null) {
		return Integer.parseInt(response.get("retCode").toString());
	    }
	} catch (Exception e) {
	    return 500;
	}

	return 500;
    }

    public int addToServRestricted(String msisdn, String sender) {
	SOAPConnectionFactory soapClient;
	try {
	    soapClient = SOAPConnectionFactory.newInstance();
	    SOAPConnection soapConnection = soapClient.createConnection();

	    String action = "addToServRestricted";
	    String request = "addToServRestrictedRequest";

	    String URL = pConfig.get("URL").toString() + "/" + action;

	    System.out.println("URL : " + URL);

	    SOAPMessage soapResponse = soapConnection.call(
		    createMessage(action, request, msisdn, sender), URL);

	    HashMap<String, Object> response = printResponse(soapResponse);

	    System.out.println(response);
	    soapConnection.close();

	    if (response.get("retCode") != null) {
		return Integer.parseInt(response.get("retCode").toString());
	    }
	} catch (Exception e) {
	    return 500;
	}

	return 500;
    }

    public int delFromServRestricted(String msisdn, String sender) {
	SOAPConnectionFactory soapClient;
	try {
	    soapClient = SOAPConnectionFactory.newInstance();
	    SOAPConnection soapConnection = soapClient.createConnection();

	    String action = "delFromServRestricted";
	    String request = "delFromServRestrictedRequest";

	    String URL = pConfig.get("URL").toString() + "/" + action;

	    System.out.println("URL : " + URL);

	    SOAPMessage soapResponse = soapConnection.call(
		    createMessage(action, request, msisdn, sender), URL);

	    HashMap<String, Object> response = printResponse(soapResponse);

	    System.out.println(response);
	    soapConnection.close();

	    if (response.get("retCode") != null) {
		return Integer.parseInt(response.get("retCode").toString());
	    }
	} catch (Exception e) {
	    return 500;
	}

	return 500;
    }

    private SOAPMessage createMessage(String action, String request,
	    String subscriber, String sender) throws Exception {
	MessageFactory messageFactory = MessageFactory.newInstance();
	SOAPMessage soapMessage = messageFactory.createMessage();
	SOAPPart soapPart = soapMessage.getSOAPPart();

	String serverURI = "http://ws.tiaxa.net/tggProvSoapService/";

	SOAPEnvelope envelope = soapPart.getEnvelope();
	envelope.addNamespaceDeclaration("tgg", serverURI);

	SOAPBody body = envelope.getBody();
	SOAPElement soapBodyElem = body.addChildElement(request, "tgg");

	SOAPElement soapBodySubscriber = soapBodyElem
		.addChildElement("subscriber");
	soapBodySubscriber.addTextNode(subscriber);

	SOAPElement soapBodySender = soapBodyElem.addChildElement("sender");
	soapBodySender.addTextNode(sender);

	MimeHeaders headers = soapMessage.getMimeHeaders();
	headers.addHeader("SOAPAction", '"' + serverURI + action + '"');
	headers.setHeader("Content-Type", "text/xml;charset=ISO-8859-1");

	String credentials = pConfig.get("USER").toString() + ":"
		+ pConfig.get("PASS").toString();
	headers.addHeader("Authorization",
		"Basic " + Base64Utils.encodeToString(credentials.getBytes()));

	soapMessage.saveChanges();

	System.out.print("Request SOAP Message = ");
	soapMessage.writeTo(System.out);
	System.out.println();

	System.out.println('"' + serverURI + action + '"');

	return soapMessage;
    }

    private HashMap<String, Object> printResponse(SOAPMessage soapResponse)
	    throws Exception {
	
	TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        Source sourceContent = soapResponse.getSOAPPart().getContent();
        System.out.print("\nResponse SOAP Message = ");
        StreamResult result = new StreamResult(System.out);
        transformer.transform(sourceContent, result);
	
	HashMap<String, Object> responseMap = new HashMap<String, Object>();

	SOAPPart soapPart = soapResponse.getSOAPPart();
	SOAPEnvelope envelope = soapPart.getEnvelope();

	NodeList nodeList = envelope.getBody().getChildNodes();

	for (int i = 0; i < nodeList.getLength(); i++) {
	    Node elementNode = nodeList.item(i);

	    if (elementNode.getChildNodes() != null) {
		for (int x = 0; x < elementNode.getChildNodes().getLength(); x++) {
		    Node childNode = elementNode.getChildNodes().item(x);

		    if (childNode.getNodeType() == Node.ELEMENT_NODE) {
			responseMap.put(childNode.getNodeName(), childNode
				.getFirstChild().getNodeValue());
		    }
		}
	    }
	}

	return responseMap;
    }
}
