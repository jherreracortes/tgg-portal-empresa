package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.annotation.JsonInclude.Include;

//@JsonInclude(Include.NON_NULL)
public class Service implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id_service;
    private String service_type;
    private String created_time;
    private String mod_time;
    private String service_status;
    private String service_status_description;
    private String service_name;
    private String service_tag;
    private String multi_operador;
    private String receive_mo;
    private String id_facturacion;
    private int msg_limit;
    private String ctrl_msg_limit;
    private String ctrl_time;
    private String msg_time_ini;
    private String msg_time_fin;
    private String check_whitelist;
    private String check_blacklist;
    private String ctrl_msg_day;
    private int msg_day;
    private String create_user;
    private String mod_user;
    private String notify_optout;
    private String notify_others;
    private String date_ini;
    private String date_fin;
    private String invitation;
    private String date_ini_inv;
    private String date_fin_inv;
    private String allow_exceed;
    private String dispatch_mt_channel;
    private String dispatch_mo_flag;
    private String dispatch_mo_channel;
    private String dispatch_dr_flag;
    private String use_fake_sender;
    private String ask_for_ack;
    private String enable_sms_web;
    private String enable_sms_ws;
    private String enable_sms_smpp;
    private String enable_prov_web;
    private String enable_prov_ws;
    private String allowed_fake_senders;
    private String bill_ack;
    private String country_name;

    public int getId_service() {
	return id_service;
    }

    public void setId_service(int id_service) {
	this.id_service = id_service;
    }

    public String getService_type() {
	return service_type;
    }

    public void setService_type(String service_type) {
	this.service_type = service_type;
    }

    public String getCreated_time() {
	return created_time;
    }

    public void setCreated_time(String created_time) {
	this.created_time = created_time;
    }

    public String getMod_time() {
	return mod_time;
    }

    public void setMod_time(String mod_time) {
	this.mod_time = mod_time;
    }

    public String getService_status() {
	return service_status;
    }

    public void setService_status(String service_status) {
	this.service_status = service_status;
    }

    public String getService_status_description() {
	return service_status_description;
    }

    public void setService_status_description(String service_status_description) {
	this.service_status_description = service_status_description;
    }

    public String getService_name() {
	return service_name;
    }

    public void setService_name(String service_name) {
	this.service_name = service_name;
    }

    public String getService_tag() {
	return service_tag;
    }

    public void setService_tag(String service_tag) {
	this.service_tag = service_tag;
    }

    public String getMulti_operador() {
	return multi_operador;
    }

    public void setMulti_operador(String multi_operador) {
	this.multi_operador = multi_operador;
    }

    public String getReceive_mo() {
	return receive_mo;
    }

    public void setReceive_mo(String receive_mo) {
	this.receive_mo = receive_mo;
    }

    public String getId_facturacion() {
	return id_facturacion;
    }

    public void setId_facturacion(String id_facturacion) {
	this.id_facturacion = id_facturacion;
    }

    public int getMsg_limit() {
	return msg_limit;
    }

    public void setMsg_limit(int msg_limit) {
	this.msg_limit = msg_limit;
    }

    public String getCtrl_msg_limit() {
	return ctrl_msg_limit;
    }

    public void setCtrl_msg_limit(String ctrl_msg_limit) {
	this.ctrl_msg_limit = ctrl_msg_limit;
    }

    public String getCtrl_time() {
	return ctrl_time;
    }

    public void setCtrl_time(String ctrl_time) {
	this.ctrl_time = ctrl_time;
    }

    public String getMsg_time_ini() {
	return msg_time_ini;
    }

    public void setMsg_time_ini(String msg_time_ini) {
	this.msg_time_ini = msg_time_ini;
    }

    public String getMsg_time_fin() {
	return msg_time_fin;
    }

    public void setMsg_time_fin(String msg_time_fin) {
	this.msg_time_fin = msg_time_fin;
    }

    public String getCheck_whitelist() {
	return check_whitelist;
    }

    public void setCheck_whitelist(String check_whitelist) {
	this.check_whitelist = check_whitelist;
    }

    public String getCheck_blacklist() {
	return check_blacklist;
    }

    public void setCheck_blacklist(String check_blacklist) {
	this.check_blacklist = check_blacklist;
    }

    public String getCtrl_msg_day() {
	return ctrl_msg_day;
    }

    public void setCtrl_msg_day(String ctrl_msg_day) {
	this.ctrl_msg_day = ctrl_msg_day;
    }

    public int getMsg_day() {
	return msg_day;
    }

    public void setMsg_day(int msg_day) {
	this.msg_day = msg_day;
    }

    public String getCreate_user() {
	return create_user;
    }

    public void setCreate_user(String create_user) {
	this.create_user = create_user;
    }

    public String getMod_user() {
	return mod_user;
    }

    public void setMod_user(String mod_user) {
	this.mod_user = mod_user;
    }

    public String getNotify_optout() {
	return notify_optout;
    }

    public void setNotify_optout(String notify_optout) {
	this.notify_optout = notify_optout;
    }

    public String getNotify_others() {
	return notify_others;
    }

    public void setNotify_others(String notify_others) {
	this.notify_others = notify_others;
    }

    public String getDate_ini() {
	return date_ini;
    }

    public void setDate_ini(String date_ini) {
	this.date_ini = date_ini;
    }

    public String getDate_fin() {
	return date_fin;
    }

    public void setDate_fin(String date_fin) {
	this.date_fin = date_fin;
    }

    public String getInvitation() {
	return invitation;
    }

    public void setInvitation(String invitation) {
	this.invitation = invitation;
    }

    public String getDate_ini_inv() {
	return date_ini_inv;
    }

    public void setDate_ini_inv(String date_ini_inv) {
	this.date_ini_inv = date_ini_inv;
    }

    public String getDate_fin_inv() {
	return date_fin_inv;
    }

    public void setDate_fin_inv(String date_fin_inv) {
	this.date_fin_inv = date_fin_inv;
    }

    public String getAllow_exceed() {
	return allow_exceed;
    }

    public void setAllow_exceed(String allow_exceed) {
	this.allow_exceed = allow_exceed;
    }

    public String getDispatch_mt_channel() {
	return dispatch_mt_channel;
    }

    public void setDispatch_mt_channel(String dispatch_mt_channel) {
	this.dispatch_mt_channel = dispatch_mt_channel;
    }

    public String getDispatch_mo_flag() {
	return dispatch_mo_flag;
    }

    public void setDispatch_mo_flag(String dispatch_mo_flag) {
	this.dispatch_mo_flag = dispatch_mo_flag;
    }

    public String getDispatch_mo_channel() {
	return dispatch_mo_channel;
    }

    public void setDispatch_mo_channel(String dispatch_mo_channel) {
	this.dispatch_mo_channel = dispatch_mo_channel;
    }

    public String getDispatch_dr_flag() {
	return dispatch_dr_flag;
    }

    public void setDispatch_dr_flag(String dispatch_dr_flag) {
	this.dispatch_dr_flag = dispatch_dr_flag;
    }

    public String getUse_fake_sender() {
	return use_fake_sender;
    }

    public void setUse_fake_sender(String use_fake_sender) {
	this.use_fake_sender = use_fake_sender;
    }

    public String getAsk_for_ack() {
	return ask_for_ack;
    }

    public void setAsk_for_ack(String ask_for_ack) {
	this.ask_for_ack = ask_for_ack;
    }

    public String getEnable_sms_web() {
	return enable_sms_web;
    }

    public void setEnable_sms_web(String enable_sms_web) {
	this.enable_sms_web = enable_sms_web;
    }

    public String getEnable_sms_ws() {
	return enable_sms_ws;
    }

    public void setEnable_sms_ws(String enable_sms_ws) {
	this.enable_sms_ws = enable_sms_ws;
    }

    public String getEnable_sms_smpp() {
	return enable_sms_smpp;
    }

    public void setEnable_sms_smpp(String enable_sms_smpp) {
	this.enable_sms_smpp = enable_sms_smpp;
    }

    public String getEnable_prov_web() {
	return enable_prov_web;
    }

    public void setEnable_prov_web(String enable_prov_web) {
	this.enable_prov_web = enable_prov_web;
    }

    public String getEnable_prov_ws() {
	return enable_prov_ws;
    }

    public void setEnable_prov_ws(String enable_prov_ws) {
	this.enable_prov_ws = enable_prov_ws;
    }

    public String getAllowed_fake_senders() {
	return allowed_fake_senders;
    }

    public void setAllowed_fake_senders(String allowed_fake_senders) {
	this.allowed_fake_senders = allowed_fake_senders;
    }

    public String getBill_ack() {
	return bill_ack;
    }

    public void setBill_ack(String bill_ack) {
	this.bill_ack = bill_ack;
    }

    public String getCountry_name() {
	return country_name;
    }

    public void setCountry_name(String country_name) {
	this.country_name = country_name;
    }
}
