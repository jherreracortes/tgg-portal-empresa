package cl.mobid.wsa.telcel.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cl.mobid.wsa.telcel.bean.Company;
import cl.mobid.wsa.telcel.bean.Service;
import cl.mobid.wsa.telcel.bean.TrafficDateOut;
import cl.mobid.wsa.telcel.bean.TrafficTotalOut;
import cl.mobid.wsa.telcel.bean.WhiteList;
import cl.mobid.wsa.telcel.tgg.mapper.TggMapper;

@org.springframework.stereotype.Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class TggService {

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    @Autowired
    private TggMapper tggMapper;

    public Company getCompany(int idCompany) {
	return tggMapper.selectCompany(idCompany);
    }

    public String getTranslation(int idLanguage, String searchWord) {
	return tggMapper.selectWord(idLanguage, searchWord);
    }

    public List<Service> getServices(int idCompany, int searchCountry) {
	return tggMapper.selectServices(idCompany, searchCountry);
    }

    public Service getServiceById(int idCompany, int idService) {
	return tggMapper.selectServiceById(idCompany, idService);
    }

    public int countServiceRestricted(int idService) {
	return tggMapper.countServiceRestricted(idService);
    }

    public List<String> getServiceRestricted(int limit, int offset,
	    int idService) {
	return tggMapper.selectServiceRestricted(limit, offset, idService);
    }

    public int countWhiteLists(int idCompany) {
	return tggMapper.countWhiteLists(idCompany);
    }

    public List<WhiteList> getWhiteLists(int limit, int offset, int idCompany) {
	return tggMapper.selectWhiteLists(limit, offset, idCompany);
    }

    public List<Service> getWhiteListById(int idCompany, int idList) {
	return tggMapper.selectWhiteListById(idCompany, idList);
    }

    public int countBlackLists(int idCompany) {
	return tggMapper.countBlackLists(idCompany);
    }

    public List<Service> getBlackLists(int limit, int offset, int idCompany) {
	return tggMapper.selectBlackLists(limit, offset, idCompany);
    }

    public List<TrafficDateOut> getTrafficDate(String startDate,
	    String endDate, Integer idCountry, Integer idCompany,
	    Integer idService, String channel, String type, Integer language) {
	try {
	    return tggMapper.selectDateTraffic(formatter.parse(startDate),
		    formatter.parse(endDate), idCountry, idCompany, idService,
		    channel, type, language);
	} catch (ParseException e) {
	    return new ArrayList<TrafficDateOut>();
	}
    }

    public List<TrafficTotalOut> getTrafficTotal(String startDate,
	    String endDate, Integer idCountry, Integer idCompany,
	    Integer idService, Integer language) {
	try {
	    return tggMapper.selectTotalTraffic(formatter.parse(startDate),
		    formatter.parse(endDate), idCountry, idCompany, idService,
		    language);
	} catch (ParseException e) {
	    return new ArrayList<TrafficTotalOut>();
	}
    }

    public int validateWhiteList(String msisdn, int idService) {
	return tggMapper.validateWhiteList(msisdn, idService);
    }

    public HashMap<String, Integer> validateBlackList(String msisdn,
	    int idService) {
	HashMap<String, Integer> response = new HashMap<String, Integer>();

	response.put("validate_blists",
		tggMapper.validateBlackList(msisdn, idService));
	response.put("validate_blists_service",
		tggMapper.validateBlackListService(msisdn, idService));

	return response;
    }
}
