package cl.mobid.wsa.telcel.oauth;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

public class CustomTokenEnhancer implements TokenEnhancer {

    private List<TokenEnhancer> delegates = Collections.emptyList();

    public void setTokenEnhancers(List<TokenEnhancer> delegates) {
	this.delegates = delegates;
    }

//    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken,
	    OAuth2Authentication authentication) {

	DefaultOAuth2AccessToken tempResult = (DefaultOAuth2AccessToken) accessToken;
	OauthUser oauthUser = (OauthUser) authentication.getPrincipal();

	final Map<String, Object> additionalInformation = new HashMap<String, Object>();

	additionalInformation.put("id_user", oauthUser.getId_user());
	additionalInformation.put("full_name", oauthUser.getFull_name());
	additionalInformation.put("type", oauthUser.getCountry().getType());
	additionalInformation.put("id_country", oauthUser.getCountry()
		.getId_country());
	additionalInformation.put("language", oauthUser.getLanguage());
	additionalInformation.put("code", oauthUser.getCountry()
		.getCountry_shortname());
	additionalInformation.put("suspended", oauthUser.getCountry()
		.getCompany_status().toLowerCase().equals("suspended"));

	tempResult.setAdditionalInformation(additionalInformation);

	OAuth2AccessToken result = tempResult;
	for (TokenEnhancer enhancer : delegates) {
	    result = enhancer.enhance(result, authentication);
	}

	return result;
    }
}
