package cl.mobid.wsa.telcel.oauth;

import java.util.Collection;
import java.util.List;
import java.util.Collections;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import cl.mobid.wsa.telcel.bean.Country;
import cl.mobid.wsa.telcel.bean.User;

public class OauthUser implements UserDetails {

    private static final long serialVersionUID = 1L;

    private static final List<? extends GrantedAuthority> DEFAULT_AUTHORITIES = Collections
	    .singletonList(new SimpleGrantedAuthority("ROLE_USER"));

    private int id_user;
    private int id_company;
    private String full_name;
    private String username;
    private String password;
    private String language;
    private int enabled;
    private Country country;

    private boolean accountNonLocked = true;
    private boolean accountNonExpired = true;
    private boolean credentialsNonExpired = true;

    public OauthUser(User user) {
	setId_user(user.getId_user());
	setId_company(user.getId_company());
	setFull_name(user.getFull_name());
	setUsername(user.getUsername());
	setPassword(user.getPassword());
	setLanguage(user.getLanguage());
	setEnabled(user.getEnabled());
    }

    public int getId_user() {
	return id_user;
    }

    public void setId_user(int id_user) {
	this.id_user = id_user;
    }

    public int getId_company() {
	return id_company;
    }

    public void setId_company(int id_company) {
	this.id_company = id_company;
    }

    public String getFull_name() {
	return full_name;
    }

    public void setFull_name(String full_name) {
	this.full_name = full_name;
    }

    public String getUsername() {
	return username;
    }

    public void setUsername(String username) {
	this.username = username;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getLanguage() {
	return language;
    }

    public void setLanguage(String language) {
	this.language = language;
    }

    public int getEnabled() {
	return enabled;
    }

    public void setEnabled(int enabled) {
	this.enabled = enabled;
    }

    public boolean getAccountNonLocked() {
	return accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
	this.accountNonLocked = accountNonLocked;
    }

    public boolean getAccountNonExpired() {
	return accountNonExpired;
    }

    public void setAccountNonExpired(boolean accountNonExpired) {
	this.accountNonExpired = accountNonExpired;
    }          

    public boolean getCredentialsNonExpired() {
	return credentialsNonExpired;
    }

    public void setCredentialsNonExpired(boolean credentialsNonExpired) {
	this.credentialsNonExpired = credentialsNonExpired;
    }

    public Country getCountry() {
	return country;
    }

    public void setCountry(Country country) {
	this.country = country;
    }

//    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
	return DEFAULT_AUTHORITIES;
    }

//    @Override
    public boolean isAccountNonExpired() {
	return getAccountNonLocked();
    }

//    @Override
    public boolean isAccountNonLocked() {
	return getAccountNonLocked();
    }

//    @Override
    public boolean isCredentialsNonExpired() {
	return getCredentialsNonExpired();
    }

//    @Override
    public boolean isEnabled() {
	return getEnabled() == 1;
    }
}
