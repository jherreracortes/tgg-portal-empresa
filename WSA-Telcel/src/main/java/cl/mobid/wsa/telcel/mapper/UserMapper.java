package cl.mobid.wsa.telcel.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cl.mobid.wsa.telcel.bean.User;

public interface UserMapper {

    public int createUser(User user);

    public int createUserRole(@Param("idUser") int idUser,
	    @Param("idRole") int idRole);

    public int countUsers( @Param("idCompany") int idCompany,
	    @Param("searchName") String searchName);

    public List<User> selectUsers(@Param("limit") int limit,
	    @Param("offset") int offset, @Param("idUser") int idUser,
	    @Param("idCompany") int idCompany,
	    @Param("searchName") String searchName);

    public User selectByIdUser(int idUser);

    public User selectByUsername(String username);

    public String selectPassword(int idUser);

    public int hasPermission(@Param("idRole") int idRole,
	    @Param("title") String title);

    public int updateUser(User user);

    public int updateUserRole(@Param("idUser") int idUser,
	    @Param("idRole") int idRole);

    public int updateUserPassword(@Param("idUser") int idUser,
	    @Param("password") String password);

    public int setEnabled(@Param("idUser") int idUser,
	    @Param("modifiedBy") int modifiedBy, @Param("enabled") int enabled);

    public int callCreateUser(User user);
}
