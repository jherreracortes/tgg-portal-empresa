package cl.mobid.wsa.telcel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cl.mobid.wsa.telcel.bean.ConsolReport;
import cl.mobid.wsa.telcel.bean.ConsolidatedMo;
import cl.mobid.wsa.telcel.bean.DetailedMo;
import cl.mobid.wsa.telcel.bean.DetailedMt;
import cl.mobid.wsa.telcel.bean.FileMessage;
import cl.mobid.wsa.telcel.bean.GroupMessage;
import cl.mobid.wsa.telcel.bean.InputProcess;
import cl.mobid.wsa.telcel.bean.SingleMessage;
import cl.mobid.wsa.telcel.mapper.MessageMapper;

@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class MessageService {

    @Autowired
    private MessageMapper messageMapper;

    public int createInputProcess(InputProcess process) {
	return messageMapper.createInputProcess(process);
    }

    public int createSingleMessage(SingleMessage message) {
	return messageMapper.createSingleMessage(message);
    }

    public int createGroupMessage(InputProcess process, GroupMessage message) {
	return messageMapper.createGroupMessage(process, message);
    }

    public int createFileMessage(InputProcess process, FileMessage message,
	    String stage) {
	return messageMapper.createFileMessage(process, message, stage);
    }

    public List<ConsolReport> getConsolidatedReport(int idCompany,
	    String timeGMT, String startDate, String endDate,
	    String searchMsisdn, String searchSender, String searchStage,
	    String searchType, int searchCCost, int searchUser, String order) {
	return messageMapper.consolidatedReport(idCompany, timeGMT, startDate,
		endDate, searchMsisdn, searchSender, searchStage, searchType,
		searchCCost, searchUser, order);
    }

    public void getDetailedReport(int idCompany, String timeGMT,
	    String fileName, String startDate, String endDate,
	    String searchMsisdn, String searchSender, String searchStage,
	    String searchType, int searchCCost, int searchUser, String order, String language) {
	messageMapper.detailedReport(idCompany, timeGMT, fileName, startDate,
		endDate, searchMsisdn, searchSender, searchStage, searchType,
		searchCCost, searchUser, order, language);
    }

    public List<DetailedMt> getDetailedReportLimit(int idCompany,
	    String timeGMT, String startDate, String endDate,
	    String searchMsisdn, String searchSender, String searchStage,
	    String searchType, int searchCCost, int searchUser, String order, String language) {
	return messageMapper.detailedReportLimit(idCompany, timeGMT, startDate,
		endDate, searchMsisdn, searchSender, searchStage, searchType,
		searchCCost, searchUser, order, language);
    }

    public List<ConsolidatedMo> getConsolidatedMo(int idCompany,
	    String timeGMT, String startDate, String endDate,
	    String searchMsisdn, String searchSender, String searchStage,
	    String order) {
	return messageMapper.consolidatedMo(idCompany, timeGMT, startDate,
		endDate, searchMsisdn, searchSender, searchStage, order);
    }

    public void getDetailedMo(int idCompany, String timeGMT, String fileName,
	    String startDate, String endDate, String searchMsisdn,
	    String searchSender, String searchStage, String order,
	    String language) {
	messageMapper.detailedMo(idCompany, timeGMT, fileName, startDate,
		endDate, searchMsisdn, searchSender, searchStage, order,
		language);
    }

    public List<DetailedMo> getDetailedMoLimit(int idCompany, String timeGMT,
	    String startDate, String endDate, String searchMsisdn,
	    String searchSender, String searchStage, String order) {
	return messageMapper.detailedMoLimit(idCompany, timeGMT, startDate,
		endDate, searchMsisdn, searchSender, searchStage, order);
    }

    public String convertDispatchTime(int idCountry, String dispatchTime) {
	return messageMapper.convertDispatchTime(idCountry, dispatchTime);
    }

    public String getGMT(int idCountry) {
	return messageMapper.selectGMT(idCountry);
    }
}
