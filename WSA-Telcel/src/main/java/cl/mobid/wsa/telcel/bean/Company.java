package cl.mobid.wsa.telcel.bean;

import java.io.Serializable;

//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.annotation.JsonInclude.Include;

//@JsonInclude(Include.NON_EMPTY)
public class Company implements Serializable {

    private static final long serialVersionUID = 1L;

    private int id_company;
    private String name;
    private String company_status;
    private String created_time;
    private String mod_time;
    private String type;
    private String allow_exceed;
    private String id_facturacion;
    private int bill_cycle;
    private int msg_limit;
    private String ctrl_msg_limit;
    private String create_user;
    private String mod_user;
    private String country_name;
    private int operator_query;

    public int getId_company() {
	return id_company;
    }

    public void setId_company(int id_company) {
	this.id_company = id_company;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getCompany_status() {
	return company_status;
    }

    public void setCompany_status(String company_status) {
	this.company_status = company_status;
    }

    public String getCreated_time() {
	return created_time;
    }

    public void setCreated_time(String created_time) {
	this.created_time = created_time;
    }

    public String getMod_time() {
	return mod_time;
    }

    public void setMod_time(String mod_time) {
	this.mod_time = mod_time;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public String getAllow_exceed() {
	return allow_exceed;
    }

    public void setAllow_exceed(String allow_exceed) {
	this.allow_exceed = allow_exceed;
    }

    public String getId_facturacion() {
	return id_facturacion;
    }

    public void setId_facturacion(String id_facturacion) {
	this.id_facturacion = id_facturacion;
    }

    public int getBill_cycle() {
	return bill_cycle;
    }

    public void setBill_cycle(int bill_cycle) {
	this.bill_cycle = bill_cycle;
    }

    public int getMsg_limit() {
	return msg_limit;
    }

    public void setMsg_limit(int msg_limit) {
	this.msg_limit = msg_limit;
    }

    public String getCtrl_msg_limit() {
	return ctrl_msg_limit;
    }

    public void setCtrl_msg_limit(String ctrl_msg_limit) {
	this.ctrl_msg_limit = ctrl_msg_limit;
    }

    public String getCreate_user() {
	return create_user;
    }

    public void setCreate_user(String create_user) {
	this.create_user = create_user;
    }

    public String getMod_user() {
	return mod_user;
    }

    public void setMod_user(String mod_user) {
	this.mod_user = mod_user;
    }

    public String getCountry_name() {
	return country_name;
    }

    public void setCountry_name(String country_name) {
	this.country_name = country_name;
    }

    public int getOperator_query() {
	return operator_query;
    }

    public void setOperator_query(int operator_query) {
	this.operator_query = operator_query;
    }

}
