package cl.mobid.wsa.telcel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;

import cl.mobid.wsa.telcel.bean.User;
import cl.mobid.wsa.telcel.oauth.OauthUser;
import cl.mobid.wsa.telcel.service.UserService;
import cl.mobid.wsa.telcel.tgg.mapper.TggMapper;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    TggMapper tggMapper;

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createUser(@RequestBody User user) {

	HttpStatus status;
	String message;

	Authentication auth = SecurityContextHolder.getContext()
		.getAuthentication();
	OauthUser oauthUser = (OauthUser) auth.getPrincipal();

	if (user.getPassword() != null) {
	    user.setPassword(passwordEncoder.encode(user.getPassword()));
	}

	if (user.getId_company() == 0) {
	    user.setId_company(oauthUser.getId_company());
	}

	user.setModified_by(oauthUser.getId_user());

	if (userWasUpdated(user)) {
	    userService.updateUserRole(user);

	    status = HttpStatus.OK;
	    message = "user was updated";
	} else if (userCanBeCreated(user)) {
	    user.setId_country((tggMapper.selectCountry(user.getId_company()).getId_country()));
	    if (userService.createrUser(user) == 1) {
		userService.createUserRole(user);
	    }

	    status = HttpStatus.CREATED;
	    message = "user was created";

	    /*
	     * user.setModified_by(oauthUser.getId_user());
	     * user.setId_company(oauthUser.getId_company()); int result =
	     * userService.callCreateUser(user); System.out.println("resultado"
	     * + result); if (result == 1) { status = HttpStatus.OK; message =
	     * "user was updated";
	     */
	} else {
	    status = HttpStatus.NOT_ACCEPTABLE;
	    message = "The information provided is not sufficient to perform either an UPDATE or an INSERT";
	}

	return new ResponseEntity<String>(message, status);
    }

    @RequestMapping(value = "/admin", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createAdmin(@RequestBody User user) {
	if (userCanBeCreated(user)) {
	    user.setPassword(passwordEncoder.encode(user.getPassword()));
	    if (userService.createrUser(user) == 1) {
		userService.createUserRole(user);
		return new ResponseEntity<String>("user was created",
			HttpStatus.OK);
	    }
	}

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getUsers(
	    @RequestParam(value = "offset", defaultValue = "-1") int offset,
	    @RequestParam(value = "limit", defaultValue = "-1") int limit,
	    @RequestParam(value = "searchname", required = false) String searchName,
	    @RequestParam(value = "searchcompany", required = false, defaultValue = "-1") int searchCompany) {

	Authentication auth = SecurityContextHolder.getContext()
		.getAuthentication();
	OauthUser oauthUser = (OauthUser) auth.getPrincipal();

	if (offset == -1 && limit == -1) {
	    return String.valueOf(userService.countUsers(oauthUser, searchName, searchCompany));
	}

	return userService.selectUsers(limit, offset, oauthUser, searchName,
		searchCompany);
    }

    @RequestMapping(value = "/{idUser}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public User getUserById(@PathVariable int idUser) {
	return userService.selectUserByIdUser(idUser);
    }

    @RequestMapping(value = "/permission/{title}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public String checkPermission(@PathVariable String title) {

	Authentication auth = SecurityContextHolder.getContext()
		.getAuthentication();
	OauthUser oauthUser = (OauthUser) auth.getPrincipal();

	return String.valueOf(userService.hasPermission(oauthUser.getId_user(),
		title));
    }

    @RequestMapping(value = "/pass_change", method = RequestMethod.PUT, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> updatePassword(@RequestBody ObjectNode user) {
	OauthUser oauthUser = (OauthUser) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal();

	int idUser = user.get("id_user").asInt();
	String oldPassword = user.get("old_password").asText().trim();
	String newPassword = user.get("new_password").asText().trim();

	String password = userService.selectPasswordByIdUser(idUser);

	if (passwordEncoder.matches(oldPassword, password)
		&& idUser == oauthUser.getId_user()) {
	    if (userService.updateUserPassword(idUser,
		    passwordEncoder.encode(newPassword)) == 1) {
		return new ResponseEntity<String>("change password success",
			HttpStatus.OK);
	    }
	} else {
	    return new ResponseEntity<String>("change password failed",
		    HttpStatus.NOT_ACCEPTABLE);
	}
	/*
	 * if (userService.updateUserPassword(user) == 1) { return new
	 * ResponseEntity<String>("password changed", HttpStatus.OK); }
	 */

	return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @RequestMapping(method = RequestMethod.DELETE, produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> setEnabled(
	    @RequestParam(value = "id_user") int idUser,
	    @RequestParam(value = "enabled") int enabled) {
	HttpStatus status;
	String message = "";

	Authentication auth = SecurityContextHolder.getContext()
		.getAuthentication();
	OauthUser oauthUser = (OauthUser) auth.getPrincipal();

	if (userWasDisabled(idUser, oauthUser.getId_user(), enabled)) {
	    status = HttpStatus.OK;
	    message = "enabled was updated";
	} else {
	    status = HttpStatus.NOT_FOUND;
	}

	return new ResponseEntity<String>(message, status);
    }

    private boolean userCanBeCreated(User user) {
	return (user.getUsername() != null && user.getPassword() != null);
    }

    private boolean userWasUpdated(User user) {
	return userService.updateUser(user) == 1;
    }

    private boolean userWasDisabled(int idUser, int modifiedBy, int enabled) {
	return userService.setEnabled(idUser, modifiedBy, enabled) == 1;
    }
}
