package cl.mobid.main;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class MainChecker {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		toLog("INICIA CHEQUEO");
		DBAccess db = new DBAccess("wsa_telcel_la");
		ArrayList<HashMap<String, String>> al = db.getUserCompany();
		if (al != null) {
			toLog("SE OBTUBO " + al.size() + " COMPANYS");
			for (HashMap<String, String> hm : al) {
				String company = hm.get("id_company");
				String country = hm.get("id_country");

				if (!db.checkService(company)) {
					toLog("NO HAY SERVICIO PARA LA COMPANY " + company);
					String zona = db.getZone(country);
					toLog("LA COMPANY TIENE COMO ZONA " + zona);
					int re = db.crearService(company, zona);
					toLog("SE INSERTA NUEVO SERVICE ACK ID " + re);

				} else {
					toLog("YA TIENE SERVICE ACK COMPANY " + company);
				}

			}
		} else {
			toLog("NO HAY COMPANYS QUE REVISAR");

		}

	}
	public static synchronized void toLog(String mess) {

		try {
			String timestamp = fecha_log(false);
			String eol = System.getProperty("line.separator");
			String date = fecha_log(true);
			FileWriter out = new FileWriter(
					"/home/appmobid/Apps/Logs/fileprocess" + "." + date
							+ ".log", true);

			out.write("[" + timestamp + "] " + mess + eol);
			out.close();
			System.out.println("[" + timestamp + "] " + mess);
		} catch (Exception e) {
			toLog("toLog ERROR: " + e.getMessage());

		}
	}

	public static String fecha_log(boolean d) {
		Date date = new Date();
		String finalDate = new String();
		Calendar calendario = java.util.Calendar.getInstance();
		calendario.setTime(date);
		calendario.add(java.util.Calendar.DATE, 0);
		String formatFecha;
		if (d) {
			formatFecha = "yyyyMMdd";
		} else {
			formatFecha = "yyyy-MM-dd HH:mm:ss.SSS";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formatFecha);
		finalDate = sdf.format(calendario.getTime());

		return finalDate;
	}
}
