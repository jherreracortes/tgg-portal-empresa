package cl.mobid.main;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;

public class DBAccess {

	static BoneCP connectionPool = null;

	public DBAccess(String DB) {

		try {
			// load the database driver (make sure this is in your classpath!)
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		try {
			// setup the connection pool
			BoneCPConfig config = new BoneCPConfig();
			config.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/" + DB);
			config.setUsername("appmobid");
			config.setPassword("mobid123");
			//config.setJdbcUrl("jdbc:mysql://192.168.1.15:3306/" + DB);
			//config.setUsername("admtiaxa");
			//config.setPassword("tiaxa");
			config.setMinConnectionsPerPartition(5);
			config.setMaxConnectionsPerPartition(10);
			config.setPartitionCount(1);
			connectionPool = new BoneCP(config); // setup the connection pool

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private static ArrayList<HashMap<String, String>> getMysqlResponse(
			String sql, boolean data) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
		try {
			con = connectionPool.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			if (!data) {
				while (rs.next()) {
					HashMap<String, String> hm = new HashMap<String, String>();

					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						String colname = rsmd.getColumnName(i);
						hm.put(colname, rs.getString(colname));
					};
					arrayList.add(hm);
				}

			} else {
				HashMap<String, String> hm = new HashMap<String, String>();

				while (rs.next()) {

					hm.put(rs.getString(rsmd.getColumnName(2)),
							rs.getString(rsmd.getColumnName(1)));
				}
				arrayList.add(hm);
			}
		} catch (SQLException e) {

			e.printStackTrace();

			return null;

		} finally {
			try {
				if (rs != null) {
					rs.close();
				};
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();

				return null;
			};
		};
		return arrayList;
	};

	private int exec(String sql) {
		Connection con = null;
		Statement stmt = null;
		int i=0;
		try {
			MainChecker.toLog("SQL "+sql);
			con = connectionPool.getConnection();
			stmt = con.createStatement();
			i=stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}

			} catch (SQLException e) {
				e.printStackTrace();

			};
		};
		return i;
	};

	public ArrayList<HashMap<String, String>> getUserCompany() {
		ArrayList<HashMap<String, String>> bal = getMysqlResponse(
				"SELECT id_company , id_country FROM rbac_user group by id_company;",
				false);
		if (bal.size() > 1) {
			return bal;
		} else {
			return null;

		}

	}
	
	public boolean checkService(String company) {
		ArrayList<HashMap<String, String>> bal = getMysqlResponse("SELECT * FROM serviceACK WHERE id_company="+company+";",
				false);
		if (bal.size() == 1) {
			return true;
		} else {
			return false;

		}

	}
	
	public int crearService(String company, String zona) {
		return exec("INSERT INTO serviceACK (id_company, zona , stage) VALUES ("+company+", '"+zona+"', 'MORE');");

	}
	
	public String getZone(String country) {
		ArrayList<HashMap<String, String>> bal = getMysqlResponse(
				"SELECT * FROM tgg_region where id_country="+country+";",
				false);
		if (bal.size() == 1) {
			HashMap<String, String>hm=	bal.get(0);
			
			return hm.get("name");
		} else {
			return null;

		}

	}
}
