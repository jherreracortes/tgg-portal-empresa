package cl.mobid.Apps;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sql.DataSource;

public class DBAccess {

	public DBAccess() {
	}

	private static ArrayList<HashMap<String, String>> getMysqlResponse(String sql, boolean data) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
		try {
			PoolConexionConfig pool =  PoolConexionConfig.getInstancia(MainGlobal.DATA_BASE);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			if (!data) {
				while (rs.next()) {
					HashMap<String, String> hm = new HashMap<String, String>();

					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						String colname = rsmd.getColumnName(i);
						hm.put(colname, rs.getString(colname));
					}
					arrayList.add(hm);
				}
			} else {
				HashMap<String, String> hm = new HashMap<String, String>();

				while (rs.next()) {
					hm.put(rs.getString(rsmd.getColumnName(2)), rs.getString(rsmd.getColumnName(1)));
				}
				arrayList.add(hm);
			}
		} catch (SQLException e) {
			StringBuilder sb = null;
			sb = MainGlobal.stackTraceToString(e);
			MainGlobal.toLog("ERROR: "+sb.toString());
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = null;
				sb = MainGlobal.stackTraceToString(e);
				MainGlobal.toLog("ERROR: "+sb.toString());

				return null;
			}
		}
		return arrayList;
	}

	private static void exec(String sql) {
		Connection con = null;
		Statement stmt = null;
		try {
			PoolConexionConfig pool =  PoolConexionConfig.getInstancia(MainGlobal.DATA_BASE);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			stmt.execute(sql);
		} catch (SQLException e) {
			StringBuilder sb = null;
			sb = MainGlobal.stackTraceToString(e);
			MainGlobal.toLog("ERROR: "+sb.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = null;
				sb = MainGlobal.stackTraceToString(e);
				MainGlobal.toLog("ERROR: "+sb.toString());
			}
		}
	}

	public static HashMap<String, String> getDataFile() {
		ArrayList<HashMap<String, String>> bal = getMysqlResponse(
				"SELECT  * FROM file_process WHERE stage='GLOBAL' AND (dispatch_time < now() OR dispatch_time is NULL) ORDER BY created_time DESC LIMIT 1;",
				false);
		if (bal.size() == 1) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}

	public static String getDelivery(String sender) {
		String sql = "Select delivery FROM services WHERE sender='" + sender + "';";
		ArrayList<HashMap<String, String>> resp = getMysqlResponse(sql, false);
		String respuesta = "0";
		for (int i = 0; i < resp.size(); i++) {
			HashMap<String, String> hm = (HashMap<String, String>) resp.get(i);

			respuesta = (String) hm.get("delivery");
		}

		return respuesta;
	}

	public static void updateprocess(String id, String table, String s) {
		String sql = "UPDATE " + table + " SET stage = '" + s + "' WHERE id_process=" + id;
		//MainGlobal.toLog("[" + MainGlobal.fecha_log(false) + "] SQL  " + sql);
		exec(sql);
	}

	public static void uploadMessage(GetData g, DataFile f) {
		String id_process = g.getId_process();
		String id_pais = "0";
		String stage = g.getStage();
		String delivery_receipt = g.getDelivery_receipt();
		String data_coding = g.getData_coding();
		String id_company = g.getId_company();
		String sender = f.getSender();
		String texto = f.getTexto();
		String numero = f.getMsisdn();
		String agendado = g.getDispatch_time();

		Connection con = null;
		CallableStatement cs = null;
		try {
			String sqlStore;
			if (agendado.length() > 0) {
				sqlStore =

						"{call createMessage_global('" + numero + "', '" + sender + "', '" + stage + "', '"
								+ Integer.parseInt(delivery_receipt) + "', '" + data_coding + "', '"
								+ Integer.parseInt(id_company) + "', '" + Integer.parseInt(id_pais) + "', '"
								+ Integer.parseInt(id_process) + "', '" + agendado + "', '" + texto + "','"
								+ g.getResponse_code() + "', '" + g.getResponse_text() + "')}";
			} else {
				sqlStore =

						"{call createMessage_global('" + numero + "', '" + sender + "', '" + stage + "', '"
								+ Integer.parseInt(delivery_receipt) + "', '" + data_coding + "', '"
								+ Integer.parseInt(id_company) + "', '" + Integer.parseInt(id_pais) + "', '"
								+ Integer.parseInt(id_process) + "', null, '" + texto + "','" + g.getResponse_code()
								+ "', '" + g.getResponse_text() + "')}";
			}

			PoolConexionConfig pool =  PoolConexionConfig.getInstancia(MainGlobal.DATA_BASE);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			cs = con.prepareCall(sqlStore);
			cs.execute();
		} catch (SQLException e) {
			StringBuilder sb = null;
			sb = MainGlobal.stackTraceToString(e);
			MainGlobal.toLog("ERROR: "+sb.toString());
			
		} finally {
			try {
				if (con != null) {
					con.close();
				}
				if (cs != null) {
					cs.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = null;
				sb = MainGlobal.stackTraceToString(e);
				MainGlobal.toLog("ERROR: "+sb.toString());
			}
		}
	}

	public static ArrayList<HashMap<String, String>> getPaisData() {
		String sql = "SELECT * FROM tgg_country WHERE id_country > 0";

		ArrayList<HashMap<String, String>> resp = getMysqlResponse(sql, false);

		return resp;
	}

	public static String getTime0(String id_country, String dispatch_time) {
		String f = "select convertDispatchTime(" + id_country + ",'" + dispatch_time + "') as text ";
		ArrayList<HashMap<String, String>> resp = getMysqlResponse(f, false);
		HashMap<String, String> hm = (HashMap<String, String>) resp.get(0);
		return (String) hm.get("text");
	}

}
