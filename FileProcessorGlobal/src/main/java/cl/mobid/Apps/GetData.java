package cl.mobid.Apps;

import java.util.HashMap;

public class GetData {

	private String id;
	private String id_process;
	private String id_pais;
	private String stage;
	private String ruta_archivo;
	private String file;
	private String ip_server;
	private String ts;
	private boolean data;
	private String delivery_receipt;
	private String data_coding;
	private String id_company;
	private String sender;
	private String response_code;
	private String response_text;
	private String dispatch_time;

	public GetData() {
		HashMap<String, String> hm = DBAccess.getDataFile();

		if (hm != null) {
			id = (String) hm.get("id_process");
			id_process = (String) hm.get("input_process");
			id_pais = (String) hm.get("id_country");
			stage = (String) hm.get("stage");
			ruta_archivo = (String) hm.get("file_path");
			file = (String) hm.get("file_name");
			ip_server = (String) hm.get("ip_server");
			ts = (String) hm.get("created_time");
			delivery_receipt = (String) hm.get("delivery_receipt");
			data_coding = (String) hm.get("data_coding");
			id_company = (String) hm.get("id_company");
			sender = (String) hm.get("sender");
			dispatch_time = (String) hm.get("dispatch_time");
			response_code = null;
			data = true;
		} else {
			data = false;
		}
	}

	public String getDispatch_time() {
		return dispatch_time;
	}

	public void setDispatch_time(String dispatch_time) {
		this.dispatch_time = dispatch_time;
	}

	public String getResponse_code() {
		return response_code;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getId() {
		return id;
	}

	public String getId_process() {
		return id_process;
	}

	public String getId_pais() {
		return id_pais;
	}

	public String getStage() {
		return stage;
	}

	public String getRuta_archivo() {
		return ruta_archivo;
	}

	public String getIp_server() {
		return ip_server;
	}

	public String getTs() {
		return ts;
	}

	public boolean isData() {
		return data;
	}

	public String getDelivery_receipt() {
		return delivery_receipt;
	}

	public void setDelivery_receipt(String delivery_receipt) {
		this.delivery_receipt = delivery_receipt;
	}

	public String getData_coding() {
		return data_coding;
	}

	public void setData_coding(String data_coding) {
		this.data_coding = data_coding;
	}

	public String getId_company() {
		return id_company;
	}

	public void setId_company(String id_company) {
		this.id_company = id_company;
	}

	public String getSender() {
		return sender;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setId_process(String id_process) {
		this.id_process = id_process;
	}

	public void setId_pais(String id_pais) {
		this.id_pais = id_pais;
	}

	public void setRuta_archivo(String ruta_archivo) {
		this.ruta_archivo = ruta_archivo;
	}

	public void setIp_server(String ip_server) {
		this.ip_server = ip_server;
	}

	public void setTs(String ts) {
		this.ts = ts;
	}

	public void setData(boolean data) {
		this.data = data;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public void setResponse_code(String response_code) {
		this.response_code = response_code;
	}

	public String getResponse_text() {
		return response_text;
	}

	public void setResponse_text(String response_text) {
		this.response_text = response_text;
	}
}
