package cl.mobid.Apps;

import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

public class MainGlobal {
	public static final String DATA_BASE="wsa_telcel_la";
	
	public MainGlobal() {
	}

	public static void main(String[] args) {
		int inter = Integer.parseInt(args[0]);
		try {
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				public void run() {
					MainGlobal.get();
					MainGlobal.toLog("Start Ciclo");
				}
			}, 0L, inter);
		} catch (Exception e) {
			StringBuilder sb = null;
			sb = stackTraceToString(e);
			toLog("ERROR: "+sb.toString());
		}
	}

	public static void get() {
		GetData g = new GetData();
		try {
			if (g.isData()) {
				DBAccess.updateprocess(g.getId(), "file_process", "PROCESSING");
				DataUpload.setDataFile(GetFile.isfile(g.getIp_server(), g.getRuta_archivo(), g.getFile()));
				if (DataUpload.data.size() > 0) {
					DataUpload.subir(g);
				} else {
					DBAccess.updateprocess(g.getId(), "file_process", "FAILED");
				}
			}
		} catch (Exception e) {
			StringBuilder sb = null;
			sb = stackTraceToString(e);
			toLog("ERROR: "+sb.toString());
		}
	}

	public static String fecha_log(boolean d) {
		Date date = new Date();
		String finalDate = new String();
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(date);
		calendario.add(5, 0);

		String formatFecha;
		if (d) {
			formatFecha = "yyyyMMdd";
		} else {
			formatFecha = "yyyy-MM-dd HH:mm:ss.SSS";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formatFecha);
		finalDate = sdf.format(calendario.getTime());

		return finalDate;
	}

	public static boolean validateMSISDN(String msisdn, String len) {
		boolean resp = false;

		if (msisdn.length() == Integer.parseInt(len)) {
			resp = true;
		} else {
			resp = false;
		}

		return resp;
	}

	public static String changeHour(String fecha, String time_offset) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String fecha_respuesta = null;

		Date d = new Date();
		try {
			d = df.parse(fecha);

			Calendar gc = new GregorianCalendar();
			gc.setTime(d);
			gc.add(10, Integer.parseInt(time_offset));
			Date d2 = gc.getTime();
			fecha_respuesta = df.format(d2);
		} catch (ParseException e) {
			StringBuilder sb = null;
			sb = stackTraceToString(e);
			toLog("ERROR: "+sb.toString());
		}

		return fecha_respuesta;
	}

	public static synchronized void toLog(String mess) {
		try {
			String timestamp = fecha_log(false);
			String eol = System.getProperty("line.separator");
			String date = fecha_log(true);
			FileWriter out = new FileWriter("/home/appmobid/Apps/Logs/fileprocessglobal." + date + ".log", true);

			out.write("[" + timestamp + "] " + mess + eol);
			out.close();
		} catch (Exception e) {
			// esto es por si falla escritura en log 
			e.getMessage();
			e.printStackTrace();
		}
	}
	
	public static StringBuilder stackTraceToString(Exception e) {
		StringBuilder sb = new StringBuilder();
		StackTraceElement[] stackTrace = e.getStackTrace();
		for (StackTraceElement element : stackTrace) {
			sb.append("ERROR: ");
			sb.append(element.toString());
			sb.append("\n");
		}
		return sb;
	}
}
