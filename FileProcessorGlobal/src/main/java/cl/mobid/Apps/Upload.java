package cl.mobid.Apps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Upload implements Runnable {

	GetData s = null;
	ArrayList<TGG_Country> tgg;

	public Upload(GetData s) {
		tgg = new ArrayList<TGG_Country>();

		ArrayList<HashMap<String, String>> d = DBAccess.getPaisData();
		for (HashMap<String, String> hm : d) {
			TGG_Country tc = new TGG_Country();
			tc.setId_country((String) hm.get("id_country"));
			tc.setLargo((String) hm.get("length"));
			tc.setTime_gmt((String) hm.get("time_gmt"));
			tc.setName((String) hm.get("name"));
			tc.setCode((String) hm.get("code"));
			tc.setTime_offset((String) hm.get("time_offset"));
			if ((((String) hm.get("prefix")).length() == 0) || (hm.get("prefix") == null)) {
				tc.setPrefix("NO");
			} else {
				tc.setPrefix((String) hm.get("prefix"));
			}
			tgg.add(tc);
		}
		this.s = s;
	}

	@Override
	public void run() {
		try {
			MainGlobal.toLog("[" + MainGlobal.fecha_log(false) + "] EMPIEZA EL THREAD");

			ArrayList<DataFile> data = new ArrayList<DataFile>();

			data = DataUpload.data;
			Iterator<DataFile> iterator = data.iterator();
			while (iterator.hasNext()) {
				DataFile dataFile = (DataFile) iterator.next();

				TGG_Country tg = null;

				for (int i = 0; i < tgg.size(); i++) {
					tg = (TGG_Country) tgg.get(i);
					if (dataFile.getMsisdn().startsWith(tg.getPrefix())) {
						MainGlobal.toLog("Id_pais " + tg.getId_country() + " SENDER " + dataFile.getSender());
						s.setId_pais(tg.getId_country());
						i = tgg.size();
					}
				}

				if ((s.getDispatch_time() == null) || (s.getDispatch_time().length() <= 0)) {
					s.setDispatch_time("");
				}
				if (!tg.getLargo().equals( String.valueOf( dataFile.getMsisdn().length() ))) {
					MainGlobal.toLog("FAIL 400 " + dataFile.getMsisdn().length());
					s.setStage("NOUPLOAD");
					s.setResponse_code("400");
					s.setResponse_text("Numero incorrecto");
				} else if ((dataFile.getTexto().length() < 0) && (dataFile.getTexto().length() > 161)) {
					s.setStage("NOUPLOAD");
					s.setResponse_code("401");
					s.setResponse_text("Texto incorrecto");
				} else {
					s.setDelivery_receipt(DBAccess.getDelivery(dataFile.getSender()));
					MainGlobal.toLog("CORRECTO");
					s.setStage("QUEUED");
					s.setResponse_code("200");
				}
				DBAccess.uploadMessage(s, dataFile);
			}
			DBAccess.updateprocess(s.getId(), "file_process", "FINISH");
			MainGlobal.toLog("[" + MainGlobal.fecha_log(false) + "] TERMINA EL THREAD");
		} catch (Exception e) {
			StringBuilder sb = null;
			sb = MainGlobal.stackTraceToString(e);
			MainGlobal.toLog("ERROR: "+sb.toString());
		}
	}

}