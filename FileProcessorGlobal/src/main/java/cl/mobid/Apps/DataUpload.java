package cl.mobid.Apps;

import java.util.ArrayList;

public class DataUpload {
	public static ArrayList<DataFile> data;

	public DataUpload() {
	}

	public static void setDataFile(ArrayList<DataFile> df) {
		data = new ArrayList<DataFile>();
		data = df;
	}

	public static void subir(GetData g) {
		Upload r = new Upload(g);
		r.run();
	}

}
