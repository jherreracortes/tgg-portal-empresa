package cl.mobid.Apps;

import com.jcraft.jsch.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;

public class GetFile {

	public GetFile() {
	}

	public static ArrayList<DataFile> isfile(String ip, String ruta, String file) {
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader buffer = null;
		ArrayList<DataFile> data = new ArrayList<DataFile>();
		try {
			MainGlobal.toLog("[" + MainGlobal.fecha_log(false) + "] EMPIEZA CONEXION " + ip + " " + ruta + file);

			session = jsch.getSession("procfile", ip);

			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword("proc_tgg-file");

			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			MainGlobal.toLog("Is connected to IP:" + channel.isConnected());
			sftpChannel.cd(ruta);
			
			MainGlobal.toLog("revisando file: " + file);
			String[] fileArr = file.split("_");
			String charset = null;
			if (fileArr[0].toUpperCase().equals("UTF-8")) {
				charset = "UTF-8";
			} else if (fileArr[0].toUpperCase().equals("LATIN1")) {
				charset = "ISO-8859-1";
			} else if (fileArr[0].toUpperCase().equals("ASCII")) {
				charset = "ASCII";
			} else {
				charset = "otro";
			}
			if (!charset.equals("otro")) {
				InputStream is = sftpChannel.get(file);

				buffer = new BufferedReader(new InputStreamReader(is, charset));
				String getLine = ""; 

				while ((getLine = buffer.readLine()) != null) {
					// se transforma a encoding utf8 la linea 
					if (charset.equals("ASCII")) {
						byte[] latin1arr = getLine.getBytes(charset);
						byte[] utf8arr = new String(latin1arr, charset).getBytes("UTF-8");
						String textUtf8 = new String(utf8arr, "UTF-8");
						getLine = textUtf8;
					} else if (!charset.equals("UTF-8")) {
						byte[] latin1arr = getLine.getBytes(charset);
						byte[] utf8arr = new String(latin1arr, charset).getBytes("UTF-8");
						String textUtf8 = new String(utf8arr, "UTF-8");
						getLine = textUtf8;
					}
					DataFile df = new DataFile();
					String[] split = getLine.split(";",3);
					df.setMsisdn(split[0]);
					df.setSender(split[1]);
					df.setTexto(split[2]);
					data.add(df);
				}
				sftpChannel.exit();
				session.disconnect();

				MainGlobal.toLog("[" + MainGlobal.fecha_log(false) + "] Line: " + data.size());
			}else {
				MainGlobal.toLog("[" + MainGlobal.fecha_log(false) + "] Line: archivo sin encoding conocido");
			}
			
		} catch (Exception e) {
			StringBuilder sb = null;
			sb = MainGlobal.stackTraceToString(e);
			MainGlobal.toLog("ERROR: " + sb.toString());
		} finally {
			try {
				if (buffer != null)
					buffer.close();
			} catch (Exception e) {
				StringBuilder sb = null;
				sb = MainGlobal.stackTraceToString(e);
				MainGlobal.toLog("ERROR: " + sb.toString());
			}
		}
		MainGlobal.toLog("OUT");
		return data;
	}

}
