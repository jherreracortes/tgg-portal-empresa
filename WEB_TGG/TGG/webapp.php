<?php
setcookie("webapp", "tgg", time() + 3600, "/", "");
header('Content-Type: text/html; charset=utf-8');
?>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <link rel="stylesheet" type="text/css" href="css/style.css?t=<?= time() ?>" />
        <LINK REL="stylesheet" TYPE="text/css" href="css/timepicki.css"> 
        <link rel="stylesheet" type="text/css" href="menu/css/component.css" />
        <link rel="stylesheet" type="text/css" media="all" href="js/calendar/calendar-green.css" title="win2k-cold-1" />
        <link rel="stylesheet" type="text/css" href="css/jquery.alerts.css" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<style type="text/css">
			.visible{
				display: inline-block;
			}
			.hidden{
				display: none;
			}
		</style>
        <script type="text/javascript" charset="UTF-8" src="js/calendar/calendar.js"></script>
        <script type="text/javascript" charset="UTF-8" src="js/calendar/calendar-es.js"></script>
        <script type="text/javascript" charset="UTF-8" src="js/calendar/calendar-setup.js"></script>
        <script type="text/javascript" charset="UTF-8" src="jquery-1.11.3.min.js"></script>
        <script type="text/javascript" charset="UTF-8" src="js/funcsJS.js?t=<?= time() ?>"></script>
        <script type="text/javascript" charset="UTF-8" src="js/jquery.alerts.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script src="menu/js/modernizr.custom.js"></script>
        <?php
        include('modules.php');
        include('includes/utils.php');
        include('config.php');

        /*
          if(empty($_COOKIE['webapp']) || empty($_SESSION['id_user']))
          {
          //header('Location: index.php');
          }

          //echo $_COOKIE['webapp'];
         */

        $access_token = $_SESSION['access_token'];
        $url = URL_WS . "WSA-Telcel/api/tgg/company?access_token=" . $access_token;
        $companyService = parseUrl($url);
        $service = strtoupper($companyService['name']);

        $lan = $_SESSION['lang'];
        $lan_file = simplexml_load_file('language.xml');
        $inicio = $lan_file->$lan->menu[0]->inicio;
        $administracion = $lan_file->$lan->menu[0]->administracion;
        $adm_usuarios = $lan_file->$lan->menu[0]->adm_usuarios;
        $adm_lista_envios = $lan_file->$lan->menu[0]->adm_lista_envios;
        $adm_centro_costos = $lan_file->$lan->menu[0]->adm_centro_costos;
        $aprovisionamiento = $lan_file->$lan->menu[0]->aprovisionamiento;
        $aprov_lista_blanca = $lan_file->$lan->menu[0]->aprov_lista_blanca;
        $aprov_lista_negra = $lan_file->$lan->menu[0]->aprov_lista_negra;
        $aprov_rechazos = $lan_file->$lan->menu[0]->aprov_rechazos;
        $envio_mensajeria = $lan_file->$lan->menu[0]->envio_mensajeria;
        $envio_individual = $lan_file->$lan->menu[0]->envio_individual;
        $envio_lista = $lan_file->$lan->menu[0]->envio_lista;
        $envio_archivo = $lan_file->$lan->menu[0]->envio_archivo;
        $envio_agendados = $lan_file->$lan->menu[0]->envio_agendados;
        $consulta_operadora = $lan_file->$lan->menu[0]->consulta_operadora;
        $consulta_individual = $lan_file->$lan->menu[0]->consulta_individual;
        $consulta_lista = $lan_file->$lan->menu[0]->consulta_lista;
        $consulta_archivo = $lan_file->$lan->menu[0]->consulta_archivo;
        $consulta_bitacora = $lan_file->$lan->menu[0]->consulta_bitacora;
        $reportes = $lan_file->$lan->menu[0]->reportes;
        $reporte_enviados = $lan_file->$lan->menu[0]->reporte_enviados;
        $reporte_recibidos = $lan_file->$lan->menu[0]->reporte_recibidos;
        $reporte_archivo = $lan_file->$lan->menu[0]->reporte_archivo;
        $reporte_enviados_dia = $lan_file->$lan->menu[0]->reporte_enviados_dia;
        $reporte_enviados_empresa = $lan_file->$lan->menu[0]->reporte_enviados_empresa;
        $ayuda = $lan_file->$lan->menu[0]->ayuda;
        $contrasena = $lan_file->$lan->menu[0]->contrasena;
        $manual = $lan_file->$lan->menu[0]->manual;
        $faq = $lan_file->$lan->menu[0]->faq;
        $titulo = $lan_file->$lan->cabecera[0]->titulo;
        $sesion = $lan_file->$lan->cabecera[0]->sesion;
        ?>

        <title>TGG</title>
    </head>
    <body>
        <div id="global">
            <header>
                <div id="header-up">
                    <div id="logo">
                        <?= strtoupper($titulo); ?>
                    </div>
                    <div id="company">
                        <strong><?= $_SESSION['full_name']; ?></strong> | <a href="?module=logOut" class="logout"><?= $sesion; ?></a><div id="hora_seccion"></div> <br/>
                        <?= $service; ?>
                        
                    </div>
                 <div id="hora_seccion"></div>   
              </div>
                <div id="header-down">
                    <div id="menu">
                        <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
                            <div class="cbp-hsinner">
                                <ul class="cbp-hsmenu">
                                    <li>
                                        <a href="?module=company"><?= $inicio; ?></a>
                                    </li>
                                    <li>
                                        <a href="#"><?= $administracion; ?></a>
                                        <ul class="cbp-hssubmenu">
                                            <li><a href="?module=adminUser"><span><?= $adm_usuarios; ?></span></a></li>
                                            <li><a href="?module=adminGroupList&searchname=&field=name&enabled=on&order=ASC"><span><?= $adm_lista_envios; ?></span></a></li>
                                            <li><a href="?module=adminCostCenter"><span><?= $adm_centro_costos; ?></span></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><?= $aprovisionamiento; ?></a>
                                        <ul class="cbp-hssubmenu cbp-hssub-rows">
                                            <li><a href="?module=provWhiteList"><span><?= $aprov_lista_blanca; ?></span></a></li>
                                            <li><a href="?module=provBlackList"><span><?= $aprov_lista_negra; ?></span></a></li>
                                            <li><a href="?module=provRejected"><span><?= $aprov_rechazos; ?></span></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><?= $envio_mensajeria; ?></a>
                                        <ul class="cbp-hssubmenu cbp-hssub-rows">
                                            <li><a href="?module=singleMessage"><span><?= $envio_individual; ?></span></a></li>
                                            <li><a href="?module=groupMessage"><span><?= $envio_lista; ?></span></a></li>
                                            <li><a href="?module=fileMessage"><span><?= $envio_archivo; ?></span></a></li>
                                            <li><a href="?module=cancelMessage"><span><?= $envio_agendados; ?></span></a></li>
                                        </ul>
                                    </li>
                                    <!-- MHL 2016-12-14 -->
<?php
	if ($companyService['operator_query'] == '1')
	{
?>
                                    <li>
                                        <a href="#"><?= $consulta_operadora; ?></a>
                                        <ul class="cbp-hssubmenu cbp-hssub-rows">
                                            <li><a href="?module=singleQuery"><span><?= $consulta_individual; ?></span></a></li>
                                            <li><a href="?module=listQuery"><span><?= $consulta_lista; ?></span></a></li>
                                            <li><a href="?module=fileQuery"><span><?= $consulta_archivo; ?></span></a></li>
                                            <li><a href="?module=resultsQuery"><span><?= $consulta_bitacora; ?></span></a></li>
                                        </ul>
                                    </li>
<?php
	}
?>
                                    <!-- -------------------- -->
                                    <li>
                                        <a href="#"><?= $reportes; ?></a>
                                        <ul class="cbp-hssubmenu">
                                            <li><a href="?module=trafficMT"><span><?= $reporte_enviados; ?></span></a></li>  
                                            <li><a href="?module=trafficMO"><span><?= $reporte_recibidos; ?></span></a></li>
                                            <li><a href="?module=trafficFile"><span><?= $reporte_archivo; ?></span></a></li>
                                            <li><a href="?module=trafficCountry"><span><?= $reporte_enviados_dia; ?></span></a></li> 
                                            <li><a href="?module=trafficCompany"><span><?= $reporte_enviados_empresa; ?></span></a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#"><?= $ayuda; ?></a>
                                        <ul class="cbp-hssubmenu">
                                            <li><a href="?module=changePass"><span> <?= $contrasena; ?></span></a></li>  
                                            <li><a href="?module=manual"><span><?= $manual; ?></span></a></li>
                                            <li><a href="?module=faq"><span> <?= $faq; ?></span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>	
                    </div>
                </div>
            </header>

            <!--<script>
                var menu = new cbpHorizontalSlideOutMenu(document.getElementById('cbp-hsmenu-wrapper'));
            </script>-->

            <div id="container">
                <?php
                $status = $_SESSION['suspended'];
                //$module = $_GET['module'];
                $module = filter_input(INPUT_GET, 'module');

                if ($status == true) {
                    switch ($module) {
                        case "company": company();
                            break;
                        case "companyService": companyService();
                            break;
                        default: echo '<div id="logout">Cuenta Suspendida</div>';
                    }
                } else {
                    switch ($module) {
                        case "company": company();
                            break;
                        case "companyService": companyService();
                            break;
                        case "adminUser": adminUser();
                            break;
                        case "adminAddUser": adminAddUser();
                            break;
                        case "adminEditUser": adminEditUser();
                            break;
                        case "adminStatusUser": adminStatusUser();
                            break;
                        case "adminGroupList": adminGroupList();
                            break;
                        case "adminAddGroupList": adminAddGroupList();
                            break;
                        case "adminEditGroupList": adminEditGroupList();
                            break;
                        case "adminAddPhoneGroupList": adminAddPhoneGroupList();
                            break;
                        case "adminAddFileGroupList": adminAddFileGroupList();
                            break;
                        case "adminStatusGroupList": adminStatusGroupList();
                            break;
                        case "adminCostCenter": adminCostCenter();
                            break;
                        case "adminAddCostCenter": adminAddCostCenter();
                            break;
                        case "adminEditCostCenter": adminEditCostCenter();
                            break;
                        case "adminStatusCostCenter": adminStatusCostCenter();
                            break;
                        case "provWhiteList": provWhiteList();
                            break;
                        case "provWhiteListAdd": provWhiteListAdd();
                            break;
                        case "provWhiteListSearch": provWhiteListSearch();
                            break;
                        case "provWhiteListDelete": provWhiteListDelete();
                            break;
                        case "provBlackList": provBlackList();
                            break;
                        case "provBlackListAdd": provBlackListAdd();
                            break;
                        case "provBlackListDelete": provBlackListDelete();
                            break;
                        case "provBlackListSearch": provBlackListSearch();
                            break;
                        case "provBlackListMobiles": provBlackListMobiles();
                            break;
                        case "provRejected": provRejected();
                            break;
                        case "singleMessage": singleMessage();
                            break;
                        case "groupMessage": groupMessage();
                            break;
                        case "fileMessage": fileMessage();
                            break;
                        case "cancelMessage": cancelMessage();
                            break;
                        case "cancelStatusMessage": cancelStatusMessage();
                            break;
                        case "singleQuery": singleQuery();
                            break;
                        case "listQuery": listQuery();
                            break;
                        case "fileQuery": fileQuery();
                            break;
                        case "resultsQuery": resultsQuery();
                            break;
                        case "trafficMT": trafficMT();
                            break;
                        case "trafficMTDetail": trafficMTDetail();
                            break;
                        case "trafficMO": trafficMO();
                            break;
                        case "trafficMODetail": trafficMODetail();
                            break;
                        case "trafficFile": trafficFile();
                            break;
                        case "trafficCountry": trafficCountry();
                            break;
                        case "trafficCompany": trafficCompany();
                            break;
                        case "changePass": changePass();
                            break;
                        case "faq": faq();
                            break;
						case "manual": manual();
                            break;
                        case "logOut": logOut();
                            break;
                        case "": company();
                            break;
                    }
                }
                ?> 
            </div>
            <footer></footer>
        </div>
<?php
//$paises		= arrayCountry_hora();

$paises		= arrayCountryCBO();
$code_c			= strtoupper($_SESSION['code']);
$type			= $_SESSION['type'];
if($type == "GLOBAL")
	{
		$code_file = "MX";
	} else {
		$code_file = $code_c;
	}
	//echo"paises : $paises ,code_c :$code_c ,type : $type , code_file :$code_file";
 $gmt=funcion_hora ($paises,$code_file,$type) ;
 //echo"gmt : $gmt";
?>

<script type="text/javascript">
var myVar = setInterval(function(){ verHora() }, 10);
//Declaro Zona Horaria: -3:Zona horaria Argentina:
//-------------------------------------

var diferenciaConGTM=<?php echo $gmt ?>;
//-------------------------------------
function verHora(){
hoy=new Date();
hora=hoy.getHours();
hora=hora+(hoy.getTimezoneOffset()/60);
hora=hora+diferenciaConGTM;
if (hora < 10)
hora = "0" + hora
else
hora = "" + hora
if (hora>23)
{
	hora=hora-24	
}
if (hora<0)
{
	hora=24+hora
}
minutos=hoy.getMinutes();
segundos=hoy.getSeconds();
if (minutos < 10)
minutos = "0" + minutos
else
minutos =  minutos

if (segundos < 10)
segundos = "0" + segundos
else
segundos =  segundos


horario=hora+":"+minutos+":"+segundos;
document.getElementById("hora_seccion").innerHTML=horario ;}

</script>

        <script src="menu/js/cbpHorizontalSlideOutMenu.js"></script>
        <script>
            $(document).ready(function () {
                var menu = new cbpHorizontalSlideOutMenu(document.getElementById('cbp-hsmenu-wrapper'));

		if ('<?= $module ?>' == 'listQuery')
		{
			$('#country').change();
		}

            });
        </script>
    </body>
</html>
