<?php
session_start();

function company ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->inicio[0]->titulo;
	$lb_estado			= $lan_file->$lan->inicio[0]->estado;
	$lb_limitemensajes	= $lan_file->$lan->inicio[0]->limitemensajes;
	$lb_creadoel		= $lan_file->$lan->inicio[0]->creadoel;
	$lb_creadopor		= $lan_file->$lan->inicio[0]->creadopor;
	$lb_tipo			= $lan_file->$lan->inicio[0]->tipo;
	$lb_nlimite			= $lan_file->$lan->inicio[0]->nlimite;
	$lb_pais			= $lan_file->$lan->inicio[0]->pais;
	$lb_exceder			= $lan_file->$lan->inicio[0]->exceder;
	$lb_consultaoperadora		= $lan_file->$lan->inicio[0]->consultaoperadora;
	$lb_modificadael	= $lan_file->$lan->inicio[0]->modificadael;
	$lb_modificadapor	= $lan_file->$lan->inicio[0]->modificadapor;
	$lb_idfac			= $lan_file->$lan->inicio[0]->idfac;
	$lb_ciclo			= $lan_file->$lan->inicio[0]->ciclo;
	$tb_service			= $lan_file->$lan->inicio[0]->tbservice;
	$tb_marcacion		= $lan_file->$lan->inicio[0]->tbmarcacion;
	$tb_tiposervice		= $lan_file->$lan->inicio[0]->tbtiposervice;
	$tb_estado			= $lan_file->$lan->inicio[0]->tbestado;
	$tb_mensajes		= $lan_file->$lan->inicio[0]->tbmensajes;
	$tb_excede			= $lan_file->$lan->inicio[0]->tbexcede;
	$tb_idfac			= $lan_file->$lan->inicio[0]->tbidfac;
	$tb_pais			= $lan_file->$lan->inicio[0]->tbpais;
	$tb_creado			= $lan_file->$lan->inicio[0]->tbcreado;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission("view_company");

	if($permission == "Y") 
	{
		$access_token	= $_SESSION['access_token'];
       		$url			= URL_WS."WSA-Telcel/api/tgg/company?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);	
		$name			= $parseUrl['name'];
		$status			= $parseUrl['company_status'];
		$created_time	= returnFormatDate($parseUrl['created_time']);
		$create_user	= $parseUrl['create_user'];
		$mod_time		= returnFormatDate($parseUrl['mod_time']);
		$mod_user		= $parseUrl['mod_user'];
		$allow_exceed	= $parseUrl['allow_exceed'];
		$bill_cycle		= $parseUrl['bill_cycle'];
		$msg_limit		= $parseUrl['msg_limit'];
		$type			= $parseUrl['type'];
		$id_facturacion	= $parseUrl['id_facturacion'];
		$ctrl_msg_limit	= $parseUrl['ctrl_msg_limit'];
		$country		= $parseUrl['country_name'];
		$operator_query = $parseUrl['operator_query'];

		if ($operator_query)
		{
			$consultaoperadora = $lan_file->$lan->tips[0]->afirmativo;
		}
		else
		{
			$consultaoperadora = $lan_file->$lan->tips[0]->negativo;
		}

		?>
        <h2><?=$name;?></h2>
        <form>
        <div class="frm-label-small"> 
        	<label><?=$lb_estado;?></label> <input class="frm" value="ACTIVO" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label><?=$lb_limitemensajes;?></label> <input class="frm" value="<?=$ctrl_msg_limit;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label><?=$lb_creadoel;?></label> <input class="frm" value="<?=$created_time;?>" size="15" readonly>
        </div>
        <br>
        <div class="frm-label-small">
        	<label><?=$lb_tipo;?></label> <input class="frm" value="<?=$type;?>" size="15" readonly> 
        </div>
        <div class="frm-label-small">
        	<label><?=$lb_nlimite;?></label> <input class="frm" value="<?=$msg_limit;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label><?=$lb_creadopor;?></label> <input class="frm" value="<?=$create_user;?>" size="15" readonly><br>
        </div>
        <br>
        <div class="frm-label-small">
        	<label><?=$lb_pais;?></label> <input class="frm" value="<?=$country;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label><?=$lb_exceder;?></label> <input class="frm"  value="<?=$allow_exceed;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label><?=$lb_modificadael;?></label> <input class="frm" value="<?=$mod_time;?>" size="15" readonly>
        </div>
        <br>
        <div class="frm-label-small">
        	<label><?=$lb_idfac;?></label> <input class="frm" value="<?=$id_facturacion;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label><?=$lb_consultaoperadora;?></label> <input class="frm"  value="<?=$consultaoperadora;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label><?=$lb_modificadapor;?></label> <input class="frm" value="<?=$mod_user;?>" size="15" readonly>
        </div>
        <br>
        <div class="frm-label-small">
        	<label><?=$lb_ciclo;?></label> <input class="frm" value="<?=$bill_cycle;?>" size="15" readonly>
        </div>
        </form>
        
        <table>
        	<tr>
				<th><?=$tb_service;?></th>
				<th><?=$tb_marcacion;?></th>
				<th><?=$tb_tiposervice;?></th>
				<th><?=$tb_estado;?></th>
                <th><?=$tb_mensajes;?></th>
                <th><?=$tb_excede;?></th>
                <th><?=$tb_idfac;?></th>
                <th><?=$tb_pais;?></th>
				<th><?=$tb_creado;?></th>
			</tr>
        <?php
		$urlSrv			= URL_WS."WSA-Telcel/api/tgg/service?access_token=".$access_token;
		$iUrlSrv		= curl_init($urlSrv);
		curl_setopt($iUrlSrv, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrlSrv		= curl_exec($iUrlSrv);
		$parseUrlSrv	= json_decode($pUrlSrv,true);
		
		for($i = 0; $i < count($parseUrlSrv); $i++)
			{
				$id_service	= $parseUrlSrv[$i]['id_service'];
				$service_type	= $parseUrlSrv[$i]['service_type'];
				$service_name	= $parseUrlSrv[$i]['service_name'];
				$service_status	= $parseUrlSrv[$i]['service_status'];
				$service_status_description	= $parseUrlSrv[$i]['service_status_description'];
				$created_time	= returnFormatDate($parseUrlSrv[$i]['created_time']);
				$service_tag	= $parseUrlSrv[$i]['service_tag'];
				$id_facturacion	= $parseUrlSrv[$i]['id_facturacion'];
				$msg_limit	= $parseUrlSrv[$i]['msg_limit'];
				$ctrl_msg_limit	= $parseUrlSrv[$i]['ctrl_msg_limit'];
				$country_name	= $parseUrlSrv[$i]['country_name'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Suspendido";
				} else {
					$status = "Activo";
				}
				?>
				<tr class="<?=$class;?>">
					<td><a href="?module=companyService&id_service=<?=$id_service;?>"><?=$service_name;?></a> </td>
                    <td><?=$service_tag;?> </td>
                    <td><?=$service_type;?> </td>
                    <td><?=$service_status_description;?></td>
                    <td><?=$msg_limit;?> </td>
                    <td><?=$ctrl_msg_limit;?> </td>
                    <td><?=$id_facturacion;?> </td>
                    <td><?=$country_name;?> </td>
                    <td><?=$created_time;?> </td>
				</tr>
				<?php
			}
			?>
			</table>
        
        <?php
	} else {
	?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function companyService ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->ini_detalle_servicio[0]->titulo;
	$lb_nombre			= $lan_file->$lan->ini_detalle_servicio[0]->nombre;
	$lb_general			= $lan_file->$lan->ini_detalle_servicio[0]->general;
	$lb_pais			= $lan_file->$lan->ini_detalle_servicio[0]->pais;
	$lb_tipo			= $lan_file->$lan->ini_detalle_servicio[0]->tipo;
	$lb_estado			= $lan_file->$lan->ini_detalle_servicio[0]->estado;
	$lb_marcacion		= $lan_file->$lan->ini_detalle_servicio[0]->marcacion;
	$lb_idfac			= $lan_file->$lan->ini_detalle_servicio[0]->idfac;
	$lb_ini_servicio	= $lan_file->$lan->ini_detalle_servicio[0]->ini_servicio;
	$lb_fin_servicio	= $lan_file->$lan->ini_detalle_servicio[0]->fin_servicio;
	$lb_canal			= $lan_file->$lan->ini_detalle_servicio[0]->canal;
	$lb_can_web			= $lan_file->$lan->ini_detalle_servicio[0]->can_web;
	$lb_can_ws			= $lan_file->$lan->ini_detalle_servicio[0]->can_ws;
	$lb_can_smpp		= $lan_file->$lan->ini_detalle_servicio[0]->can_smpp;
	$lb_canal_prov		= $lan_file->$lan->ini_detalle_servicio[0]->canal_prov;
	$lb_can_prov_web	= $lan_file->$lan->ini_detalle_servicio[0]->can_prov_web;
	$lb_can_prov_ws		= $lan_file->$lan->ini_detalle_servicio[0]->can_prov_ws;
	$lb_val_servicio	= $lan_file->$lan->ini_detalle_servicio[0]->val_servicio;
	$lb_val_limite_sms	= $lan_file->$lan->ini_detalle_servicio[0]->val_limite_sms;
	$lb_val_limite_nsms	= $lan_file->$lan->ini_detalle_servicio[0]->val_limite_nsms;
	$lb_val_excede		= $lan_file->$lan->ini_detalle_servicio[0]->val_excede;
	$lb_val_hor_envio	= $lan_file->$lan->ini_detalle_servicio[0]->val_hor_envio;
	$lb_val_hor_ini		= $lan_file->$lan->ini_detalle_servicio[0]->val_hor_ini;
	$lb_val_hor_fin		= $lan_file->$lan->ini_detalle_servicio[0]->val_hor_fin;
	$lb_val_lb			= $lan_file->$lan->ini_detalle_servicio[0]->val_lb;
	$lb_val_ln			= $lan_file->$lan->ini_detalle_servicio[0]->val_ln;
	$lb_cmp_pub			= $lan_file->$lan->ini_detalle_servicio[0]->cmp_pub;
	$lb_cmp_invitacion	= $lan_file->$lan->ini_detalle_servicio[0]->cmp_invitacion;
	$lb_cmp_ini			= $lan_file->$lan->ini_detalle_servicio[0]->cmp_ini;
	$lb_cmp_fin			= $lan_file->$lan->ini_detalle_servicio[0]->cmp_fin;
	$lb_cmp_limite		= $lan_file->$lan->ini_detalle_servicio[0]->cmp_limite;
	$lb_cmp_nlimite		= $lan_file->$lan->ini_detalle_servicio[0]->cmp_nlimite;
	$lb_notificaciones	= $lan_file->$lan->ini_detalle_servicio[0]->notificaciones;
	$lb_not_baja		= $lan_file->$lan->ini_detalle_servicio[0]->not_baja;
	$lb_not_int			= $lan_file->$lan->ini_detalle_servicio[0]->not_int;
	$lb_info			= $lan_file->$lan->ini_detalle_servicio[0]->info;
	$lb_inf_can_mt		= $lan_file->$lan->ini_detalle_servicio[0]->inf_can_mt;
	$lb_inf_can_mo		= $lan_file->$lan->ini_detalle_servicio[0]->inf_can_mo;
	$lb_inf_mul			= $lan_file->$lan->ini_detalle_servicio[0]->inf_mul;
	$lb_inf_mas			= $lan_file->$lan->ini_detalle_servicio[0]->inf_mas;
	$lb_inf_txt_mas		= $lan_file->$lan->ini_detalle_servicio[0]->inf_txt_mas;
	$lb_inf_bidir		= $lan_file->$lan->ini_detalle_servicio[0]->inf_bidir;
	$lb_inf_mo_int		= $lan_file->$lan->ini_detalle_servicio[0]->inf_mo_int;
	$lb_inf_dr_int		= $lan_file->$lan->ini_detalle_servicio[0]->inf_dr_int;
	$lb_inf_estado_dr	= $lan_file->$lan->ini_detalle_servicio[0]->inf_estado_dr;
	$lb_inf_dr_rep		= $lan_file->$lan->ini_detalle_servicio[0]->inf_dr_rep;
	$lb_ctrl			= $lan_file->$lan->ini_detalle_servicio[0]->ctrl;
	$lb_ctrl_creado_el	= $lan_file->$lan->ini_detalle_servicio[0]->ctrl_creado_el;
	$lb_ctrl_creado_por	= $lan_file->$lan->ini_detalle_servicio[0]->ctrl_creado_por;
	$lb_ctrl_mod_el		= $lan_file->$lan->ini_detalle_servicio[0]->ctrl_mod_el;
	$lb_ctrl_mod_por	= $lan_file->$lan->ini_detalle_servicio[0]->ctrl_mod_por;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_service");
		
	if($permission == "Y") {
		$access_token			= $_SESSION['access_token'];
		$id_service				= $_GET['id_service'];
       		$url		= URL_WS."WSA-Telcel/api/tgg/service/".$id_service."?access_token=".$access_token;
		$iUrl					= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl					= curl_exec($iUrl);
		$parseUrl				= json_decode($pUrl,true);
		$service_name			= $parseUrl['service_name'];
		$country				= $parseUrl['country_name'];
		$service_type			= $parseUrl['service_type'];
		$service_status			= $parseUrl['service_status'];
		$service_tag			= $parseUrl['service_tag'];
		$id_facturacion			= $parseUrl['id_facturacion'];
		$date_ini				= returnFormatDate($parseUrl['date_ini']);
		$date_fin				= returnFormatDate($parseUrl['date_fin']);
		$invitation				= $parseUrl['invitation'];
		$date_ini_inv			= returnFormatDate($parseUrl['date_ini_inv']);
		$date_fin_inv			= returnFormatDate($parseUrl['date_fin_inv']);
		$ctrl_msg_day			= $parseUrl['ctrl_msg_day'];
		$msg_day				= $parseUrl['msg_day'];
		$ctrl_msg_limit			= $parseUrl['ctrl_msg_limit'];
		$msg_limit				= $parseUrl['msg_limit'];
		$allow_exceed			= $parseUrl['allow_exceed'];
		$ctrl_time				= $parseUrl['ctrl_time'];
		$msg_time_ini			= substr($parseUrl['msg_time_ini'], 0,8);
		$msg_time_fin			= substr($parseUrl['msg_time_fin'], 0, 8);
		$check_whitelist		= $parseUrl['check_whitelist'];
		$check_blacklist		= $parseUrl['check_blacklist'];
		$dispatch_mt_channel	= $parseUrl['dispatch_mt_channel'];	
		$multi_operador			= $parseUrl['multi_operador'];
		$use_fake_sender		= $parseUrl['use_fake_sender'];
		$allowed_fake_senders	= $parseUrl['allowed_fake_senders'];
		$receive_mo				= $parseUrl['receive_mo'];
		$dispatch_mo_channel	= $parseUrl['dispatch_mo_channel'];
		$dispatch_dr_flag		= $parseUrl['dispatch_dr_flag'];
		$ask_for_ack			= $parseUrl['ask_for_ack'];
		$bill_ack				= $parseUrl['bill_ack'];
		$enable_prov_ws			= $parseUrl['enable_prov_ws'];
		$enable_prov_web		= $parseUrl['enable_prov_web'];	
		$enable_sms_web			= $parseUrl['enable_sms_web'];
		$enable_sms_ws			= $parseUrl['enable_sms_ws'];
		$enable_sms_smpp		= $parseUrl['enable_sms_smpp'];
		$created_time			= returnFormatDate($parseUrl['created_time']);
		$create_user			= $parseUrl['create_user'];
		$mod_time				= returnFormatDate($parseUrl['mod_time']);
		$mod_user				= $parseUrl['mod_user'];
		$notify_optout			= $parseUrl['notify_optout'];
		$notify_others			= $parseUrl['notify_others'];
		?>
        <h2><?=$lb_nombre;?>: <?=$service_name;?></h2>
        <button type="button" class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        

        <form>
        <div class="frm-label-small">
            <fieldset>
            <legend><?=$lb_general;?></legend>
            	<label><?=$lb_pais;?></label> <input class="frm" value="<?=$country;?>" size="15" readonly>
            	<br>
            	<label><?=$lb_tipo;?></label> <input class="frm" value="<?=$service_type;?>" size="15" readonly>
                <br>
                <label><?=$lb_estado;?></label> <input class="frm" value="<?=$service_status;?>" size="15" readonly>
                <br>
                <label><?=$lb_marcacion;?></label> <input class="frm" value="<?=$service_tag;?>" size="15" readonly>
                <br>
                <label><?=$lb_idfac;?></label> <input class="frm" value="<?=$id_facturacion;?>" size="15" readonly>
                <br>
                <label><?=$lb_ini_servicio;?></label> <input class="frm" value="<?=$date_ini;?>" size="15" readonly>
                <br>
                <label><?=$lb_fin_servicio;?></label> <input class="frm" value="<?=$date_fin;?>" size="15" readonly>
            </fieldset>
            <br>
            <fieldset>
            <legend><?=$lb_canal;?></legend>
            	<label><?=$lb_can_web;?></label> <input  class="frm" value="<?=$enable_sms_web;?>" size="15" readonly>
            	<br>
            	<label><?=$lb_can_ws;?></label> <input  class="frm" value="<?=$enable_sms_ws;?>" size="15" readonly>
                <br>
                <label><?=$lb_can_smpp;?></label> <input  class="frm" value="<?=$enable_sms_smpp;?>" size="15" readonly>
            </fieldset>
            <br>
            <fieldset>
            <legend><?=$lb_canal_prov;?></legend>
            	<label><?=$lb_can_prov_web;?></label> <input  class="frm" value="<?=$enable_prov_web;?>" size="15" readonly>
            	<br>
            	<label><?=$lb_can_prov_ws;?></label> <input  class="frm" value="<?=$enable_prov_ws;?>" size="15" readonly>
            </fieldset>
        </div>
        <div class="frm-label-small">
            <fieldset>
            <legend><?=$lb_val_servicio;?></legend>
            	<label><?=$lb_val_limite_sms;?></label> <input  class="frm" value="<?=$ctrl_msg_limit;?>" size="15" readonly>
            	<br>
            	<label><?=$lb_val_limite_nsms;?></label> <input class="frm" value="<?=$msg_limit;?>" size="15" readonly>
                <br>
                <label><?=$lb_val_excede;?></label> <input class="frm" value="<?=$allow_exceed;?>" size="15" readonly>
                <br>
                <label><?=$lb_val_hor_envio;?></label> <input class="frm" value="<?=$ctrl_time;?>" size="15" readonly>
                <br>
                <label><?=$lb_val_hor_ini;?></label> <input class="frm" value="<?=$msg_time_ini;?>" size="15" readonly>
                <br>
                <label><?=$lb_val_hor_fin;?></label> <input class="frm" value="<?=$msg_time_fin;?>" size="15" readonly>
                <br>
                <label><?=$lb_val_lb;?></label> <input class="frm" value="<?=$check_whitelist;?>" size="15" readonly>
                <br>
                <label><?=$lb_val_ln;?></label> <input class="frm" value="<?=$check_blacklist;?>" size="15" readonly>
            </fieldset>
            <br>
            <fieldset>
            <legend><?=$lb_cmp_pub;?></legend>
            	<label><?=$lb_cmp_invitacion;?></label> <input class="frm" value="<?=$invitation;?>" size="15" readonly>
            	<br>
            	<label><?=$lb_cmp_ini;?></label> <input class="frm" value="<?=$date_ini_inv;?>" size="15" readonly>
                <br>
                <label><?=$lb_cmp_fin;?></label> <input class="frm" value="<?=$date_fin_inv;?>" size="15" readonly>
                <br>
                <label><?=$lb_cmp_limite;?></label> <input class="frm" value="<?=$ctrl_msg_day;?>" size="15" readonly>
                <br>
                <label><?=$lb_cmp_nlimite;?></label> <input class="frm" value="<?=$msg_day;?>" size="15" readonly>
            </fieldset>
            <br>
            <fieldset>
            <legend><?=$lb_notificaciones;?></legend>
            	<label><?=$lb_not_baja;?></label> <input class="frm" value="<?=$notify_optout;?>" size="15" readonly>
            	<br>
            	<label><?=$lb_not_int;?></label> <input class="frm" value="<?=$notify_others;?>" size="15" readonly>
            </fieldset>
        </div>
        <div class="frm-label-small">
            <fieldset>
            <legend><?=$lb_info;?></legend>
            	<label><?=$lb_inf_can_mt;?></label> <input class="frm" value="<?=$dispatch_mt_channel;?>" size="10" readonly>
            	<br>
                <label><?=$lb_inf_can_mo;?></label> <input class="frm" value="<?=$dispatch_mo_channel;?>" size="10" readonly>
            	<br>
            	<label><?=$lb_inf_mul;?></label> <input class="frm" value="<?=$multi_operador;?>" size="10" readonly>
                <br>
                <label><?=$lb_inf_mas;?></label> <input class="frm" value="<?=$use_fake_sender;?>" size="10" readonly>
                <br>
                <label><?=$lb_inf_txt_mas;?></label> <input class="frm" value="<?=$allowed_fake_senders
;?>" size="10" readonly>
                <br>
                <label><?=$lb_inf_bidir;?></label> <input class="frm" value="<?=$receive_mo;?>" size="10" readonly>
                <br>
                <label><?=$lb_inf_mo_int;?></label> <input class="frm" value="<?=$receive_mo;?>" size="10" readonly>
                <br>
                <label><?=$lb_inf_dr_int;?></label> <input class="frm" value="<?=$dispatch_dr_flag;?>" size="10" readonly>
                <br>
                <label><?=$lb_inf_estado_dr;?></label> <input class="frm" value="<?=$ask_for_ack;?>" size="10" readonly>
                <br>
                <label><?=$lb_inf_dr_rep;?></label> <input class="frm" value="<?=$bill_ack;?>" size="10" readonly>
            </fieldset>
            <br>
            <fieldset>
            <legend><?=$lb_ctrl;?></legend>
            	<label><?=$lb_ctrl_creado_por;?></label> <input class="frm" value="<?=$create_user;?>" size="15" readonly>
            	<br>
            	<label><?=$lb_ctrl_creado_el;?></label> <input class="frm" value="<?=$created_time;?>" size="15" readonly>
                <br>
                <label><?=$lb_ctrl_mod_por;?></label> <input class="frm" value="<?=$mod_user;?>" size="15" readonly>
                <br>
                <label><?=$lb_ctrl_mod_el;?></label> <input class="frm" value="<?=$mod_time;?>" size="15" readonly>
            </fieldset>
        </div>
        <br>
        </form>

        
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminUser ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_usuarios[0]->titulo;
	$lb_nombre			= $lan_file->$lan->adm_usuarios[0]->nombre;
	$tbnombre			= $lan_file->$lan->adm_usuarios[0]->tbnombre;
	$tbusuario			= $lan_file->$lan->adm_usuarios[0]->tbusuario;
	$tbperfil			= $lan_file->$lan->adm_usuarios[0]->tbperfil;
	$tbestado			= $lan_file->$lan->adm_usuarios[0]->tbestado;
	$tbaccion			= $lan_file->$lan->adm_usuarios[0]->tbaccion;
	$activar_usuario	= $lan_file->$lan->tips[0]->activar_usuario;
	$desactivar_usuario	= $lan_file->$lan->tips[0]->desactivar_usuario;
	$modificar_usuario	= $lan_file->$lan->tips[0]->modificar_usuario;
	$b_consultar		= $lan_file->$lan->botones[0]->b_consultar;
	$b_ver_todo			= $lan_file->$lan->botones[0]->b_ver_todo;
	$b_agregar_usuario	= $lan_file->$lan->botones[0]->b_agregar_usuario;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_user_list");
	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		?>
        <div class="frm-label-big">
        	<label><?=$lb_nombre;?></label>
        	<input type="text" name="username" id="username" class="frm" value="<?=$searchname?>"> &nbsp;
        	<button type="button" class="frm-button" onClick="AdminUserSearch('<?=$lan;?>');"><?=$b_consultar;?></button>
        </div>
        <div class="frm-label-big">
        	<button class="frm-button" onClick="goUrl('?module=adminUser');"><?=$b_ver_todo;?></button> <button class="frm-button" onClick="goUrl('?module=adminAddUser');"><?=$b_agregar_usuario;?></button>
        </div>
        
        <br>
        <br>
        <?php
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		$data			.= "&searchname=".urlencode($searchname);
	    $url_base		= URL_WS."WSA-Telcel/api/user?access_token=".$access_token."&searchname=".urlencode($searchname);
		$url			= URL_WS."WSA-Telcel/api/user?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= urlTotalRows($url_base);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=adminUser&searchname=".$searchname;
		
		if($totalRows == 0)
		{
			if (! empty($searchname))
			{
		?>
            <div class="show-message">
		<?php
			showMessage (9);
			}
		}
		else
		{
			pag_pages ($page, $total_pages, $url_app);
				
			?>
			<table>
			<tr>
				<th><?=$tbnombre;?></th>
				<th><?=$tbusuario;?></th>
				<th><?=$tbperfil;?></th>
				<th><?=$tbestado;?></th>
				<th><?=$tbaccion;?></th>
			</tr>
			<?php
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$id_user		= $parseUrl[$i]['id_user'];
				$username		= $parseUrl[$i]['username'];
				$full_name		= $parseUrl[$i]['full_name'];
				$role			= $parseUrl[$i]['title'];
				$statusCode		= $parseUrl[$i]['enabled'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Suspendido";
				} else {
					$status = "Activo";
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$full_name;?></td>
					<td><?=$username;?></td>
					<td><?=$role;?></td>
					<td><?=$status;?></td>
					<td>
						<?php 
						echo "<div id=\"response$id_user\" style=\"max-width: 130px; height:20px;\">";
						?>
						<!--<div id="response< ?=$id_user?>" style="max-width: 130px; height:20px;"> -->
							<a href="?module=adminEditUser&id_user=<?=$id_user;?>" class="tip">
								<span><?=$modificar_usuario;?></span>
								<img src="images/icons/edit_user.png" class="button-img">
							</a>
							<?php
							if($statusCode == 0)
							{
								?>
								<a href="?module=adminStatusUser&id_user=<?=$id_user;?>" class="tip">
									<span><?=$activar_usuario;?></span>
									<img src="images/icons/confirm.png" class="button-img">
								</a>
								<?php
							} else {
								?>
								<a href="?module=adminStatusUser&id_user=<?=$id_user;?>" class="tip">
									<span><?=$desactivar_usuario;?></span>
									<img src="images/icons/delete.png" class="button-img">
								</a>
								<?php
							}?>
						<?php echo "</div>";?> 
					</td>
				</tr>
				<?php
			}
			?>
			</table>
			
			<?php
			pag_pages ($page, $total_pages, $url_app);
		}
	}
	else
	{
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminAddUser ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_agregar_usuario[0]->titulo;
	$lb_nombre			= $lan_file->$lan->adm_agregar_usuario[0]->nombre;
	$lb_email			= $lan_file->$lan->adm_agregar_usuario[0]->email;
	$lb_clave			= $lan_file->$lan->adm_agregar_usuario[0]->clave;
	$lb_perfil			= $lan_file->$lan->adm_agregar_usuario[0]->perfil;
	$lb_lenguaje		= $lan_file->$lan->adm_agregar_usuario[0]->lenguaje;
	$b_agregar			= $lan_file->$lan->botones[0]->b_agregar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("add_user");
	
	if($permission == "Y") {
		?>
        <div class="frm-label-big">
        	<label><?=$lb_nombre;?></label>
        	<input type="text" name="fullname" id="fullname" class="frm" size="47" maxlength="45"> &nbsp;(*)
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_email;?></label>
        	<input type="text" name="username" id="username" class="frm"> &nbsp;(*)
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_clave;?></label>
        	<input type="password" name="password" id="password" class="frm"> &nbsp;(*)
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_perfil;?></label> 
        	<?php selectPerfil(); ?> &nbsp;(*)
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_lenguaje;?></label> 
        	<?php selectLanguage(); ?> &nbsp;(*)
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	(*) <?=$campo_obligatorio;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label> 
        	<button class="frm-button" onClick="adminAddUser('<?=$lan;?>');"><?=$b_agregar;?></button> 
            <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminEditUser ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_editar_usuario[0]->titulo;
	$lb_nombre			= $lan_file->$lan->adm_editar_usuario[0]->nombre;
	$lb_email			= $lan_file->$lan->adm_editar_usuario[0]->email;
	$lb_clave			= $lan_file->$lan->adm_editar_usuario[0]->clave;
	$lb_perfil			= $lan_file->$lan->adm_editar_usuario[0]->perfil;
	$lb_lenguaje		= $lan_file->$lan->adm_editar_usuario[0]->lenguaje;
	$b_modificar		= $lan_file->$lan->botones[0]->b_modificar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("edit_user");
	
	if($permission == "Y") {
		$id_user		= $_GET['id_user'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/user/$id_user?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$iduser			= $parseUrl['id_user'];
		$fullname		= $parseUrl['full_name'];
		$username		= $parseUrl['username'];
		$password		= $parseUrl['password'];
		$role			= $parseUrl['id_role'];
		$language		= $parseUrl['language'];
		$title_role		= $parseUrl['title'];
		
		$roles			= array(3 => 'Administrador', 4 => 'Operaci&oacute;n', 5 => 'Gesti&oacute;n');
		?>
        <div class="frm-label-big">
        	<label><?=$lb_nombre;?></label>
        	<input type="text" name="fullname" id="fullname" class="frm" size="47" maxlength="45" value="<?=$fullname;?>"> &nbsp;(*)
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_email;?></label>
        	<input type="text" name="username" id="username" class="frm" value="<?=$username;?>" readonly> &nbsp;(*)
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_clave;?></label>
        	<input type="password" name="password" id="password" class="frm" value="<?=$password;?>"> &nbsp;(*)
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_perfil;?></label>
        	<?php selectPerfil($role); ?> &nbsp;(*)
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_lenguaje;?></label> 
        	<?php selectLanguage($language); ?> &nbsp;(*) 
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	(*) <?=$campo_obligatorio;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminEditUser('<?=$id_user;?>', '<?=$lan;?>');"><?=$b_modificar;?></button> 
            <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminStatusUser ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_estado_usuario[0]->titulo;
	$lbemail			= $lan_file->$lan->adm_estado_usuario[0]->lbemail;
	$lbestado			= $lan_file->$lan->adm_estado_usuario[0]->lbestado;
	$b_modificar		= $lan_file->$lan->botones[0]->b_modificar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("del_user");
	
	if($permission == "Y") {
		$id_user		= $_GET['id_user'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/user/$id_user?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$iduser			= $parseUrl['id_user'];
		$fullname		= $parseUrl['full_name'];
		$username		= $parseUrl['username'];
		$status			= $parseUrl['enabled'];
		$role			= $parseUrl['id_role'];
		$title_role		= $parseUrl['title'];
		
		$roles			= array(3 => 'Administrador', 4 => 'Operaci&oacute;n', 5 => 'Gesti&oacute;n');
		?>
        <div class="frm-label-big">
        	<label><?=$lbemail;?></label>
        	<?=$username;?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbestado;?></label>
        	<?php selectStatus($status); ?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminStatusUser('<?=$id_user;?>', '<?=$lan;?>');"><?=$b_modificar;?></button> <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminGroupList ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lbordenasc			= $lan_file->$lan->adm_lista_envio[0]->lbordenasc;
	$lbordendes			= $lan_file->$lan->adm_lista_envio[0]->lbordendes;
	$lbestado			= $lan_file->$lan->adm_lista_envio[0]->lbestado;
	$lbestadocol		= $lan_file->$lan->adm_lista_envio[0]->lbestadocol;
	$lb_titulo			= $lan_file->$lan->adm_lista_envio[0]->titulo;
	$lb_nombre			= $lan_file->$lan->adm_lista_envio[0]->nombre;
	$tbnombre			= $lan_file->$lan->adm_lista_envio[0]->tbnombre;
	$tbcreado_el		= $lan_file->$lan->adm_lista_envio[0]->tbcreado_el;
	$tbcreado_por		= $lan_file->$lan->adm_lista_envio[0]->tbcreado_por;
	$tbestado			= $lan_file->$lan->adm_lista_envio[0]->tbestado;
	$tbpais				= $lan_file->$lan->adm_lista_envio[0]->tbpais;
	$tbtotal			= $lan_file->$lan->adm_lista_envio[0]->tbtotal;
	$tbaccion			= $lan_file->$lan->adm_lista_envio[0]->tbaccion;
	$asociar_lista		= $lan_file->$lan->tips[0]->asociar_lista;
	$activar_lista		= $lan_file->$lan->tips[0]->activar_lista;
	$desactivar_lista	= $lan_file->$lan->tips[0]->desactivar_lista;
	$b_consultar		= $lan_file->$lan->botones[0]->b_consultar;
	$b_ver_todo			= $lan_file->$lan->botones[0]->b_ver_todo;
	$b_agregar_lista	= $lan_file->$lan->botones[0]->b_agregar_lista;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_group_list");
	
	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		$stagecolum			= $_GET['field'];
		$stagelist	= $_GET['enabled'];
		$order			= $_GET['order'];
		?>
        
        <div class="frm-label-big">
        	<label><?=$tbnombre;?></label>
        	<input type="text" name="name_group" id="name_group" class="frm" value="<?=$searchname?>">&nbsp;
            
        </div>
        
        <div class="frm-label-big">
        	<label><?=$lbestadocol?></label> <?php  selectColumGroupList($stagecolum); ?> <?php selectOrder($order); ?> 
        </div>
       <div class="frm-label-big">
        	<label><?=$lbestado?></label> <?php selectStageGroupList($stagelist); ?>
        </div>
        <div class="frm-label-big">
            <button class="frm-button" onClick="AdminSearchGroupList('<?=$lan;?>');"><?=$b_consultar;?></button>
        </div>
        <div class="frm-label-big">
        	<button class="frm-button" onClick="goUrl('?module=adminGroupList');"><?=$b_ver_todo;?></button> 
            <button class="frm-button" onClick="goUrl('?module=adminAddGroupList');"><?=$b_agregar_lista;?></button>
        </div>
		<br>
        <br>
        <?php
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		if(!empty($order)) 
		{
			 $data_extra .= "&order=".$order; 
		}
		
		if(!empty($stagecolum)) 
		{ 
			$data_extra .= "&field=".$stagecolum; 
		}
		
		if(!empty($stagelist))
		{
			if($stagelist == "on")
			{
			 $data_extra .= "&enabled=1";
			}
			else
			{
				$data_extra .= "&enabled=0";
			}
		
		}
		
		if(!empty($searchname)) { $data_extra .= "&searchname=".$searchname; }
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		//$data			.= "&searchname=".$searchname;
		$url_base		= URL_WS."WSA-Telcel/api/group?access_token=".$access_token.$data_extra;
			
		$url			= URL_WS."WSA-Telcel/api/group?access_token=".$access_token.$data.$data_extra;
		//echo"$url";
		$iUrl			= curl_init($url);
		
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= urlTotalRows($url_base);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=adminGroupList";
		//echo"$url_app";
		
		
		
		if($totalRows == 0)
		{
			if (! empty($searchname))
			{
			?>
			<div class="show-message">
			<?php
			showMessage (9);
			}
			?>
			</div>
			<?php
		}
		else
		{
		
			pag_pages ($page, $total_pages, $url_app);
			
			?>
			
			<table>
			<tr>
				<th><?=$tbnombre;?></th>
				<th><?=$tbcreado_el;?></th>
				<th><?=$tbcreado_por;?></th>
				<th><?=$tbestado;?></th>
                <th><?=$tbpais;?></th>
				<th><?=$tbtotal;?></th>
				<th><?=$tbaccion;?></th>
			</tr>
			<?php
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$id_group		= $parseUrl[$i]['id_group'];
				$name			= $parseUrl[$i]['name'];
				$created_time	= returnFormatDate($parseUrl[$i]['created_time']);
				$created_by		= $parseUrl[$i]['username'];
				$statusCode		= $parseUrl[$i]['enabled'];
				$id_country		= getCountryName($parseUrl[$i]['id_country']);
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Suspendido";
				} else {
					$status = "Activo";
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$name;?> </td>
					<td><?=$created_time;?> </td>
					<td><?=$created_by;?> </td>
					<td><?=$status;?></td>
                    <td><?=$id_country;?></td>
					<td><?=$total = urlTotalRows(URL_WS."WSA-Telcel/api/group/mobile/$id_group?access_token=".$access_token);?>
					<td width="150">
					<?php 
					echo "<div id=\"response$id_group\">";
					?>
						<a href="?module=adminEditGroupList&id_group=<?=$id_group;?>" class="tip"><span><?=$asociar_lista;?></span><img src="images/icons/edit_user.png" class="button-img"></a>
                        <?php
                        if($statusCode == 0)
						{
							?>
                            <a href="?module=adminStatusGroupList&id_group=<?=$id_group;?>" class="tip"><span><?=$activar_lista;?></span><img src="images/icons/confirm.png" class="button-img"></a>
                            <?php
						} else {
							?>
                            <a href="?module=adminStatusGroupList&id_group=<?=$id_group;?>&id_country=<?=$id_country;?>" class="tip"><span><?=$desactivar_lista;?></span><img src="images/icons/delete.png" class="button-img"></a>
                            <?php
						}?>
                        
                        <?php /*? > <a href="ajaxFuncs/repGroupList.php?report=<?=$id_group;?>"><img src="images/icons/excel.png"></a><?php */?>
					<?php 
					echo "</div>";
					?>
					</td>
				</tr>
				<?php
			}
			?>
			</table>
			
			<?php
			pag_pages ($page, $total_pages, $url_app);
		}
		
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminAddGroupList ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_agregar_lista_envio[0]->titulo;
	$lb_nombre			= $lan_file->$lan->adm_agregar_lista_envio[0]->nombre;
	$lb_pais			= $lan_file->$lan->adm_agregar_lista_envio[0]->pais;
	$b_agregar			= $lan_file->$lan->botones[0]->b_agregar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("add_list_group");
	
	if($permission == "Y") 
	{
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$countrys		= groupArray($paises, "code");
		?>
        <div class="frm-label-big">
            <label><?=$lb_pais;?></label>
                <select name="country" id="country" class="frm">
                    <option value="">Seleccionar un Pa&iacute;s para el env&iacute;o</option>
                    <?php
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
							$id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$id?>" selected> <?=$ct?></option>
                                    <?php
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?php
                            }
                        }
                    }
                ?>
                </select> &nbsp; (*)
            </div>
        <div class="frm-label-big">
        	<label><?=$lb_nombre;?></label>
        	<input type="text" name="name_group" id="name_group" class="frm" size="35"> &nbsp; (*)
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	(*) <?=$campo_obligatorio;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminAddGroupList('<?=$lan;?>');"><?=$b_agregar;?></button> <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>
        <br>
		<div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminEditGroupList ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_editar_lista_envio[0]->titulo;
	$lb_movil			= $lan_file->$lan->adm_editar_lista_envio[0]->movil;
	$tbdestinatarios	= $lan_file->$lan->adm_editar_lista_envio[0]->tbdestinatarios;
	$tbaccion			= $lan_file->$lan->adm_editar_lista_envio[0]->tbaccion;
	$eliminar_movil		= $lan_file->$lan->tips[0]->eliminar_movil;
	$b_consultar		= $lan_file->$lan->botones[0]->b_consultar;
	$b_ver_todo			= $lan_file->$lan->botones[0]->b_ver_todo;
	$b_individual		= $lan_file->$lan->botones[0]->b_agregar_individual;
	$b_archivo			= $lan_file->$lan->botones[0]->b_agregar_archivo;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("edit_list_group");
	
	if($permission == "Y") {
		$id_group		= $_GET['id_group'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/group/$id_group?access_token=".$access_token;
		
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$id_group		= $parseUrl['id_group'];
		$name_group		= $parseUrl['name'];
		
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$searchmsisdn		= $_GET['searchmsisdn'];
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);		
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		$data			.= "&searchmsisdn=".$searchmsisdn;
        $url_base_list	= URL_WS."WSA-Telcel/api/group/mobile/$id_group?access_token=".$access_token."&searchname=".$searchname;
		
		$url_list		= URL_WS."WSA-Telcel/api/group/mobile/$id_group?access_token=".$access_token.$data;
		
		$iUrlList		= curl_init($url_list);
		curl_setopt($iUrlList, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrlList		= curl_exec($iUrlList);
		$parseUrlList	= json_decode($pUrlList,true);
		//echo"$parseUrlList";
		$totalRows		= urlTotalRows($url_base_list);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=adminEditGroupList&id_group=$id_group&searchmsisdn=".$searchname;
		?>
        <h2><?=$name_group;?></h2>
        <div class="frm-label-big">
        	<label><?=$lb_movil;?></label>
        	<input type="text" name="celular" id="celular" class="frm" value="<?=$searchmsisdn?>"> &nbsp;
        	<button type="button" class="frm-button" onClick="adminEditGroupListSearch('<?=$id_group;?>', '<?=$lan;?>');"><?=$b_consultar;?></button>
        </div>
        <div class="frm-label-big">
        	<button class="frm-button" onClick="goUrl('?module=adminEditGroupList&id_group=<?=$id_group;?>');"><?=$b_ver_todo;?></button> 
        	<button class="frm-button" onClick="goUrl('?module=adminAddPhoneGroupList&id_group=<?=$id_group?>');"><?=$b_individual;?></button> 
            <button class="frm-button" onClick="goUrl('?module=adminAddFileGroupList&id_group=<?=$id_group?>');"><?=$b_archivo;?></button>
            <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>
        <br>
        <?php
		
		if($totalRows == 0)
		{
			if (! empty($searchmsisdn))
			{
			?>
            <div class="show-message">
            <?php
			showMessage (9);
			}
			?>
            <?php
		}
		else
		{
			pag_pages ($page, $total_pages, $url_app);
			
			?>
			
			<table>
			<tr>
				<th><?=$tbdestinatarios;?></th>
                <th>fecha creacion</th>
                <th>creado por</th>
				<th><?=$tbaccion;?></th>
			</tr>
			<?php
			
			for($i = 0; $i < count($parseUrlList); $i++)
			{
				$id_mobile		= $parseUrlList[$i]['id_mobile'];
				$msisdn			= $parseUrlList[$i]['msisdn'];
				$fecha_creacion	= returnFormatDate($parseUrlList[$i]['created_time']);
				$creado_por		= $parseUrlList[$i]['created_by'];
				//echo"telefono :$msisdn";
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$msisdn;?> </td>
                    <td><?=$fecha_creacion;?> </td>
                    <td><?=$creado_por;?> </td>
					<td width="150">
					<?php
					echo "<div id=\"response$id_mobile\">" ;
					?>
						<a onClick="adminDeletePhoneGroupList('<?=$id_group;?>', '<?=$id_mobile;?>');" class="tip"><img src="images/icons/delete.png"  class="button-img"><span><?=$eliminar_movil;?></span></a>
					<?php echo "</div>"; ?>
					</td>
				</tr>
				<?php
			}
			?>
			</table>
			
			<?php
			pag_pages ($page, $total_pages, $url_app);
		}
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminAddPhoneGroupList ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_agregar_lista_individual[0]->titulo;
	$lb_movil			= $lan_file->$lan->adm_agregar_lista_individual[0]->movil;
	$lb_pais			= $lan_file->$lan->adm_agregar_lista_individual[0]->pais;
	$b_asociar			= $lan_file->$lan->botones[0]->b_asociar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("add_phone_list_group");
	
	if($permission == "Y") {
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$id_group		= $_GET['id_group'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/group/$id_group?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$id_group		= $parseUrl['id_group'];
		$name_group		= $parseUrl['name'];
		$idcountry		= $parseUrl['id_country'];
		$paises		= arrayCountry();
		$countrys	= groupArray($paises, "code");
		?>
        <h2><?=$name_group;?></h2>
        <div class="frm-label-big">
        	<label><?=$lb_pais;?></label>
                <select name="country" id="country" class="frm">
                    <?php
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
							$id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            if($id == $idcountry)
                            {
								?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?php
                            }
                        }
                    }
                ?>
                </select> &nbsp; (*)
            </div>
        <div class="frm-label-big">
        	<label><?=$lb_movil;?></label> 
        	<input type="text" name="msisdn" id="msisdn" class="frm"> &nbsp; (*)
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	(*) <?=$campo_obligatorio;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminAddPhoneGroupList('<?=$id_group;?>', '<?=$lan;?>');"><?=$b_asociar;?></button> <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>

        <br>
		<div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminAddFileGroupList ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_agregar_lista_archivo[0]->titulo;
	$lb_archivo			= $lan_file->$lan->adm_agregar_lista_archivo[0]->archivo;
	$lb_pais			= $lan_file->$lan->adm_agregar_lista_archivo[0]->pais;
	$b_asociar			= $lan_file->$lan->botones[0]->b_asociar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("add_file_list_group");
	
	if($permission == "Y") 
	{
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$id_group		= $_GET['id_group'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/group/$id_group?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$id_group		= $parseUrl['id_group'];
		$name_group		= $parseUrl['name'];
		$idcountry		= $parseUrl['id_country'];
		$paises			= arrayCountry();
		$countrys		= groupArray($paises, "code");
		?>
        <h2><?=$name_group;?></h2>
        
        <form id="formulario" method="POST" enctype="multipart/form-data">
        	<div class="frm-label-big">
        	<label><?=$lb_pais;?></label>
                <select name="country" id="country" class="frm">
                    <?php
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
							$id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            if($id == $idcountry)
                            {
								?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?php
                            }
                        }
                    }
                ?>
                </select> &nbsp; (*)  
            </div>
            <div class="frm-label-big">
            	<label><?=$lb_archivo;?></label>
            	<input type="file" name="archivo" id="phone_list" class="frm" />
                <input type="hidden" name="id_group" id="id_group" class="frm" value="<?=$id_group;?>" /> &nbsp; (*)
            </div>
            <div class="frm-label-big">
        	<label>&nbsp;</label>
        	(*) <?=$campo_obligatorio;?>
        </div>
            

        </form>
      
<div class="frm-label-big">
            	<label>&nbsp;</label>
            	<button type="button" class="frm-button"  onClick="adminAddFileGroupList('<?=$lan;?>');"><?=$b_asociar;?></button> <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
            </div>
        <br>
		<div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminStatusGroupList ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_borrar_lista_archivo[0]->titulo;
	$lb_nombre			= $lan_file->$lan->adm_borrar_lista_archivo[0]->nombre;
	$lb_estado			= $lan_file->$lan->adm_borrar_lista_archivo[0]->estado;
	$lb_pais			= $lan_file->$lan->adm_borrar_lista_archivo[0]->pais;
	$b_editar			= $lan_file->$lan->botones[0]->b_modificar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("del_phone_list_group");
	
	if($permission == "Y") {
		$id_group		= $_GET['id_group'];
		$id_country		= $_GET['id_country'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/group/$id_group?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$id_group		= $parseUrl['id_group'];
		$name_group		= $parseUrl['name'];
		$status			= $parseUrl['enabled'];
		
		$roles			= array(3 => 'Administrador', 4 => 'Operaci&oacute;n', 5 => 'Gesti&oacute;n');
		?>
       
        <div class="frm-label-big">
        	<label><?=$lb_nombre;?></label>
        	<?=$name_group;?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_estado;?></label>
        	<?php selectStatus($status); ?>
        </div>
         <div class="frm-label-big">
        	<label><?=$lb_pais;?></label>
        	<?=$id_country;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminStatusGroupList('<?=$id_group;?>', '<?=$lan;?>');"><?=$b_editar;?></button> <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminCostCenter ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_centro_costo[0]->titulo;
	$lb_nombre			= $lan_file->$lan->adm_centro_costo[0]->lbnombre;
	$tbcc				= $lan_file->$lan->adm_centro_costo[0]->tbcc;
	$tbnombre			= $lan_file->$lan->adm_centro_costo[0]->tbnombre;
	$tbcreado_el		= $lan_file->$lan->adm_centro_costo[0]->tbcreado_el;
	$tbcreado_por		= $lan_file->$lan->adm_centro_costo[0]->tbcreado_por;
	$tbaccion			= $lan_file->$lan->adm_centro_costo[0]->tbaccion;
	$activar_cc			= $lan_file->$lan->tips[0]->activar_centro_costo;
	$desactivar_cc		= $lan_file->$lan->tips[0]->desactivar_centro_costo;
	$modificar_cc		= $lan_file->$lan->tips[0]->modificar_centro_costo;
	$b_consultar		= $lan_file->$lan->botones[0]->b_consultar;
	$b_ver_todo			= $lan_file->$lan->botones[0]->b_ver_todo;
	$b_agregar_cc		= $lan_file->$lan->botones[0]->b_agregar_centrocosto;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_cost_center");
	
	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		
		?>
        <div class="frm-label-big">
        	<label><?=$lb_nombre;?></label>
        	<input type="text" name="username" id="username" class="frm" value="<?=$searchname?>">&nbsp;<button class="frm-button" onClick="AdminCostCenterSearch('<?=$lan;?>');"><?=$b_consultar;?></button> 
        </div>
        <div class="frm-label-big">
        	<button class="frm-button" onClick="goUrl('?module=adminCostCenter');"><?=$b_ver_todo;?></button> 
            <button class="frm-button" onClick="goUrl('?module=adminAddCostCenter');"><?=$b_agregar_cc;?></button>
        </div>
        <br>

        <?php
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		$data			.= "&searchname=".$searchname;
        $url_base		= URL_WS."WSA-Telcel/api/center?access_token=".$access_token."&searchname=".$searchname;
		
		$url			= URL_WS."WSA-Telcel/api/center?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= urlTotalRows($url_base);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=adminCostCenter&searchname=".$searchname;
		
		if($totalRows == 0)
		{
			if (! empty($searchname))
			{
			?>
            <div class="show-message">
            <?php
			showMessage (9);
			}
		}
		else
		{
			pag_pages ($page, $total_pages, $url_app);
				
			?>
			
			<table>
			<tr>
				<th><?=$tbcc;?></th>
				<th><?=$tbnombre;?></th>
                <th><?=$tbcreado_por;?></th>
				<th><?=$tbcreado_el;?></th>
				<th><?=$tbaccion;?></th>
			</tr>
			<?php
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$id_center		= $parseUrl[$i]['id_center'];
				$code			= $parseUrl[$i]['code'];
				$name			= $parseUrl[$i]['name'];
				$created_time	= returnFormatDate($parseUrl[$i]['created_time']);
				$username		= $parseUrl[$i]['username'];
				$enabled		= $parseUrl[$i]['enabled'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				?>
				<tr class="<?=$class;?>">
					<td><?=$code;?></td>
					<td><?=$name;?></td>
					<td><?=$username;?></td>
					<td><?=$created_time;?></td>
					<td width="150">
					<?php 
					echo"<div id=\"response$id_center\" style=\"max-width: 130px; height:20px;\">";
					?> 
						<a href="?module=adminEditCostCenter&id_center=<?=$id_center;?>" class="tip"><span><?=$modificar_cc;?></span><img src="images/icons/edit_user.png"  class="button-img"></a>
                        <?php
                        if($enabled == 0)
						{
							?>
                            <a href="?module=adminStatusCostCenter&id_center=<?=$id_center;?>" class="tip"><span><?=$activar_cc;?></span><img src="images/icons/confirm.png" class="button-img"></a>
                            <?php
						} else {
							?>
                            <a href="?module=adminStatusCostCenter&id_center=<?=$id_center;?>" class="tip"><span><?=$desactivar_cc;?></span><img src="images/icons/delete.png" class="button-img"></a>
                            <?php
						}?>
					<?php echo "</div>"?>
					</td>
				</tr>
				<?php
			}
			?>
			</table>
			
			<?php
			pag_pages ($page, $total_pages, $url_app);
		}
	}
	else
	{
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminAddCostCenter ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_agregar_centro_costo[0]->titulo;
	$lb_cc				= $lan_file->$lan->adm_agregar_centro_costo[0]->cc;
	$lb_nombre			= $lan_file->$lan->adm_agregar_centro_costo[0]->nombre;
	$b_agregar			= $lan_file->$lan->botones[0]->b_agregar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("add_cost_center");
	
	if($permission == "Y") {
		?>
        <div class="frm-label-big">
        	<label><?=$lb_cc;?></label>
        	<input type="text" name="code" id="code" class="frm" size="30">  &nbsp; (*)
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_nombre;?></label>
        	<input type="text" name="name_cc" id="name_cc" class="frm" size="35"> &nbsp; (*)
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	(*) <?=$campo_obligatorio;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminAddCostCenter('<?=$lan;?>');"><?=$b_agregar;?></button> <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>

        <br>
		<div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminEditCostCenter ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_editar_centro_costo[0]->titulo;
	$lb_cc				= $lan_file->$lan->adm_editar_centro_costo[0]->cc;
	$lb_nombre			= $lan_file->$lan->adm_editar_centro_costo[0]->nombre;
	$b_modificar		= $lan_file->$lan->botones[0]->b_modificar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("edit_cost_center");
	
	if($permission == "Y") {
		$id_center		= $_GET['id_center'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/center/$id_center?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		
		$code			= $parseUrl['code'];
		$name			= $parseUrl['name'];
		$created_time	= $parseUrl['created_time'];
		$username		= $parseUrl['username'];
		
		?>
        <div class="frm-label-big">
        	<label><?=$lb_cc;?></label>
        	<input type="text" name="code" id="code" class="frm" size="30" value="<?=$code;?>" readonly> 
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_nombre;?></label>
        	<input type="text" name="name_cc" id="name_cc" class="frm" size="35" value="<?=$name;?>"> &nbsp; (*)
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	(*) <?=$campo_obligatorio;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminEditCostCenter('<?=$id_center;?>', '<?=$lan;?>');"><?=$b_modificar;?></button> <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>

        <br>
		<div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function adminStatusCostCenter ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->adm_estado_centro_costo[0]->titulo;
	$lb_cc				= $lan_file->$lan->adm_estado_centro_costo[0]->lbcc;
	$lbestado			= $lan_file->$lan->adm_estado_centro_costo[0]->lbestado;
	$b_modificar		= $lan_file->$lan->botones[0]->b_modificar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("del_cost_center");
	
	if($permission == "Y") {
		$id_center		= $_GET['id_center'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/center/$id_center?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$code			= $parseUrl['code'];
		$name			= $parseUrl['name'];
		$created_time	= $parseUrl['created_time'];
		$username		= $parseUrl['username'];
		$status			= $parseUrl['enabled'];
		
		$roles			= array(3 => 'Administrador', 4 => 'Operaci&oacute;n', 5 => 'Gesti&oacute;n');
		?>
        <div class="frm-label-big">
        	<label><?=$lb_cc;?></label>
        	<?=$name;?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbestado;?></label>
        	<?php selectStatus($status); ?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminStatusCostCenter('<?=$id_center;?>', '<?=$lan;?>');"><?=$b_modificar;?></button> <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function provWhiteList ()
{
	$lan 					= $_SESSION['lang'];
	$lan_file				= simplexml_load_file('language.xml');
	$lb_titulo				= $lan_file->$lan->prov_lista_blanca[0]->titulo;
	$tbnombre				= $lan_file->$lan->prov_lista_blanca[0]->tbnombre;
	$tbcontrola				= $lan_file->$lan->prov_lista_blanca[0]->tbcontrola;
	$tbmaximo				= $lan_file->$lan->prov_lista_blanca[0]->tbmaximo;
	$tbactual				= $lan_file->$lan->prov_lista_blanca[0]->tbactual;
	$lista_blanca_servicios	= $lan_file->$lan->tips[0]->lista_blanca_servicios;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_white_list");

	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		?>    
        <br>
        <br>
        <?php
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		$data			.= "&searchname=".$searchname;
        $url_base		= URL_WS."WSA-Telcel/api/tgg/whitelist?access_token=".$access_token."&searchname=".$searchname;
		$url			= URL_WS."WSA-Telcel/api/tgg/whitelist?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= urlTotalRows($url_base);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=adminUser&searchname=".$searchname;
		
		if($totalRows == 0)
		{
			?>
            <div class="show-message">
            <?php
			showMessage (9);
			?>
            <?php
		}
		else
		{
			//pag_pages ($page, $total_pages, $url_app);
				
			?>
			
			<table>
			<tr>
				<th><?=$tbnombre;?></th>
				<th><?=$tbcontrola;?></th>
				<th><?=$tbmaximo;?></th>
				<th><?=$tbactual;?></th>
			</tr>
			<?php
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$id_list		= $parseUrl[$i]['id_list'];
				$list_name		= $parseUrl[$i]['list_name'];
				$control_lines	= $parseUrl[$i]['control_lines'];
				$max_lines		= $parseUrl[$i]['max_lines'];
				$current_lines	= $parseUrl[$i]['current_lines'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Suspendido";
				} else {
					$status = "Activo";
				}
				
				if($control_lines == "Y")
				{
					$control_lines = "Si";
				} else {
					$control_lines = "No";
				}
				?>
				<tr class="<?=$class;?>">
					<td><a onClick="viewWhiteList('<?=$id_list;?>', '<?=$lan;?>')" style="cursor: pointer;" class="tip"><span><?=$lista_blanca_servicios;?></span><?=$list_name;?></a></td>
					<td><?=$control_lines;?></td>
					<td><?=$max_lines;?></td>
					<td><?=$current_lines;?></td>
				</tr>
				<?php
			}
			?>
			</table>
			<br>
            <br>
            <div id="viewWhiteList"></div>
            
			<?php
			//pag_pages ($page, $total_pages, $url_app);
		}
	}
	else
	{
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function provWhiteListAdd ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->prov_lista_blanca_alta[0]->titulo;
	$lbnombre			= $lan_file->$lan->prov_lista_blanca_alta[0]->lbnombre;
	$lbservicio			= $lan_file->$lan->prov_lista_blanca_alta[0]->lbservicio;
	$lbalta_numero		= $lan_file->$lan->prov_lista_blanca_alta[0]->lbalta_numero;
	$lbalta_archivo		= $lan_file->$lan->prov_lista_blanca_alta[0]->lbalta_archivo;
	$lbmovil			= $lan_file->$lan->prov_lista_blanca_alta[0]->lbmovil;
	$lbarchivo			= $lan_file->$lan->prov_lista_blanca_alta[0]->lbarchivo;
	$b_alta				= $lan_file->$lan->botones[0]->b_alta;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$largo_prefijos		= $lan_file->$lan->tips[0]->largo_prefijos;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("add_white_list");
	
	if($permission == "Y") 
	{
		$access_token			= $_SESSION['access_token'];
		$id_list				= $_GET['id_list'];
		$id_service				= $_GET['id_service'];
       	$list_name				= getWhiteListName($id_list);
		$url					= URL_WS."WSA-Telcel/api/tgg/service/".$id_service."?access_token=".$access_token;
		$parseUrlTag 			= parseUrl($url);
		$service_name			= $parseUrlTag['service_name'];
		$service_tag			= $parseUrlTag['service_tag'];
		?>
        <div class="frm-label-big">
        	<label><?=$lbnombre;?></label>
        	<input type="text" name="fullname" id="fullname" class="frm" size="47" maxlength="45" value="<?=$list_name;?>" readonly>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbservicio;?></label>
        	<input type="text" name="username" id="username" class="frm" value="<?=$service_name;?>" size="50" readonly>
        </div>
        <div class="frm-label-medium">
        	<fieldset style="width:370px;">
            <legend><?=$lbalta_numero;?></legend>
            	<label><?=$lbmovil;?> &nbsp;(*)</label> <input type="text" name="msisdn" id="msisdn" class="frm" size="15"> 
                &nbsp; &nbsp; <a class="tip"><img src="images/icons/info.png" id="opener" onClick="info();" style="cursor: pointer;"><span><?=$largo_prefijos;?></span></a>
                <div id="dialog" title="<?=$largo_prefijos;?>" style="display: none;">
                  <?php getPrefMxCountry(); ?>
                </div>
                <br>
                <br>
            	<button class="frm-button" onClick="provWhiteListPhoneAdd('<?=$service_tag;?>', '<?=$lan;?>');"><?=$b_alta;?></button>
                <br>
                <br>
                <div id="provWhiteListPhone"></div>
            </fieldset>
        </div>
        <div class="frm-label-medium">
        	<form method="POST" id="formulario" enctype="multipart/form-data">
        	<fieldset style="width:420px;">
            <legend><?=$lbalta_archivo;?></legend>
            	<label><?=$lbarchivo;?> &nbsp;(*)</label> <input type="file" id="phone_list" name="phone_list" class="frm">
            	<br>
                <br>
            	<button type="button" class="frm-button" onClick="provWhiteListFileAdd('<?=$service_tag;?>', '<?=$lan;?>');"><?=$b_alta;?></button>
                <br>
                <br>
                <div id="provWhiteListFile"></div>
            </fieldset>
            </form>
        </div>
        <button class="frm-button" onClick="goUrl('?module=provWhiteList');"><?=$b_volver;?></button>
        <br>
        
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function provWhiteListDelete ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->prov_lista_blanca_baja[0]->titulo;
	$lbnombre			= $lan_file->$lan->prov_lista_blanca_baja[0]->lbnombre;
	$lbservicio			= $lan_file->$lan->prov_lista_blanca_baja[0]->lbservicio;
	$lbalta_numero		= $lan_file->$lan->prov_lista_blanca_baja[0]->lbalta_numero;
	$lbalta_archivo		= $lan_file->$lan->prov_lista_blanca_baja[0]->lbalta_archivo;
	$lbmovil			= $lan_file->$lan->prov_lista_blanca_baja[0]->lbmovil;
	$lbarchivo			= $lan_file->$lan->prov_lista_blanca_baja[0]->lbarchivo;
	$b_baja				= $lan_file->$lan->botones[0]->b_baja;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$largo_prefijos		= $lan_file->$lan->tips[0]->largo_prefijos;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("del_white_list");
	
	if($permission == "Y") 
	{
		$access_token			= $_SESSION['access_token'];
		$id_list				= $_GET['id_list'];
		$id_service				= $_GET['id_service'];
       	$list_name				= getWhiteListName($id_list);
		$url					= URL_WS."WSA-Telcel/api/tgg/service/".$id_service."?access_token=".$access_token;
		$parseUrlTag 			= parseUrl($url);
		$service_name			= $parseUrlTag['service_name'];
		$service_tag			= $parseUrlTag['service_tag'];
		?>
        <div class="frm-label-big">
        	<label><?=$lbnombre;?></label>
        	<input type="text" name="fullname" id="fullname" class="frm" size="47" maxlength="45" value="<?=$list_name;?>" readonly>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbservicio;?></label>
        	<input type="text" name="username" id="username" class="frm" value="<?=$service_name;?>" readonly>
        </div>
        <div class="frm-label-medium">
        	<fieldset style="width:370px;">
            <legend><?=$lbalta_numero;?></legend>
            	<label><?=$lbmovil;?> &nbsp; (*)</label> <input type="text" name="msisdn" id="msisdn" class="frm" size="15"> 
                &nbsp; &nbsp; <a class="tip"><img src="images/icons/info.png" id="opener" onClick="info();" style="cursor: pointer;"><span><?=$largo_prefijos;?></span></a>
                <div id="dialog" title="<?=$largo_prefijos;?>" style="display: none;">
                  <?php getPrefMxCountry(); ?>
                </div>
                <br>
                
                <br>
            	<button class="frm-button" onClick="provWhiteListPhoneDelete('<?=$service_tag;?>', '<?=$lan;?>');"><?=$b_baja;?></button> 
                <br>
                <br>
                <div id="provWhiteListPhone"></div>
            </fieldset>
        </div>
        
        <div class="frm-label-medium">
        	<form method="POST" id="formulario" enctype="multipart/form-data">
        	<fieldset style="width:420px;">
            <legend><?=$lbalta_archivo;?></legend>
            	<label><?=$lbarchivo;?> &nbsp; (*)</label> <input type="file" id="phone_list" name="phone_list" class="frm">
            	<br>
                
                <br>
            	<button type="button" class="frm-button" onClick="provWhiteListFileDelete('<?=$service_tag;?>', '<?=$lan;?>');"><?=$b_baja;?></button>
                <br>
                <br>
                <div id="provWhiteListFile"></div>
            </fieldset>
            </form>
        </div>
        <button class="frm-button" onClick="goUrl('?module=provWhiteList');"><?=$b_volver;?></button>
        <br>
        
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function provWhiteListSearch ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->prov_lista_blanca_buscar[0]->titulo;
	$lbnombre			= $lan_file->$lan->prov_lista_blanca_buscar[0]->lbnombre;
	$lbservicio			= $lan_file->$lan->prov_lista_blanca_buscar[0]->lbservicio;
	$lbalta_numero		= $lan_file->$lan->prov_lista_blanca_buscar[0]->lbalta_numero;
	$lbmovil			= $lan_file->$lan->prov_lista_blanca_buscar[0]->lbmovil;
	$b_buscar			= $lan_file->$lan->botones[0]->b_buscar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$largo_prefijos		= $lan_file->$lan->tips[0]->largo_prefijos;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("find_white_list");
	
	if($permission == "Y") 
	{
		$access_token			= $_SESSION['access_token'];
		$id_list				= $_GET['id_list'];
		$id_service				= $_GET['id_service'];
		$msisdn					= trim($_GET['msisdn']);
		
       	$list_name				= getWhiteListName($id_list);
		
		$url					= URL_WS."WSA-Telcel/api/tgg/service/".$id_service."?access_token=".$access_token;
		$parseUrlTag 			= parseUrl($url);
		$service_name			= $parseUrlTag['service_name'];
		$service_tag			= $parseUrlTag['service_tag'];
		?>
        <div class="frm-label-big">
        	<label><?=$lbnombre;?></label>
        	<input type="text" name="fullname" id="fullname" class="frm" size="47" value="<?=$list_name;?>" readonly>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbservicio;?></label>
        	<input type="text" name="username" id="username" class="frm" size="47" value="<?=$service_name;?>" readonly>
        </div>
        <div class="frm-label-medium">
        	<fieldset style="width:370px;">
            <legend><?=$lbalta_numero;?></legend>
            	<label><?=$lbmovil;?></label> <input type="text" name="msisdn" id="msisdn" value="<?=$msisdn;?>" class="frm" size="15">
                &nbsp; &nbsp; <a class="tip"><img src="images/icons/info.png" id="opener" onClick="info();" style="cursor: pointer;"><span><?=$largo_prefijos;?></span></a>
                <div id="dialog" title="<?=$largo_prefijos;?>" style="display: none;">
                  <?php getPrefMxCountry(); ?>
                </div>
                <br>
                <br>
            	<button class="frm-button" onClick="provWhiteListSearch('<?=$id_list;?>','<?=$id_service;?>');"><?=$b_buscar;?></button> <button class="frm-button" onClick="goUrl('?module=provWhiteList');"><?=$b_volver;?></button>
            </fieldset>
        </div>
        <br>
        <?php
		
		if(!empty($msisdn)) { $data .= "&msisdn=".$msisdn; }
		if(!empty($id_list)) { $data .= "&id_list=".$id_list; }
		if(!empty($id_service)) { $data .= "&id_service=".$id_service; }
		
		if(!empty($msisdn))
		{
			$urlSr					= URL_WS."WSA-Telcel/api/tgg/whitelist/validate?access_token=".$access_token.$data;
			$iUrlSr					= curl_init($urlSr);
			curl_setopt($iUrlSr, CURLOPT_RETURNTRANSFER, TRUE);
			$pUrlSr					= curl_exec($iUrlSr);
			$parseUrlSr				= json_decode($pUrlSr,true);
			?>
			<div class="show-message">
			<?php
			if($parseUrlSr == 0)
			{
				showMessage (53);
			} else if($parseUrlSr == 1)
			{
				showMessage (52);
			}
			?>
			</div>
			<?php
		}
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function provBlackList ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->prov_lista_negra[0]->titulo;
	$tbnombre			= $lan_file->$lan->prov_lista_negra[0]->tbnombre;
	$tbmarcacion		= $lan_file->$lan->prov_lista_negra[0]->tbmarcacion;
	$tbtiposervicio		= $lan_file->$lan->prov_lista_negra[0]->tbtiposervicio;
	$tbpais				= $lan_file->$lan->prov_lista_negra[0]->tbpais;
	$tbaccion			= $lan_file->$lan->prov_lista_negra[0]->tbaccion;
	$alta				= $lan_file->$lan->tips[0]->alta;
	$baja				= $lan_file->$lan->tips[0]->baja;
	$buscar				= $lan_file->$lan->tips[0]->buscar;
	$lista_negra_moviles= $lan_file->$lan->tips[0]->lista_negra_moviles;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_black_list");

	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		?>    
        <br>
        <br>
        <?php
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		$data			.= "&searchname=".$searchname;
        $url_base		= URL_WS."WSA-Telcel/api/tgg/blacklist?access_token=".$access_token."&searchname=".$searchname;
		$url		= URL_WS."WSA-Telcel/api/tgg/blacklist?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= urlTotalRows($url_base);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=adminUser&searchname=".$searchname;
		
		
		
		if($totalRows == 0)
		{
			?>
            <div class="show-message">
            <?php
			showMessage (9);
			?>
            <?php
		}
		else
		{
			//pag_pages ($page, $total_pages, $url_app);
				
			?>
			
			<table>
			<tr>
				<th><?=$tbnombre;?></th>
				<th><?=$tbmarcacion;?></th>
				<th><?=$tbtiposervicio;?></th>
				<th><?=$tbpais;?></th>
                <th><?=$tbaccion;?></th>
			</tr>
			<?php
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$id_service		= $parseUrl[$i]['id_service'];
				$service_type	= $parseUrl[$i]['service_type'];
				$service_name	= $parseUrl[$i]['service_name'];
				$service_tag	= $parseUrl[$i]['service_tag'];
				$country_name	= $parseUrl[$i]['country_name'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Suspendido";
				} else {
					$status = "Activo";
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$service_name;?></td>
					<td><?=$service_tag;?></td>
					<td><?=$service_type;?></td>
                    <td><?=$country_name;?></td>
                    <td>
                    <a href="?module=provBlackListAdd&id_list=<?=$id_service;?>" class="tip"><span><?=$alta;?></span><img src="images/icons/up.png"></a> &nbsp;&nbsp;
                    <a href="?module=provBlackListDelete&id_list=<?=$id_service;?>" class="tip"><span><?=$baja;?></span><img src="images/icons/down.png"></a> &nbsp;&nbsp;
                    <a href="?module=provBlackListSearch&id_service=<?=$id_service;?>" class="tip"><span><?=$buscar;?></span><img src="images/icons/search.png"></a> 
                    <a href="?module=provBlackListMobiles&id_service=<?=$id_service;?>" class="tip"><span><?=$lista_negra_moviles;?></span><img src="images/icons/movil.png"></a></td>
				</tr>
				<?php
			}
			?>
			</table>
			<br>
            <br>

            
			<?php
			//pag_pages ($page, $total_pages, $url_app);
		}
	}
	else
	{
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function provBlackListAdd ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->prov_lista_negra_alta[0]->titulo;
	$lbservicio			= $lan_file->$lan->prov_lista_negra_alta[0]->lbservicio;
	$lbalta_numero		= $lan_file->$lan->prov_lista_negra_alta[0]->lbalta_numero;
	$lbalta_archivo		= $lan_file->$lan->prov_lista_negra_alta[0]->lbalta_archivo;
	$lbmovil			= $lan_file->$lan->prov_lista_negra_alta[0]->lbmovil;
	$lbarchivo			= $lan_file->$lan->prov_lista_negra_alta[0]->lbarchivo;
	$b_alta				= $lan_file->$lan->botones[0]->b_alta;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$largo_prefijos		= $lan_file->$lan->tips[0]->largo_prefijos;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("add_black_list");
	
	if($permission == "Y") 
	{
		$access_token			= $_SESSION['access_token'];
		$id_list				= $_GET['id_list'];
		$msisdn					= $_GET['msisdn'];
		$url					= URL_WS."WSA-Telcel/api/tgg/service/".$id_list."?access_token=".$access_token;
		$parseUrlTag = parseUrl($url);
		$service_name			= $parseUrlTag['service_name'];
		$service_tag			= $parseUrlTag['service_tag'];
		?>
        <div class="frm-label-big">
        	<label><?=$lbservicio;?></label>
        	<input type="text" name="username" id="username" class="frm" value="<?=$service_name;?>" readonly>
        </div>
        <div class="frm-label-medium">
        	<fieldset style="width:370px;">
            <legend><?=$lbalta_numero;?></legend>
            	<label><?=$lbmovil;?> &nbsp;(*)</label> <input type="text" name="msisdn" id="msisdn" class="frm" size="15"> 
                &nbsp; &nbsp; <a class="tip"><img src="images/icons/info.png" id="opener" onClick="info();" style="cursor: pointer;"><span><?=$largo_prefijos;?></span></a>
                <div id="dialog" title="<?=$largo_prefijos;?>" style="display: none;">
                  <?php getPrefMxCountry(); ?>
                </div>
                <br>
                <br>
            	<button class="frm-button" onClick="provBlackListPhoneAdd('<?=$service_tag;?>', '<?=$lan;?>');"><?=$b_alta;?></button>
                <br>
                <br>
                <div id="provBlackListPhone"></div>
            </fieldset>
        </div>
        <div class="frm-label-medium">
        	<form method="POST" id="formulario" enctype="multipart/form-data">
        	<fieldset style="width:420px;">
            <legend><?=$lbalta_archivo;?></legend>
            	<label><?=$lbarchivo;?> &nbsp;(*)</label> <input type="file" id="phone_list" name="phone_list" class="frm">
            	<br>
                <br>
            	<button type="button" class="frm-button" onClick="provBlackListFileAdd('<?=$service_tag;?>', '<?=$lan;?>');"><?=$b_alta;?></button>
                <br>
                <br>
                <div id="provBlackListFile"></div>
            </fieldset>
            </form>
        </div>
        <button class="frm-button" onClick="goUrl('?module=provBlackList');"><?=$b_volver;?></button>
        <br>
        
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function provBlackListDelete ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->prov_lista_negra_baja[0]->titulo;
	$lbservicio			= $lan_file->$lan->prov_lista_negra_baja[0]->lbservicio;
	$lbalta_numero		= $lan_file->$lan->prov_lista_negra_baja[0]->lbalta_numero;
	$lbalta_archivo		= $lan_file->$lan->prov_lista_negra_baja[0]->lbalta_archivo;
	$lbmovil			= $lan_file->$lan->prov_lista_negra_baja[0]->lbmovil;
	$lbarchivo			= $lan_file->$lan->prov_lista_negra_baja[0]->lbarchivo;
	$b_baja				= $lan_file->$lan->botones[0]->b_baja;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$largo_prefijos		= $lan_file->$lan->tips[0]->largo_prefijos;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("del_black_list");
	
	if($permission == "Y") 
	{
		$access_token			= $_SESSION['access_token'];
		$id_list				= $_GET['id_list'];
		$msisdn					= $_GET['msisdn'];
		$url					= URL_WS."WSA-Telcel/api/tgg/service/".$id_list."?access_token=".$access_token;
		$parseUrlTag 			= parseUrl($url);
		$service_name			= $parseUrlTag['service_name'];
		$service_tag			= $parseUrlTag['service_tag'];
		?>
        <div class="frm-label-big">
        	<label><?=$lbservicio;?></label>
        	<input type="text" name="username" id="username" class="frm" value="<?=$service_name;?>" readonly>
        </div>
        <div class="frm-label-medium">
        	<fieldset style="width:370px;">
            <legend><?=$lbalta_numero;?></legend>
            	<label><?=$lbmovil;?> &nbsp;(*)</label> <input type="text" name="msisdn" id="msisdn" class="frm" size="15">
                &nbsp; &nbsp; <a class="tip"><img src="images/icons/info.png" id="opener" onClick="info();" style="cursor: pointer;"><span><?=$largo_prefijos;?></span></a>
                <div id="dialog" title="<?=$largo_prefijos;?>" style="display: none;">
                  <?php getPrefMxCountry(); ?>
                </div>
                <br>
                <br>
            	<button class="frm-button" onClick="provBlackListPhoneDelete('<?=$service_tag;?>', '<?=$lan;?>');"><?=$b_baja;?></button>
                <br>
                <br>
                <div id="provBlackListPhone"></div>
            </fieldset>
        </div>
        <div class="frm-label-medium">
        	<form method="POST" id="formulario" enctype="multipart/form-data">
        	<fieldset style="width:420px;">
            <legend><?=$lbalta_archivo;?></legend>
            	<label><?=$lbarchivo;?> &nbsp;(*)</label> <input type="file" id="phone_list" name="phone_list" class="frm">
            	<br>
                <br>
            	<button type="button" class="frm-button" onClick="provBlackListFileDelete('<?=$service_tag;?>', '<?=$lan;?>');"><?=$b_baja;?></button>
                <br>
                <br>
                <div id="provBlackListFile"></div>
            </fieldset>
            </form>
        </div>
        <button class="frm-button" onClick="goUrl('?module=provBlackList');"><?=$b_volver;?></button>
        <br>
        
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function provBlackListSearch ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->prov_lista_negra_buscar[0]->titulo;
	$lbservicio			= $lan_file->$lan->prov_lista_negra_buscar[0]->lbservicio;
	$lbalta_numero		= $lan_file->$lan->prov_lista_negra_buscar[0]->lbalta_numero;
	$lbmovil			= $lan_file->$lan->prov_lista_negra_buscar[0]->lbmovil;
	$b_buscar			= $lan_file->$lan->botones[0]->b_buscar;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	$largo_prefijos		= $lan_file->$lan->tips[0]->largo_prefijos;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("find_black_list");
	
	if($permission == "Y") 
	{
		$access_token			= $_SESSION['access_token'];
		$id_service				= $_GET['id_service'];
		$msisdn					= $_GET['msisdn'];
		$url					= URL_WS."WSA-Telcel/api/tgg/service/".$id_service."?access_token=".$access_token;
		$parseUrlTag = parseUrl($url);
		$service_name			= $parseUrlTag['service_name'];
		$service_tag			= $parseUrlTag['service_tag'];
		
		?>
        <div class="frm-label-big">
        	<label><?=$lbservicio;?></label>
        	<input type="text" name="username" id="username" class="frm" size="47" value="<?=$service_name;?>" readonly>
        </div>
        <div class="frm-label-medium">
        	<fieldset style="width:370px;">
            <legend><?=$lbalta_numero;?></legend>
            	<label><?=$lbmovil;?></label> <input type="text" name="msisdn" id="msisdn" value="<?=$msisdn;?>" class="frm" size="15">
                &nbsp; &nbsp; <a class="tip"><img src="images/icons/info.png" id="opener" onClick="info();" style="cursor: pointer;"><span><?=$largo_prefijos;?></span></a>
                <div id="dialog" title="<?=$largo_prefijos;?>" style="display: none;">
                  <?php getPrefMxCountry(); ?>
                </div>
                <br>
                <br>
            	<button class="frm-button" onClick="provBlackListSearch('<?=$id_service;?>');"><?=$b_buscar;?></button> <button class="frm-button" onClick="goUrl('?module=provBlackList');"><?=$b_volver;?></button>
            </fieldset>
        </div>
        <br>
        <?php
		
		if(!empty($msisdn)) { $data .= "&msisdn=".$msisdn; }
		if(!empty($id_service)) { $data .= "&id_service=".$id_service; }
		
		if(!empty($msisdn))
		{
				
			$urlSr					= URL_WS."WSA-Telcel/api/tgg/blacklist/validate?access_token=".$access_token.$data;
			$iUrlSr						= curl_init($urlSr);
			curl_setopt($iUrlSr, CURLOPT_RETURNTRANSFER, TRUE);
			$pUrlSr						= curl_exec($iUrlSr);
			$parseUrlSr					= json_decode($pUrlSr,true);
			$validate_blists			= $parseUrlSr['validate_blists'];
			$validate_blists_service	= $parseUrlSr['validate_blists_service'];
			?>
			<div class="show-message">
			<?php
			if($validate_blists == 0)
			{
				showMessage (55); echo "<br>";
			} else if($validate_blists == 1)
			{
				showMessage (54); echo "<br>";
			}
			
			if($validate_blists_service == 0)
			{
				showMessage (57);
			} else if($validate_blists_service == 1)
			{
				showMessage (56);
			}
			?>
			</div>
			<?php
		}
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function provBlackListMobiles ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->prov_lista_negra_moviles[0]->titulo;
	$lb_subtitulo		= $lan_file->$lan->prov_lista_negra_moviles[0]->subtitulo;
	$tbmoviles			= $lan_file->$lan->prov_lista_negra_moviles[0]->tbmoviles;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_report_black_list");

	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		?>    
        <?php
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token		= $_SESSION['access_token'];
		$id_service		= $_GET['id_service'];
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		$data			.= "&id_service=".$id_service;
        $url_base			= URL_WS."WSA-Telcel/api/tgg/service/restricted?access_token=".$access_token."&id_service=".$id_service;
		$url			= URL_WS."WSA-Telcel/api/tgg/service/restricted?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= urlTotalRows($url_base);
		$total_pages		= ceil($totalRows / $limit);
		$url_app		= "?module=provBlackListMobiles&id_service=".$id_service;
		$name_serv		= getBlackListName($id_service);
		
		?>
        <h2><?=$lb_subtitulo;?> <?=$name_serv;?></h2>
        <button class="frm-button" onClick="goUrl('?module=provBlackList');"><?=$b_volver;?></button>
        <br>
        <br>
        <?php
		if($totalRows == 0)
		{
			?>
            <div class="show-message">
            <?php
			showMessage (9);
			?>
            <?php
		}
		else
		{
			$urlRepTotal	= URL_WS."WSA-Telcel/api/tgg/service/restricted/file?access_token=".$access_token."&id_service=".$id_service;
			$fileRepTotal	= getFileReport($urlRepTotal);		
			?>
			<div class="show-message">
                <a href="ajaxFuncs/repBlackListMobiles.php?report=<?=$fileRepTotal;?>"><?php showMessage(58); ?></a>
            </div>
            <?php pag_pages ($page, $total_pages, $url_app); ?>
			<table>
			<tr>
				<th><?=$tbmoviles;?></th>
			</tr>
			<?php
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$movil		= $parseUrl[$i];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				?>
				<tr class="<?=$class;?>">
					<td><?=$movil;?></td>
				</tr>
				<?php
			}
			?>
			</table>
			<br>
            <br>
            <div id="viewWhiteList"></div>
            
			<?php
			//pag_pages ($page, $total_pages, $url_app);
		}
	}
	else
	{
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function provRejected ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->prov_rechazos[0]->titulo;
	$lbfechainicio		= $lan_file->$lan->prov_rechazos[0]->lbfechainicio;
	$lbfechafin			= $lan_file->$lan->prov_rechazos[0]->lbfechafin;
	$lbtipolista		= $lan_file->$lan->prov_rechazos[0]->lbtipolista;
	$lbaccion			= $lan_file->$lan->prov_rechazos[0]->lbaccion;
	$tbfecha			= $lan_file->$lan->prov_rechazos[0]->tbfecha;
	$tbarchivo			= $lan_file->$lan->prov_rechazos[0]->tbarchivo;
	$tbtipolista		= $lan_file->$lan->prov_rechazos[0]->tbtipolista;
	$tbaccion			= $lan_file->$lan->prov_rechazos[0]->tbaccion;
	$tbtotal			= $lan_file->$lan->prov_rechazos[0]->tbtotal;
	$tbdescarga			= $lan_file->$lan->prov_rechazos[0]->tbdescarga;
	$b_consultar		= $lan_file->$lan->botones[0]->b_consultar;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_report_rejected_msg");

	if($permission == "Y") {
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		$type_list		= $_GET['type_list'];
		$type_action	= $_GET['type_action'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
		
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($type_list)) { $data_extra .= "&searchtype=".$type_list; }
		if(!empty($type_action)) { $data_extra .= "&searchaction=".$type_action; }
		$url			= URL_WS."WSA-Telcel/api/process/wb/consolidated?access_token=".$access_token.$data.$data_extra;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= count($parseUrl);
		
		if(!empty($start_date))
		{
			$dateFrom	= $start_date;
			$dateTo		= $end_date;
		} else {
			$dateFrom	= date('01-m-Y');
			$dateTo		= date('d-m-Y');
		}
		
		?>
		<div class="frm-label-big">
        	<label><?=$lbfechainicio;?></label>
        	<input type="text" name="start_date" id="start_date" class="frm" value="<?=$dateFrom;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador1">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbfechafin;?></label>
        	<input type="text" name="end_date" id="end_date" class="frm" value="<?=$dateTo;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador2">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbtipolista;?></label>
        	<?php selectTypeList($type_list); ?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbaccion;?></label>
        	<?php selectTypeListAction($type_action); ?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>  
	        <button class="frm-button" onClick="provRejected();"><?=$b_consultar;?></button> 
        </div>
        <br>
        <br>
        <br>
        <br>
        <script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "start_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador1"
            }
            )
        </script>
        
        <script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "end_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador2"
            }
            )
        </script>
        <?php
        
		if(!empty($start_date))
		{
			if($totalRows == 0)
			{
				?>
                <div class="show-message">
                <?php
                showMessage (9);
                ?>
                </div>
                <?php
			}
			else
			{
				?>			
				<table>
				<tr>
					<th><?=$tbfecha;?></th>
                    <th><?=$tbarchivo;?></th>
                    <th><?=$tbtipolista;?></th>
                    <th><?=$tbaccion;?></th>
					<th><?=$tbtotal;?></th>
                    <th><?=$tbdescarga;?></th>
				</tr>
				<?php
				
				for($i = 0; $i < count($parseUrl); $i++)
				{
					$report_date	= returnFormatDate($parseUrl[$i]['created_time']);
					$report_file	= $parseUrl[$i]['created_time'];
					$filename		= $parseUrl[$i]['file_name'];
					$pid			= $parseUrl[$i]['pid'];
					$type			= $parseUrl[$i]['input_mode'];
					$input_mode		= getTypeListName(substr($parseUrl[$i]['input_mode'], 0,2));
					$action			= getTypeListActionName(substr($parseUrl[$i]['input_mode'], 3,3));
					$count			= $parseUrl[$i]['total'];
					$report_xls		= substr($report_file, 0,10);
					$url_excel		= URL_WS."WSA-Telcel/api/process/wb/detailed?access_token=".$access_token."&id_country=".$id_country."&id_process=".$pid."&type=".$type.$data_extra;
					$fileReport		= getFileReport($url_excel);
					
					$tr_color = $i % 2;
					
					if($tr_color == 0)
					{
						$class	= ""; 
					} else {
						$class	= "tr-color"; 
					}
					
					if($statusCode == 0)
					{
						$status = "Suspendido";
					} else {
						$status = "Activo";
					}
					?>
					<tr class="<?=$class;?>">
						<td><?=$report_date;?></td>
						<td><?=$filename;?></td>
                        <td><?=$input_mode;?></td>
                        <td><?=$action;?></td>
						<td><?=$count;?></td>
                        <td><a href="ajaxFuncs/repProvRejected.php?report=<?=$fileReport;?>" class="tip"><span>Descargar</span><img src="images/icons/excel.png"></a></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<th></th>
					<th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
				</tr>
				</table>
				
				<?php
			}
		}
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function singleMessage ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->envio_individual[0]->titulo;
	$lbpais				= $lan_file->$lan->envio_individual[0]->lbpais;
	$lbservicio			= $lan_file->$lan->envio_individual[0]->lbservicio;
	$lbcentrocosto		= $lan_file->$lan->envio_individual[0]->lbcentrocosto;
	$lbtipoenvio		= $lan_file->$lan->envio_individual[0]->lbtipoenvio;
	$lbmovil			= $lan_file->$lan->envio_individual[0]->lbmovil;
	$lbtipotexto		= $lan_file->$lan->envio_individual[0]->lbtipotexto;
	$lbmensaje			= $lan_file->$lan->envio_individual[0]->lbmensaje;
	$b_enviar			= $lan_file->$lan->botones[0]->b_enviar;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	$seleccionar		= $lan_file->$lan->general[0]->seleccionar;
	$largo_prefijos		= $lan_file->$lan->tips[0]->largo_prefijos;
	$tipo_texto			= $lan_file->$lan->msg_js[0]->tipo_texto;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("send_msg_individual");

	if($permission == "Y") 
	{
		$access_token	= $_SESSION['access_token'];
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$services		= arrayService();
		$countrys		= groupArray($paises, "code");

		?>
        
        <form method="POST">
        	

        	<div class="frm-label-medium">
            	<label><?=$lbpais;?></label> 
                <select name="country" id="country" class="frm" onChange="selectServiceCountrySingle(this.value, '<?=$lan;?>');">
                    <option value=""><?=$seleccionar;?></option>
                    <?php
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
							$id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$id?>"> <?=$ct?></option>
                                    <?php
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?php
                            }
                        }
                    }
                ?>
                </select> <div id="getMaxCountry"></div> &nbsp;(*)
            </div>
            <div class="frm-label-medium">
            <label><?=$lbservicio;?></label> 
                <select id="service" name="service" class="frm">
                	<option value=""><?=$seleccionar;?></option>
                </select> &nbsp;(*)
            </div>
            <div class="frm-label-big">
            <label><?=$lbcentrocosto;?></label> 
            	<?php selectCostCenter(); ?> &nbsp;
            </div>
            <div class="frm-label-big">
            	<label><?=$lbtipoenvio;?></label> 
            	<?php selectTypeSend();?> &nbsp;(*)
            </div>
            <div class="frm-label-big">
                <div id="typeSendTime" style="display:none;"> 
       	    <label>&nbsp;</label>
           	  <input type="text" name="dispatchTime" id="dispatchTime" class="frm" size="16" readonly>
                  <label><img src="images/icons/calendar.png" id="lanzador1"></label>
                  <script type="text/javascript" charset="UTF-8">
                    var fecha = new Date();
                    var year=fecha.getFullYear();
                        Calendar.setup({ inputField  : "dispatchTime", range       :  [2015, 2999],
                        ifFormat    : "%d-%m-%Y",
                        showsTime      :    false,
                        timeFormat     :    "24",
                        button      : "lanzador1"
                        }
                        )
                    </script>
              <input type="text" name="sendDateFinish" id="sendDateFinish" class="frm" size="16" value="<?=$sendDateFinish?>" readonly>
            <img src="images/icons/calendar.png" >
          
                    <script src="js/timepicki.js"></script>
                    <script>
					$('#sendDateFinish').timepicki({
						show_meridian:false,
						min_hour_value:0,
						max_hour_value:23,
						//step_size_minutes:0,
						overflow_minutes:true,
						increase_direction:'up',
						disable_keyboard_mobile: true});
    				</script>
	
					
   			<script src="js/bootstrap.min.js"></script>
            </div>
            </div>
            <div class="frm-label-big">
            	<label><?=$lbmovil;?></label> 
            	<input type="text" name="msisdn" id="msisdn" class="frm"> &nbsp;(*) 
                &nbsp; &nbsp; <a class="tip"><img src="images/icons/info.png" id="opener" onClick="info();" style="cursor: pointer;"><span><?=$largo_prefijos;?></span></a>
                <div id="dialog" title="<?=$largo_prefijos;?>" style="display: none;">
                  <?php getPrefMxCountry(); ?>
                </div>
            </div>
            <div class="frm-label-big">
            	<label><?=$lbtipotexto;?></label> 
            	<?php selectDataCoding();?> &nbsp;(*)
            </div>
            <div id="typeDataCoding">
                <div class="frm-label-big">
                    <label><?=$lbmensaje;?></label>
                    <textarea name="message" id="message" class="frm" cols="50" rows="6" onFocus="alert('<?=$tipo_texto;?>');" readonly></textarea>       &nbsp;(*) 
                </div>
            	<div class="frm-label-big">
                    <label>&nbsp;</label> <input type="text" name="remLen" value="0" size="3" class="frm" readonly>                
                </div>
            </div>
            <div class="frm-label-big">
        		<label>&nbsp;</label>
        		(*) <?=$campo_obligatorio;?>
        	</div>
            <div class="frm-label-big">
                <label>&nbsp;</label> <button type="button" class="frm-button" onClick="singleMessage('<?=$lan;?>');"><?=$b_enviar;?></button> 
            </div>            
        </form>
        <br>

        <br>
		<div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function groupMessage ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->envio_lista[0]->titulo;
	$lbpais				= $lan_file->$lan->envio_lista[0]->lbpais;
	$lbservicio			= $lan_file->$lan->envio_lista[0]->lbservicio;
	$lblista			= $lan_file->$lan->envio_lista[0]->lblista;
	$lbcentrocosto		= $lan_file->$lan->envio_lista[0]->lbcentrocosto;
	$lbtipoenvio		= $lan_file->$lan->envio_lista[0]->lbtipoenvio;
	$lbmovil			= $lan_file->$lan->envio_lista[0]->lbmovil;
	$lbtipotexto		= $lan_file->$lan->envio_lista[0]->lbtipotexto;
	$lbmensaje			= $lan_file->$lan->envio_lista[0]->lbmensaje;
	$tipo_texto			= $lan_file->$lan->msg_js[0]->tipo_texto;
	$b_enviar			= $lan_file->$lan->botones[0]->b_enviar;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	$seleccionar		= $lan_file->$lan->general[0]->seleccionar;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("send_msg_group");

	if($permission == "Y") {
		$type			= $_SESSION['type'];
		//$type			= "LOCAL";
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$data_coding	= arrayDataCoding();
		$services		= arrayService();
		$countrys		= groupArray($paises, "code");
		?>
        
        <form method="POST">
            <div class="frm-label-medium">
            	<label><?=$lbpais;?></label> 
                <select name="country" id="country" class="frm" onChange="selectServiceCountry(this.value, '<?=$lan;?>');">
                    <option value=""><?=$seleccionar;?></option>
                    <?php
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
							$id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$id?>"> <?=$ct?></option>
                                    <?php
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?php
                            }
                        }
                    }
                ?>
                </select> &nbsp;(*)
            </div>
            <div class="frm-label-medium">
            <label><?=$lbservicio;?></label> 
                <select id="service" name="service" class="frm" onChange="selectSendListCountry();">
                	<option value=""><?=$seleccionar;?></option> 
                </select> &nbsp;(*)
            </div>
            <div class="frm-label-big">
            	<label><?=$lblista;?></label> 
            	<select id="id_group" name="id_group" class="frm">
                	<option value=""><?=$seleccionar;?></option>
                </select> &nbsp;(*)
                <div id="selectSendListCountry"></div>
            </div>
            <div class="frm-label-big">
            	<label><?=$lbcentrocosto;?></label> 
            	<?php selectCostCenter(); ?> &nbsp;
            </div>
            <div class="frm-label-big">
            	<label><?=$lbtipoenvio;?></label> 
            	<?php selectTypeSend();?> &nbsp;(*)
            </div>
            <div class="frm-label-big">
                <div id="typeSendTime" style="display:none;">
                	<label>&nbsp;</label>
           	  <input type="text" name="dispatchTime" id="dispatchTime" class="frm" size="16" readonly>
                  <label><img src="images/icons/calendar.png" id="lanzador1"></label>
                  <script type="text/javascript" charset="UTF-8">
                    var fecha = new Date();
                    var year=fecha.getFullYear();
                        Calendar.setup({ inputField  : "dispatchTime", range       :  [2015, 2999],
                        ifFormat    : "%d-%m-%Y",
                        showsTime      :    false,
                        timeFormat     :    "24",
                        button      : "lanzador1"
                        }
                        )
                    </script>
              <input type="text" name="sendDateFinish" id="sendDateFinish" class="frm" size="16" value="<?=$sendDateFinish?>" readonly>
            <img src="images/icons/calendar.png" >
          
                    <script src="js/timepicki.js"></script>
                    <script>
					$('#sendDateFinish').timepicki({
						show_meridian:false,
						min_hour_value:0,
						max_hour_value:23,
						//step_size_minutes:0,
						overflow_minutes:true,
						increase_direction:'up',
						disable_keyboard_mobile: true});
    				</script>
	
					
   			<script src="js/bootstrap.min.js"></script>
            </div>
            </div>
            <div class="frm-label-big">
            	<label><?=$lbtipotexto;?></label> 
            	<?php selectDataCoding();?> &nbsp;(*)
            </div>
            <div id="typeDataCoding">
                <div class="frm-label-big">
                    <label><?=$lbmensaje;?></label>
                    <textarea name="message" id="message" class="frm" cols="50" rows="6" onFocus="alert('<?=$tipo_texto;?>');" readonly></textarea> &nbsp;(*)      
                </div>
            	<div class="frm-label-big">
                    <label>&nbsp;</label> <input type="text" name="remLen" value="0" size="3" class="frm" readonly>                
                </div>
            </div>
            <div class="frm-label-big">
        		<label>&nbsp;</label>
        		(*) <?=$campo_obligatorio;?>
        	</div>
            <div class="frm-label-big">
                <label>&nbsp;</label> <button type="button" class="frm-button" onClick="groupMessage('<?=$lan;?>');"><?=$b_enviar;?></button> 
            </div>
        </form>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function fileMessage ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->envio_archivo[0]->titulo;
	$lbpais				= $lan_file->$lan->envio_archivo[0]->lbpais;
	$lbservicio			= $lan_file->$lan->envio_archivo[0]->lbservicio;
	$lblista			= $lan_file->$lan->envio_archivo[0]->lblista;
	$lbcentrocosto		= $lan_file->$lan->envio_archivo[0]->lbcentrocosto;
	$lbtipoenvio		= $lan_file->$lan->envio_archivo[0]->lbtipoenvio;
	$lbmovil			= $lan_file->$lan->envio_archivo[0]->lbmovil;
	$lbtipotexto		= $lan_file->$lan->envio_archivo[0]->lbtipotexto;
	$lbarchivo			= $lan_file->$lan->envio_archivo[0]->lbarchivo;
	$formato_local		= $lan_file->$lan->envio_archivo[0]->formato_local;
	$formato_inter		= $lan_file->$lan->envio_archivo[0]->formato_internacional;
	$b_enviar			= $lan_file->$lan->botones[0]->b_enviar;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	$seleccionar		= $lan_file->$lan->general[0]->seleccionar;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("send_msg_file");

	if($permission == "Y") {
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$data_coding	= arrayDataCoding();
		$services		= arrayService();
		$countrys		= groupArray($paises, "code");
		?>
        
        <form method="POST" id="formulario" enctype="multipart/form-data">
        	<?php
			if($type == "LOCAL")
			{
				?>
        	<div class="frm-label-medium">
            	<label><?=$lbservicio;?></label> 
            	<?php selectServiceActive(); ?> &nbsp;(*)
            </div>
            <div class="frm-label-medium">
            	<label><?=$lbpais;?></label> 
                <select name="country" id="country" class="frm">
                    <option value=""><?=$seleccionar;?></option>
                    <?php
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$code?>" selected> <?=$ct?></option>
                                    <?php
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$code?>"> <?=$ct?></option>
                                <?php
                            }
                        }
                    }
                ?>
                </select> &nbsp;(*)
            </div>
            <?php 
			}
			?>
            <div class="frm-label-big">
            	<label><?=$lbcentrocosto;?></label> 
            	<?php selectCostCenter(); ?> &nbsp;
            </div>
            <div class="frm-label-big">
            	<label><?=$lbtipoenvio;?></label> 
            	<?php selectTypeSend();?> &nbsp;(*)
            </div>
             <div class="frm-label-big">
                <div id="typeSendTime" style="display:none;">
                	<label>&nbsp;</label>
           	  <input type="text" name="dispatchTime" id="dispatchTime" class="frm" size="16" readonly>
                  <label><img src="images/icons/calendar.png" id="lanzador1"></label>
                  <script type="text/javascript" charset="UTF-8">
                    var fecha = new Date();
                    var year=fecha.getFullYear();
                        Calendar.setup({ inputField  : "dispatchTime", range       :  [2015, 2999],
                        ifFormat    : "%d-%m-%Y",
                        showsTime      :    false,
                        timeFormat     :    "24",
                        button      : "lanzador1"
                        }
                        )
                    </script>
              <input type="text" name="sendDateFinish" id="sendDateFinish" class="frm" size="16" value="<?=$sendDateFinish?>" readonly>
            <img src="images/icons/calendar.png" >
          
                    <script src="js/timepicki.js"></script>
                    <script>
					$('#sendDateFinish').timepicki({
						show_meridian:false,
						min_hour_value:0,
						max_hour_value:23,
						//step_size_minutes:0,
						overflow_minutes:true,
						increase_direction:'up',
						disable_keyboard_mobile: true});
    				</script>
	
					
   			<script src="js/bootstrap.min.js"></script>
            </div>
            </div>
            <div class="frm-label-big">
            	<label><?=$lbtipotexto;?></label> 
            	<?php selectDataCoding();?> &nbsp;(*)
            </div>
            <div class="frm-label-big">
            	<label><?=$lbarchivo;?> </label>
            	<input type="file" id="phone_list" name="phone_list" class="frm"> &nbsp;(*)
            </div>
            <div class="frm-label-big">
            	<label> </label>
                <?php
                if($type == "LOCAL")
				{
					echo $formato_local;
				} else {
					echo $formato_inter;
				}
				?> 
            </div>
            <div class="frm-label-big">
        		<label>&nbsp;</label>
        		(*) <?=$campo_obligatorio;?>
        	</div>
            <div class="frm-label-big">
            	<label>&nbsp;</label>
            	<button id="btnSend" type="button" class="frm-button" onClick="fileMessage('<?=$type;?>', '<?=$lan;?>');"><?=$b_enviar;?></button>
            </div>         
        </form>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function cancelMessage ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->envio_cancelados[0]->titulo;
	$tbnombre			= $lan_file->$lan->envio_cancelados[0]->tbnombre;
	$tbtipoenvio		= $lan_file->$lan->envio_cancelados[0]->tbtipoenvio;
	$tbfechacreado		= $lan_file->$lan->envio_cancelados[0]->tbfechacreado;
	$tbfechaagendado	= $lan_file->$lan->envio_cancelados[0]->tbfechaagendado;
	$tbrechazados		= $lan_file->$lan->envio_cancelados[0]->tbrechazados;
	$tbtotal			= $lan_file->$lan->envio_cancelados[0]->tbtotal;
	$tbaccion			= $lan_file->$lan->envio_cancelados[0]->tbaccion;
	$tbpais				= $lan_file->$lan->envio_cancelados[0]->tbpais;
	$tbnumc			    = $lan_file->$lan->envio_cancelados[0]->tbnumc;
	$cancelar_envio		= $lan_file->$lan->tips[0]->cancelar_envio;
	$descargar_reporte	= $lan_file->$lan->tips[0]->descargar_reporte;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_list_cancel_msg");
	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		?>
        <?php
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		//$data			.= "&offset=".$offset;
		//$data			.= "&limit=".$limit;
		$data			.= "&id_country=".$id_country;
        //$url_base		= URL_WS."WSA-Telcel/api/process/cancel?access_token=".$access_token."&id_country=".$id_country;
		$url			= URL_WS."WSA-Telcel/api/process/cancel?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= count($parseUrl);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=cancelMessage&searchname=".$searchname;
		$paises		= arrayCountry();
		$type = $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		
		
		if($totalRows == 0)
		{
			?>
            <div class="show-message">
            <?php
			showMessage (50);
			?>
            <?php
		}
		else
		{
			//pag_pages ($page, $total_pages, $url_app);
				
			?>
			
			<table>
			<tr>
				<th><?=$tbnombre;?></th>
				<th><?=$tbtipoenvio;?></th>
				<th><?=$tbfechacreado;?></th>
                <th><?=$tbfechaagendado;?></th>
                <th><?=$tbrechazados;?></th>
                <th><?=$tbpais;?></th>
                <th><?=$tbnumc;?></th>
				<th><?=$tbtotal;?></th>
				<th><?=$tbaccion;?></th>
			</tr>
			<?php
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$pid			= $parseUrl[$i]['pid'];
				$total			= $parseUrl[$i]['total'];
				$input_mode		= $parseUrl[$i]['input_mode'];
				$input_mode		= getInputModeName(substr($input_mode, 0, -4));
				$file_name		= $parseUrl[$i]['file_name'];
				//var_dump($input_mode);
				$created_time	= returnFormatDate($parseUrl[$i]['created_time']);
				$dispatch_time	= returnFormatDate($parseUrl[$i]['dispatch_time']);
				$canceleable	= $parseUrl[$i]['canceleable'];
				$bad_messages	= $parseUrl[$i]['bad_messages'];
				$sender			= $parseUrl[$i]['sender'];
				$country_name	= $parseUrl[$i]['country_name'];
			
				
				if($bad_messages > 0)
				{
					$urlRepTotal	= URL_WS."WSA-Telcel/api/process/detailed/cancel?access_token=".$access_token."&id_country=".$id_country."&id_process=".$pid;
					$fileRepTotal	= getFileReport($urlRepTotal);
				}
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Suspendido";
				} else {
					$status = "Activo";
				}
				?>
				<tr class="<?=$class;?>">
                	
					<td><?=$file_name;?></td>
                    <td><?=$input_mode;?></td>
					<td><?=$created_time;?></td>
                    <td><?=$dispatch_time;?></td>
                    <td><?=$bad_messages;?></td> 
                    
                    <?php
                    if($input_mode == "Individual")						
						{
							
                            $phone=$file_name;
							$var123=telefono_country($paises, $phone, $code_c,$type);
							$country_name=$var123;
							
						} 
                     	$type = $_SESSION['type'];
						
						if($type == "GLOBAL" && $input_mode == "Lista de Envío")
						{
							
							$country_name="------";
							$sender="------------";
							
						}  
                        else 
						{
							$country_name=$country_name;
                            $sender=$sender;
							
                            
						}
					 ?>
                     <td><?=$country_name;?></td>
                     <td><?=$sender;?></td>
					<td><?=$total;?></td>
					<td width="150">
                    
                    <?php
					if($canceleable == 1)
					{
						?>
                        <a href="?module=cancelStatusMessage&pid=<?=$pid;?>&file_name=<?=$file_name;?>" class="tip"><span><?=$cancelar_envio;?></span><img src="images/icons/delete.png"></a>
                        <?php
					}
					if($bad_messages > 0)
					{
						?>
                        <a href="ajaxFuncs/repCanceled.php?report=<?=$fileRepTotal;?>" class="tip"><span><?=$descargar_reporte;?></span><img src="images/icons/excel.png"></a>
                        <?php
					}
					?></td>
				</tr>
				<?php
			}
			?>
			</table>
			
			<?php
			//pag_pages ($page, $total_pages, $url_app);
		}
	}
	else
	{
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

//MHL 2016-12-14
function singleQuery ()
{
	$lan 			= $_SESSION['lang'];
	$lan_file		= simplexml_load_file('language.xml');
	$lb_titulo		= $lan_file->$lan->consulta_individual[0]->titulo;
	$lbpais			= $lan_file->$lan->consulta_individual[0]->lbpais;
	$lbmovil		= $lan_file->$lan->consulta_individual[0]->lbmovil;
	$b_consultar		= $lan_file->$lan->botones[0]->b_consultar;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	$seleccionar		= $lan_file->$lan->general[0]->seleccionar;
	$largo_prefijos		= $lan_file->$lan->tips[0]->largo_prefijos;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("single_find_operadora");
	if($permission == "Y") 
	{
		$access_token	= $_SESSION['access_token'];
		$type		= $_SESSION['type'];
		$code_c		= strtoupper($_SESSION['code']);
		$paises		= arrayCountry();
		$countrys	= groupArray($paises, "code");

		?>
        
        <form method="POST">
        	

        	<div class="frm-label-medium">
            	<label><?=$lbpais;?></label> 
                <select name="country" id="country" class="frm">
                    <option value=""><?=$seleccionar;?></option>
                    <?php
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
                            $id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];

                            // solo para Mexico por ahora
                            if ($id != 1)
                            {
                                continue;
                            }
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$id?>" selected> <?=$ct?></option>
                                    <?php
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?php
                            }
                        }
                    }
                ?>
                </select> <div id="getMaxCountry"></div> &nbsp;(*)
            </div>
            <div class="frm-label-medium">
            </div>
            <div class="frm-label-big">
            	<label><?=$lbmovil;?></label> 
            	<input type="text" name="msisdn" id="msisdn" class="frm"> &nbsp;(*) 
                &nbsp; &nbsp; <a class="tip"><img src="images/icons/info.png" id="opener" onClick="info();" style="cursor: pointer;"><span><?=$largo_prefijos;?></span></a>
                <div id="dialog" title="<?=$largo_prefijos;?>" style="display: none;">
                  <?php getPrefMxCountry(); ?>
                </div>
            </div>
            <div class="frm-label-big">
        		<label>&nbsp;</label>
        		(*) <?=$campo_obligatorio;?>
        	</div>
            <div class="frm-label-big">
                <label>&nbsp;</label> <button type="button" class="frm-button" onClick="singleQuery('<?=$lan;?>');"><?=$b_consultar;?></button> 
            </div>            
        </form>
        <br>

        <br>
		<div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function listQuery ()
{
	$lan 			= $_SESSION['lang'];
	$lan_file		= simplexml_load_file('language.xml');
	$lb_titulo		= $lan_file->$lan->consulta_lista[0]->titulo;
	$lbpais			= $lan_file->$lan->consulta_lista[0]->lbpais;
	$lblista		= $lan_file->$lan->consulta_lista[0]->lblista;
	$b_consultar		= $lan_file->$lan->botones[0]->b_procesar_consulta;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	$seleccionar		= $lan_file->$lan->general[0]->seleccionar;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("group_find_operadora");

	if($permission == "Y") {
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$countrys		= groupArray($paises, "code");
		?>
        
        <form method="POST">
            <div class="frm-label-medium">
            	<label><?=$lbpais;?></label> 
                <select name="country" id="country" class="frm" onChange="selectSendListCountry();">
                    <option value=""><?=$seleccionar;?></option>
                    <?php
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
			    $id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            
                            // solo para Mexico por ahora
                            if ($id != 1)
                            {
                                continue;
                            }
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$id?>" selected> <?=$ct?></option>
                                    <?php
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?php
                            }
                        }
                    }
                ?>
                </select> &nbsp;(*)
            </div>
            <div class="frm-label-medium">
            </div>
            <div class="frm-label-big">
            	<label><?=$lblista;?></label> 
            	<select id="id_group" name="id_group" class="frm">
                	<option value=""><?=$seleccionar;?></option>
                </select> &nbsp;(*)
                <div id="selectSendListCountry"></div>
            </div>
            <div class="frm-label-big">
        		<label>&nbsp;</label>
        		(*) <?=$campo_obligatorio;?>
        	</div>
            <div class="frm-label-big">
                <label>&nbsp;</label> <button type="button" class="frm-button" onClick="listQuery('<?=$lan;?>');"><?=$b_consultar;?></button> 
            </div>
        </form>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function fileQuery ()
{
	$lan 			= $_SESSION['lang'];
	$lan_file		= simplexml_load_file('language.xml');
	$lb_titulo		= $lan_file->$lan->consulta_archivo[0]->titulo;
	$lbpais			= $lan_file->$lan->consulta_archivo[0]->lbpais;
	$lbarchivo		= $lan_file->$lan->consulta_archivo[0]->lbarchivo;
	$formato		= $lan_file->$lan->consulta_archivo[0]->formato;
	$b_consultar		= $lan_file->$lan->botones[0]->b_procesar_consulta;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	$seleccionar		= $lan_file->$lan->general[0]->seleccionar;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("file_find_operadora");

	if($permission == "Y") {
		$type		= $_SESSION['type'];
		$code_c		= strtoupper($_SESSION['code']);
		$paises		= arrayCountry();
		$countrys	= groupArray($paises, "code");
		?>
        
        <form method="POST" id="formulario" enctype="multipart/form-data">
            <div class="frm-label-big">
            	<label><?=$lbpais;?></label> 
                <select name="country" id="country" class="frm">
                    <option value=""><?=$seleccionar;?></option>
                    <?php
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
			    $id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            
                            // solo para Mexico por ahora
                            if ($id != 1)
                            {
                                continue;
                            }
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$id?>" selected> <?=$ct?></option>
                                    <?php
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?php
                            }
                        }
                    }
                ?>
                </select> &nbsp;(*)
            </div>
            <div class="frm-label-big">
            	<label><?=$lbarchivo;?> </label>
            	<input type="file" id="phone_list" name="phone_list" class="frm"> &nbsp;(*)
            </div>
            <div class="frm-label-big">
            	<label> </label>
            	<div class="div-label"><?=nl2br($formato);?></div>
            </div>
            <div class="frm-label-big">
        		<label>&nbsp;</label>
        		(*) <?=$campo_obligatorio;?>
        	</div>
            <div class="frm-label-big">
            	<label>&nbsp;</label>
            	<button type="button" class="frm-button" onClick="fileQuery('<?=$lan;?>');"><?=$b_consultar;?></button>
            </div>         
        </form>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

//MHL 2017-01-03
function resultsQuery ()
{
	$lan 			= $_SESSION['lang'];
	$lan_file		= simplexml_load_file('language.xml');
	$lb_titulo		= $lan_file->$lan->consulta_bitacora[0]->titulo;
	$lbfechainicio		= $lan_file->$lan->consulta_bitacora[0]->lbfechainicio;
	$lbfechafin		= $lan_file->$lan->consulta_bitacora[0]->lbfechafin;
	$lbarchivo		= $lan_file->$lan->consulta_bitacora[0]->lbarchivo;
	$lborden		= $lan_file->$lan->consulta_bitacora[0]->lborden;
	$lbordenasc		= $lan_file->$lan->consulta_bitacora[0]->lbordenasc;
	$lbordendes		= $lan_file->$lan->consulta_bitacora[0]->lbordendes;

	$tbmode			= $lan_file->$lan->reporte_consulta_bitacora[0]->tbmode;
	$tbarchivo		= $lan_file->$lan->reporte_consulta_bitacora[0]->tbarchivo;
	$tbfecha		= $lan_file->$lan->reporte_consulta_bitacora[0]->tbfecha;
	$tbestado		= $lan_file->$lan->reporte_consulta_bitacora[0]->tbestado;
	$tbresultado		= $lan_file->$lan->reporte_consulta_bitacora[0]->tbresultado;
	$tbtotal		= $lan_file->$lan->reporte_consulta_bitacora[0]->tbtotal;
	$tbok			= $lan_file->$lan->reporte_consulta_bitacora[0]->tbok;
	$tbunknown		= $lan_file->$lan->reporte_consulta_bitacora[0]->tbunknown;
	$tbtimeout		= $lan_file->$lan->reporte_consulta_bitacora[0]->tbtimeout;

	$tbmode_file		= $lan_file->$lan->reporte_consulta_bitacora[0]->tbmodeFILE;
	$tbmode_list		= $lan_file->$lan->reporte_consulta_bitacora[0]->tbmodeLIST;
	$tbestado_queued	= $lan_file->$lan->reporte_consulta_bitacora[0]->tbestadoQUEUED;
	$tbestado_rejected	= $lan_file->$lan->reporte_consulta_bitacora[0]->tbestadoREJECTED;
	$tbestado_sent		= $lan_file->$lan->reporte_consulta_bitacora[0]->tbestadoSENT;
	$tbestado_processed	= $lan_file->$lan->reporte_consulta_bitacora[0]->tbestadoPROCESSED;
	$tbestado_failed	= $lan_file->$lan->reporte_consulta_bitacora[0]->tbestadoFAILED;
	$tbestado_expired	= $lan_file->$lan->reporte_consulta_bitacora[0]->tbestadoEXPIRED;

	$b_consultar		= $lan_file->$lan->botones[0]->b_consultar;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_report_find_operadora");
	
	if($permission == "Y") 
	{
		$access_token	= $_SESSION['access_token'];
		$id_country	= $_SESSION['id_country'];
		$start_date	= $_GET['start_date'];
		$end_date	= $_GET['end_date'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
				
		$archivo		= $_GET['archivo'];

		$order			= $_GET['order'];
		if (empty($order))
		{
			$order = "DESC";
		}

		$data = "";
		$data	.= "&id_country=".$id_country;
		$data	.= "&start_date=".$start_date_get;
		$data	.= "&end_date=".$end_date_get;
		$data	.= "&archivo=".$archivo;
		$data	.= "&order=".$order;

		$url			= URL_WS."WSA-Telcel/api/tgg/bitacora?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= count($parseUrl);
		
		if(!empty($start_date))
		{
			$dateFrom	= $start_date;
			$dateTo		= $end_date;
		} else {
			$dateFrom	= date('d-m-Y');
			$dateTo		= date('d-m-Y');
		}
		
		?>
        <div class="frm-label-big">
        	<label><?=$lbfechainicio?></label>
        	<input type="text" name="start_date" id="start_date" class="frm" value="<?=$dateFrom;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador1">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbfechafin?></label>
        	<input type="text" name="end_date" id="end_date" class="frm" value="<?=$dateTo;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador2">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbarchivo?></label>
        	<input type="text" name="archivo" id="archivo" class="frm" value="<?=$archivo;?>">
        </div>    
        <div class="frm-label-big">
        	<label><?=$lborden?></label> <?php selectOrder($order); ?>       
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>  
	        <button class="frm-button" onClick="resultsQuery();"><?=$b_consultar;?></button> 
        </div>
        <br>
        <br>

<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "start_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador1"
            }
            )
        </script>
        
<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "end_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador2"
            }
            )
        </script>
        <?php
        
		if(!empty($start_date))
		{
			if($totalRows == 0)
			{
				?>
                <div class="show-message">
                <?php
                showMessage (9);
                ?>
                </div>
                <?php
			}
			else
			{
				?>
				<div class="frame-scroll-table">
				<table>
				<tr>
					<th><?=$tbmode;?></th>
					<th><?=$tbarchivo;?></th>
					<th><?=$tbfecha;?></th>
					<th><?=$tbestado;?></th>
					<th><?=$tbresultado;?></th>
					<th><?=$tbtotal;?></th>
					<th><?=$tbok;?></th>
					<th><?=$tbunknown;?></th>
					<th><?=$tbtimeout;?></th>
				</tr>
				<?php
				
				for($i = 0; $i < count($parseUrl); $i++)
				{
					$id		= $parseUrl[$i]['id'];
					$mode		= $parseUrl[$i]['mode'];
					$archivo	= $parseUrl[$i]['file_original_name'];
					$filename	= $parseUrl[$i]['file_internal_name'];
					$resultname	= $parseUrl[$i]['file_result_name'];
					$fecha		= returnFormatDate($parseUrl[$i]['insert_time']);
					$sent_time	= returnFormatDate($parseUrl[$i]['sent_time']);
					$estado		= $parseUrl[$i]['status'];
					$total		= $parseUrl[$i]['rows_total'];
					$ok		= $parseUrl[$i]['rows_ok'];
					$unknown	= $parseUrl[$i]['rows_unknown'];
					$timeout	= $parseUrl[$i]['rows_timeout'];
					
					// ===========================================================
					// Parche temporal para corregir expirados y ajuste de fechas
					if (($estado == 'SENT') && ((strtotime("now") - strtotime($sent_time) ) > 24 * 3600))
					{
						$estado = 'EXPIRED';
					}
					$fecha = date('d-m-Y H:i:s', strtotime($fecha) - 6 * 3600);
					// ===========================================================

					$tr_color = $i % 2;
					
					if($tr_color == 0)
					{
						$class	= ""; 
					} else {
						$class	= "tr-color"; 
					}
					
					if ($mode == 'FILE')
					{
						$mode_txt = $tbmode_file;
					}
					else if ($mode == 'LIST')
					{
						$mode_txt = $tbmode_list;
					}
					else
					{
						$mode_txt = $mode;
					}

					if ($estado == 'QUEUED')
					{
						$estado_txt = $tbestado_queued;
					}
					else if ($estado == 'REJECTED')
					{
						$estado_txt = $tbestado_rejected;
					}
					else if ($estado == 'SENT')
					{
						$estado_txt = $tbestado_sent;
					}
					else if ($estado == 'PROCESSED')
					{
						$estado_txt = $tbestado_processed;
					}
					else if ($estado == 'FAILED')
					{
						$estado_txt = $tbestado_failed;
					}
					else if ($estado == 'EXPIRED')
					{
						$estado_txt = $tbestado_expired;
					}
					else
					{
						$estado_txt = $estado;
					}
					
					?>
					<tr class="<?=$class;?>">
						<td><?=$mode_txt;?></td>
						<td><?=$archivo;?></td>
						<td><?=$fecha;?></td>
						<td><?=$estado_txt;?></td>
					<?php
					if ($estado == 'PROCESSED')
					{
						$archivo = urlencode($archivo);
					?>
						<td style="text-align: center !important">
							<a href="ajaxFuncs/resultsQuery.php?archivo=<?=$archivo;?>&type=OUT&resultname=<?=$resultname;?>">
								<img src="images/icons/down.png">
							</a>
						</td>
					<?php
					}
					else if ($estado == 'FAILED')
					{
						$archivo = urlencode($archivo);
					?>
						<td style="text-align: center !important">
							<a href="ajaxFuncs/resultsQuery.php?archivo=<?=$archivo;?>&type=ERR&resultname=<?=$resultname;?>">
								<img src="images/icons/down_red.png">
							</a>
						</td>
					<?php
					}
					else
					{
					?>
						<td></td>
					<?php
					}
					?>
						<td style="text-align: right !important"><?= number_format($total, 0, ',', '.') ;?></td>
						<td style="text-align: right !important"><?= number_format($ok, 0, ',', '.');?></td>
						<td style="text-align: right !important"><?= number_format($unknown, 0, ',', '.');?></td>
						<td style="text-align: right !important"><?= number_format($timeout, 0, ',', '.');?></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				</table>
				</div>
				<?php
			}
		}
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function cancelStatusMessage ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->envio_cancelado_estado[0]->titulo;
	$lbestado			= $lan_file->$lan->envio_cancelado_estado[0]->lbestado;
	$lbmensaje			= $lan_file->$lan->envio_cancelado_estado[0]->lbmensaje;
	$b_si				= $lan_file->$lan->botones[0]->b_si;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("del_cancel_msg");
	
	if($permission == "Y") {
		$pid			= $_GET['pid'];
		$file_name_new			= $_GET['file_name'];
		$id_country		= $_SESSION['id_country'];
		$access_token	= $_SESSION['access_token'];
		$data			.= "&id_country=".$id_country;
        $url			= URL_WS."WSA-Telcel/api/process/cancel/$pid?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$file_name		= $parseUrl['file_name'];

		?>
        <div class="frm-label-big">
        	<label><?=$lbestado;?></label>
        	<?=$file_name_new;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label> <?=$lbmensaje;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="cancelStatusMessage('<?=$pid;?>', '<?=$lan;?>');"><?=$b_si;?></button> <button class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function trafficMT ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->reporte_enviados_web[0]->titulo;
	$lbfechainicio		= $lan_file->$lan->reporte_enviados_web[0]->lbfechainicio;
	$lbfechafin			= $lan_file->$lan->reporte_enviados_web[0]->lbfechafin;
	$lbmovil			= $lan_file->$lan->reporte_enviados_web[0]->lbmovil;
        $lbcentrocosto          = $lan_file->$lan->reporte_enviados_web[0]->lbcentrocosto;
	$lbusuario			= $lan_file->$lan->reporte_enviados_web[0]->lbusuario;
	$lbtipoingreso		= $lan_file->$lan->reporte_enviados_web[0]->lbtipoingreso;
	$lbestado			= $lan_file->$lan->reporte_enviados_web[0]->lbestado;
	$lbmarcacion		= $lan_file->$lan->reporte_enviados_web[0]->lbmarcacion;
	$lborden			= $lan_file->$lan->reporte_enviados_web[0]->lborden;
	$lbordenasc			= $lan_file->$lan->reporte_enviados_web[0]->lbordenasc;
	$lbordendes			= $lan_file->$lan->reporte_enviados_web[0]->lbordendes;
	$tbfecha			= $lan_file->$lan->reporte_enviados_web[0]->tbfecha;
	$tbenviados			= $lan_file->$lan->reporte_enviados_web[0]->tbenviados;
	$tbconfirmados		= $lan_file->$lan->reporte_enviados_web[0]->tbconfirmados;
	$tbfallidos			= $lan_file->$lan->reporte_enviados_web[0]->tbfallidos;
	$tbrechazados		= $lan_file->$lan->reporte_enviados_web[0]->tbrechazados;
	$tbpendientes		= $lan_file->$lan->reporte_enviados_web[0]->tbpendientes;
	$tbcancelados		= $lan_file->$lan->reporte_enviados_web[0]->tbcancelados;
	$tbtotal			= $lan_file->$lan->reporte_enviados_web[0]->tbtotal;
	$tbdescargar		= $lan_file->$lan->reporte_enviados_web[0]->tbdescargar;
	$b_consultar		= $lan_file->$lan->botones[0]->b_consultar;
	
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_report_mt_web");
	
	if($permission == "Y") 
	{
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
				
		$msisdn			= $_GET['msisdn'];
		$ccost			= $_GET['ccost'];
		$user			= $_GET['user'];
		$input_mode		= $_GET['input_mode'];
		$stage			= $_GET['stage'];
		$service_tag	= $_GET['service_tag'];
		$order			= $_GET['order'];
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($msisdn)) { $data_extra .= "&searchmsisdn=".$msisdn; }
		if(!empty($ccost)) { $data_extra .= "&searchccost=".$ccost; }
		if(!empty($user)) { $data_extra .= "&searchuser=".$user; }
		if(!empty($input_mode)) { $data_extra .= "&searchtype=".$input_mode; }
		if(!empty($stage)) { $data_extra .= "&searchstage=".$stage; }
		if(!empty($service_tag)) { $data_extra .= "&searchsender=".$service_tag; }
		if(!empty($order)) { $data_extra .= "&order=".$order; }
		$url			= URL_WS."WSA-Telcel/api/message/consolidated?access_token=".$access_token.$data.$data_extra;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= count($parseUrl);
		
		if(!empty($start_date))
		{
			$dateFrom	= $start_date;
			$dateTo		= $end_date;
		} else {
			$dateFrom	= date('01-m-Y');
			$dateTo		= date('d-m-Y');
		}
		
		?>
        <div class="frm-label-big">
        	<label><?=$lbfechainicio?></label>
        	<input type="text" name="start_date" id="start_date" class="frm" value="<?=$dateFrom;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador1">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbfechafin?></label>
        	<input type="text" name="end_date" id="end_date" class="frm" value="<?=$dateTo;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador2">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbmovil?></label>
        	<input type="text" name="msisdn" id="msisdn" class="frm" value="<?=$msisdn;?>">
        </div>    
        <div class="frm-label-big">
		<label><?=$lbcentrocosto;?></label>
		<?php filterCostCenter($ccost); ?> &nbsp;
        </div>
        <div class="frm-label-big">
        	<label><?=$lbusuario?></label>  <?php selectUser($user); ?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbtipoingreso?></label>  <?php selectInputMode($input_mode); ?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbestado?></label> <?php selectStage($stage); ?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbmarcacion?></label> <?php selectServiceTag($service_tag);?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lborden?></label> <?php selectOrder($order); ?>       
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>  
	        <button class="frm-button" onClick="trafficMT();"><?=$b_consultar;?></button> 
        </div>
        <br>
        <br>
<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "start_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador1"
            }
            )
        </script>
        
<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "end_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador2"
            }
            )
        </script>
        <?php
        
		if(!empty($start_date))
		{
			if($totalRows == 0)
			{
				?>
                <div class="show-message">
                <?php
                showMessage (9);
                ?>
                </div>
                <?php
			}
			else
			{
				$urlRepTotal	= URL_WS."WSA-Telcel/api/message/detailed?access_token=".$access_token."&id_country=".$id_country."&start_date=".$start_date_get."&end_date=".$end_date_get.$data_extra;
				$fileRepTotal	= getFileReport($urlRepTotal);
				
				?>
				<div class="show-message">
                	<a href="ajaxFuncs/repTrafficTotalMT.php?report=<?=$fileRepTotal;?>"><?php showMessage(58); ?></a>
                </div>
				<table>
				<tr>
					<th><?=$tbfecha;?></th>
					<th><?=$tbenviados;?></th>
					<th><?=$tbconfirmados;?></th>
					<th><?=$tbfallidos;?></th>
					<th><?=$tbrechazados;?></th>
					<th><?=$tbpendientes;?></th>
					<th><?=$tbcancelados;?></th>
					<th><?=$tbtotal;?></th>
                    <th><?=$tbdescargar;?></th>
				</tr>
				<?php
				
				for($i = 0; $i < count($parseUrl); $i++)
				{
					$report_date	= returnFormatDate($parseUrl[$i]['report_date']);
					$report_file	= $parseUrl[$i]['report_date'];
					$pushed			= $parseUrl[$i]['pushed'];
					$confirmed		= $parseUrl[$i]['confirmed'];
					$failed			= $parseUrl[$i]['failed'];
					$nopushed		= $parseUrl[$i]['nopushed'];
					$queued			= $parseUrl[$i]['queued'];
					$canceled		= $parseUrl[$i]['canceled'];
					$count			= $parseUrl[$i]['count'];
					$url_excel		= URL_WS."WSA-Telcel/api/message/detailed?access_token=".$access_token."&id_country=".$id_country."&start_date=".$report_file."&end_date=".$report_file.$data_extra;
					$fileReport		= getFileReport($url_excel);
					
					$tr_color = $i % 2;
					
					if($tr_color == 0)
					{
						$class	= ""; 
					} else {
						$class	= "tr-color"; 
					}
					
					
					?>
					<tr class="<?=$class;?>">
						<td><a href="?module=trafficMTDetail&start_date=<?=$report_date;?>&end_date=<?=$report_date;?>&id_country=<?=$id_country;?>&msisdn=<?=$msisdn;?>&stage=<?=$stage;?>&input_mode=<?=$input_mode;?>&ccost=<?=$ccost;?>&user=<?=$user;?>&service_tag=<?=$service_tag;?>&order=<?=$order;?>"><?=$report_date;?></a></td>
						<td><?=$pushed;?></td>
						<td><?=$confirmed;?></td>
						<td><?=$failed;?></td>
						<td><?=$nopushed;?></td>
						<td><?=$queued;?></td>
						<td><?=$canceled;?></td>
						<td><?=$count;?></td>
                        <td><a href="ajaxFuncs/repTrafficMT.php?report=<?=$fileReport;?>"><img src="images/icons/excel.png"></a>
                      </td>
					</tr>
					<?php
				}
				?>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
                    <th></th>
				</tr>
				</table>
				<div id="download"></div>
				<?php
			}
		}
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function trafficMTDetail ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->reporte_enviados_detalle_web[0]->lb_titulo;
	$tbmovil			= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbmovil;
	$tbmarcacion		= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbmarcacion;
	$tbestado			= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbestado;
	$tbresultado		= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbresultado;
	$tbfechacreado		= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbfechacreado;
	$tbfechagendado		= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbfechagendado;
	$tbfechaestado		= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbfechaestado;
	$tbtipoingreso		= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbtipoingreso;
	$tbtipoenvio		= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbtipoenvio;
	$tbcreadopor		= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbcreadopor;
        $tbccost			= $lan_file->$lan->reporte_enviados_web[0]->lbcentrocosto;
	$tbmensaje			= $lan_file->$lan->reporte_enviados_detalle_web[0]->tbmensaje;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
		
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_report_mt_web_detail");
	
	if($permission == "Y") 
	{
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$searchname		= $_GET['searchname'];
		$id_country		= $_GET['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
		
		$date			= returnFormatDate(substr($_GET['end_date'], 0,10));
		$msisdn			= $_GET['msisdn'];
		$ccost			= $_GET['ccost'];
		$user			= $_GET['user'];
		$input_mode		= $_GET['input_mode'];
		$stage			= $_GET['stage'];
		$service_tag	= $_GET['service_tag'];
		$order			= $_GET['order'];
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($msisdn)) { $data .= "&searchmsisdn=".$msisdn; }
		if(!empty($ccost)) { $data .= "&searchccost=".$ccost; }
		if(!empty($user)) { $data .= "&searchuser=".$user; }
		if(!empty($input_mode)) { $data .= "&searchtype=".$input_mode; }
		if(!empty($stage)) { $data .= "&searchstage=".$stage; }
		if(!empty($service_tag)) { $data .= "&searchsender=".$service_tag; }
		if(!empty($order)) { $data .= "&order=".$order; }
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$url			= URL_WS."WSA-Telcel/api/message/detailed/mt?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		
		?>
        <h2><?=$date;?></h2>
        <div class="frm-label-big">
        	<button type="button" class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>
        <br>
        <br>
        <?php
		
		
		
			//pag_pages ($page, $total_pages, $url_app);
				
			?>
			<div class="frame-scroll-table">
			<table class="scroll-table">
			<tr>
				<th><?=$tbmovil;?></th>
				<th><?=$tbmarcacion;?></th>
				<th><?=$tbestado;?></th>
                <th><?=$tbresultado;?></th>
				<th><?=$tbfechacreado;?></th>
                <th><?=$tbfechagendado;?></th>
                <th><?=$tbfechaestado;?></th>
                <th><?=$tbtipoingreso;?></th>
                <th><?=$tbtipoenvio;?></th>
                <th><?=$tbcreadopor;?></th>
                <th><?=$tbccost;?></th>
                <th><?=$tbmensaje;?></th>
			</tr>
			<?php
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$msisdn			= $parseUrl[$i]['msisdn'];
				$sender			= $parseUrl[$i]['sender'];
				$stage			= $parseUrl[$i]['stage'];
				$text			= $parseUrl[$i]['text'];
				$created_time	= returnFormatDate($parseUrl[$i]['created_time']);
				$dispatch_time	= returnFormatDate($parseUrl[$i]['dispatch_time']);
				$result_time	= returnFormatDate($parseUrl[$i]['result_time']);
				$content		= $parseUrl[$i]['content'];
				//$inputmode		= $parseUrl[$i]['input_mode'];
				$input_mode		= $parseUrl[$i]['input_mode'];
				$type_send		= $parseUrl[$i]['input_type'];
				$full_name		= $parseUrl[$i]['full_name'];
				$ccost			= $parseUrl[$i]['ccost'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				?>
				<tr class="<?=$class;?>">
					<td><?=$msisdn;?></td>
					<td><?=$sender;?></td>
					<td><?=$stage;?> </td>
                    <td><?=$text;?></td>
					<td><?=$created_time;?></td>
                    <td><?=$dispatch_time;?></td>
                    <td><?=$result_time;?></td>
                    <td><?=$input_mode;?></td>
                    <td><?=$type_send;?></td>
                    <td><?=$full_name;?></td>
                    <td><?=$ccost;?></td>
                    <td><?=$content;?></td>
				</tr>
				<?php
			}
			?>
			</table>
			</div>
            <br>
			<?php
			//pag_pages ($page, $total_pages, $url_app);
		
	}
	else
	{
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function trafficMO ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->reporte_recibidos_web[0]->lb_titulo;
	$lbfechainicio		= $lan_file->$lan->reporte_recibidos_web[0]->lbfechainicio;
	$lbfechafin			= $lan_file->$lan->reporte_recibidos_web[0]->lbfechafin;
	$lbmovil			= $lan_file->$lan->reporte_recibidos_web[0]->lbmovil;
	$lbmarcacion		= $lan_file->$lan->reporte_recibidos_web[0]->lbmarcacion;
	$lbtipo				= $lan_file->$lan->reporte_recibidos_web[0]->lbtipo;
	$lborden			= $lan_file->$lan->reporte_recibidos_web[0]->lborden;
	$tbfecha			= $lan_file->$lan->reporte_recibidos_web[0]->tbfecha;
	$tbtotal			= $lan_file->$lan->reporte_recibidos_web[0]->tbtotal;
	$tbdescargar		= $lan_file->$lan->reporte_recibidos_web[0]->tbdescargar;
	$b_consultar		= $lan_file->$lan->botones[0]->b_consultar;

	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_report_mo_web");
	
	if($permission == "Y") {
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		$stage			= $_GET['stage'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}	
		
		$msisdn			= $_GET['msisdn'];
		$service_tag	= $_GET['service_tag'];
		$order			= $_GET['order'];
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($msisdn)) { $data_extra .= "&searchmsisdn=".$msisdn; }
		if(!empty($service_tag)) { $data_extra .= "&searchsender=".$service_tag; }
		if(!empty($stage)) { $data_extra .= "&searchstage=".$stage; }
		if(!empty($order)) { $data_extra .= "&order=".$order; }
		$url			= URL_WS."WSA-Telcel/api/message/consolidated/mo?access_token=".$access_token.$data.$data_extra;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= count($parseUrl);
		
		if(!empty($start_date))
		{
			$dateFrom	= $start_date;
			$dateTo		= $end_date;
		} else {
			$dateFrom	= date('01-m-Y');
			$dateTo		= date('d-m-Y');
		}
		
		?>
		<div class="frm-label-big">
        	<label><?=$lbfechainicio;?></label>
        	<input type="text" name="start_date" id="start_date" class="frm" value="<?=$dateFrom;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador1">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbfechafin;?></label>
        	<input type="text" name="end_date" id="end_date" class="frm" value="<?=$dateTo;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador2">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbmovil;?></label>
        	<input type="text" name="msisdn" id="msisdn" class="frm" value="<?=$msisdn;?>">
        </div>    
        <div class="frm-label-big">
        	<label><?=$lbmarcacion;?></label> <?php selectServiceTag($service_tag);?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbtipo;?></label> <?php selectStageMo($stage);?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lborden;?></label> <?php selectOrder($order);?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>  
	        <button class="frm-button" onClick="trafficMO();"><?=$b_consultar;?></button> 
        </div>
        <br>
        <br>
        <br>
        <br>
<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "start_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador1"
            }
            )
        </script>
        
<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "end_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador2"
            }
            )
        </script>
        <?php
        
		if(!empty($start_date))
		{
			if($totalRows == 0)
			{
				?>
                <div class="show-message">
                <?php
                showMessage (9);
                ?>
                </div>
                <?php
			}
			else
			{
				#$urlRepTotal	= URL_WS."WSA-Telcel/api/message/detailed/mo/?access_token=".$access_token."&id_country=".$id_country."&start_date=".$start_date_get."&end_date=".$start_date_get.$data_extra;
				$urlRepTotal	= URL_WS."WSA-Telcel/api/message/detailed/mo/?access_token=".$access_token."&id_country=".$id_country."&start_date=".$start_date_get."&end_date=".$end_date_get.$data_extra;
				$fileRepTotal	= getFileReport($urlRepTotal);
				
				?>
				<div class="show-message">
                    <a href="ajaxFuncs/repTrafficTotalMO.php?report=<?=$fileRepTotal;?>"><?php showMessage(58); ?></a>
                </div>
				
				
				<table>
				<tr>
					<th><?=$tbfecha;?></th>
					<th><?=$tbtotal;?></th>
                    <th><?=$tbdescargar;?></th>
				</tr>
				<?php
				
				for($i = 0; $i < count($parseUrl); $i++)
				{
					$report_date	= returnFormatDate($parseUrl[$i]['report_date']);
					$report_file	= $parseUrl[$i]['report_date'];
					$count			= $parseUrl[$i]['count'];
					$url_excel		= URL_WS."WSA-Telcel/api/message/detailed/mo/?access_token=".$access_token."&id_country=".$id_country."&start_date=".$report_file."&end_date=".$report_file.$data_extra;
					$fileReport		= getFileReport($url_excel);
					
					$tr_color = $i % 2;
					
					if($tr_color == 0)
					{
						$class	= ""; 
					} else {
						$class	= "tr-color"; 
					}
					

					?>
					<tr class="<?=$class;?>">
						<td><a href="?module=trafficMODetail&start_date=<?=$report_date;?>&end_date=<?=$report_date;?>&id_country=<?=$id_country;?>&msisdn=<?=$msisdn;?>&service_tag=<?=$service_tag;?>&order=<?=$order;?>"><?=$report_date;?></a></td>
						<td><?=$count;?></td>
                        <td><a href="ajaxFuncs/repTrafficMO.php?report=<?=$fileReport;?>"><img src="images/icons/excel.png"></a></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<th></th>
					<th></th>
                    <th></th>
				</tr>
				</table>
				
				<?php
			}
		}
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function trafficMODetail ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->reporte_recibidos_detalle_web[0]->titulo;
	$tbmovil			= $lan_file->$lan->reporte_recibidos_detalle_web[0]->tbmovil;
	$tbmarcacion		= $lan_file->$lan->reporte_recibidos_detalle_web[0]->tbmarcacion;
	$tbfecharecepcion	= $lan_file->$lan->reporte_recibidos_detalle_web[0]->tbfecharecepcion;
	$tbmensaje			= $lan_file->$lan->reporte_recibidos_detalle_web[0]->tbmensaje;
	$b_volver			= $lan_file->$lan->botones[0]->b_volver;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_report_mo_web_detail");
	
	if($permission == "Y") 
	{
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$searchname		= $_GET['searchname'];
		$id_country		= $_GET['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
		
		$msisdn			= $_GET['msisdn'];
		$service_tag	= $_GET['service_tag'];
		$order			= $_GET['order'];
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($msisdn)) { $data .= "&searchmsisdn=".$msisdn; }
		if(!empty($service_tag)) { $data .= "&searchsender=".$service_tag; }
		if(!empty($order)) { $data .= "&order=".$order; }
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$url			= URL_WS."WSA-Telcel/api/message/detailed/mo/limit?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		
		?>
        <h2><?=$start_date;?></h2>
        <div class="frm-label-big">
        	<button type="button" class="frm-button" onClick="backUrl();"><?=$b_volver;?></button>
        </div>
        <br>
        <br>
        <?php
		
		
		
			//pag_pages ($page, $total_pages, $url_app);
				
			?>
			<div class="frame-scroll-table">
			<table class="scroll-table">
			<tr>
				<th><?=$tbmovil;?></th>
				<th><?=$tbmarcacion;?></th>
                <th><?=$tbfecharecepcion;?></th>
                <th><?=$tbmensaje;?></th>
			</tr>
			<?php
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$msisdn			= $parseUrl[$i]['msisdn'];
				$sender			= $parseUrl[$i]['sender'];
				$created_time	= returnFormatDate($parseUrl[$i]['created_time']);
				$content		= $parseUrl[$i]['content'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Suspendido";
				} else {
					$status = "Activo";
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$msisdn;?></td>
					<td><?=$sender;?></td>
					<td><?=$created_time;?></td>
                    <td><?=$content;?></td>
				</tr>
				<?php
			}
			?>
			</table>
			</div>
			<?php
			//pag_pages ($page, $total_pages, $url_app);
		
	}
	else
	{
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function trafficFile ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->reporte_carga_archivo[0]->titulo;
	$lbfechainicio		= $lan_file->$lan->reporte_carga_archivo[0]->lbfechainicio;
	$lbfechafin			= $lan_file->$lan->reporte_carga_archivo[0]->lbfechafin;
	$lbestado			= $lan_file->$lan->reporte_carga_archivo[0]->lbestado;
	$tbfecha			= $lan_file->$lan->reporte_carga_archivo[0]->tbfecha;
	$tbarchivo			= $lan_file->$lan->reporte_carga_archivo[0]->tbarchivo;
	$tbtotal			= $lan_file->$lan->reporte_carga_archivo[0]->tbtotal;
        $tbccost			= $lan_file->$lan->reporte_carga_archivo[0]->lbcentrocosto;
        $lbcentrocosto          = $lan_file->$lan->reporte_carga_archivo[0]->lbcentrocosto;
	$tbdescargar		= $lan_file->$lan->reporte_carga_archivo[0]->tbdescargar;
	$b_consultar		= $lan_file->$lan->botones[0]->b_consultar;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_report_mt_file");

	if($permission == "Y") {
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		$stage			= $_GET['stage'];
		$ccost			= $_GET['ccost'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
		
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($stage)) { $data_extra .= "&searchstage=".$stage; }
		if(!empty($ccost)) { $data_extra .= "&searchccost=".$ccost; }
		$url			= URL_WS."WSA-Telcel/api/process/consolidated?access_token=".$access_token.$data.$data_extra;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= count($parseUrl);
		
		if(!empty($start_date))
		{
			$dateFrom	= $start_date;
			$dateTo		= $end_date;
		} else {
			$dateFrom	= date('01-m-Y');
			$dateTo		= date('d-m-Y');
		}
		
		?>
		<div class="frm-label-big">
        	<label><?=$lbfechainicio;?></label>
        	<input type="text" name="start_date" id="start_date" class="frm" value="<?=$dateFrom;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador1">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbfechafin;?></label>
        	<input type="text" name="end_date" id="end_date" class="frm" value="<?=$dateTo;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador2">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbestado;?></label> <?php selectStageFile($stage); ?>
        </div>
        <div class="frm-label-big">
		<label><?=$lbcentrocosto;?></label>
		<?php filterCostCenter($ccost); ?> &nbsp;
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>  
	        <button class="frm-button" onClick="trafficFile();"><?=$b_consultar;?></button> 
        </div>
        <br>
        <br>
        <br>
        <br>
<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "start_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador1"
            }
            )
        </script>
        
<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "end_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador2"
            }
            )
        </script>
        <?php
        
		if(!empty($start_date))
		{
			if($totalRows == 0)
			{
				?>
                <div class="show-message">
                <?php
                showMessage (9);
                ?>
                </div>
                <?php
			}
			else
			{
				?>			
				<table>
				<tr>
					<th><?=$tbfecha;?></th>
                    <th><?=$tbarchivo;?></th>
					<th><?=$tbtotal;?></th>
					<th><?=$tbccost;?></th>
                    <th><?=$tbdescargar;?></th>
				</tr>
				<?php
				
				for($i = 0; $i < count($parseUrl); $i++)
				{
					$report_date	= returnFormatDate($parseUrl[$i]['created_time']);
					$report_file	= $parseUrl[$i]['created_time'];
					$filename		= $parseUrl[$i]['file_name'];
					$pid			= $parseUrl[$i]['pid'];
					$count			= $parseUrl[$i]['total'];
					$ccost			= $parseUrl[$i]['ccost'];
					$report_xls		= substr($report_file, 0,10);
					$url_excel		= URL_WS."WSA-Telcel/api/process/detailed?access_token=".$access_token."&id_country=".$id_country."&id_process=".$pid.$data_extra;
					$fileReport		= getFileReport($url_excel);
					
					$tr_color = $i % 2;
					
					if($tr_color == 0)
					{
						$class	= ""; 
					} else {
						$class	= "tr-color"; 
					}
					
					
					?>
					<tr class="<?=$class;?>">
						<td><?=$report_date;?></td>
						<td><?=str_ireplace("UTF-8_","",$filename);str_ireplace("LATIN1_","",$filename);?></td>
						<td><?=$count;?></td>
						<td><?=$ccost;?></td>
                        <td><a href="ajaxFuncs/repTrafficFile.php?report=<?=$fileReport;?>&filename=<?=$filename?>"><img src="images/icons/excel.png"></a></td>
					</tr>
					<?php
				}
				?>
				<tr>
					<th></th>
					<th></th>
                    <th></th>
                    <th></th>
					<th></th>
				</tr>
				</table>
				
				<?php
			}
		}
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function trafficCountry ()
{
	$lan 						= $_SESSION['lang'];
	$lan_file					= simplexml_load_file('language.xml');
	$lb_titulo					= $lan_file->$lan->reporte_enviados_por_dia[0]->titulo;
	$lbfechainicio				= $lan_file->$lan->reporte_enviados_por_dia[0]->lbfechainicio;
	$lbfechafin					= $lan_file->$lan->reporte_enviados_por_dia[0]->lbfechafin;
	$lbservicio					= $lan_file->$lan->reporte_enviados_por_dia[0]->lbservicio;
	$lbtipocanal				= $lan_file->$lan->reporte_enviados_por_dia[0]->lbtipocanal;
	$lbtiposervicio				= $lan_file->$lan->reporte_enviados_por_dia[0]->lbtiposervicio;
	$tbempresa_titulo			= $lan_file->$lan->reporte_enviados_por_dia[0]->tbempresa_titulo;
	$tbempresa_fecha			= $lan_file->$lan->reporte_enviados_por_dia[0]->tbempresa_fecha;
	$tbempresa_id				= $lan_file->$lan->reporte_enviados_por_dia[0]->tbempresa_id;
	$tbempresa_nombre			= $lan_file->$lan->reporte_enviados_por_dia[0]->tbempresa_nombre;
	$tbempresa_tipo				= $lan_file->$lan->reporte_enviados_por_dia[0]->tbempresa_tipo;
	$tbservicio_titulo			= $lan_file->$lan->reporte_enviados_por_dia[0]->tbservicio_titulo;
	$tbservicio_id				= $lan_file->$lan->reporte_enviados_por_dia[0]->tbservicio_id;
	$tbservicio_marcacion		= $lan_file->$lan->reporte_enviados_por_dia[0]->tbservicio_marcacion;
	$tbservicio_nombre			= $lan_file->$lan->reporte_enviados_por_dia[0]->tbservicio_nombre;
	$tbservicio_tipo			= $lan_file->$lan->reporte_enviados_por_dia[0]->tbservicio_tipo;
	$tbservicio_pais			= $lan_file->$lan->reporte_enviados_por_dia[0]->tbservicio_pais;
	$tbservicio_entrada			= $lan_file->$lan->reporte_enviados_por_dia[0]->tbservicio_entrada;
	$tbtrafico_mo_titulo		= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_mo_titulo;
	$tbtrafico_mo_cursados		= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_mo_cursados;
	$tbtrafico_mo_nocursados	= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_mo_nocursados;
	$tbtrafico_mo_total			= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_mo_total;
	$tbtrafico_mt_titulo		= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_mt_titulo;
	$tbtrafico_mt_cursados		= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_mt_cursados;
	$tbtrafico_mt_nocursados	= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_mt_nocursados;
	$tbtrafico_mt_total			= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_mt_total;
	$tbtrafico_momt_titulo		= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_momt_titulo;
	$tbtrafico_momt_cursados	= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_momt_cursados;
	$tbtrafico_momt_nocursados	= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_momt_nocursados;
	$tbtrafico_momt_total		= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtrafico_momt_total;
	$tbtotales					= $lan_file->$lan->reporte_enviados_por_dia[0]->tbtotales;
	$tberror_date				= $lan_file->$lan->reporte_enviados_por_dia[0]->tberror_date;
	$tbporcentajes				= $lan_file->$lan->reporte_enviados_por_dia[0]->tbporcentajes;
	$b_consultar				= $lan_file->$lan->botones[0]->b_consultar;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_report_mt_daily");
	
	if($permission == "Y") 
	{
		$access_token	= $_SESSION['access_token'];
		$id_cnt			= $_SESSION['id_country'];
		$id_country		= $_GET['id_country'];
		$type			= $_SESSION['type'];
		//$type			= "LOCAL";
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$services		= arrayService();
		$countrys		= groupArray($paises, "code");
		//var_dump($services);
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		if($type == "GLOBAL")
		{
			$id_pais=$id_country;
		}
		else
		{
			$id_pais=$id_cnt;
		}
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
				
		$type_service	= $_GET['type_service'];
		$type_channel	= $_GET['type_channel'];
		$getCountry		= $_GET['country'];
		$service		= $_GET['service'];
		$order			= $_GET['order'];
		$data			.= "&id_country=".$id_pais;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($service)) { $data_extra .= "&id_service=".$service; }
		if(!empty($type_channel)) { $data_extra .= "&searchchannel=".$type_channel; }
		if(!empty($type_service)) { $data_extra .= "&searchtype=".$type_service; }
		
		$url			= URL_WS."WSA-Telcel/api/tgg/traffic/date?access_token=".$access_token.$data.$data_extra;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$error_code		= $parseUrl[0]['error_code'];
		$error_msg		= $parseUrl[0]['error_msg'];
		
		$totalRows		= count($parseUrl);
		//$totalRows		= 1;
		
		if(!empty($start_date))
		{
			$dateFrom	= $start_date;
			$dateTo		= $end_date;
		} else {
			$dateFrom	= date('01-m-Y');
			$dateTo		= date('d-m-Y');
		}
		
		?>
        
        <div class="frm-label-big">
        	<label><?=$lbfechainicio;?></label>
        	<input type="text" name="start_date" id="start_date" class="frm" value="<?=$dateFrom;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador1">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbfechafin;?></label>
        	<input type="text" name="end_date" id="end_date" class="frm" value="<?=$dateTo;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador2">
        </div>
        
        <div class="frm-label-big">
        	<label>Pais</label> <?php selectcbocountry($id_country);?>
        </div>
        
            <div class="frm-label-big">
        	<label><?=$lbservicio;?></label> 
            <?php selectRepService($service); ?>
        	</div>

        <div class="frm-label-big">
        	<label><?=$lbtipocanal;?></label> <?php selectTypeChannel($type_channel);?>
        </div>
        <div class="frm-label-big">
        	<label><?=$lbtiposervicio;?></label> <?php selectTypeService($type_service);?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>  
	        <button class="frm-button" onClick="trafficCountry();"><?=$b_consultar;?></button> 
        </div>
        <br>
        <br>
		<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "start_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador1"
            }
            )
        </script>
        
        <script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "end_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador2"
            }
            )
        </script>
        <div id="resultado"></div>
        <?php
        
		if(!empty($start_date))
		{
			if($totalRows == 0)
			{
				?>
                <div class="show-message">
                <?php
                showMessage (9);
                ?>
                </div>
                <?php
			} else
			if($error_code == -1009)
			{
				//$error_msg='La Fecha No debe ser mayor a 6 meses';
				?>
                <div class="show-message"><img src="images/icons/alert.png"> <?=$tberror_date;?></div>
                <?php
			}
			elseif($error_code != 0)
			{
				?>
                <div class="show-message"><img src="images/icons/alert.png"> <?=$error_msg;?></div>
                <?php
			}
			
			else
			{
				
				$urlRepTotal	= URL_WS."WSA-Telcel/api/tgg/traffic/date/file?access_token=".$access_token."&id_country=".$id_pais."&start_date=".$start_date_get."&end_date=".$end_date_get.$data_extra;
				$fileRepTotal	= getFileReport($urlRepTotal);
				
				
				
				?>
                <div class="frame-scroll-table">
				<div class="show-message">
                    <a href="ajaxFuncs/repTrafficCountry.php?report=<?=$fileRepTotal;?>"><?php showMessage(58); ?></a>
                </div>
				<table class="scroll-table">
                <tr>
					<th class="th-nocolor"></th>
					<th colspan="3"><?=$tbempresa_titulo;?></th>
                    <th colspan="6"><?=$tbservicio_titulo;?></th>
                    <th colspan="3"><?=$tbtrafico_mo_titulo;?></th>
                    <th colspan="3"><?=$tbtrafico_mt_titulo;?></th>
                    <th colspan="3"><?=$tbtrafico_momt_titulo;?></th>
				</tr>
				<tr>
					<th width="70"><?=$tbempresa_fecha;?></th>
					<th><?=$tbempresa_id;?></th>
					<th><?=$tbempresa_nombre;?></th>
					<th><?=$tbempresa_tipo;?></th>
                    <th><?=$tbservicio_id;?></th>
					<th><?=$tbservicio_marcacion;?></th>
                    <th><?=$tbservicio_nombre;?></th>
					<th><?=$tbservicio_tipo;?></th>
                    <th><?=$tbservicio_pais;?></th>
                    <th><?=$tbservicio_entrada;?></th>
					<th><?=$tbtrafico_mo_cursados;?></th>
                    <th><?=$tbtrafico_mo_nocursados;?></th>
                    <th><?=$tbtrafico_mo_total;?></th>
					<th><?=$tbtrafico_mt_cursados;?></th>
                    <th><?=$tbtrafico_mt_nocursados;?></th>
                    <th><?=$tbtrafico_mt_total;?></th>
                    <th><?=$tbtrafico_momt_cursados;?></th>
                    <th><?=$tbtrafico_momt_nocursados;?></th>
                    <th><?=$tbtrafico_momt_total;?></th>
				</tr>
				<?php
				
				for($i = 0; $i < count($parseUrl); $i++)
				{
					$report_date		= returnFormatDate($parseUrl[$i]['date']);
					$report_file		= $parseUrl[$i]['date'];
					$id_company			= $parseUrl[$i]['id_company'];
					$company_type		= $parseUrl[$i]['company_type'];
					$comp_country_name	= $parseUrl[$i]['comp_country_name'];
					$company_name		= $parseUrl[$i]['company_name'];
					$service_id			= $parseUrl[$i]['service_id'];
					$service_type		= $parseUrl[$i]['service_type'];
					$service_tag		= $parseUrl[$i]['service_tag'];
					$service_name		= $parseUrl[$i]['service_name'];
					$serv_country_name	= $parseUrl[$i]['serv_country_name'];
					$channel_in			= $parseUrl[$i]['channel_in'];
					$mo_ok				= $parseUrl[$i]['mo_ok'];
					$mo_nook			= $parseUrl[$i]['mo_nook'];
					$mo_total			= $parseUrl[$i]['mo_total'];
					$mt_ok				= $parseUrl[$i]['mt_ok'];
					$mt_nook			= $parseUrl[$i]['mt_nook'];
					$mt_total			= $parseUrl[$i]['mt_total'];
					$total_ok			= $parseUrl[$i]['total_ok'];
					$total_nook			= $parseUrl[$i]['total_nook'];
					$total				= $parseUrl[$i]['total'];
					//$url_excel			= URL_WS."WSA-Telcel/api/message/detailed?access_token=".$access_token."&id_country=".$id_country."&start_date=".$report_file."&end_date=".$report_file.$data_extra;
					//$fileReport			= getFileReport($url_excel);
					
					$tr_color = $i % 2;
					
					if($tr_color == 0)
					{
						$class	= ""; 
					} else {
						$class	= "tr-color"; 
					}
					?>
					<tr class="<?=$class;?>">
						<td><?=$report_date;?></td>
						<td><?=$id_company;?></td>
						<td><?=$company_name;?></td>
						<td><?=$company_type;?></td>
                        <td><?=$service_id;?></td>
						<td><?=$service_tag;?></td>
                        <td><?=$service_name;?></td>
						<td><?=$service_type;?></td>
                        <td><?=$serv_country_name;?></td>
                        <td><?=$channel_in;?></td>
						<td><?=$mo_ok;?></td>
						<td><?=$mo_nook;?></td>
						<td><?=$mo_total;?></td>
						<td><?=$mt_ok;?></td>
                        <td><?=$mt_nook;?></td>
                        <td><?=$mt_total;?></td>
                        <td><?=$total_ok;?></td>
                        <td><?=$total_nook;?></td>
                        <td><?=$total;?></td>
					</tr>
					<?php
					$total_mo_ok	= $mo_ok + $total_mo_ok;
					$total_mo_nook	= $mo_nook + $total_mo_nook;
					$total_mo_total	= $mo_total + $total_mo_total;
					$total_mt_ok	= $mt_ok + $total_mt_ok;
					$total_mt_nook	= $mt_nook + $total_mt_nook;
					$total_mt_total	= $mt_total + $total_mt_total;
					$total__ok		= $total_ok + $total__ok;
					$total__nook	= $total_nook + $total__nook;
					$total_			= $total + $total_;
					
					$por_mo_total	= $total_mo_ok + $total_mo_nook;
					$por_mt_total	= $total_mt_ok + $total_mt_nook;
					$por_momt_total	= $total__ok + $total__nook;
			
					$por_mo_ok		= returnPor($total_mo_ok, $por_mo_total);
					$por_mo_nook	= returnPor($total_mo_nook, $por_mo_total);
					$por_mt_ok		= returnPor($total_mt_ok, $por_mt_total);
					$por_mt_nook	= returnPor($total_mt_nook, $por_mt_total);
					$por_momt_ok	= returnPor($total__ok, $por_momt_total);
					$por_momt_nook	= returnPor($total__nook, $por_momt_total);	
					
				}
				?>
                <tr>
					<th colspan="10"><?=$tbtotales;?></th>
                    <th><?=substr(number_format($total_mo_ok, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_mo_nook, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_mo_total, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_mt_total, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_mt_nook, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_mt_total, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total__ok, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total__nook, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_, 2, ",", "."), 0, -3);?></th>
				</tr>
                <tr>
					<th colspan="10"><?=$tbporcentajes;?></th>
                    <th><?=$por_mo_ok;?>%</th>
                    <th><?=$por_mo_nook;?>%</th>
                    <th></th>
                    <th><?=$por_mt_ok;?>%</th>
                    <th><?=$por_mt_nook;?>%</th>
                    <th></th>
                    <th><?=$por_momt_ok;?>%</th>
                    <th><?=$por_momt_nook;?>%</th>
                    <th></th>
				</tr>
				</table>
				<div id="download"></div>
                </div>
				<?php
			}
		}
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function returnPor ($value1, $total) 
{
	//$value		= $value1 * 100;
	//$value		= $value / $total;
	
	return round($value1 / $total * 100, 2);
}

function trafficCompany ()
{
	$lan 								= $_SESSION['lang'];
	$lan_file							= simplexml_load_file('language.xml');
	$lb_titulo							= $lan_file->$lan->reporte_enviados_por_empresa[0]->titulo;
	$lbfechainicio						= $lan_file->$lan->reporte_enviados_por_empresa[0]->lbfechainicio;
	$lbfechafin							= $lan_file->$lan->reporte_enviados_por_empresa[0]->lbfechafin;
	$lbservicio							= $lan_file->$lan->reporte_enviados_por_empresa[0]->lbservicio;
	$lbtipocanal						= $lan_file->$lan->reporte_enviados_por_empresa[0]->lbtipocanal;
	$lbtiposervicio						= $lan_file->$lan->reporte_enviados_por_empresa[0]->lbtiposervicio;
	$tbempresa_titulo					= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbempresa_titulo;
	$tbempresa_fecha					= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbempresa_fecha;
	$tbempresa_id						= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbempresa_id;
	$tbempresa_nombre					= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbempresa_nombre;
	$tbempresa_tipo						= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbempresa_tipo;
	$tbservicio_titulo					= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbservicio_titulo;
	$tbservicio_id						= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbservicio_id;
	$tbservicio_marcacion				= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbservicio_marcacion;
	$tbservicio_nombre					= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbservicio_nombre;
	$tbservicio_tipo					= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbservicio_tipo;
	$tbservicio_pais					= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbservicio_pais;
	$tbtrafico_exitoso					= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_exitoso;
	$tbtrafico_exitoso_totalmo			= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_exitoso_totalmo;
	$tbtrafico_exitoso_totaldr			= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_exitoso_totaldr;
	$tbtrafico_exitoso_totalmt			= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_exitoso_totalmt;
	$tbtrafico_exitoso_totalcobrar		= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_exitoso_totalcobrar;
	$tbtrafico_exitoso_totaldrsincobrar	= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_exitoso_totaldrsincobrar;
	$tbtrafico_exitoso_total			= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_exitoso_total;
	$tbtrafico_noexitoso				= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_noexitoso;
	$tbtrafico_noexitoso_totalmo		= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_noexitoso_totalmo;
	$tbtrafico_noexitoso_totalmt		= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_noexitoso_totalmt;
	$tbtrafico_noexitoso_total			= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtrafico_noexitoso_total;
	$tbtotales							= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbtotales;
	$tbporcentajes						= $lan_file->$lan->reporte_enviados_por_empresa[0]->tbporcentajes;
	$b_consultar						= $lan_file->$lan->botones[0]->b_consultar;
	$tberror_date				= $lan_file->$lan->reporte_enviados_por_empresa[0]->tberror_date;
	?>
    <h1><?=$lb_titulo;?></h1>

    <?php
	$permission = permission ("view_report_mt_company");
	
	if($permission == "Y") 
	{
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		$type			= $_SESSION['type'];
		//$type			= "LOCAL";
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$services		= arrayService();
		$countrys		= groupArray($paises, "code");
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
				
		$type_service	= $_GET['type_service'];
		$type_channel	= $_GET['type_channel'];
		$getCountry		= $_GET['country'];
		$service		= $_GET['service'];
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($service)) { $data_extra .= "&id_service=".$service; }		
		$url			= URL_WS."WSA-Telcel/api/tgg/traffic/total?access_token=".$access_token.$data.$data_extra;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$error_code		= $parseUrl[0]['error_code'];
		$error_msg		= $parseUrl[0]['error_msg'];
		
		$totalRows		= count($parseUrl);
		//$totalRows		= 1;
		
		if(!empty($start_date))
		{
			$dateFrom	= $start_date;
			$dateTo		= $end_date;
		} else {
			$dateFrom	= date('01-m-Y');
			$dateTo		= date('d-m-Y');
		}
		
		?>
        
        <div class="frm-label-big">
        	<label><?=$lbfechainicio;?></label>
        	<input type="text" name="start_date" id="start_date" class="frm" value="<?=$dateFrom;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador1">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbfechafin;?></label>
        	<input type="text" name="end_date" id="end_date" class="frm" value="<?=$dateTo;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador2">
        </div>
        <div class="frm-label-big">
        	<label><?=$lbservicio;?></label> 
        	<?php selectRepService($service); ?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>  
	        <button class="frm-button" onClick="trafficCompany();"><?=$b_consultar;?></button> 
        </div>
        <br>
        <br>
<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "start_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador1"
            }
            )
        </script>
        
<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "end_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador2"
            }
            )
        </script>
        <div id="resultado"></div>
        <?php
        
		if(!empty($start_date))
		{
			if($totalRows == 0)
			{
				?>
                <div class="show-message">
                <?php
                showMessage (9);
                ?>
                </div>
                <?php
			} else
			if($error_code == -1009)
			{
				//$error_msg='La Fecha No debe ser mayor a 6 meses';
				?>
                <div class="show-message"><img src="images/icons/alert.png"> <?=$tberror_date;?></div>
                <?php
			}
			elseif($error_code != 0)
			{
				?>
                <div class="show-message"><img src="images/icons/alert.png"> <?=$error_msg;?></div>
                <?php
			}
			else
			{
				
				$urlRepTotal	= URL_WS."WSA-Telcel/api/tgg/traffic/total/file?access_token=".$access_token."&id_country=".$id_country."&start_date=".$start_date_get."&end_date=".$end_date_get.$data_extra;
				$fileRepTotal	= getFileReport($urlRepTotal);
				
				
				
				?>
                <div class="frame-scroll-table">
				<div class="show-message">
                    <a href="ajaxFuncs/repTrafficCompany.php?report=<?=$fileRepTotal;?>"><?php showMessage(58); ?></a>
                </div>
                <?php
				
				?>
                
				<table>
                <tr>
					<th class="th-nocolor"></th>
					<th colspan="3"><?=$tbempresa_titulo;?></th>
                    <th colspan="5"><?=$tbservicio_titulo;?></th>
                    <th colspan="6"><?=$tbtrafico_exitoso;?></th>
                    <th colspan="3"><?=$tbtrafico_noexitoso;?></th>
				</tr>
				<tr>
					<th width="70"><?=$tbempresa_fecha;?></th>
					<th><?=$tbempresa_id;?></th>
					<th><?=$tbempresa_nombre;?></th>
					<th><?=$tbempresa_tipo;?></th>
                    <th><?=$tbservicio_id;?></th>
					<th><?=$tbservicio_marcacion;?></th>
                    <th><?=$tbservicio_nombre;?></th>
					<th><?=$tbservicio_tipo;?></th>
                    <th><?=$tbservicio_pais;?></th>
                    <th><?=$tbtrafico_exitoso_totalmo;?></th>
					<th><?=$tbtrafico_exitoso_totaldr;?></th>
                    <th><?=$tbtrafico_exitoso_totalmt;?></th>
                    <th><?=$tbtrafico_exitoso_totalcobrar;?></th>
					<th><?=$tbtrafico_exitoso_totaldrsincobrar;?></th>
                    <th><?=$tbtrafico_exitoso_total;?></th>
                    <th><?=$tbtrafico_noexitoso_totalmo;?></th>
					<th><?=$tbtrafico_noexitoso_totalmt;?></th>
                    <th><?=$tbtrafico_noexitoso_total;?></th>
				</tr>
				<?php
				
				for($i = 0; $i < count($parseUrl); $i++)
				{
					$report_date		= returnFormatDate($parseUrl[$i]['date']);
					$report_file		= $parseUrl[$i]['date'];
					$id_company			= $parseUrl[$i]['id_company'];
					$company_type		= $parseUrl[$i]['company_type'];
					$comp_country_name	= $parseUrl[$i]['comp_country_name'];
					$company_name		= $parseUrl[$i]['company_name'];
					$service_id			= $parseUrl[$i]['service_id'];
					$service_type		= $parseUrl[$i]['service_type'];
					$service_tag		= $parseUrl[$i]['service_tag'];
					$service_name		= $parseUrl[$i]['service_name'];
					$serv_country_name	= $parseUrl[$i]['serv_country_name'];
					$mo_ok				= $parseUrl[$i]['total_mo_ok'];
					$dr_bill_ok			= $parseUrl[$i]['total_dr_bill_ok'];
					$mt_ok				= $parseUrl[$i]['total_mt_ok'];
					$bill_ok			= $parseUrl[$i]['total_bill_ok'];
					$dr_no_bill_ok		= $parseUrl[$i]['total_dr_no_bill_ok'];
					$ok					= $parseUrl[$i]['total_ok'];
					$mo_nook			= $parseUrl[$i]['total_mo_nook'];
					$mt_nook			= $parseUrl[$i]['total_mt_nook'];
					$nook				= $parseUrl[$i]['total_nook'];
					//$url_excel			= URL_WS."WSA-Telcel/api/message/detailed?access_token=".$access_token."&id_country=".$id_country."&start_date=".$report_file."&end_date=".$report_file.$data_extra;
					//$fileReport			= getFileReport($url_excel);
					
					$tr_color = $i % 2;
					
					if($tr_color == 0)
					{
						$class	= ""; 
					} else {
						$class	= "tr-color"; 
					}
					?>
					<tr class="<?=$class;?>">
						<td><?=$report_date;?></td>
						<td><?=$id_company;?></td>
						<td><?=$company_name;?></td>
						<td><?=$company_type;?></td>
                        <td><?=$service_id;?></td>
						<td><?=$service_tag;?></td>
                        <td><?=$service_name;?></td>
						<td><?=$service_type;?></td>
                        <td><?=$serv_country_name;?></td>
						<td><?=$mo_ok;?></td>
						<td><?=$dr_bill_ok;?></td>
						<td><?=$mt_ok;?></td>
                        <td><?=$bill_ok;?></td>
						<td><?=$dr_no_bill_ok;?></td>
                        <td><?=$ok;?></td>
                        <td><?=$mo_nook;?></td>
                        <td><?=$mt_nook;?></td>
                        <td><?=$nook;?></td>
					</tr>
					<?php
					$total_mo_ok		= $mo_ok + $total_mo_ok;
					$total_dr_bill_ok	= $dr_bill_ok + $total_dr_bill_ok;
					$total_mt_ok		= $mt_ok + $total_mt_ok;
					$total_bill_ok		= $bill_ok + $total_bill_ok;
					$total_dr_no_bill_ok= $dr_no_bill_ok + $total_dr_no_bill_ok;
					$total_mt_total		= $mt_total + $total_mt_total;
					$total_ok			= $ok + $total_ok;
					$total_mo_nook		= $mo_nook + $total_mo_nook;
					$total_mt_nook		= $mt_nook + $total_mt_nook;
					$total_nook			= $nook + $total_nook;
					
					$total				= $total_bill_ok + $total_dr_no_bill_ok + $total_mt_nook + $total_mo_nook;
					$total_tf_ok		= $total_bill_ok + $total_dr_no_bill_ok;
					$total_tf_nook		= $total_mt_nook + $total_mo_nook;
					$por_mt_total		= $total_mt_ok + $total_mt_nook;
					$por_momt_total		= $total__ok + $total__nook;
			
					$por_bill_ok		= returnPor($total_bill_ok, $total_tf_ok);
					$por_dr_no_bill_ok	= returnPor($total_dr_no_bill_ok, $total_tf_ok);
					$por_mo_nook		= returnPor($total_mo_nook, $total_tf_nook);
					$por_mt_nook		= returnPor($total_mt_nook, $total_tf_nook);
					
					
				}
				?>
                <tr>
					<th colspan="9"><?=$tbtotales;?></th>
                    <th><?=substr(number_format($total_mo_ok, 2, ",", "."), 0, -3);?> </th>
                    <th><?=substr(number_format($total_dr_bill_ok, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_mt_ok, 2, ",", "."), 0, -3)?></th>
                    <th><?=substr(number_format($total_bill_ok, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_dr_no_bill_ok, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_ok, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_mo_nook, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_mt_nook, 2, ",", "."), 0, -3);?></th>
                    <th><?=substr(number_format($total_nook, 2, ",", "."), 0, -3);?></th>
				</tr>
                <tr>
					<th colspan="9"><?=$tbporcentajes;?></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th><?=$por_bill_ok;?>%</th>
                    <th><?=$por_dr_no_bill_ok;?>%</th>
                    <th></th>
                    <th><?=$por_mo_nook;?>%</th>
                    <th><?=$por_mt_nook;?>%</th>
                    <th></th>
				</tr>
				</table>
				<div id="download"></div>
                </div>
				<?php
			}
		}
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function changePass ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->cambio_clave[0]->titulo;
	$lb_antigua_clave	= $lan_file->$lan->cambio_clave[0]->antigua_clave;
	$lb_nueva_clave		= $lan_file->$lan->cambio_clave[0]->nueva_clave;
	$lb_re_nueva_clave	= $lan_file->$lan->cambio_clave[0]->re_nueva_clave;
	$b_modificar		= $lan_file->$lan->cambio_clave[0]->b_modificar;
	$campo_obligatorio	= $lan_file->$lan->general[0]->campo_obligatorio;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("pass_change");
	
	if($permission == "Y") {
		?>
        <div class="frm-label-big">
        	<label><?=$lb_antigua_clave;?></label> &nbsp; (*)
        	<input type="password" name="old_password" id="old_password" class="frm">
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_nueva_clave;?></label> &nbsp;  (*)
        	<input type="password" name="new_password" id="new_password" class="frm">
        </div>
        <div class="frm-label-big">
        	<label><?=$lb_re_nueva_clave;?></label> &nbsp;  (*) 
        	<input type="password" name="repeat_password" id="repeat_password" class="frm">
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	(*) <?=$campo_obligatorio;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label> 
        	<button class="frm-button" onClick="changePass('<?=$lan;?>');"><?=$b_modificar;?></button>
        </div>
        <br>
        <div id="resultado"></div>
        <?php
	} else {
		?>
        <div class="show-message">
        	<?php showMessage (1); ?>
        </div>
        <?php
	}
}

function faq ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->faq[0]->titulo;
	?>
    <h1><?=$lb_titulo;?></h1>
    <?php
	$permission = permission ("view_list_prefix");
	
	
	
	if($permission == "Y") 
	{
		
        
        $access_token			= $_SESSION['access_token'];
		$id_service				= $_GET['id_service'];
		$msisdn					= $_GET['msisdn'];
		$url					= URL_WS."WSA-Telcel/api/tgg/service/".$id_service."?access_token=".$access_token;
		$parseUrlTag = parseUrl($url);
		$service_name			= $parseUrlTag['service_name'];
		$service_tag			= $parseUrlTag['service_tag'];
		
		?>
       
        
        	<?=$largo_prefijos;?>
                  <?php getPrefMxCountry(); ?>
                
       
        
        <?php
	}
	 else {
		?>
<div class="show-message">
        <?php
		showMessage (51); 
		?>
        </div>
        <?php
	}
}

function manual ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->manual[0]->titulo;
	?>
<h1><?=$lb_titulo;?></h1>
    <p>
      <?php
	$permission = permission ("view_manual");
	
	
	
	if($permission == "Y") 
	{
		
  
		
		?>
    </p>
    <p><img src="images/png/Brazil-Flag-32.png" >   <a href="TGG_Por.pdf" target="_blank"> MANUAL DE USUARIO</a></p>
<p>&nbsp;</p>
    <p>
    <img src="images/png/England-Flag-32.png" > <a href="TGG_Eng.pdf" target="_blank">MANUAL DE USUARIO</a></p>
    <p>&nbsp;</p>
    <p>
    <img src="images/png/Spain-Flag-32.png" > <a href="TGG_Esp.pdf" target="_blank">MANUAL DE USUARIO</a></p>
    <p>
      
      
      
      <?php
	}
	 else {
		?>
    </p>
    <div class="show-message">
      <?php
		showMessage (51); 
		?>
      </div>
        <?php
	}
}

function logOut ()
{
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('language.xml');
	$lb_titulo			= $lan_file->$lan->sesion[0]->titulo;
	$access_token	= $_SESSION['access_token'];
	$url			= URL_WS."WSA-Telcel/api/oauth/revoke?access_token=".$access_token;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	//$parseUrl		= json_decode($pUrl,true);
	
	if($statusCode == 401)
	{
		//header("refresh: 2; url = index.php");
		?>
        <script>
			setTimeout ("redireccionar('index.php')", 2000);
		</script>
        <div id="logout"><?=$lb_titulo;?></div>
        <?php
	} else {
		?>
        <script>
			setTimeout ("redireccionar('index.php')", 2000);
		</script>
        <?php
		//header("refresh: 2; url = index.php");
		?>
        
        <div id="logout"><?=$lb_titulo;?></div>
        <?php
	}
}

function permission ($module)
{
	
	$access_token	= $_SESSION['access_token'];
	$refresh_token	= $_SESSION['refresh_token'];

	$data			= "";
	$data			.= $module;
	$data			.= "?access_token=".$access_token;
	$url			= URL_WS."WSA-Telcel/api/user/permission/".$data;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	$parseUrl		= json_decode($pUrl,true);
	$response		= $parseUrl;
	

	if($statusCode == "401" || $response['error'] == "invalid_token")
	{
		$data1			.= "grant_type=refresh_token";
		$data1			.= "&client_id=trusted-client";
		$data1			.= "&refresh_token=".$refresh_token;
		$url2			= URL_WS."WSA-Telcel/oauth/token?".$data1;
		$iUrl1			= curl_init($url2);
		curl_setopt($iUrl1, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl1			= curl_exec($iUrl1);
		$pUrl2			= json_decode($pUrl1,true);
		
		$_SESSION['access_token'] = $pUrl2['access_token'];
		
		$url_dir = "http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
		?>
        <script>
			setTimeout ("redireccionar('?module=logOut')", 1000);
		</script>
        <?php
	
	} else {
		
		if($response == true)
		{
			$return = "Y";
		} elseif($response == false) 
		{
			$return = "N";
		} 		
	}
		
	return $return;
	
	
	
	curl_close($iUrl);	
}


function pag_pages ($page, $total_pages, $url)
{
	?>
    <div class="pagination-container">
	<?php
	if($page > 1) {
		echo "<a title='Primera P&aacute;gina' href='$url&p=1'><img src='images/icons/pag_first.png'></a> "; 
	} else {
	}
	
	if ($page> 1) {
		echo "<a title='Anterior' href='$url&p=".($page-1)."'><img src='images/icons/pag_back.png'></a> "; 
	} else {
	}
	
    for($i = $page-3; $i < $page; $i++) {
		if($i < 1) {
		} else {
			if($i <= 9) { 
				$i = "0$i";
			}
			echo " <a title='P&aacute;gina $i' href='$url&p=$i' class='pag-off'>$i</a> ";
		}
	}
	
	if($i <= 9) {
		$i = "0$i";
	}
	
	echo "<b class='pag-on'>$i</b>";
	
	for($n = $page+1; $n < $page+6; $n++) {
		if($n > $total_pages) {
		} else {
			if($n <= 9) {
				$n = "0$n";
			}
			echo " <a title='P&aacute;gina $n' href='$url&p=$n' class='pag-off'>$n</a> ";
		}
	}
	
	if ($page < $total_pages) {
		echo "<a title='Siguiente' href='$url&p=".($page+1)."'><img src='images/icons/pag_next.png'></a> "; 
	} else {
	}
	
	if ($page < $total_pages) {
		echo " <a title='Última P&aacute;gina' href='$url&p=$total_pages'><img src='images/icons/pag_last.png'></a> "; 
	} else {
	}
	?> 
    </div>
    <br>
    <?php
}
?>
