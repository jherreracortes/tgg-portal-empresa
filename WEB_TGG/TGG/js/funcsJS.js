function redireccionar(url)
{
  window.location = url;
} 

function cargarXMLDoc(archivoXML) 
{
 var xmlDoc;

 if (window.XMLHttpRequest)
   {
    xmlDoc = new window.XMLHttpRequest();
    xmlDoc.open("GET", archivoXML, false);
    xmlDoc.send("");
    return xmlDoc.responseXML;
   }
 // para IE 5 y IE 6
 else if (ActiveXObject("Microsoft.XMLDOM"))
   {
    xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
    xmlDoc.async = false;
    xmlDoc.load(archivoXML);
    return xmlDoc;
   }
 alert("Error cargando el documento.");
 return null;
}

function download (report)
{
	report		= report;
	var parametros = {
		"report" : report
	};
	
	$.ajax({
		url:   'ajaxFuncs/download.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#download").html(data);
			window.location = "hola.csv";		
		},
        error: function(data) 
		{
			$("#download").html("Error");			
		},
	});
}

function login() 
{
    lan = "es";
    
    grant_type = "password";
    client_id  = "trusted-client";
    user_name  = document.getElementById("user").value;
    password   = document.getElementById("passwd").value; 
    
    var xmlDoc = cargarXMLDoc('language.xml');
    if (xmlDoc !== null)
    {
        var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
        for (var i = 0; i < links_tag.length; i++)
        {
            var msg_username = links_tag[i].getElementsByTagName("usuario")[0].childNodes[0].nodeValue;
            var msg_contrasena = links_tag[i].getElementsByTagName("contrasena")[0].childNodes[0].nodeValue;
			 var msg_validacion = links_tag[i].getElementsByTagName("validacion_token")[0].childNodes[0].nodeValue;
			 var msg_validacion_cancel = links_tag[i].getElementsByTagName("validacion_token_cancel")[0].childNodes[0].nodeValue;
        }
    }
    
    if(empty(user_name))
    {
        alert(msg_username);
        document.getElementById("user").focus();
        return false;
    }
    
    if(empty(password))
    {
        alert(msg_contrasena);
        document.getElementById("passwd").focus();
        return false;
    }
    
    var parametros = {
        "grant_type" : grant_type,
        "client_id" : client_id,
        "username" : user_name,
        "password" : password
    };
    
    $("#resultado").html('<p style="text-align:center;"><img src="images/waiting.gif"></p>').removeClass('show-message');
    
    $.get('ajaxFuncs/checkToken.php', {username: user_name} , function(data){
        if(data === 'true') {
            if (confirm(msg_validacion)) {
                doLogin(parametros);
            } else {
                $("#resultado").html(msg_validacion_cancel).addClass('show-message');
            }
            console.log(data + ' hay que mostrar el mensaje');
        } else {
            doLogin(parametros);       
        }
    });
    
    /*if (doLogin === true) {
      $.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/login.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			
			if(data == "Y")
			{
				document.location='webapp.php';	
			} else {
				$("#resultado").html("<img src='images/icons/alert.png' align='left'> &nbsp;&nbsp; Usuario y/o Clave incorrecta").addClass('show-message');	
			}
			
			
		},
                error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
            });
        }*/
}

function doLogin(parametros)
{
    $.ajax({
        //url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
        url:   'ajaxFuncs/login.php',
        data: parametros,
        type:  'POST', 
        success: function(data) {
            if(data === "Y") {
                document.location='webapp.php';	
            } else {
                $("#resultado").html("<img src='images/icons/alert.png' align='left'> &nbsp;&nbsp; Usuario y/o Clave incorrecta").addClass('show-message');
            }
        },
        error: function(data)
        {
            $("#resultado").html("Error").addClass('show-message');
        }
    });
}

function AdminUserSearch (lan)
{
	username		= document.getElementById("username").value;
	//alert("variable username"+username);
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_usuario		= links_tag[i].getElementsByTagName("usuario_buscar")[0].childNodes[0].nodeValue;
		}
	}
	
	
	if(empty(username))
	{
		alert(msg_usuario);
		document.getElementById("username").focus();
		return false;

	} else
	{
		document.location= "?module=adminUser&searchname="+username;
	}
}

function adminAddUser(lan)
{
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_nombrecompleto		= links_tag[i].getElementsByTagName("nombrecompleto")[0].childNodes[0].nodeValue;
			var msg_usuario				= links_tag[i].getElementsByTagName("usuario")[0].childNodes[0].nodeValue;
			var msg_usuario_invalido	= links_tag[i].getElementsByTagName("usuario_invalido")[0].childNodes[0].nodeValue;
			var msg_contrasena			= links_tag[i].getElementsByTagName("contrasena")[0].childNodes[0].nodeValue;
			var msg_contrasena_invalida	= links_tag[i].getElementsByTagName("contrasena_invalida")[0].childNodes[0].nodeValue;
			var msg_perfil				= links_tag[i].getElementsByTagName("perfil")[0].childNodes[0].nodeValue;
			var msg_idioma				= links_tag[i].getElementsByTagName("idioma")[0].childNodes[0].nodeValue;
		}
	}

	username		= document.getElementById("username").value;
	fullname		= document.getElementById("fullname").value;
	password		= document.getElementById("password").value;
	role			= document.getElementById("role").value;
	language		= document.getElementById("language").value;
	var regex 		= /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
	
	if(empty(fullname))
	{
		alert(msg_nombrecompleto);
		document.getElementById("fullname").focus();
		return false;
	}
	
	if(empty(username))
	{
		alert(msg_usuario);
		document.getElementById("username").focus();
		return false;
	}

    if (!regex.test($('#username').val().trim())) 
	{
		alert(msg_usuario_invalido);
		document.getElementById("username").focus();
		return false;
    }
	
	if(empty(password))
	{
		alert(msg_contrasena);
		document.getElementById("password").focus();
		return false;
	}
	
	if(password.length < 8)
	{
		alert(msg_contrasena_invalida);
		document.getElementById("password").focus();
		return false;
	}
	
	if(empty(role))
	{
		alert(msg_perfil);
		document.getElementById("role").focus();
		return false;
	}
	
	if(empty(language))
	{
		alert(msg_idioma);
		document.getElementById("language").focus();
		return false;
	}
	
    var parametros = {
		"username" : username,
		"fullname" : fullname,
		"password" : password,
		"role" : role, 
		"language" : language
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminAddUser.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function adminEditUser(id_user, lan)
{
	username		= document.getElementById("username").value;
	fullname		= document.getElementById("fullname").value;
	password		= document.getElementById("password").value;
	role			= document.getElementById("role").value;
	language		= document.getElementById("language").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_nombrecompleto		= links_tag[i].getElementsByTagName("nombrecompleto")[0].childNodes[0].nodeValue;
			var msg_usuario				= links_tag[i].getElementsByTagName("usuario")[0].childNodes[0].nodeValue;
			var msg_usuario_invalido	= links_tag[i].getElementsByTagName("usuario_invalido")[0].childNodes[0].nodeValue;
			var msg_contrasena			= links_tag[i].getElementsByTagName("contrasena")[0].childNodes[0].nodeValue;
			var msg_contrasena_invalida	= links_tag[i].getElementsByTagName("contrasena_invalida")[0].childNodes[0].nodeValue;
			var msg_perfil				= links_tag[i].getElementsByTagName("perfil")[0].childNodes[0].nodeValue;
			var msg_idioma				= links_tag[i].getElementsByTagName("idioma")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(fullname))
	{
		alert(msg_nombrecompleto);
		document.getElementById("fullname").focus();
		return false;
	}
	
	if(!empty(password))
	{
		if(password.length < 8)
		{
			alert(msg_contrasena_invalida);
			document.getElementById("password").focus();
			return false;
		}
	}
	
	if(empty(role))
	{
		alert(msg_perfil);
		document.getElementById("role").focus();
		return false;
	}
	
	
	
	if(empty(language))
	{
		alert(msg_idioma);
		document.getElementById("language").focus();
		return false;
	}
	
    var parametros = {
		"id_user" : id_user,
		"username" : username,
		"fullname" : fullname,
		"password" : password,
		"role" : role,
		"language" : language
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminEditUser.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function adminStatusUser (id_user)
{
	status		= document.getElementById("status").value;
	
    var parametros = {
		"id_user" : id_user,
		"enabled" : status
	};
	
	$.ajax({
		url:   'ajaxFuncs/adminStatusUser.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}


function adminAddGroupList(lan)
{
	country			= document.getElementById("country").value;
	name_group		= document.getElementById("name_group").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_pais		= links_tag[i].getElementsByTagName("pais")[0].childNodes[0].nodeValue;
			var msg_lista		= links_tag[i].getElementsByTagName("lista_envio")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(country))
	{
		alert(msg_pais);
		document.getElementById("country").focus();
		return false;
	}
	
	if(empty(name_group))
	{
		alert(msg_lista);
		document.getElementById("name_group").focus();
		return false;
	}
	
    var parametros = {
		"name_group" : name_group,
		"country" : country
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminAddGroupList.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function adminStatusGroupList (id_group)
{
	status			= document.getElementById("status").value;
    var parametros	= {
		"id_group" : id_group,
		"enabled" : status
	};
	
	$.ajax({
		url:   'ajaxFuncs/adminStatusGroupList.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');		
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function AdminSearchGroupList (lan)
{	
	name_group		= document.getElementById("name_group").value;
	stagecolum		= document.getElementById("stagecolum").value;
	stagelist		= document.getElementById("stagelist").value;
	order			= $('input:radio[name=order]:checked').val();
	
	if (typeof order == "undefined")
	{
		order = "ASC";
	}
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_lista		= links_tag[i].getElementsByTagName("lista_envio")[0].childNodes[0].nodeValue;
		}
	}
	
	
	
		document.location= "?module=adminGroupList&searchname="+name_group+"&field="+stagecolum+"&enabled="+stagelist+"&order="+order;
	
}

function adminEditGroupListSearch (id_group, lan)
{	
	celular		= document.getElementById("celular").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_movil		= links_tag[i].getElementsByTagName("movil")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(celular))
	{
		alert(msg_movil);
		document.getElementById("celular").focus();
		return false;

	} else
	{
		document.location= "?module=adminEditGroupList&id_group="+id_group+"&searchmsisdn="+celular;
	}
}

function adminDeletePhoneGroupList (id_group, id_mobile)
{
    var parametros = {
		"id_group" : id_group,
		"id_mobile" : id_mobile
	};
	
	$.ajax({
		url:   'ajaxFuncs/adminDeletePhoneGroupList.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#response"+id_mobile).html(data);		
		},
        error: function(data) 
		{
			$("#response"+id_mobile).html("Error");			
		},
	});
}

function adminAddPhoneGroupList(id_group, lan)
{
	country			= document.getElementById("country").value;
	msisdn			= document.getElementById("msisdn").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_movil		= links_tag[i].getElementsByTagName("movil")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(msisdn))
	{
		alert(msg_movil);
		document.getElementById("msisdn").focus();
		return false;
	}

    var parametros = {
		"id_group" : id_group,
		"msisdn" : msisdn,
		"country" : country,
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminAddPhoneGroupList.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}


function adminAddFileGroupList(lan)
{
	country			= document.getElementById("country").value;
	phone_list		= document.getElementById("phone_list").value;
	id_group		= document.getElementById("id_group").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_pais		= links_tag[i].getElementsByTagName("pais")[0].childNodes[0].nodeValue;
			var msg_archivo		= links_tag[i].getElementsByTagName("archivo")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(country))
	{ 
		alert(msg_pais);
		document.getElementById("country").focus();
		return;
	}
	
	if(empty(phone_list))
	{ 
		alert(msg_archivo);
		document.getElementById("phone_list").focus();
		return;
	}
	
	var f = $(this);
	
	var file			= $("#phone_list")[0].files[0];
	var formData		= new FormData($("#formulario")[0], file, id_group, country);
	
	$.ajax({
		url:   'ajaxFuncs/adminAddFileGroupList.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');
			document.getElementById("phone_list").value="";	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});

}

function adminAddCostCenter (lan)
{	
	name_cc		= document.getElementById("name_cc").value;
	code		= document.getElementById("code").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_cc			= links_tag[i].getElementsByTagName("nombre_centro_costo")[0].childNodes[0].nodeValue;
			var msg_code_cc		= links_tag[i].getElementsByTagName("codigo_centro_costo")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(name_cc))
	{
		alert(msg_cc);
		document.getElementById("name_cc").focus();
		return false;
	}
	
	if(empty(code))
	{
		alert(msg_code_cc);
		document.getElementById("code").focus();
		return false;
	}
	
    var parametros = {
		"name_cc" : name_cc,
		"code" : code
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminAddCostCenter.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function adminEditCostCenter (id_center, lan)
{	
	name_cc		= document.getElementById("name_cc").value;
	code		= document.getElementById("code").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_cc			= links_tag[i].getElementsByTagName("nombre_centro_costo")[0].childNodes[0].nodeValue;
			var msg_code_cc		= links_tag[i].getElementsByTagName("codigo_centro_costo")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(name_cc))
	{
		alert(msg_cc);
		document.getElementById("name_cc").focus();
		return false;
	}
	
	if(empty(code))
	{
		alert(msg_code_cc);
		document.getElementById("code").focus();
		return false;
	}
	
    var parametros = {
		"id_center": id_center,
		"name_cc" : name_cc,
		"code" : code
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminEditCostCenter.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function adminStatusCostCenter (id_center)
{
	status		= document.getElementById("status").value;
	
    var parametros = {
		"id_center" : id_center,
		"enabled" : status
	};
	
	$.ajax({
		url:   'ajaxFuncs/adminStatusCostCenter.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');			
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function AdminCostCenterSearch (lan)
{	
	username		= document.getElementById("username").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_code_cc		= links_tag[i].getElementsByTagName("codigo_centro_costo")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(username))
	{
		alert(msg_code_cc);
		document.getElementById("username").focus();
		return false;

	} else
	{
		document.location= "?module=adminCostCenter&searchname="+username;
	}
}

function viewWhiteList (id_list)
{
	id_list		= id_list;
	var parametros = {
		"id_list" : id_list
	};
	
	$("#viewWhiteList").html('<p style="text-align:center;"><img src="images/waiting.gif"></p>').removeClass('show-message');
	
	$.ajax({
		url:   'ajaxFuncs/viewWhiteList.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#viewWhiteList").html(data);

		},
        error: function(data) 
		{
			$("#viewWhiteList").html("Error");			
		},
	});
}

function provWhiteListPhoneAdd(sender, lan)
{
	msisdn		= document.getElementById("msisdn").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_movil		= links_tag[i].getElementsByTagName("movil")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(msisdn))
	{ 
		alert(msg_movil);
		document.getElementById("msisdn").focus();
		return;
	}
	
	formData 		= new FormData();
	formData.append('msisdn', msisdn);
	formData.append('sender', sender);
	
	$.ajax({
		url:   'ajaxFuncs/provWhiteListPhoneAdd.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#provWhiteListPhone").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#provWhiteListPhone").html("Error").addClass('show-message');			
		},
	});

}

function provWhiteListFileAdd(service_tag, lan)
{
	phone_list		= document.getElementById("phone_list").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_archivo		= links_tag[i].getElementsByTagName("archivo")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(phone_list))
	{ 
		alert(msg_archivo	);
		document.getElementById("phone_list").focus();
		return;
	}
	
	formData 		= new FormData();
		
	phone_list		= document.getElementById("phone_list").files[0];

	formData.append('phone_list', phone_list);
	formData.append('service_tag', service_tag);
	
	$.ajax({
		url:   'ajaxFuncs/provWhiteListFileAdd.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#provWhiteListFile").html(data).addClass('show-message');
			document.getElementById("phone_list").value="";	
		},
        error: function(data) 
		{
			$("#provWhiteListFile").html("Error").addClass('show-message');			
		},
	});

}

function provWhiteListPhoneDelete(sender, lan)
{
	msisdn		= document.getElementById("msisdn").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_movil		= links_tag[i].getElementsByTagName("movil")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(msisdn))
	{ 
		alert(msg_movil);
		document.getElementById("msisdn").focus();
		return;
	}
	
	formData 		= new FormData();
	formData.append('msisdn', msisdn);
	formData.append('sender', sender);
	
	$.ajax({
		url:   'ajaxFuncs/provWhiteListPhoneDelete.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#provWhiteListPhone").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#provWhiteListPhone").html("Error").addClass('show-message');			
		},
	});

}

function provWhiteListFileDelete(service_tag, lan)
{
	phone_list		= document.getElementById("phone_list").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_archivo		= links_tag[i].getElementsByTagName("archivo")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(phone_list))
	{ 
		alert(msg_archivo);
		document.getElementById("phone_list").focus();
		return;
	}
	
	formData 		= new FormData();
		
	phone_list		= document.getElementById("phone_list").files[0];

	formData.append('phone_list', phone_list);
	formData.append('service_tag', service_tag);
	
	$.ajax({
		url:   'ajaxFuncs/provWhiteListFileDelete.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#provWhiteListFile").html(data).addClass('show-message');
			document.getElementById("phone_list").value="";	
		},
        error: function(data) 
		{
			$("#provWhiteListFile").html("Error").addClass('show-message');			
		},
	});

}

function provWhiteListSearch (id_list, id_service)
{	
	msisdn			= document.getElementById("msisdn").value;
		
	document.location= "?module=provWhiteListSearch&id_list="+id_list+"&id_service="+id_service+"&msisdn="+msisdn;

}

function provBlackListPhoneAdd(sender, lan)
{
	msisdn		= document.getElementById("msisdn").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_movil		= links_tag[i].getElementsByTagName("movil")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(msisdn))
	{ 
		alert(msg_movil);
		document.getElementById("msisdn").focus();
		return;
	}
	
	formData 		= new FormData();
	formData.append('msisdn', msisdn);
	formData.append('sender', sender);
	
	$.ajax({
		url:   'ajaxFuncs/provBlackListPhoneAdd.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#provBlackListPhone").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#provBlackListPhone").html("Error").addClass('show-message');			
		},
	});

}

function provBlackListFileAdd(service_tag, lan)
{
	phone_list		= document.getElementById("phone_list").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_archivo		= links_tag[i].getElementsByTagName("archivo")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(phone_list))
	{ 
		alert(msg_archivo);
		document.getElementById("phone_list").focus();
		return;
	}
	
	formData 		= new FormData();
		
	phone_list		= document.getElementById("phone_list").files[0];

	formData.append('phone_list', phone_list);
	formData.append('service_tag', service_tag);
	
	$.ajax({
		url:   'ajaxFuncs/provBlackListFileAdd.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#provBlackListFile").html(data).addClass('show-message');
			document.getElementById("phone_list").value="";	
		},
        error: function(data) 
		{
			$("#provBlackListFile").html("Error").addClass('show-message');			
		},
	});

}

function provBlackListPhoneDelete(sender, lan)
{
	msisdn		= document.getElementById("msisdn").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_movil		= links_tag[i].getElementsByTagName("movil")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(msisdn))
	{ 
		alert(msg_movil);
		document.getElementById("msisdn").focus();
		return;
	}
	
	formData 		= new FormData();
	formData.append('msisdn', msisdn);
	formData.append('sender', sender);
	
	$.ajax({
		url:   'ajaxFuncs/provBlackListPhoneDelete.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#provBlackListPhone").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#provBlackListPhone").html("Error").addClass('show-message');			
		},
	});

}

function provBlackListFileDelete(service_tag, lan)
{
	phone_list		= document.getElementById("phone_list").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_archivo		= links_tag[i].getElementsByTagName("archivo")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(phone_list))
	{ 
		alert(msg_archivo);
		document.getElementById("phone_list").focus();
		return;
	}
	
	formData 		= new FormData();
		
	phone_list		= document.getElementById("phone_list").files[0];

	formData.append('phone_list', phone_list);
	formData.append('service_tag', service_tag);
	
	$.ajax({
		url:   'ajaxFuncs/provBlackListFileDelete.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#provBlackListFile").html(data).addClass('show-message');
			document.getElementById("phone_list").value="";	
		},
        error: function(data) 
		{
			$("#provBlackListFile").html("Error").addClass('show-message');			
		},
	});

}

function provBlackListSearch (id_service)
{	
	msisdn			= document.getElementById("msisdn").value;
		
	document.location= "?module=provBlackListSearch&id_service="+id_service+"&msisdn="+msisdn;

}

function getMaxCountry (id)
{
	var parametros = {
		"country" : id
	};
	
	$.ajax({
		url:   'ajaxFuncs/getMaxCountry.php',
		data: parametros,
		type:  'POST', 
		success: function(res) 
		{
			res = res;
			
			
			return res;
			
		},
        error: function(res) 
		{
			res = "Error";	
			return res;
		},
		
		
	});
	
	return res;
	
}

function singleMessage(lan)
{	
	country		= document.getElementById("country").value;
	msisdn		= document.getElementById("msisdn").value;
	cost_center	= document.getElementById("cost_center").value;
	message		= document.getElementById("message").value;
	type_send	= document.getElementById("type_send").value;
	data_coding	= document.getElementById("data_coding").value;
	service		= document.getElementById("service").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{	

		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_pais		= links_tag[i].getElementsByTagName("pais")[0].childNodes[0].nodeValue;
			var msg_servicio	= links_tag[i].getElementsByTagName("servicio")[0].childNodes[0].nodeValue;
			var msg_tipo_envio	= links_tag[i].getElementsByTagName("tipo_envio")[0].childNodes[0].nodeValue;
			var msg_fecha_envio	= links_tag[i].getElementsByTagName("fecha_envio")[0].childNodes[0].nodeValue;
			var msg_hora_envio	= links_tag[i].getElementsByTagName("hora_envio")[0].childNodes[0].nodeValue;
			var msg_movil		= links_tag[i].getElementsByTagName("movil")[0].childNodes[0].nodeValue;
			var msg_tipo_texto	= links_tag[i].getElementsByTagName("tipo_texto")[0].childNodes[0].nodeValue;
			var msg_mensaje		= links_tag[i].getElementsByTagName("mensaje")[0].childNodes[0].nodeValue;
		}
	}

	//function limpiar(){
   //document.getElementById("msisdn1").innerHTML="";
   //document.getElementById("numMovil").indexOf="";
  // document.getElementById("msisdn1").indexOf="";
  // }

   

	//var inputfocused = "";
	//document.getElementById("singleMessage").onclick = limpiaCampo;
	//var elements = document.querySelectorAll("input[type='text']");
	//function limpiaCampo() {
    //inputfocused.value = ""; }

 
	
	$(".frm").focus(function() {
		$(this).removeClass("frm-on");
		$(this).addClass("frm");
	});
	$(".frm").blur(function() {
		$(this).removeClass("frm-on");  
		$(this).addClass("frm");
	});
	
	if($("#country").val() === '') {
		alert(msg_pais);
		$("#country").addClass("frm-on");
		return false;
	}
	
	if($("#service").val() === '') {
		alert(msg_servicio);
		$("#service").addClass("frm-on");
		return false;
	}
	
	if($("#type_send").val() === '') {
		alert(msg_tipo_envio);
		$("#type_send").addClass("frm-on");
		return false;
	}
	
	if(type_send == 2)
	{
		dispatchTime = document.getElementById("dispatchTime").value;
		sendDateFinish = document.getElementById("sendDateFinish").value;
		
		if($("#dispatchTime").val() === '') {
			alert(msg_fecha_envio);
			$("#dispatchTime").addClass("frm-on");
			return false;
		}
		if($("#sendDateFinish").val() === '') {
			alert(msg_hora_envio);
			$("#sendDateFinish").addClass("frm-on");
			return false;
		}
		
		
	
	} else {
		dispatchTime = null;
	}
	
	if($("#msisdn").val() === '') {
		alert(msg_movil);
		$("#msisdn").addClass("frm-on");
		return false;
	}
	
	if($("#data_coding").val() === '') {
		alert(msg_tipo_texto);
		$("#data_coding").addClass("frm-on");
		return false;
	}
	
	if($("#message").val() === '') {
		alert(msg_mensaje);
		$("#message").addClass("frm-on");
		return false;
	}
			
	if(dispatchTime == null)
	{	
	var parametros = {
		"country" : country,
		"msisdn" : msisdn,
		"message" : message, 
		"cost_center" : cost_center,
		"dispatchTime" : dispatchTime,
		"data_coding" : data_coding,
		"service" : service
	};
	}
	else
	{
		var parametros = {
		"country" : country,
		"msisdn" : msisdn,
		"message" : message, 
		"cost_center" : cost_center,
		"dispatchTime" : dispatchTime,
		"sendDateFinish" : sendDateFinish,
		"data_coding" : data_coding,
		"service" : service
	};
		
	}
	
	$.ajax({
		url:   'ajaxFuncs/singleMessage.php',
		data: parametros,
		type:  'POST',

		
		success: function(data,textStatus, xhr) 
		{
			if(xhr.status == 200)
			{
			$("#resultado").html(data).addClass('show-message');
			$('#message').val('');
			$('#msisdn').val('');
			}
			else
			{
				$("#resultado").html(data).addClass('show-message');
			}	
		},
        	error: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');			
		},
		complete:function(){
		
        } 
        
	});
}

function groupMessage(lan)
{	
	country		= document.getElementById("country").value;
	id_group	= document.getElementById("id_group").value;
	cost_center	= document.getElementById("cost_center").value;
	message		= document.getElementById("message").value;
	type_send	= document.getElementById("type_send").value;
	data_coding	= document.getElementById("data_coding").value;
	service		= document.getElementById("service").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_pais		= links_tag[i].getElementsByTagName("pais")[0].childNodes[0].nodeValue;
			var msg_servicio	= links_tag[i].getElementsByTagName("servicio")[0].childNodes[0].nodeValue;
			var msg_tipo_envio	= links_tag[i].getElementsByTagName("tipo_envio")[0].childNodes[0].nodeValue;
			var msg_fecha_envio	= links_tag[i].getElementsByTagName("fecha_envio")[0].childNodes[0].nodeValue;
			var msg_hora_envio	= links_tag[i].getElementsByTagName("hora_envio")[0].childNodes[0].nodeValue;
			var msg_movil		= links_tag[i].getElementsByTagName("movil")[0].childNodes[0].nodeValue;
			var msg_tipo_texto	= links_tag[i].getElementsByTagName("tipo_texto")[0].childNodes[0].nodeValue;
			var msg_lista_envio	= links_tag[i].getElementsByTagName("lista_envio")[0].childNodes[0].nodeValue;
			var msg_mensaje		= links_tag[i].getElementsByTagName("mensaje")[0].childNodes[0].nodeValue;
		}
	}


	
	$(".frm").focus(function() {
		$(this).removeClass("frm-on");
		$(this).addClass("frm");
	});
	$(".frm").blur(function() {
		$(this).removeClass("frm-on");  
		$(this).addClass("frm");
	});
	
	if($("#country").val() === '') {
		alert(msg_pais);
		$("#country").addClass("frm-on");
		return false;
	}
	
	if($("#service").val() === '') {
		alert(msg_servicio);
		$("#service").addClass("frm-on");
		return false;
	}
	
	if($("#type_send").val() === '') {
		alert(msg_tipo_envio);
		$("#type_send").addClass("frm-on");
		return false;
	}
	
	if(type_send == 2)
	{
		dispatchTime = document.getElementById("dispatchTime").value;
		sendDateFinish = document.getElementById("sendDateFinish").value;
		
		if($("#dispatchTime").val() === '') {
			alert(msg_fecha_envio);
			$("#dispatchTime").addClass("frm-on");
			return false;
		}
		if($("#sendDateFinish").val() === '') {
			alert(msg_hora_envio);
			$("#sendDateFinish").addClass("frm-on");
			return false;
		}
		
		
	} else {
		dispatchTime = null;
	}
	
	if($("#id_group").val() === '') {
		alert(msg_lista_envio);
		$("#id_group").addClass("frm-on");
		return false;
	}
	
	if($("#data_coding").val() === '') {
		alert(msg_tipo_texto);
		$("#data_coding").addClass("frm-on");
		return false;
	}
	
	if($("#message").val() === '') {
		alert(msg_mensaje);
		$("#message").addClass("frm-on");
		return false;
	}
	
    if(dispatchTime == null)
	{
	
    var parametros = {
		//"country" : country,
		"id_group" : id_group,
		"cost_center" : cost_center,
		"message" : message,
		"dispatchTime" : dispatchTime,
		"data_coding" : data_coding,
		"service" : service
	};
	}
	else
	{
		var parametros = {
		//"country" : country,
		"id_group" : id_group,
		"cost_center" : cost_center,
		"message" : message,
		"dispatchTime" : dispatchTime,
		"sendDateFinish" : sendDateFinish,
		"data_coding" : data_coding,
		"service" : service
	};
		
	}
	
	$.ajax({
		url:   'ajaxFuncs/groupMessage.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
		complete:function(){
            $('#message').val('');
            $('#service').val(''); 
            $('#id_group').val(''); 
        }
	});
}

function fileMessage(type, lan)
{
	
	phone_list	= document.getElementById("phone_list").value;
	cost_center	= document.getElementById("cost_center").value;
	type_send	= document.getElementById("type_send").value;
	data_coding	= document.getElementById("data_coding").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_pais		= links_tag[i].getElementsByTagName("pais")[0].childNodes[0].nodeValue;
			var msg_servicio	= links_tag[i].getElementsByTagName("servicio")[0].childNodes[0].nodeValue;
			var msg_tipo_envio	= links_tag[i].getElementsByTagName("tipo_envio")[0].childNodes[0].nodeValue;
			var msg_fecha_envio	= links_tag[i].getElementsByTagName("fecha_envio")[0].childNodes[0].nodeValue;
			var msg_hora_envio	= links_tag[i].getElementsByTagName("hora_envio")[0].childNodes[0].nodeValue;
			var msg_movil		= links_tag[i].getElementsByTagName("movil")[0].childNodes[0].nodeValue;
			var msg_tipo_texto	= links_tag[i].getElementsByTagName("tipo_texto")[0].childNodes[0].nodeValue;
			var msg_lista_envio	= links_tag[i].getElementsByTagName("lista_envio")[0].childNodes[0].nodeValue;
			var msg_mensaje		= links_tag[i].getElementsByTagName("mensaje")[0].childNodes[0].nodeValue;
			var msg_archivo		= links_tag[i].getElementsByTagName("archivo")[0].childNodes[0].nodeValue;
		}
	}
	
	$(".frm").focus(function() {
		$(this).removeClass("frm-on");
		$(this).addClass("frm");
	});
	$(".frm").blur(function() {
		$(this).removeClass("frm-on");  
		$(this).addClass("frm");
	});
	
	if(type == "LOCAL")
	{
		country		= document.getElementById("country").value;
		service		= document.getElementById("service").value;
		
		
		if($("#country").val() === '') {
			alert(msg_pais);
			$("#country").addClass("frm-on");
			return false;
		}
		
		if($("#service").val() === '') {
			alert(msg_servicio);
			$("#service").addClass("frm-on");
			return false;
		}
	
		

	}
	
	if($("#type_send").val() === '') {
		alert(msg_tipo_envio);
		$("#type_send").addClass("frm-on");
		return false;
	}
	
	if(type_send == 2)
	{
		dispatchTime = document.getElementById("dispatchTime").value;
		sendDateFinish = document.getElementById("sendDateFinish").value;
		
		if($("#dispatchTime").val() === '') {
			alert(msg_fecha_envio);
			$("#dispatchTime").addClass("frm-on");
			return false;
		}
		if($("#sendDateFinish").val() === '') {
			alert(msg_hora_envio);
			$("#sendDateFinish").addClass("frm-on");
			return false;
		}
		
		
	} else {
		dispatchTime = null;
	}
	
	if($("#data_coding").val() === '') {
		alert(msg_tipo_texto);
		$("#data_coding").addClass("frm-on");
		return false;
	}
	
	if($("#phone_list").val() === '') {
		alert(msg_archivo);
		$("#phone_list").addClass("frm-on");
		return false;
	}
	
	phone_list	= document.getElementById("phone_list").files[0];
	
	formData 		= new FormData();
	
	if(type == "LOCAL")
	{
		formData.append('country', country);
		formData.append('service', service);
	}
	if(dispatchTime != null)
	{
		formData.append('sendDateFinish', sendDateFinish);
	}
	
	formData.append('phone_list', phone_list);
	formData.append('cost_center', cost_center);
	formData.append('data_coding',data_coding);
	formData.append('type_send', type_send);
	formData.append('dispatchTime', dispatchTime);
	
	$("#resultado").html('<p style="text-align:left;"><img src="images/waiting.gif"></p>').removeClass('show-message');
	
	$.ajax({
                url:   'ajaxFuncs/fileMessage.php',
                data: formData,
                type:  'POST',
                cache: false,
                contentType: false,
                processData: false,
                success: function(data)
                {
                        $("#resultado").html(data).addClass('show-message');
                        $("#service").val('');
                        $("#country").val('');
			document.getElementById("phone_list").value='';
                },
        error: function(data)
                {
                        $("#resultado").html("Error").addClass('show-message');
                },
        });
		
}

function cancelStatusMessage (pid, lan)
{
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_envio		= links_tag[i].getElementsByTagName("envio_cancelado")[0].childNodes[0].nodeValue;
			var msg_noenvio		= links_tag[i].getElementsByTagName("envio_nocancelado")[0].childNodes[0].nodeValue;
		}
	}
	
    var parametros = {
		"pid" : pid
	};
	
	$.ajax({
		url:   'ajaxFuncs/cancelStatusMessage.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			if(data == "Y")
			{
				$("#resultado").html("<img src='images/icons/confirm.png'> "+msg_envio).addClass('show-message');
				setTimeout ("redireccionar('?module=cancelMessage')", 2000);				
			} else {
				$("#resultado").html(msg_noenvio).addClass('show-message');
			}
				
			
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function singleQuery(lan)
{
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("valida_consulta_operadora");
		for (var i = 0; i < links_tag.length; i++)
		{
			var valida_pais		= links_tag[i].getElementsByTagName("pais")[0].childNodes[0].nodeValue;
			var valida_movil	= links_tag[i].getElementsByTagName("movil")[0].childNodes[0].nodeValue;
		}
	}
	
	$(".frm").focus(function() {
		$(this).removeClass("frm-on");
		$(this).addClass("frm");
	});
	$(".frm").blur(function() {
		$(this).removeClass("frm-on");  
		$(this).addClass("frm");
	});
	
	if($("#country").val() === '') {
		alert(valida_pais);
		$("#country").addClass("frm-on");
		return false;
	}
	
	if($("#msisdn").val() === '') {
		alert(valida_movil);
		$("#msisdn").addClass("frm-on");
		return false;
	}

	$("#resultado").html('<p style="text-align:left;"><img src="images/waiting.gif"></p>').removeClass('show-message');

	country		= document.getElementById("country").value;
	msisdn		= document.getElementById("msisdn").value;

	var parametros = {
		"country" : country,
		"msisdn"  : msisdn
	};
	$.ajax({
		url:   'ajaxFuncs/singleQuery.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			if (data.indexOf('confirm.png') !== -1)
			{
				document.getElementById("msisdn").value = '';
			}
			$("#resultado").html(data).addClass('show-message');	
		},
		error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		}
	});
}

function listQuery(lan)
{
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("valida_consulta_operadora");
		for (var i = 0; i < links_tag.length; i++)
		{
			var valida_pais		= links_tag[i].getElementsByTagName("pais")[0].childNodes[0].nodeValue;
			var valida_lista	= links_tag[i].getElementsByTagName("lista")[0].childNodes[0].nodeValue;
		}
	}
	
	$(".frm").focus(function() {
		$(this).removeClass("frm-on");
		$(this).addClass("frm");
	});
	$(".frm").blur(function() {
		$(this).removeClass("frm-on");  
		$(this).addClass("frm");
	});
	
	if($("#country").val() === '') {
		alert(valida_pais);
		$("#country").addClass("frm-on");
		return false;
	}

	if($("#id_group").val() === '') {
		alert(valida_lista);
		$("#id_group").addClass("frm-on");
		return false;
	}

	$("#resultado").html('<p style="text-align:left;"><img src="images/waiting.gif"></p>').removeClass('show-message');

	country		= document.getElementById("country").value;
	id_group	= document.getElementById("id_group").value;
        list_name       = $('#id_group option:selected').text();
	
	var parametros = {
		"country"    : country,
		"id_group"   : id_group,
		"name_group" : $('#id_group option:selected').text()
	};
	$.ajax({
		url:   'ajaxFuncs/listQuery.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
                        data = data.replace('_LISTNAME_', ' <b>' + list_name + '</b> ');
	                document.getElementById("id_group").value = '';
			$("#resultado").html(data).addClass('show-message');	
		},
		error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		}
	});
}

function fileQuery(lan)
{
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("valida_consulta_operadora");
		for (var i = 0; i < links_tag.length; i++)
		{
			var valida_pais		= links_tag[i].getElementsByTagName("pais")[0].childNodes[0].nodeValue;
			var valida_archivo	= links_tag[i].getElementsByTagName("archivo")[0].childNodes[0].nodeValue;
		}
	}
	
	$(".frm").focus(function() {
		$(this).removeClass("frm-on");
		$(this).addClass("frm");
	});
	$(".frm").blur(function() {
		$(this).removeClass("frm-on");  
		$(this).addClass("frm");
	});
	
	if($("#country").val() === '') {
		alert(valida_pais);
		$("#country").addClass("frm-on");
		return false;
	}
		
	if($("#phone_list").val() === '') {
		alert(valida_archivo);
		$("#phone_list").addClass("frm-on");
		return false;
	}
	
	country		= document.getElementById("country").value;
	phone_list	= document.getElementById("phone_list").files[0];
	
	$("#resultado").html('<p style="text-align:left;"><img src="images/waiting.gif"></p>').removeClass('show-message');
	
	formData 	= new FormData();
	formData.append('country', country);
	formData.append('phone_list', phone_list);
	
	$.ajax({
		url:   'ajaxFuncs/fileQuery.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
                        data = data.replace('_FILENAME_', ' <b>' + phone_list.name + '</b> ');
	                document.getElementById("phone_list").value = '';
			$("#resultado").html(data).addClass('show-message');	
		},
	        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function resultsQuery()
{
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	archivo			= document.getElementById("archivo").value;
	order			= $('input:radio[name=order]:checked').val();
	
	if (typeof order == "undefined")
	{
		order = "DESC";
	}

	archivo = archivo.replace(/^\s+|\s+$/gm,'');
	document.location= "?module=resultsQuery&start_date="+start_date+"&end_date="+end_date+"&archivo="+encodeURIComponent(archivo)+"&order="+order;
}

function selectServiceCountrySingle (country)
{
	//document.getElementById("id_group").disabled= true;
	
	//document.getElementById("id_group").value = "";
	//$("#id_group").html("<option value=''>Seleccionar</option>");
	
	formData 		= new FormData();
	
	formData.append('country', country);
	
	$.ajax({
		url:   'ajaxFuncs/selectServiceCountry.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#service").html(data);
		},
        error: function(data) 
		{
			$("#service").html("Error");			
		},
	});
}

function selectServiceCountry (country, lan)
{
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		
		for (var i = 0; i < links_tag.length; i++)
		{
			var frselect	= links_tag[i].getElementsByTagName("seleccionar")[0].childNodes[0].nodeValue;
		}
	}
	
	document.getElementById("id_group").value = "";
	$("#id_group").html("<option value=''>"+frselect+"</option>");
	
	formData 		= new FormData();
	
	formData.append('country', country);
	
	$.ajax({
		url:   'ajaxFuncs/selectServiceCountry.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#service").html(data);
		},
        error: function(data) 
		{
			$("#selectServiceCountry").html("Error");			
		},
	});
}

function selectServiceRepCountry (country)
{
	formData 		= new FormData();
	
	formData.append('country', country);
	
	$.ajax({
		url:   'ajaxFuncs/selectServiceRepCountry.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#service").html(data);
		},
        error: function(data) 
		{
			$("#selectServiceCountry").html("Error");			
		},
	});
}

function selectSendListCountry ()
{
	country		= document.getElementById("country").value;
	
	formData 	= new FormData();
	
	formData.append('country', country);
	
	$.ajax({
		url:   'ajaxFuncs/selectSendListCountry.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#id_group").html(data);	
		},
        error: function(data) 
		{
			$("#selectServiceCountry").html("Error");			
		},
	});
}



function typeDataCoding (id_coding)
{
	formData 		= new FormData();
	
	formData.append('id_coding', id_coding);
	
	$.ajax({
		url:   'ajaxFuncs/typeDataCoding.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#typeDataCoding").html(data);	
		},
        error: function(data) 
		{
			$("#typeDataCoding").html("Error");			
		},
	});
}

function contador (campo, cuentacampo, limite) 
{
	if (campo.value.length  > limite) 
	{
		campo.value = campo.value.substring(0, limite);
	} else {
		cuentacampo.value = limite - campo.value.length;
	}
}

function selectTypeMessage (value)
{
	if(value == 2)
	{
		document.getElementById('typeSendTime').style.display = 'block';
	} else {
		document.getElementById('typeSendTime').style.display = 'none';
	}
}

function trafficMT ()
{	
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	msisdn			= document.getElementById("msisdn").value;
	input_mode		= document.getElementById("input_mode").value;
	user			= document.getElementById("user").value;
	ccost			= document.getElementById("cost_center").value;
	stage			= document.getElementById("stage").value;
	service_tag		= document.getElementById("service_tag").value;
	order			= $('input:radio[name=order]:checked').val();
	
	if (typeof order == "undefined")
	{
		order = "ASC";
	}
		
	document.location= "?module=trafficMT&start_date="+start_date+"&end_date="+end_date+"&msisdn="+msisdn+"&input_mode="+input_mode+"&ccost="+ccost+"&user="+user+"&stage="+stage+"&service_tag="+service_tag+"&order="+order;

}

function trafficMO ()
{	
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	msisdn			= document.getElementById("msisdn").value;
	service_tag		= document.getElementById("service_tag").value;
	stage			= document.getElementById("stage").value;
	order			= $('input:radio[name=order]:checked').val();
	
	if (typeof order == "undefined")
	{
		order = "ASC";
	}
	
	document.location= "?module=trafficMO&start_date="+start_date+"&end_date="+end_date+"&msisdn="+msisdn+"&service_tag="+service_tag+"&stage="+stage+"&order="+order;

}

function trafficFile ()
{	
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	stage			= document.getElementById("stage").value;
	ccost			= document.getElementById("cost_center").value;

	document.location= "?module=trafficFile&start_date="+start_date+"&end_date="+end_date+"&stage="+stage+"&ccost="+ccost;
}

function trafficCountry ()
{	
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	service			= document.getElementById("service").value;
	type_service	= document.getElementById("type_service").value;
	type_channel	= document.getElementById("type_channel").value;
	id_country		= document.getElementById("id_country").value;
	//alert("cbo"+country_cbo);
	
	document.location= "?module=trafficCountry&start_date="+start_date+"&end_date="+end_date+"&type_service="+type_service+"&type_channel="+type_channel+"&service="+service+"&id_country="+id_country;

}

function trafficCompany ()
{	
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	service			= document.getElementById("service").value;

	document.location= "?module=trafficCompany&start_date="+start_date+"&end_date="+end_date+"&service="+service;

}

function trafficCountry1 ()
{
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	
	formData 	= new FormData();
	
	formData.append('start_date', start_date);
	formData.append('end_date', end_date);
	
	$("#resultado").html('<p style="text-align:center;"><img src="images/waiting.gif"></p>').removeClass('show-message');
	
	$.ajax({
		url:   'ajaxFuncs/repTrafficDate.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#resultado").html(data);	
		},
        error: function(data) 
		{
			$("#resultado").html("Error");			
		},
	});
}

function provRejected ()
{	
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	type_list		= document.getElementById("type_list").value;
	type_action		= document.getElementById("type_action").value;

	document.location= "?module=provRejected&start_date="+start_date+"&end_date="+end_date+"&type_list="+type_list+"&type_action="+type_action;

}

function changePass (lan)
{	
	old_password	= document.getElementById("old_password").value;
	new_password	= document.getElementById("new_password").value;
	repeat_password	= document.getElementById("repeat_password").value;
	
	var xmlDoc = cargarXMLDoc('language.xml');
	if (xmlDoc != null)
	{
		var links_tag = xmlDoc.getElementsByTagName(lan)[0].getElementsByTagName("msg_js");
		for (var i = 0; i < links_tag.length; i++)
		{
			var msg_actual_clave		= links_tag[i].getElementsByTagName("actual_clave")[0].childNodes[0].nodeValue;
			var msg_nueva_clave			= links_tag[i].getElementsByTagName("nueva_clave")[0].childNodes[0].nodeValue;
			var msg_nueva_clave_error	= links_tag[i].getElementsByTagName("nueva_clave_error")[0].childNodes[0].nodeValue;
			var msg_repetir_clave		= links_tag[i].getElementsByTagName("repetir_clave")[0].childNodes[0].nodeValue;
			var msg_clave_distinta		= links_tag[i].getElementsByTagName("clave_distinta")[0].childNodes[0].nodeValue;
		}
	}
	
	if(empty(old_password))
	{
		alert(msg_actual_clave);
		document.getElementById("old_password").focus();
		return false;
	}
	
	if(empty(new_password))
	{
		alert(msg_nueva_clave);
		document.getElementById("new_password").focus();
		return false;
	}
	
	if(new_password.length < 8)
	{
		alert(msg_nueva_clave_error);
		document.getElementById("new_password").focus();
		return false;
	}
	
	if(empty(repeat_password))
	{
		alert(msg_repetir_clave);
		document.getElementById("repeat_password").focus();
		return false;
	}
	
	if(repeat_password.length < 8)
	{
		alert(msg_nueva_clave_error);
		document.getElementById("repeat_password").focus();
		return false;
	}
	
	if(new_password != repeat_password)
	{
		alert(msg_clave_distinta);
		document.getElementById("repeat_password").focus();
		return false;
	}
	
    var parametros = {
		"old_password" : old_password,
		"new_password" : new_password,
		"repeat_password" : repeat_password
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/changePass.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function info ()
{
	
    $( "#dialog" ).dialog({
      hide: {
        effect: "blind",
        duration: 1000
      }
    });
 
    $( "#opener" ).click(function() {
      $( "#dialog" ).dialog( "open" );
    });

}

function backUrl ()
{
	history.back();
}

function goUrl (url)
{
	document.location= url;
}

function empty(text)
{
	if(text == "")
	{
		return true;
	} else {
		return false;
	}
	
}
