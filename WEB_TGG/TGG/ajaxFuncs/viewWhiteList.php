<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	header('Content-Type: text/html; charset=UTF-8');
	
	session_start();
	
	include('../includes/utils.php');
	
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('../language.xml');
	$lb_titulo			= $lan_file->$lan->prov_lista_blanca_servicio[0]->titulo;
	$tb_servicio		= $lan_file->$lan->prov_lista_blanca_servicio[0]->tbservicio;
	$tb_marcacion		= $lan_file->$lan->prov_lista_blanca_servicio[0]->tbmarcacion;
	$tb_tiposervicio	= $lan_file->$lan->prov_lista_blanca_servicio[0]->tbtiposervicio;
	$tb_pais			= $lan_file->$lan->prov_lista_blanca_servicio[0]->tbpais;
	$tb_accion			= $lan_file->$lan->prov_lista_blanca_servicio[0]->tbaccion;
	$alta				= $lan_file->$lan->tips[0]->alta;
	$baja				= $lan_file->$lan->tips[0]->baja;
	$buscar				= $lan_file->$lan->tips[0]->buscar;
	
	$access_token	= $_SESSION['access_token'];
	$id_list		= $_POST['id_list'];
	$url			= URL_WS."WSA-Telcel/api/tgg/whitelist/".$id_list."?access_token=$access_token";
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	$parseUrl		= json_decode($pUrl,true);
	
	if($statusCode == "401")
	{
		refresh_token();
		//showMessage (28);
	} elseif($statusCode == "200")
	{
		?>
        <h2><?=$lb_titulo;?></h2>
        <table>
        	<tr>
				<th><?=$tb_servicio;?></th>
				<th><?=$tb_marcacion;?></th>
				<th><?=$tb_tiposervicio;?></th>
                <th><?=$tb_pais;?></th>
				<th><?=$tb_accion;?></th>
			</tr>
            <?
            for($i = 0; $i < count($parseUrl); $i++)
			{
				$id_service		= $parseUrl[$i]['id_service'];
				$service_type	= $parseUrl[$i]['service_type'];
				$service_name	= $parseUrl[$i]['service_name'];
				$service_tag	= $parseUrl[$i]['service_tag'];
				$country_name	= $parseUrl[$i]['country_name'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				?>
				<tr class="<?=$class;?>">
					<td><?=$service_name;?></td>
                    <td><?=$service_tag;?> </td>
                    <td><?=$service_type;?> </td>
                    <td><?=$country_name;?> </td>
                    <td>
                    <a href="?module=provWhiteListAdd&id_list=<?=$id_list;?>&id_service=<?=$id_service;?>" class="tip"><span><?=$alta;?></span><img src="images/icons/up.png"></a> &nbsp;&nbsp;
                    <a href="?module=provWhiteListDelete&id_list=<?=$id_list;?>&id_service=<?=$id_service;?>" class="tip"><span><?=$baja;?></span><img src="images/icons/down.png"></a>  &nbsp;&nbsp;
                    <a href="?module=provWhiteListSearch&id_list=<?=$id_list;?>&id_service=<?=$id_service;?>" class="tip"><span><?=$buscar;?></span><img src="images/icons/search.png"></a>
                    </td>
				</tr>
				<?
			}
			?>
			</table>
        <?php
		//showMessage (19);
	} else {
		//showMessage (27);
	}
	
	
	curl_close($iUrl);

} else {
	header('location: ../index.php');
}



?>
