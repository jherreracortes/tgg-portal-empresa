<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('../language.xml');
	$seleccionar		= $lan_file->$lan->general[0]->seleccionar;
	$listas_error		= $lan_file->$lan->general[0]->listas_error;
	$access_token		= $_SESSION['access_token'];
	$country			= $_POST['country'];
	
	if(!empty($country)) { $data .= "&searchcountry=".$country; } 

	$access_token	= $_SESSION['access_token'];
	$data			.= "&offset=0";
	//$data			.= "&limit=2000";
        // se agrega el limite de 2000 para que se traiga todas las listas
	$url			= URL_WS."WSA-Telcel/api/group?access_token=".$access_token.$data;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$parseUrl		= json_decode($pUrl,true);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	
	if(count($parseUrl) > 0)
	{
		?>
        <option value=""><?=$seleccionar;?> >></option>
        <?
		for($i = 0; $i < count($parseUrl); $i++)
		{
			$id_group		= $parseUrl[$i]['id_group'];
			$name			= $parseUrl[$i]['name'];
			$enabled		= $parseUrl[$i]['enabled'];

			if ($enabled == "1")
			{
			?>
			<option value="<?=$id_group;?>"><?=$name;?></option>
			<?
			}
		}
	} else {
		?>
        <option value=""> <?=$listas_error;?></option>
        <?php
	}
	
	curl_close($iUrl);

} else 
{
	header('location: ../index.php');
}




?>
