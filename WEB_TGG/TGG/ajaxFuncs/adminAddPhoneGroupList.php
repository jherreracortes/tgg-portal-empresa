<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$type			= $_SESSION['type'];
	$code_c			= strtoupper($_SESSION['code']);
	$access_token	= $_SESSION['access_token'];
	$id_group		= $_POST['id_group'];
	$name_country	= $_POST['country'];
	$msisdn			= $_POST['msisdn'];
	$array			= array('id_group' => $id_group, 'msisdn' => $msisdn);
	$data			= json_encode($array);
	
	if($type == "GLOBAL")
	{
		$code_file = $name_country;
	} else {
		$code_file = $code_c;
	}
	
	$paises		= arrayCountry();
	
	$valuePref	= admAddFileGroupListValuePref($paises, $msisdn, $code_file,$type);
	$valueLen	= admAddFileGroupListValueLen($paises, $msisdn, $code_file,$type);
	
	if($valuePref == "N")
	{
		$err = "Y";
		echo " <br>";
		showMessage (21);
		echo " <br>";
	}
	if($valueLen == "Y")
	{
		$err = "Y";
		echo " <br>";
		showMessage (22);
		echo " <br>";
	}
	if(!is_numeric($msisdn))
        {
                $err = "Y";
		echo " <br>";
                showMessage (37);
		echo " <br>";
        }	
	if(empty($err))
	{
		$url			= URL_WS."WSA-Telcel/api/group/assign?access_token=$access_token";
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($iUrl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($iUrl, CURLOPT_POSTFIELDS, $data);
		$pUrl			= curl_exec($iUrl);
		$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
		$parseUrl		= json_decode($pUrl,true);
		
		if($statusCode == "401")
		{
			refresh_token();
			showMessage (28);
		} elseif($statusCode == "200")
		{
			showMessage (10);
		} elseif($statusCode == "409") {
			showMessage (11);
		} else {
			showMessage (12);
		}
		
		curl_close($iUrl);
	}
	


} else {
	header('location: ../index.php');
}




?>
