<?php

	session_start();
	
	include('../includes/utils.php');
	
	$access_token	= $_SESSION['access_token'];
	$report			= $_GET['report'];
	$url			= URL_WS."WSA-Telcel/api/process/download/".$report."?access_token=".$access_token;
	$iUrl			= curl_init($url);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$date			= date('d-m-Y');
	
	header("Content-type: application/octet-stream");

	//header("Content-Disposition: attachment; filename=\"reporte-mensajes-recibidos-por-carga-archivo-$date.csv\"");
        $info = pathinfo($_GET['filename']);
        $filename = $info['filename'] . '.csv';
	$filename= str_ireplace("UTF-8_","",$filename);
	$filename=str_ireplace("LATIN1_","",$filename);
	header("Content-Disposition: attachment; filename=\"$filename\"");

	echo $pUrl;
	curl_close($iUrl);

?>
