<?php

if (filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH', FILTER_SANITIZE_STRING) && 
        strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH', FILTER_SANITIZE_STRING)) == 'xmlhttprequest') 
{   
    include '../config.php';
    
    $username = filter_input(INPUT_GET, 'username', FILTER_SANITIZE_STRING);
    $data = 'username=' . $username;
    
    $url = URL_WS . 'WSA-Telcel/api/oauth/check?' . $data;
    
    $curl = curl_init();
    
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url
    ));
    
    $resp = curl_exec($curl);
    curl_close($curl);
    
    echo $resp;
    return;
}

return FALSE;