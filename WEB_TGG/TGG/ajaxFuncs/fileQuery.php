<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$access_token	= $_SESSION['access_token'];
	$id_country	= $_POST['country'];	
	$filename	= $_FILES['phone_list']['name'];
	$filedata	= $_FILES['phone_list']['tmp_name'];
	$filesize 	= $_FILES['phone_list']['size'];
	$fileType	= pathinfo($filename);
	$basename	= basename($filename);

	if(strtolower($fileType['extension']) == "txt" || strtolower($fileType['extension']) == "csv")
	{
                $badformat = 0;
                $rowscount = 0;
		$handle = fopen($filedata, "r");
		if ($handle)
		{
			while (($line = fgets($handle)) !== false)
			{
				if (! preg_match('/\S/',$line))
				{
					continue;
				}

				$rowscount++;
				if (! preg_match('/^52\d{10}\r?\n?$/',$line))
				{
					$badformat++;
					break;
				}
			}

			fclose($handle);
		}
		else
		{
			// error opening the file.
			showMessage (61);
			return;
		}

		if ($rowscount > 1000000)
		{
			$err = "Y";
			showMessage (66);
		}
		else if ($badformat > 0)
		{
			$err = "Y";
			showMessage (67);
		}
		else
		{
			$headers = array("Content-Type:multipart/form-data");
			$postfields = array("filedata" => "@$filedata", "filename" => $filename, "rowscount" => $rowscount, "id_country" => $id_country);

			$url	= URL_WS."WSA-Telcel/api/tgg/fileQuery?access_token=$access_token";
			$iUrl   = curl_init($url);

			$options = array(
				CURLOPT_POST => true,
				CURLOPT_HTTPHEADER => $headers,
				CURLOPT_POSTFIELDS => $postfields,
				CURLOPT_INFILESIZE => $filesize,
				CURLOPT_RETURNTRANSFER => true
			);
			curl_setopt_array($iUrl, $options);

			$pUrl		= curl_exec($iUrl);
			$statusCode	= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
					
			if ($statusCode == "401")
			{
				showMessage (28);
			}
			elseif($statusCode == "200")
			{
				$parseUrl = json_decode($pUrl,true);
				if ($parseUrl['error_code'] == 0)
				{
					showMessage (65);
				}
				else if ($parseUrl['error_code'] == -100)
				{
					showMessage (67);
				}
				else
				{
					showMessage (61);
				}
			}
			else
			{
				showMessage (62);
			}
					
			curl_close($iUrl);
		}
	}
	else
	{
		showMessage (68);
	}
}
else 
{
	header('location: ../index.php');
}

?>
