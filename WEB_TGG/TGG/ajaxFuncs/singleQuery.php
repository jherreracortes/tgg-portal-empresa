<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	$type			= $_SESSION['type'];
	$code_c			= strtoupper($_SESSION['code']);
	$access_token		= $_SESSION['access_token'];
	$id_country		= $_SESSION['id_country'];
	$country		= $_POST['country'];
	$msisdn			= $_POST['msisdn'];

	$paises				= arrayCountry();
	if($type == "GLOBAL")
	{
		$code_file = $country;
	} else {
		$code_file = $code_c;
	}
	$valuePref	= admAddFileGroupListValuePref($paises, $msisdn, $code_file,$type);
	$valueLen	= admAddFileGroupListValueLen($paises, $msisdn, $code_file,$type);
	
	if(!is_numeric($msisdn))
	{
		$err = "Y";
		echo " <br>";
		showMessage (37);
		echo " <br>";
	}
	
	if($valuePref == "N")
	{
		$err = "Y";
		echo " <br>";
		showMessage (21);
		echo " <br>";
	}
	
	if($valueLen == "Y")
	{
		$err = "Y";
		echo " <br>";
		showMessage (70);
		echo " <br>";
	}
	
	if(empty($err))
	{

		$data	= array('msisdn' => $msisdn, 'id_country' => $id_country);
		
		$url	= URL_WS."WSA-Telcel/api/tgg/singleQuery?access_token=$access_token";
		$iUrl	= curl_init($url);
		curl_setopt($iUrl, CURLOPT_POST, true);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($iUrl, CURLOPT_POSTFIELDS, http_build_query($data));

		$pUrl		= curl_exec($iUrl);
		$statusCode	= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);

		if($statusCode == "401")
		{
			showMessage (28);
		}
		elseif($statusCode == "200")
		{
			$parseUrl	= json_decode($pUrl,true);
			if ($parseUrl['error_code'] == 0)
			{
                                $result = $parseUrl['result'];
				if ($result == -998)
				{
					showMessage (63);
				}
				else if ($result == -999)
				{
					showMessage (64);
				}
				else
				{
					$operator = $parseUrl['operator'];
                                        if (! empty($operator))
                                        {
                                            $operator = '- ' . $operator;
                                        }
					showMessage60 (60, $msisdn, $result, $operator);
				}	
			}
			else
			{
				showMessage (61);
			}
		}
		else
		{
			showMessage (62);
		}
		
		curl_close($iUrl);
	}

} else 
{
	header('location: ../index.php');
}

?>
