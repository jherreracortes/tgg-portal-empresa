<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
        $access_token	= $_SESSION['access_token'];
        $id_country	= $_POST['country'];
        $id_group	= $_POST['id_group'];
        $name_group	= $_POST['name_group'];

	if(empty($err))
	{

		$data	= array('id_group' => $id_group, 'id_country' => $id_country, 'name_group' => $name_group);
		
		$url	= URL_WS."WSA-Telcel/api/tgg/listQuery?access_token=$access_token";
		$iUrl	= curl_init($url);
		curl_setopt($iUrl, CURLOPT_POST, true);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($iUrl, CURLOPT_POSTFIELDS, http_build_query($data));

		$pUrl		= curl_exec($iUrl);
		$statusCode	= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);

		if($statusCode == "401")
		{
			showMessage (28);
		}
		elseif($statusCode == "200")
		{
			$parseUrl = json_decode($pUrl,true);
			if ($parseUrl['error_code'] == 0)
			{
				showMessage (69);
			}
			else
			{
				showMessage (61);
			}
		}
		else
		{
			showMessage (62);
		}
		
		curl_close($iUrl);
	}

} else 
{
	header('location: ../index.php');
}

?>
