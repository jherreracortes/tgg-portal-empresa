<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	
	$access_token	= $_SESSION['access_token'];
	$fullname		= $_POST['fullname'];
	$username		= $_POST['username'];
	$password		= $_POST['password'];
	$password		= hash('sha256', $password);
	$role			= $_POST['role'];
	$language		= $_POST['language'];
	$array			= array('full_name' => $fullname, 'username' => $username, 'password' => $password, 'id_role' => $role, 'language' => $language);
	$data			= json_encode($array);
	$url			= URL_WS."WSA-Telcel/api/user?access_token=$access_token";
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($iUrl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($iUrl, CURLOPT_POSTFIELDS, $data);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	$parseUrl		= json_decode($pUrl,true);
	
	if($statusCode == "401")
	{
		refresh_token();
		showMessage (28);
	} elseif($statusCode == "201")
	{
		showMessage (2);
	} else {
		showMessage (3);
	}
	
	
	curl_close($iUrl);

} else {
	header('location: ../index.php');
}



?>