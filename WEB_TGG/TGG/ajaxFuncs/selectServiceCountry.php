 <?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$lan 				= $_SESSION['lang'];
	$lan_file			= simplexml_load_file('../language.xml');
	$seleccionar		= $lan_file->$lan->general[0]->seleccionar;
	$servicios_error	= $lan_file->$lan->general[0]->servicios_error;
	$access_token		= $_SESSION['access_token'];
	$country			= $_POST['country'];
	
	if(!empty($country)) { $data .= "&searchcountry=".$country; } 

	$access_token	= $_SESSION['access_token'];
	$url			= URL_WS."WSA-Telcel/api/tgg/service?access_token=".$access_token.$data;
	//echo "url : $url";
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$parseUrl		= json_decode($pUrl,true);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	
	if(count($parseUrl) > 0)
	{
		?>
        <option value=""><?=$seleccionar;?> >></option>
        <?
		for($i = 0; $i < count($parseUrl); $i++)
		{
			$service_tag	= $parseUrl[$i]['service_tag'];
			$service_name	= $parseUrl[$i]['service_name'];
			$ask_for_ack	= $parseUrl[$i]['ask_for_ack'];
			$status			= $parseUrl[$i]['service_status'];
			$enable_sms_web	= $parseUrl[$i]['enable_sms_web'];
				if($status == "ACTIVE" && $enable_sms_web == "Y" )
				{	
				?>
                <option value="<?=$service_tag;?>,<?=$ask_for_ack;?>"><?=$service_tag;?> - <?=$service_name;?> </option>
                <?
				}
			}

		}
	 else {
		?>
        <option value=""> <?=$servicios_error;?></option>
        <?php
	}
	
	curl_close($iUrl);

} else 
{
	header('location: ../index.php');
}




?>
