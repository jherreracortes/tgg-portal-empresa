<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$access_token	= $_SESSION['access_token'];
	$pid			= $_POST['pid'];
	$data			.= "&id_process=".$pid;
	$url			= URL_WS."WSA-Telcel/api/process/cancel?access_token=$access_token".$data;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	$parseUrl		= json_decode($pUrl,true);
	
	if($statusCode == "401")
	{
		refresh_token();
		showMessage (28);
	} elseif($statusCode == "200")
	{
		echo "Y";
		
	} else {
		echo "N";
	}
	
	curl_close($iUrl);
} else 
{
	header('location: ../index.php');
	
}




?>