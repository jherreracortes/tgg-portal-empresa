<?php

	session_start();
	
	include('../includes/utils.php');
	
	$access_token	= $_SESSION['access_token'];
	$archivo		= $_GET['archivo'];
	$type			= $_GET['type'];
	$resultname		= $_GET['resultname'];
	$url			= URL_WS."WSA-Telcel/api/tgg/resultQuery?access_token=".$access_token."&resultname=".$resultname;
	$iUrl			= curl_init($url);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);

	if ($type == 'OUT')
	{
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"$archivo"."_OUT.csv\"");
	}
	else if ($type == 'ERR')
	{
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"$archivo"."_ERR.txt\"");
	}
	else
	{
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"$archivo"."_OUT.csv\"");
	}

	echo $pUrl;
	curl_close($iUrl);

?>
