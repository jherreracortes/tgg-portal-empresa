<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$path	= "../uploadFiles/";
	
	if(is_dir($path) == false) {
		mkdir($path, 0777);
		chmod($path, 0777);
	}
	
	$country	= $_SESSION['id_country'];
	$id_country	= $_SESSION['id_country']."/";
	
	if(is_dir($path.$id_country) == false) {
		mkdir($path.$id_country, 0777);
		chmod($path.$id_country, 0777);
	}
	
	$access_token	= $_SESSION['access_token'];
	//$type			= $_SESSION['type'];
	//$code_c			= strtoupper($_SESSION['code']);
	$service_tag	= $_POST['service_tag'];
	//$name_country	= $_POST['country'];	
	$file			= $_FILES['phone_list']['name'];
	$path_file		= $path.$id_country;
	$path_filename	= $_SERVER["DOCUMENT_ROOT"]."/".substr($path_file, 3);
	//$path_filename	= $_SERVER["DOCUMENT_ROOT"]."/TGG/".substr($path_file, 3);
	$fileType		= pathinfo($file);
	$fileTmp		= substr($file, 0, -4)."_".time().".".$fileType['extension'];
	$fileTmp		= str_replace(" ", "_", $fileTmp);
	$filename		= $path_filename.$fileTmp;
	
	/*
	if($type == "GLOBAL")
	{
		$code_file = $name_country;
	} else {
		$code_file = $code_c;
	}
	*/
		
	if(strtolower($fileType['extension']) == "txt" || strtolower($fileType['extension']) == "csv")
	{
		if (move_uploaded_file($_FILES['phone_list']['tmp_name'], $path_file.$fileTmp))
		{
			$totalFiles		= count(file($filename));
			//$totalFilesGrp	= urlTotalRows(URL_WS."WSA-Telcel/api/group/mobile/$id_group?access_token=".$access_token);
			
			if($totalFilesGrp + $totalFiles > 50000)
			{
				showMessage (20);
				break;
			} else {
				
				$lines = file($filename);
				$i = 1;

				foreach ($lines as $line_num => $line) 
				{
					$phone		= trim($line);
					
					if(!is_numeric($phone))
					{
						$err = "Y";
						showMessage (37);
						echo " (L&iacute;nea $i)<br>";
					}
					
					/*
					
					$paises		= arrayCountry();
					
					$valuePref	= admAddFileGroupListValuePref($paises, $phone, $code_file);
					$valueLen	= admAddFileGroupListValueLen($paises, $phone, $code_file);
					
					if($valuePref == "N")
					{
						$err = "Y";
						showMessage (21);
						echo "(L&iacute;nea $i)<br>";
					}
					
					if($valueLen == "Y")
					{
						$err = "Y";
						showMessage (22);
						echo "(L&iacute;nea $i)<br>";
					}
					*/
					$i++;
				}
			
				
				
				if(empty($err))
				{
					
					$array			= array('file_name' => $filename, 'sender' => $service_tag, 'total' => $totalFiles, 'id_country' => $country, 'type' => 'DWL');
					$data			= json_encode($array);
					$url			= URL_WS."WSA-Telcel/api/tgg/whitelist/process?access_token=$access_token";
					$iUrl			= curl_init($url);
					curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($iUrl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
					curl_setopt($iUrl, CURLOPT_POSTFIELDS, $data);
					$pUrl			= curl_exec($iUrl);
					$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
					$parseUrl		= json_decode($pUrl,true);
					
					if($statusCode == "401")
					{
						refresh_token();
						showMessage (28);
					} elseif($statusCode == "200")
					{
						showMessage (48);
					} else {
						showMessage (49);
					}
				}
				
				
						
			}
		} else 
		{
			showMessage (14);
		}
	}
	else
	{
		showMessage (15);
	}
} else 
{
	header('location: ../index.php');
	
}



?>
