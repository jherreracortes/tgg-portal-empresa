<?php
session_destroy();
?>
<!doctype html>
<html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <link rel="stylesheet" type="text/css" href="css/style.css" />
        <link rel="stylesheet" type="text/css" href="menu/css/component.css" />

        <script type="text/javascript" src="jquery-1.11.3.min.js"></script>
        <!--<script type="text/javascript" src="menu/js/modernizr.custom.js"></script>-->
        <script type="text/javascript" src="js/funcsJS.js?t=<?= time() ?>"></script>

        <title>TGG - INGRESO</title>
    </head>
    <body>
        <div id="global">
            <header>
                <div id="header-up">
                    <div id="logo">
                        PORTAL EMPRESAS
                    </div>
                    <div id="company">

                    </div>
                </div>
                <div id="header-down">
                    &nbsp;
                </div> 
            </header>
            <div id="container">
                <div id="login">
                    <div id="login-user">
                        <h2>Inicio de Sesi&oacute;n</h2>
                    </div>
                    <div id="login-user">
                        <div style="width: 44px; height: 44px; background-color: #999; float: left; text-align: center; line-height: 60px;">
                            <img src="images/icons/user.png">
                        </div>
                        <input type="text" name="user" id="user" placeholder="Usuario">
                    </div>
                    <div id="login-user">
                        <div style="width: 44px; height: 44px; background-color: #999; float: left; text-align: center; line-height: 60px;">
                            <img src="images/icons/password.png">
                        </div>
                        <input type="password" name="passwd" id="passwd" placeholder="Clave">
                    </div>
                    <div id="login-user">
                        <button class="frm-button" onClick="login();">Ingresar</button> 
                    </div>
                    <div id="login-user">
                        <div id="resultado"></div>
                    </div>        
                </div>
            </div>
            
            <!--<script src="menu/js/cbpHorizontalSlideOutMenu.min.js"></script>
            <script>
                var menu = new cbpHorizontalSlideOutMenu(document.getElementById('cbp-hsmenu-wrapper'));
            </script>-->
        </div>
    </body>
</html>
