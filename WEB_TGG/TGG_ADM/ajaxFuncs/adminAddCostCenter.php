<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$access_token	= $_SESSION['access_token'];
	$name_cc		= $_POST['name_cc'];
	$code			= $_POST['code'];
	$array			= array('name' => $name_cc, 'code' => $code);
	$data			= json_encode($array);
	$url			= URL_WS."WSA-Telcel/api/center?access_token=$access_token";
	
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($iUrl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($iUrl, CURLOPT_POSTFIELDS, $data);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	$parseUrl		= json_decode($pUrl,true);
	
	if($statusCode == "401")
	{
		refresh_token();
		showMessage (28);
	} elseif($statusCode == "409")
	{
		showMessage (38);
	} elseif($statusCode == "201")
	{
		showMessage (16);
	} else {
		showMessage (32);
	}

	
	curl_close($iUrl);

} else 
{
	header('location: ../index.php');
	
}



?>