<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$access_token	= $_SESSION['access_token'];
	$id_center		= $_POST['id_center'];
	$enabled		= $_POST['enabled'];
	$data			.= "&id_center=".$id_center;
	$data			.= "&enabled=".$enabled;
	$url			= URL_WS."WSA-Telcel/api/center?access_token=$access_token".$data;
	
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_URL, $url);
	curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($iUrl, CURLOPT_POSTFIELDS);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	
	if($statusCode == "401")
	{
		refresh_token();
		showMessage (28);
	} elseif($statusCode == "200")
	{
		if($enabled == 1)
		{
			showMessage (44);
		} else
		{
			showMessage (45);
		}
	} else {
		showMessage (29);
	}
	
	curl_close($iUrl);
} else 
{
	header('location: ../index.php');
	
}




?>