<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$access_token		= $_SESSION['access_token'];
	$id_user			= $_SESSION['id_user'];
	$old_password		= $_POST['old_password'];
	$old_password		= hash('sha256', $old_password);
	$new_password		= $_POST['new_password'];
	$new_password		= hash('sha256', $new_password);
	$repeat_password	= $_POST['repeat_password'];
	$repeat_password	= hash('sha256', $repeat_password);
	$array				= array('id_user' => $id_user, 'old_password' => $old_password, 'new_password' => $new_password, 'repeat_password' => $repeat_password);
	$data				= json_encode($array);
	$url				= URL_WS."WSA-Telcel/api/user/pass_change?access_token=$access_token";
	
	$iUrl				= curl_init($url);
	curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($iUrl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($iUrl, CURLOPT_POSTFIELDS, $data);
	$pUrl				= curl_exec($iUrl);
	$statusCode			= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	$parseUrl			= json_decode($pUrl,true);
	
	if($statusCode == "401")
	{
		refresh_token();
		showMessage (28);
	} elseif($statusCode == "200")
	{
		showMessage (26);
	} else {
		showMessage (31);
	}
	
	curl_close($iUrl);
} else 
{
	header('location: ../index.php');
	
}




?>