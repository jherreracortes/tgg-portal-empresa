<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$access_token	= $_SESSION['access_token'];
	$pid			= $_POST['pid'];
	$data			.= "&id_process=".$pid;
	$url			= URL_WS."WSA-Telcel/api/process/cancel?access_token=$access_token".$data;
	
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_URL, $url);
	curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($iUrl, CURLOPT_POSTFIELDS);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	
	if($statusCode == "401")
	{
		refresh_token();
		showMessage (28);
	} elseif($statusCode == "200")
	{
		showMessage (46);
		
	} else {
		showMessage (29);
	}
	
	curl_close($iUrl);
} else 
{
	header('location: ../index.php');
	
}




?>