<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$path	= "../uploadFiles/";
	
	if(is_dir($path) == false) {
		mkdir($path, 0777);
	}
	
	$id_country	= $_SESSION['id_country']."/";
	
	if(is_dir($path.$id_country) == false) {
		mkdir($path.$id_country, 0777);
	}
	
	$access_token		= $_SESSION['access_token'];
	$id_country			= $_SESSION['id_country'];
	$type_send			= $_POST['type_send'];
	$dispatchTime		= $_POST['dispatchTime'];
	$data_coding		= $_POST['data_coding'];
	$id_center			= $_POST['cost_center'];
	$service_ask		= $_POST['service'];
	$services			= explode(",", $service_ask);
	$service			= $services[0];
	$ask_for_ack		= $services[1];
	$delivery_receipt	= $ask_for_ack;
	$type				= $_SESSION['type'];
	//$type				= "GLOBAL";
	$code_c				= strtoupper($_SESSION['code']);
	$name_country		= $_POST['country'];	
	$file				= $_FILES['phone_list']['name'];
	$path_file			= $path.$id_country."/";
	$path_filename		= $_SERVER["DOCUMENT_ROOT"]."/TGG/".substr($path_file, 3);
	$fileType			= pathinfo($file);
	$fileTmp			= substr($file, 0, -4)."_".time().".".$fileType['extension'];
	$fileTmp			= str_replace(" ", "_", $fileTmp);
	$filename			= $path_filename.$fileTmp;
	
	if($dispatchTime != NULL)
	{
		$day	= substr($dispatchTime, 0, 2);
		$month	= substr($dispatchTime, 3, 2);
		$year	= substr($dispatchTime, 6, 4);
		$hour	= substr($dispatchTime, 11, 2);
		$min	= substr($dispatchTime, 3, 2);
		
		$dispatchTime	= $year."-".$month."-".$day." ".$hour.":".$min;
	}
	
	if($type == "GLOBAL")
	{
		$len_file	= 3;
		$code_file = $name_country;
	} else {
		$len_file	= 2; 
		$code_file	= $code_c;
	}
	
	if($fileType['extension'] == "txt" || $fileType['extension'] == "csv") 
	{
		if (move_uploaded_file($_FILES['phone_list']['tmp_name'], $path_file.$fileTmp))
		{
			$totalFiles		= count(file($filename));
			
			if($totalFiles > 50000)
			{
				$err = "Y";
				showMessage (25);
			} else {
				
				$lines = file($filename);
				$i = 1;
				
				foreach ($lines as $line_num => $line) 
				{
					$cols		= explode(";", $line);
					$phone		= trim($cols[0]);
					$message	= trim($cols[2]);
					
					$len_msg	= strlen($message);
					$paises		= arrayCountry();
					
					if($type == "GLOBAL")
					{
						$service	= trim($cols[1]);
					} else {
						$service	= $service;
					}
											
					if(count($cols) != $len_file)
					{
						$err = "Y";
						showMessage (24);
						echo "(L&iacute;nea $i)<br>";
					}
					
					if(!is_numeric($phone))
					{
						$err = "Y";
						showMessage (37);
						echo " (L&iacute;nea $i)<br>";
					}					
					
					if($data_coding == "utf-8")
					{
						if($len_msg > 140)
						{
							$err = "Y";
							showMessage (23);
							echo "(L&iacute;nea $i)<br>";
						}
						
					} else {
						if($len_msg > 160)
						{
							$err = "Y";
							showMessage (23);
							echo "(L&iacute;nea $i)<br>";
						}
					}
					
					if($type == "LOCAL")
					{
						$valuePref	= admAddFileGroupListValuePref($paises, $phone, $code_file);
						$valueLen	= admAddFileGroupListValueLen($paises, $phone, $code_file);
					
						if($valuePref == "N")
						{
							$err = "Y";
							showMessage (21);
							echo " (L&iacute;nea $i)<br>";
						}
						
						if($valueLen == "Y")
						{
							$err = "Y";
							showMessage (22);
							echo " (L&iacute;nea $i)<br>";
						}
					} 
					$i++;
				}
				
				
				if(empty($err))
				{
					
					
					if($type_send == 1)
					{
						$array	= array('file_name' => $filename, 'sender' => $service, 'id_center' => $id_center, 'data_coding' => $data_coding,  'delivery_receipt' => $delivery_receipt, 'total' => $totalFiles, 'id_country' => $id_country);
					} else
					{
						$array	= array('file_name' => $filename, 'sender' => $service, 'id_center' => $id_center, 'data_coding' => $data_coding, 'delivery_receipt' => $delivery_receipt, 'total' => $totalFiles, 'id_country' => $id_country, 'dispatch_time' => $dispatchTime);
					}
					
					if($type == "GLOBAL")
					{
						$url			= URL_WS."WSA-Telcel/api/message/file/global?access_token=$access_token";
					} else {
						$url			= URL_WS."WSA-Telcel/api/message/file?access_token=$access_token";
					}
					
					$data			= json_encode($array);
					$iUrl			= curl_init($url);
					curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
					curl_setopt($iUrl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
					curl_setopt($iUrl, CURLOPT_POSTFIELDS, $data);
					$pUrl			= curl_exec($iUrl);
					$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
					$parseUrl		= json_decode($pUrl,true);
					
					if($statusCode == "401")
					{
						refresh_token();
						showMessage (28);
					} elseif($statusCode == "406")
					{
						showMessage(34);
					} elseif($statusCode == "200")
					{
						showMessage (18);
					} else {
						showMessage (30);
					}
					
					curl_close($iUrl);
				} 
				
				
				
			}
			
		} else 
		{
			showMessage (14);
		}
	}
	else
	{
		showMessage (15);
	}
} else 
{
	header('location: ../index.php');
	
}



?>