<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$access_token	= $_SESSION['access_token'];
	$id_group		= $_POST['id_group'];
	$id_mobile		= $_POST['id_mobile'];
	$data			.= "&id_group=".$id_group;
	$data			.= "&id_mobile=".$id_mobile;
	$url			= URL_WS."WSA-Telcel/api/group/remove?access_token=$access_token".$data;
	
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_URL, $url);
	curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "DELETE");
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($iUrl, CURLOPT_POSTFIELDS);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	
	if($statusCode == "401")
	{
		refresh_token();
		showMessage (28);
	} elseif($statusCode == "200")
	{
		if($enabled == 1)
		{
			showMessage (7);
		} else
		{
			showMessage (6);
		}
	} else {
		showMessage (8);
	}
	
	curl_close($iUrl);
} else 
{
	header('location: ../index.php');
	
}




?>