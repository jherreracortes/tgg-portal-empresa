<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	session_start();
	
	include('../includes/utils.php');
	
	$access_token		= $_SESSION['access_token'];
	$id_country			= $_SESSION['id_country'];
	$country			= $_POST['country'];
	$msisdn				= $_POST['msisdn'];
	$id_center			= $_POST['cost_center'];
	$message			= $_POST['message'];
	$dispatchTime		= $_POST['dispatchTime'];
	$data_coding		= $_POST['data_coding'];
	$service_ask		= $_POST['service'];
	$services			= explode(",", $service_ask);
	$service			= $services[0];
	$ask_for_ack		= $services[1];
	$delivery_receipt	= $ask_for_ack;
	$paises				= arrayCountry();
	$valuePref			= admAddFileGroupListValuePref($paises, $msisdn, $country);
	$valueLen			= admAddFileGroupListValueLen($paises, $msisdn, $country);
	
	if($dispatchTime != NULL)
	{
		$day	= substr($dispatchTime, 0, 2);
		$month	= substr($dispatchTime, 3, 2);
		$year	= substr($dispatchTime, 6, 4);
		$hour	= substr($dispatchTime, 11, 2);
		$min	= substr($dispatchTime, 3, 2);
		
		$dispatchTime	= $year."-".$month."-".$day." ".$hour.":".$min;
	}
	
	if(!is_numeric($msisdn))
	{
		$err = "Y";
		showMessage (37);
	}
	
	if($valuePref == "N")
	{
		$err = "Y";
		showMessage (21);
	}
	
	if($valueLen == "Y")
	{
		$err = "Y";
		showMessage (22);
	}
	
	if(empty($err))
	{
		if($dispatchTime == NULL)
		{
			$array		= array('msisdn' => $msisdn, 'sender' => $service, 'id_center' => $id_center, 'data_coding' => $data_coding, 'delivery_receipt' => $delivery_receipt, 'id_country' => $id_country, 'content' => $message);
		} else
		{
			$array		= array('msisdn' => $msisdn, 'sender' => $service, 'id_center' => $id_center, 'data_coding' => $data_coding, 'delivery_receipt' => $delivery_receipt, 'id_country' => $id_country, 'dispatch_time' => $dispatchTime, 'content' => $message);
		}
		
		
		$data			= json_encode($array);
		
		$url			= URL_WS."WSA-Telcel/api/message/single?access_token=$access_token";
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_CUSTOMREQUEST, "PUT");
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($iUrl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($iUrl, CURLOPT_POSTFIELDS, $data);
		$pUrl			= curl_exec($iUrl);
		$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
		$parseUrl		= json_decode($pUrl,true);
		
		
		if($statusCode == "401")
		{
			refresh_token();
			showMessage (28);
		} elseif($statusCode == "200")
		{
			showMessage (36);
		} elseif($statusCode == "406") {
			showMessage (34);
		} else {
			showMessage (30);
		}
		
		
		
		curl_close($iUrl);
	}

} else 
{
	header('location: ../index.php');
}




?>