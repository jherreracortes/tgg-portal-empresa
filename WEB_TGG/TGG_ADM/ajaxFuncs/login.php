<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') 
{
	include('../config.php');
	
	$username		= $_POST['username'];
	$password		= $_POST['password'];
	$id_company		= $_POST['id_company'];
	$password		= hash('sha256', $password);
	
	$data			.= "grant_type=password";
	$data			.= "&client_id=trusted-client";
	$data			.= "&username=".$username;
	$data			.= "&password=".$password;
	
	$url			= URL_WS."WSA-Telcel/oauth/token?".$data;
	
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$parseUrl		= json_decode($pUrl,true);
	$access_token	= $parseUrl['access_token'];
	$refresh_token	= $parseUrl['refresh_token'];
	$id_country		= $id_company;
	$code			= $parseUrl['code'];
	$type			= $parseUrl['type'];
	$id_user		= $parseUrl['id_user'];
	$full_name		= $parseUrl['full_name'];
	$suspended		= $parseUrl['suspended'];
	$language		= $parseUrl['language'];
	
	if($type == "ADMIN")
	{
			
		if(!empty($access_token))
		{
			session_start();
			$_SESSION['access_token']	= $access_token;
			$_SESSION['refresh_token']	= $refresh_token;
			$_SESSION['id_country']		= $id_country;
			$_SESSION['code']			= $code;
			$_SESSION['type']			= $type;
			$_SESSION['id_user']		= $id_user;
			$_SESSION['username']		= $username;
			$_SESSION['full_name']		= $full_name;
			$_SESSION['suspended']		= $suspended;
			$_SESSION['lang']			= $language;
			$_SESSION['id_company']		= $id_company;
			
			setcookie("webapp","tgg",time()+ 15,"/","");
			
			echo "Y";
		} else {
			echo "N";
		}
	} else {
		echo "E";
	}
} else 
{
	header('location: ../index.php');
}


?>