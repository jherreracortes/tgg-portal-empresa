<?
setcookie("webapp","tgg",time()+ 15,"/","");
header('Content-Type: text/html; charset=utf-8'); 
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="menu/css/component.css" />
<link rel="stylesheet" type="text/css" media="all" href="js/calendar/calendar-green.css" title="win2k-cold-1" />
<script type="text/javascript" src="js/calendar/calendar.js"></script>
<script type="text/javascript" src="js/calendar/calendar-es.js"></script>
<script type="text/javascript" src="js/calendar/calendar-setup.js"></script>
<script type="text/javascript" src="jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/JSfuncs.js"></script>
<script type="text/javascript" src="js/jquery.alerts.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.alerts.css" />
<?
include('modules.php');
include('includes/utils.php');
include('config.php');

/*
if(empty($_COOKIE['webapp']) || empty($_SESSION['id_user'])) 
{ 
	?>
     <script>
			setTimeout ("redireccionar('index.php')");
		</script>
    <?
}
*/

$access_token	= $_SESSION['access_token'];
$id_company		= $_SESSION['id_company'];
$url			= URL_WS."WSA-Telcel/api/tgg/company/$id_company?access_token=".$access_token;
$companyService	= parseUrl($url);
$service		= strtoupper($companyService['name']);


?>
<script src="menu/js/modernizr.custom.js"></script>
<title>TGG</title>

</head>
<body>

<div id="global">
    <header>
    	<div id="header-up">
            <div id="logo">
                PORTAL EMPRESAS
            </div>
            <div id="company">
        		<strong><?=$_SESSION['full_name'];?></strong> | <a href="?module=logOut" class="logout">Cerrar Sesi&oacute;n</a><br/>
                <?=$service;?>
        	</div>
        </div>
        <div id="header-down">
        <div id="menu">
                <nav class="cbp-hsmenu-wrapper" id="cbp-hsmenu-wrapper">
					<div class="cbp-hsinner">
						<ul class="cbp-hsmenu">
                        	
						</ul>
					</div>
				</nav>	
                </div>
        </div>
        
    </header>
    <script src="menu/js/cbpHorizontalSlideOutMenu.min.js"></script>
    <script>
		var menu = new cbpHorizontalSlideOutMenu(document.getElementById('cbp-hsmenu-wrapper'));
	</script>
    <div id="container">
    	<?
		$status	= $_SESSION['suspended'];
		
			
		$module = $_GET['module'];
		
		
		if($status == true)
		{
			switch($module)
			{
				case "company": company(); break;
				case "companyService": companyService(); break;
				default: echo '<div id="logout">Cuenta Suspendida</div>';
			}
			
		} else {
		
			switch($module)
			{
				case "company": company(); break;
				case "companyService": companyService(); break;
				case "adminUser": adminUser(); break;
				case "adminAddUser": adminAddUser(); break;
				case "adminEditUser": adminEditUser(); break;
				case "adminStatusUser": adminStatusUser(); break;
				case "adminGroupList": adminGroupList(); break;
				case "adminAddGroupList": adminAddGroupList(); break;
				case "adminEditGroupList": adminEditGroupList(); break;
				case "adminAddPhoneGroupList": adminAddPhoneGroupList(); break;
				case "adminAddFileGroupList": adminAddFileGroupList(); break;
				case "adminStatusGroupList": adminStatusGroupList(); break;
				case "adminCostCenter": adminCostCenter(); break;
				case "adminAddCostCenter": adminAddCostCenter(); break;
				case "adminEditCostCenter": adminEditCostCenter(); break;
				case "adminStatusCostCenter": adminStatusCostCenter(); break;
				case "singleMessage": singleMessage(); break;
				case "groupMessage": groupMessage(); break;
				case "fileMessage": fileMessage(); break;
				case "cancelMessage": cancelMessage(); break;
				case "cancelStatusMessage": cancelStatusMessage(); break;
				case "trafficMT": trafficMT(); break;
				case "trafficMTDetail": trafficMTDetail(); break;
				case "trafficMO": trafficMO(); break;
				case "trafficMODetail": trafficMODetail(); break;
				case "trafficFile": trafficFile(); break;
				case "changePass": changePass(); break;
				case "logOut": logOut(); break;
				case "": adminUser(); break;
			}
		}
		
		
		?> 
    </div>
    <footer>
    
    </footer>
    
</div>
</body>
</html>
