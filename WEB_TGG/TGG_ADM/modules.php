<?
session_start();

function company ()
{
	?>
    <h1>Inicio</h1>
    <?
	$permission = permission ("view_company");
	echo $_SESSION['type'];

	if($permission == "Y") 
	{
		$access_token	= $_SESSION['access_token'];
       	$url			= URL_WS."WSA-Telcel/api/tgg/company?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);	
		$name			= $parseUrl['name'];
		$status			= $parseUrl['company_status'];
		$created_time	= returnFormatDate($parseUrl['created_time']);
		$create_user	= $parseUrl['create_user'];
		$mod_time		= returnFormatDate($parseUrl['mod_time']);
		$mod_user		= $parseUrl['mod_user'];
		$allow_exceed	= $parseUrl['allow_exceed'];
		$bill_cycle		= $parseUrl['bill_cycle'];
		$msg_limit		= $parseUrl['msg_limit'];
		$type			= $parseUrl['type'];
		$id_facturacion	= $parseUrl['id_facturacion'];
		$ctrl_msg_limit	= $parseUrl['ctrl_msg_limit'];
		$country		= $parseUrl['country_name'];
		$bill_cycle		= $parseUrl['bill_cycle'];
		?>
        <h2><?=$name;?></h2>
        <form>
        <div class="frm-label-small">
        	<label>Estado</label> <input class="frm" value="<?=$status;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label>Limite de Mensajes</label> <input class="frm" value="<?=$ctrl_msg_limit;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label>Creada el</label> <input class="frm" value="<?=$created_time;?>" size="15" readonly>
        </div>
        <br>
        <div class="frm-label-small">
        	<label>Tipo de Empresa</label> <input class="frm" value="<?=$type;?>" size="15" readonly> 
        </div>
        <div class="frm-label-small">
        	<label>N&uacute;mero Limite</label> <input class="frm" value="<?=$msg_limit;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label>Creada por</label> <input class="frm" value="<?=$create_user;?>" size="15" readonly><br>
        </div>
        <br>
        <div class="frm-label-small">
        	<label>Pa&iacute;s</label> <input class="frm" value="<?=$country;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label>Permite Exceder</label> <input class="frm"  value="<?=$allow_exceed;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	<label>Modificada el</label> <input class="frm" value="<?=$mod_time;?>" size="15" readonly>
        </div>
        <br>
        <div class="frm-label-small">
        	<label>ID Facturaci&oacute;n</label> <input class="frm" value="<?=$id_facturacion;?>" size="15" readonly>
        </div>
        <div class="frm-label-small">
        	&nbsp;
        </div>
        <div class="frm-label-small">
        	<label>Modificada por</label> <input class="frm" value="<?=$mod_user;?>" size="15" readonly>
        </div>
        <br>
        <div class="frm-label-small">
        	<label>Ciclo</label> <input class="frm" value="<?=$bill_cycle;?>" size="15" readonly>
        </div>
        </form>
        
        <table>
        	<tr>
				<th>Servicio</th>
				<th>Marcaci&oacute;n</th>
				<th>Tipo de Servicio</th>
				<th>Estado</th>
                <th>Mensajes</th>
                <th>Excede</th>
                <th>ID Facturaci&oacute;n</th>
                <th>Pa&iacute;s</th>
				<th>Creado el</th>
			</tr>
        <?
		$urlSrv			= URL_WS."WSA-Telcel/api/tgg/service?access_token=".$access_token;
		$iUrlSrv		= curl_init($urlSrv);
		curl_setopt($iUrlSrv, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrlSrv		= curl_exec($iUrlSrv);
		$parseUrlSrv	= json_decode($pUrlSrv,true);
		
		for($i = 0; $i < count($parseUrlSrv); $i++)
			{
				$id_service		= $parseUrlSrv[$i]['id_service'];
				$service_type	= $parseUrlSrv[$i]['service_type'];
				$service_name	= $parseUrlSrv[$i]['service_name'];
				$service_status	= $parseUrlSrv[$i]['service_status'];
				$created_time	= returnFormatDate($parseUrlSrv[$i]['created_time']);
				$service_tag	= $parseUrlSrv[$i]['service_tag'];
				$id_facturacion	= $parseUrlSrv[$i]['id_facturacion'];
				$msg_limit		= $parseUrlSrv[$i]['msg_limit'];
				$ctrl_msg_limit	= $parseUrlSrv[$i]['ctrl_msg_limit'];
				$country_name	= $parseUrlSrv[$i]['country_name'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Desactivado";
				} else {
					$status = "Activado";
				}
				?>
				<tr class="<?=$class;?>">
					<td><a href="?module=companyService&id_service=<?=$id_service;?>"><?=$service_name;?></a> </td>
                    <td><?=$service_tag;?> </td>
                    <td><?=$service_type;?> </td>
                    <td><?=$service_status;?> </td>
                    <td><?=$msg_limit;?> </td>
                    <td><?=$ctrl_msg_limit;?> </td>
                    <td><?=$id_facturacion;?> </td>
                    <td><?=$country_name;?> </td>
                    <td><?=$created_time;?> </td>
				</tr>
				<?
			}
			?>
			</table>
        </table>
        
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function companyService ()
{
	?>
    <h1>Detalles del Servicio</h1>
    <?
	$permission = permission ("view_service");
		
	if($permission == "Y") {
		$access_token			= $_SESSION['access_token'];
		$id_service				= $_GET['id_service'];
       	$url					= URL_WS."WSA-Telcel/api/tgg/service/".$id_service."?access_token=".$access_token;
		$iUrl					= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl					= curl_exec($iUrl);
		$parseUrl				= json_decode($pUrl,true);
		$service_name			= $parseUrl['service_name'];
		$country				= $parseUrl['country_name'];
		$service_type			= $parseUrl['service_type'];
		$service_status			= $parseUrl['service_status'];
		$service_tag			= $parseUrl['service_tag'];
		$id_facturacion			= $parseUrl['id_facturacion'];
		$date_ini				= returnFormatDate($parseUrl['date_ini']);
		$date_fin				= returnFormatDate($parseUrl['date_fin']);
		$invitation				= $parseUrl['invitation'];
		$date_ini_inv			= returnFormatDate($parseUrl['date_ini_inv']);
		$date_fin_inv			= returnFormatDate($parseUrl['date_fin_inv']);
		$ctrl_msg_day			= $parseUrl['ctrl_msg_day'];
		$msg_day				= $parseUrl['msg_day'];
		$ctrl_msg_limit			= $parseUrl['ctrl_msg_limit'];
		$msg_limit				= $parseUrl['msg_limit'];
		$allow_exceed			= $parseUrl['allow_exceed'];
		$ctrl_time				= $parseUrl['ctrl_time'];
		$msg_time_ini			= substr($parseUrl['msg_time_ini'], 0,8);
		$msg_time_fin			= substr($parseUrl['msg_time_fin'], 0, 8);
		$check_whitelist		= $parseUrl['check_whitelist'];
		$check_blacklist		= $parseUrl['check_blacklist'];
		$dispatch_mt_channel	= $parseUrl['dispatch_mt_channel'];	
		$multi_operador			= $parseUrl['multi_operador'];
		$use_fake_sender		= $parseUrl['use_fake_sender'];
		$allowed_fake_senders	= $parseUrl['allowed_fake_senders'];
		$receive_mo				= $parseUrl['receive_mo'];
		$dispatch_mo_channel	= $parseUrl['dispatch_mo_channel'];
		$dispatch_dr_flag		= $parseUrl['dispatch_dr_flag'];
		$ask_for_ack			= $parseUrl['ask_for_ack'];
		$bill_ack				= $parseUrl['bill_ack'];
		$enable_prov_ws			= $parseUrl['enable_prov_ws'];
		$enable_prov_web		= $parseUrl['enable_prov_web'];	
		$enable_sms_web			= $parseUrl['enable_sms_web'];
		$enable_sms_ws			= $parseUrl['enable_sms_ws'];
		$enable_sms_smpp		= $parseUrl['enable_sms_smpp'];
		$created_time			= returnFormatDate($parseUrl['created_time']);
		$create_user			= $parseUrl['create_user'];
		$mod_time				= returnFormatDate($parseUrl['mod_time']);
		$mod_user				= $parseUrl['mod_user'];
		$notify_optout			= $parseUrl['notify_optout'];
		$notify_others			= $parseUrl['notify_others'];
		?>
        <h2>Nombre del Servicio: <?=$service_name;?></h2>
        <button type="button" class="frm-button" onClick="backUrl();">Volver</button>
        

        <form>
        <div class="frm-label-small">
            <fieldset>
            <legend>Informaci&oacute;n General</legend>
            	<label>Pa&iacute;s</label> <input class="frm" value="<?=$country;?>" size="15" readonly>
            	<br>
            	<label>Tipo</label> <input class="frm" value="<?=$service_type;?>" size="15" readonly>
                <br>
                <label>Estado</label> <input class="frm" value="<?=$service_status;?>" size="15" readonly>
                <br>
                <label>Marcaci&oacute;n</label> <input class="frm" value="<?=$service_tag;?>" size="15" readonly>
                <br>
                <label>ID Facturaci&oacute;n</label> <input class="frm" value="<?=$id_facturacion;?>" size="15" readonly>
                <br>
                <label>Inicio Servicio</label> <input class="frm" value="<?=$date_ini;?>" size="15" readonly>
                <br>
                <label>Fin Servicio</label> <input class="frm" value="<?=$date_fin;?>" size="15" readonly>
            </fieldset>
            <br>
            <fieldset>
            <legend>Canales de Env&iacute;o</legend>
            	<label>Env&iacute;o SMS por Web</label> <input  class="frm" value="<?=$enable_sms_web;?>" size="15" readonly>
            	<br>
            	<label>Env&iacute;o SMS por WS</label> <input  class="frm" value="<?=$enable_sms_ws;?>" size="15" readonly>
                <br>
                <label>Env&iacute;o SMS por SMPP</label> <input  class="frm" value="<?=$enable_sms_smpp;?>" size="15" readonly>
            </fieldset>
            <br>
            <fieldset>
            <legend>Canales de Provisionamiento</legend>
            	<label>Alta y Baja por Web</label> <input  class="frm" value="<?=$enable_prov_web;?>" size="15" readonly>
            	<br>
            	<label>Alta y Baja por WS</label> <input  class="frm" value="<?=$enable_prov_ws;?>" size="15" readonly>
            </fieldset>
        </div>
        <div class="frm-label-small">
            <fieldset>
            <legend>Validaciones del Servicio</legend>
            	<label>Limita SMS</label> <input  class="frm" value="<?=$ctrl_msg_limit;?>" size="15" readonly>
            	<br>
            	<label>N&uacute;mero SMS limite</label> <input class="frm" value="<?=$msg_limit;?>" size="15" readonly>
                <br>
                <label>Permite Exceder</label> <input class="frm" value="<?=$allow_exceed;?>" size="15" readonly>
                <br>
                <label>Tiene Horario de Env&iacute;o</label> <input class="frm" value="<?=$ctrl_time;?>" size="15" readonly>
                <br>
                <label>Hora Inicio</label> <input class="frm" value="<?=$msg_time_ini;?>" size="12" readonly>
                <br>
                <label>Hora T&eacute;rmino</label> <input class="frm" value="<?=$msg_time_fin;?>" size="15" readonly>
                <br>
                <label>Valida Lista Blanca</label> <input class="frm" value="<?=$check_whitelist;?>" size="15" readonly>
                <br>
                <label>Valida Lista Negra</label> <input class="frm" value="<?=$check_blacklist;?>" size="15" readonly>
            </fieldset>
            <br>
            <fieldset>
            <legend>Campa&ntilde;as Publicitarias</legend>
            	<label>Modalidad de Invitaci&oacute;n</label> <input class="frm" value="<?=$invitation;?>" size="15" readonly>
            	<br>
            	<label>Fecha Inicio Invitaci&oacute;n</label> <input class="frm" value="<?=$date_ini_inv;?>" size="15" readonly>
                <br>
                <label>Fecha Fin Invitaci&oacute;n</label> <input class="frm" value="<?=$date_fin_inv;?>" size="15" readonly>
                <br>
                <label>Limita SMS diarios</label> <input class="frm" value="<?=$ctrl_msg_day;?>" size="15" readonly>
                <br>
                <label>N&uacute;mero SMS diarios</label> <input class="frm" value="<?=$msg_day;?>" size="15" readonly>
            </fieldset>
            <br>
            <fieldset>
            <legend>Notificaciones</legend>
            	<label>Notificaci&oacute;n Baja</label> <input class="frm" value="<?=$notify_optout;?>" size="15" readonly>
            	<br>
            	<label>Notificaci&oacute;n Interacci&oacute;n</label> <input class="frm" value="<?=$notify_others;?>" size="15" readonly>
            </fieldset>
        </div>
        <div class="frm-label-small">
            <fieldset>
            <legend>Informaci&oacute;n de Despacho</legend>
            	<label>Canal Despacho de MT</label> <input class="frm" value="<?=$dispatch_mt_channel;?>" size="10" readonly>
            	<br>
                <label>Canal Despacho de MO</label> <input class="frm" value="<?=$dispatch_mo_channel;?>" size="10" readonly>
            	<br>
            	<label>Multioperador</label> <input class="frm" value="<?=$multi_operador;?>" size="10" readonly>
                <br>
                <label>&iquest;Usa m&aacute;scara?</label> <input class="frm" value="<?=$use_fake_sender;?>" size="10" readonly>
                <br>
                <label>Textos de M&aacute;scara</label> <input class="frm" value="<?=$allowed_fake_senders
;?>" size="10" readonly>
                <br>
                <label>Bidireccional</label> <input class="frm" value="<?=$receive_mo;?>" size="10" readonly>
                <br>
                <label>Entrega MO al integrador</label> <input class="frm" value="<?=$receive_mo;?>" size="10" readonly>
                <br>
                <label>Entrega DR al integrador</label> <input class="frm" value="<?=$dispatch_dr_flag;?>" size="10" readonly>
                <br>
                <label>Estado de uso de DR</label> <input class="frm" value="<?=$ask_for_ack;?>" size="10" readonly>
                <br>
                <label>Incluye DR en Reporte</label> <input class="frm" value="<?=$bill_ack;?>" size="10" readonly>
            </fieldset>
            <br>
            <fieldset>
            <legend>Control Interno</legend>
            	<label>Creado el</label> <input class="frm" value="<?=$created_time;?>" size="15" readonly>
            	<br>
            	<label>Creado por</label> <input class="frm" value="<?=$create_user;?>" size="15" readonly>
                <br>
                <label>Actualizado el</label> <input class="frm" value="<?=$mod_time;?>" size="15" readonly>
                <br>
                <label>Actualizado por</label> <input class="frm" value="<?=$mod_user;?>" size="15" readonly>
            </fieldset>
        </div>
        <br>
        </form>

        
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminUser ()
{
	?>
    <h1>Administraci&oacute;n de Usuarios</h1>
    <?
	//$permission = permission ("user_admin");
	$permission = "Y";
	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		?>
        <div class="frm-label-big">
        	<label>Usuario / Nombre</label>
        	<input type="text" name="username" id="username" class="frm" value="<?=$searchname?>"> &nbsp;
        	<button type="button" class="frm-button" onClick="AdminUserSearch();">Consultar</button>
        </div>
        <div class="frm-label-big">
        	<button class="frm-button" onClick="goUrl('?module=adminUser');">Ver Todos</button> <button class="frm-button" onClick="goUrl('?module=adminAddUser');">Agregar Usuario</button>
        </div>
        
        <br>
        <br>
        <?
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		$data			.= "&searchname=".$searchname;
		$searchcompany	.= "&searchcompany=".$id_country;
        $url_base		= URL_WS."WSA-Telcel/api/user?access_token=".$access_token."&searchname=".$searchname.$searchcompany;
		$url			= URL_WS."WSA-Telcel/api/user?access_token=".$access_token.$data.$searchcompany;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= urlTotalRows($url_base);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=adminUser&searchname=".$searchname;
		
		if($totalRows == 0)
		{
			?>
            <div class="show-message">
            <?
			showMessage (9);
			?>
            <?
		}
		else
		{
			pag_pages ($page, $total_pages, $url_app);
				
			?>
			
			<table>
			<tr>
				<th>Nombre</th>
				<th>Usuario</th>
				<th>Perfil</th>
				<th>Estado</th>
				<th>Acci&oacute;n</th>
			</tr>
			<?
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$id_user		= $parseUrl[$i]['id_user'];
				$username		= $parseUrl[$i]['username'];
				$full_name		= $parseUrl[$i]['full_name'];
				$role			= $parseUrl[$i]['title'];
				$statusCode		= $parseUrl[$i]['enabled'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Desactivado";
				} else {
					$status = "Activado";
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$full_name;?></td>
					<td><?=$username;?></td>
					<td><?=$role;?></td>
					<td><?=$status;?></td>
					<td width="150">
					<div id="response<?=$id_user;?>" style="max-width: 130px; height:20px;"> 
						<a href="?module=adminEditUser&id_user=<?=$id_user;?>"><img src="images/icons/edit_user.png"  title="Modificar Usuario" class="button-img"></a>
                        <?
                        if($statusCode == 0)
						{
							?>
                            <a href="?module=adminStatusUser&id_user=<?=$id_user;?>"><img src="images/icons/confirm.png"  title="Activar Usuario" class="button-img"></a>
                            <?
						} else {
							?>
                            <a href="?module=adminStatusUser&id_user=<?=$id_user;?>"><img src="images/icons/delete.png"  title="Desactivar Usuario" class="button-img"></a>
                            <?
						}?>
						
					</div></td>
				</tr>
				<?
			}
			?>
			</table>
			
			<?
			pag_pages ($page, $total_pages, $url_app);
		}
	}
	else
	{
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminAddUser ()
{
	?>
    <h1>Agregar Usuario</h1>
    <?
	//$permission = permission ("user_admin");
	$permission = permission ("add_user");	
	if($permission == "Y") {
		?>
        <div class="frm-label-big">
        	<label>Nombre Completo</label>
        	<input type="text" name="fullname" id="fullname" class="frm" size="47" maxlength="45">
        </div>
        <div class="frm-label-big">
        	<label>E-mail</label>
        	<input type="text" name="username" id="username" class="frm">
        </div>
        <div class="frm-label-big">
        	<label>Clave</label>
        	<input type="password" name="password" id="password" class="frm">
        </div>
        <div class="frm-label-big">
        	<label>Perfil</label> 
        	<select name="role" id="role" class="frm">
                <option value="">- Seleccionar Perfil -</option>
                <option value="3"> Administrador </option>
                <option value="4"> Operaci&oacute;n </option>
                <option value="5"> Gesti&oacute;n </option>
        	</select>
        </div>
        <div class="frm-label-big">
        	<label>Lenguaje</label> 
        	<? selectLanguage(); ?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label> 
        	<button class="frm-button" onClick="adminAddUser();">Agregar</button> <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>
        <br>
        <div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminEditUser ()
{
	?>
    <h1>Modificar Usuario</h1>
    <?
	//$permission = permission ("user_admin");
	$permission = permission ("add_user");
	if($permission == "Y") {
		$id_user		= $_GET['id_user'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/user/$id_user?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$iduser			= $parseUrl['id_user'];
		$fullname		= $parseUrl['full_name'];
		$username		= $parseUrl['username'];
		$password		= $parseUrl['password'];
		$role			= $parseUrl['id_role'];
		$language		= $parseUrl['language'];
		$title_role		= $parseUrl['title'];
		
		$roles			= array(3 => 'Administrador', 4 => 'Operaci&oacute;n', 5 => 'Gesti&oacute;n');
		?>
        <div class="frm-label-big">
        	<label>Nombre Completo</label>
        	<input type="text" name="fullname" id="fullname" class="frm" size="47" maxlength="45" value="<?=$fullname;?>">
        </div>
        <div class="frm-label-big">
        	<label>Email</label>
        	<input type="text" name="username" id="username" class="frm" value="<?=$username;?>" readonly>
        </div>
        <div class="frm-label-big">
        	<label>Clave</label>
        	<input type="password" name="password" id="password" class="frm" value="<?=$password;?>">
        </div>
        <div class="frm-label-big">
        	<label>Perfil</label>
        	<select name="role" id="role" class="frm">
                <option value="">- Seleccionar Role -</option>
                <?
                foreach($roles as $id_role => $role_description)
                {
                    if($role == $id_role)
                    {
                        $selected = "selected";
                    } else {
                        $selected = "";
                    }  		
                    ?>
                    <option value="<?=$id_role;?>" <?=$selected;?>> <?=$role_description;?> </option>
                    <?
                }?>
        	</select>
        </div>
        <div class="frm-label-big">
        	<label>Lenguaje</label> 
        	<? selectLanguage($language); ?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminEditUser('<?=$id_user;?>');">Modificar</button> <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>
        <br>
        <div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminStatusUser ()
{
	?>
    <h1>Estado de Usuario</h1>
    <?
	//$permission = permission ("user_admin");
	$permission = permission ("add_user");	
	if($permission == "Y") {
		$id_user		= $_GET['id_user'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/user/$id_user?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$iduser			= $parseUrl['id_user'];
		$fullname		= $parseUrl['full_name'];
		$username		= $parseUrl['username'];
		$status			= $parseUrl['enabled'];
		$role			= $parseUrl['id_role'];
		$title_role		= $parseUrl['title'];
		
		$roles			= array(3 => 'Administrador', 4 => 'Operaci&oacute;n', 5 => 'Gesti&oacute;n');
		?>
        <div class="frm-label-big">
        	<label>Email</label>
        	<?=$username;?>
        </div>
        <div class="frm-label-big">
        	<label>Estado</label>
        	<? selectStatus($status); ?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminStatusUser('<?=$id_user;?>');">Modificar</button> <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>
        <br>
        <div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminGroupList ()
{
	?>
    <h1>Administraci&oacute;n de Lista de Env&iacute;o</h1>
    <?
	$permission = permission ("group_list");
	
	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		?>
        
        <div class="frm-label-big">
        	<label>Nombre Lista</label>
        	<input type="text" name="name_group" id="name_group" class="frm" value="<?=$searchname?>">&nbsp;<button class="frm-button" onClick="AdminSearchGroupList();">Consultar</button>
        </div>
        <div class="frm-label-big">
        	<button class="frm-button" onClick="goUrl('?module=adminGroupList');">Ver Todos</button> 
            <button class="frm-button" onClick="goUrl('?module=adminAddGroupList');">Agregar Lista</button>
        </div>
		<br>
        <br>
        <?
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		$data			.= "&searchname=".$searchname;
        $url_base		= URL_WS."WSA-Telcel/api/group?access_token=".$access_token."&searchname=".$searchname;
		$url			= URL_WS."WSA-Telcel/api/group?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= urlTotalRows($url_base);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=adminGroupList&searchname=".$searchname;
		
		if($totalRows == 0)
		{
			?>
			<div class="show-message">
			<?
			showMessage (9);
			?>
			</div>
			<?
		}
		else
		{
		
			pag_pages ($page, $total_pages, $url_app);
			
			?>
			
			<table>
			<tr>
				<th>Nombre</th>
				<th>Creado El</th>
				<th>Creado Por</th>
				<th>Estado</th>
				<th>Total</th>
				<th>Acci&oacute;n</th>
			</tr>
			<?
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$id_group		= $parseUrl[$i]['id_group'];
				$name			= $parseUrl[$i]['name'];
				$created_time	= returnFormatDate($parseUrl[$i]['created_time']);
				$created_by		= $parseUrl[$i]['username'];
				$statusCode		= $parseUrl[$i]['enabled'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Desactivado";
				} else {
					$status = "Activado";
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$name;?> </td>
					<td><?=$created_time;?> </td>
					<td><?=$created_by;?> </td>
					<td><?=$status;?></td>
					<td><?=$total = urlTotalRows(URL_WS."WSA-Telcel/api/group/mobile/$id_group?access_token=".$access_token);?>
					<td width="150">
					<div id="response<?=$id_group;?>"> 
						<a href="?module=adminEditGroupList&id_group=<?=$id_group;?>"><img src="images/icons/edit_user.png" title="Asociar Destinatarios a Lista" class="button-img"></a>

                        <?
                        if($statusCode == 0)
						{
							?>
                            <a href="?module=adminStatusGroupList&id_group=<?=$id_group;?>"><img src="images/icons/confirm.png"  title="Activar Lista" class="button-img"></a>
                            <?
						} else {
							?>
                            <a href="?module=adminStatusGroupList&id_group=<?=$id_group;?>"><img src="images/icons/delete.png"  title="Desactivar Lista" class="button-img"></a>
                            <?
						}?>
					</div></td>
				</tr>
				<?
			}
			?>
			</table>
			
			<?
			pag_pages ($page, $total_pages, $url_app);
		}
		
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminAddGroupList ()
{
	?>
    <h1>Agregar Lista de Env&iacute;o</h1>
    <?
	$permission = permission ("group_list");
	
	if($permission == "Y") 
	{
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$countrys		= groupArray($paises, "code");
		?>
        <div class="frm-label-big">
            <label>Pa&iacute;s</label>
                <select name="country" id="country" class="frm">
                    <option value="">Seleccionar Pa&iacute;s</option>
                    <?
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
							$id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$id?>" selected> <?=$ct?></option>
                                    <?
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?
                            }
                        }
                    }
                ?>
                </select>
            </div>
        <div class="frm-label-big">
        	<label>Nueva Lista de Env&iacute;o</label>
        	<input type="text" name="name_group" id="name_group" class="frm" size="35">
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminAddGroupList();">Agregar</button> <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>
        <br>
		<div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminEditGroupList ()
{
	?>
    <h1>Agregar Destinatarios a Lista</h1>
    <?
	//$permission = permission ("user_admin");
	$permission = permission ("add_user");	
	if($permission == "Y") {
		$id_group		= $_GET['id_group'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/group/$id_group?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$id_group		= $parseUrl['id_group'];
		$name_group		= $parseUrl['name'];
		
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$searchmsisdn		= $_GET['searchmsisdn'];
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);		
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		$data			.= "&searchmsisdn=".$searchmsisdn;
        $url_base_list	= URL_WS."WSA-Telcel/api/group/mobile/$id_group?access_token=".$access_token."&searchname=".$searchname;
		$url_list		= URL_WS."WSA-Telcel/api/group/mobile/$id_group?access_token=".$access_token.$data;
		$iUrlList		= curl_init($url_list);
		curl_setopt($iUrlList, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrlList		= curl_exec($iUrlList);
		$parseUrlList	= json_decode($pUrlList,true);
		$totalRows		= urlTotalRows($url_base_list);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=adminEditGroupList&id_group=$id_group&searchmsisdn=".$searchname;
		?>
        <h2><?=$name_group;?></h2>
        <div class="frm-label-big">
        	<label>M&oacute;vil</label>
        	<input type="text" name="celular" id="celular" class="frm" value="<?=$searchmsisdn?>"> &nbsp;
        	<button type="button" class="frm-button" onClick="adminEditGroupListSearch('<?=$id_group;?>');">Consultar</button>
        </div>
        <div class="frm-label-big">
        	<button class="frm-button" onClick="goUrl('?module=adminEditGroupList&id_group=<?=$id_group;?>');">Ver Todos</button> 
        	<button class="frm-button" onClick="goUrl('?module=adminAddPhoneGroupList&id_group=<?=$id_group?>');">Individualmente</button> 
            <button class="frm-button" onClick="goUrl('?module=adminAddFileGroupList&id_group=<?=$id_group?>');">Desde Archivo</button>
            <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>
        <br>
        <?
		
		if($totalRows == 0)
		{
			?>
            <div class="show-message">
            <?
			showMessage (9);
			?>
            <?
		}
		else
		{
			pag_pages ($page, $total_pages, $url_app);
			
			?>
			
			<table>
			<tr>
				<th>Destinatarios</th>
				<th>Acci&oacute;n</th>
			</tr>
			<?
			
			for($i = 0; $i < count($parseUrlList); $i++)
			{
				$id_mobile		= $parseUrlList[$i]['id_mobile'];
				$msisdn			= $parseUrlList[$i]['msisdn'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$msisdn;?> </td>
					<td width="150">
					<div id="response<?=$id_mobile;?>"> 
						<img src="images/icons/delete.png" onClick="adminDeletePhoneGroupList('<?=$id_group;?>', '<?=$id_mobile;?>');" class="button-img" title="Eliminar N&uacute;mero M&oacute;vil">
					</div></td>
				</tr>
				<?
			}
			?>
			</table>
			
			<?
			pag_pages ($page, $total_pages, $url_app);
		}
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminAddPhoneGroupList ()
{
	?>
    <h1>Asociar Destinatarios Individualmente a Lista</h1>
    <?
	$permission = permission ("user_admin");
	
	if($permission == "Y") {
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$id_group		= $_GET['id_group'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/group/$id_group?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$id_group		= $parseUrl['id_group'];
		$name_group		= $parseUrl['name'];
		$idcountry		= $parseUrl['id_country'];
		$paises		= arrayCountry();
		$countrys	= groupArray($paises, "code");
		?>
        <h2><?=$name_group;?></h2>
        <div class="frm-label-big">
        	<label>Pa&iacute;s</label>
                <select name="country" id="country" class="frm">
                    <?
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
							$id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            if($id == $idcountry)
                            {
								?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?
                            }
                        }
                    }
                ?>
                </select>
            </div>
        <div class="frm-label-big">
        	<label>N&uacute;mero M&oacute;vil</label>
        	<input type="text" name="msisdn" id="msisdn" class="frm">
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminAddPhoneGroupList('<?=$id_group;?>');">Asociar</button> <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>

        <br>
		<div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminAddFileGroupList ()
{
	?>
    <h1>Asociar Destinatarios desde Archivo a Lista</h1>
    <?
	$permission = permission ("user_admin");
	
	if($permission == "Y") 
	{
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$id_group		= $_GET['id_group'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/group/$id_group?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$id_group		= $parseUrl['id_group'];
		$name_group		= $parseUrl['name'];
		$idcountry		= $parseUrl['id_country'];
		$paises			= arrayCountry();
		$countrys		= groupArray($paises, "code");
		?>
        <h2><?=$name_group;?></h2>
        
        <form id="formulario" method="POST" enctype="multipart/form-data">
        	<div class="frm-label-big">
        	<label>Pa&iacute;s</label>
                <select name="country" id="country" class="frm">
                    <?
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
							$id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            if($id == $idcountry)
                            {
								?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?
                            }
                        }
                    }
                ?>
                </select>
            </div>
            <div class="frm-label-big">
            	<label>Archivo</label>
            	<input type="file" name="archivo" id="phone_list" class="frm" />
                <input type="hidden" name="id_group" id="id_group" class="frm" value="<?=$id_group;?>" />
            </div>
            <div class="frm-label-big">
            	<label>&nbsp;</label>
            	<button type="button" class="frm-button"  onClick="adminAddFileGroupList();">Asociar</button> <button class="frm-button" onClick="backUrl();">Volver</button>
            </div>

        </form>

        <br>
		<div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminStatusGroupList ()
{
	?>
    <h1>Estado de Lista de Env&iacute;o</h1>
    <?
	$permission = permission ("user_admin");
	
	if($permission == "Y") {
		$id_group		= $_GET['id_group'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/group/$id_group?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$id_group		= $parseUrl['id_group'];
		$name_group		= $parseUrl['name'];
		$status			= $parseUrl['enabled'];
		
		$roles			= array(3 => 'Administrador', 4 => 'Operaci&oacute;n', 5 => 'Gesti&oacute;n');
		?>
        <div class="frm-label-big">
        	<label>Nombre de Lista de Env&iacute;o</label>
        	<?=$name_group;?>
        </div>
        <div class="frm-label-big">
        	<label>Estado</label>
        	<? selectStatus($status); ?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminStatusGroupList('<?=$id_group;?>');">Modificar</button> <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>
        <br>
        <div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminCostCenter ()
{
	?>
    <h1>Administraci&oacute;n de Centros de Costo</h1>
    <?
	$permission = permission ("cost_center");
	
	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		?>
        <div class="frm-label-big">
        	<label>Centro de Costo / Nombre</label>
        	<input type="text" name="username" id="username" class="frm" value="<?=$searchname?>">&nbsp;<button class="frm-button" onClick="AdminCostCenterSearch();">Consultar</button> 
        </div>
        <div class="frm-label-big">
        	<button class="frm-button" onClick="goUrl('?module=adminCostCenter');">Ver Todos</button> 
            <button class="frm-button" onClick="goUrl('?module=adminAddCostCenter');">Agregar Centro Costo</button>
        </div>
        <br>

        <?
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$data			.= "&offset=".$offset;
		$data			.= "&limit=".$limit;
		$data			.= "&searchname=".$searchname;
        $url_base		= URL_WS."WSA-Telcel/api/center?access_token=".$access_token."&searchname=".$searchname;
		$url			= URL_WS."WSA-Telcel/api/center?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= urlTotalRows($url_base);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=adminCostCenter&searchname=".$searchname;
		
		if($totalRows == 0)
		{
			?>
            <div class="show-message">
            <?
			showMessage (9);
			?>
            <?;
		}
		else
		{
			pag_pages ($page, $total_pages, $url_app);
				
			?>
			
			<table>
			<tr>
				<th>Centro Costo</th>
				<th>Nombre</th>
                <th>Creado Por</th>
				<th>Creado El</th>
				<th>Acci&oacute;n</th>
			</tr>
			<?
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$id_center		= $parseUrl[$i]['id_center'];
				$code			= $parseUrl[$i]['code'];
				$name			= $parseUrl[$i]['name'];
				$created_time	= returnFormatDate($parseUrl[$i]['created_time']);
				$username		= $parseUrl[$i]['username'];
				$enabled		= $parseUrl[$i]['enabled'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($enabled == 0)
				{
					$status = "Desactivado";
				} else {
					$status = "Activado";
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$code;?></td>
					<td><?=$name;?></td>
					<td><?=$username;?></td>
					<td><?=$created_time;?></td>
					<td width="150">
					<div id="response<?=$id_center;?>" style="max-width: 130px; height:20px;"> 
						<a href="?module=adminEditCostCenter&id_center=<?=$id_center;?>"><img src="images/icons/edit_user.png" title="Modificar Centro Costo" class="button-img"></a>
                        <?
                        if($enabled == 0)
						{
							?>
                            <a href="?module=adminStatusCostCenter&id_center=<?=$id_center;?>"><img src="images/icons/confirm.png"  title="Activar Centro de Costo" class="button-img"></a>
                            <?
						} else {
							?>
                            <a href="?module=adminStatusCostCenter&id_center=<?=$id_center;?>"><img src="images/icons/delete.png"  title="Desactivar Centro de Costo" class="button-img"></a>
                            <?
						}?>
					</div></td>
				</tr>
				<?
			}
			?>
			</table>
			
			<?
			pag_pages ($page, $total_pages, $url_app);
		}
	}
	else
	{
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminAddCostCenter ()
{
	?>
    <h1>Agregar Centro de Costo</h1>
    <?
	$permission = permission ("cost_center");
	
	if($permission == "Y") {
		?>
        <div class="frm-label-big">
        	<label>C&oacute;digo o Centro de Costo</label>
        	<input type="text" name="code" id="code" class="frm" size="30">
        </div>
        <div class="frm-label-big">
        	<label>Nombre o Descripci&oacute;n</label>
        	<input type="text" name="name_cc" id="name_cc" class="frm" size="35">
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminAddCostCenter();">Agregar</button> <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>

        <br>
		<div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminEditCostCenter ()
{
	?>
    <h1>Modificar Centro de Costo</h1>
    <?
	$permission = permission ("cost_center");
	
	if($permission == "Y") {
		$id_center		= $_GET['id_center'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/center/$id_center?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		
		$code			= $parseUrl['code'];
		$name			= $parseUrl['name'];
		$created_time	= $parseUrl['created_time'];
		$username		= $parseUrl['username'];
		
		?>
        <div class="frm-label-big">
        	<label>C&oacute;digo o Centro de Costo</label>
        	<input type="text" name="code" id="code" class="frm" size="30" value="<?=$code;?>" readonly>
        </div>
        <div class="frm-label-big">
        	<label>Nombre o Descripci&oacute;n</label>
        	<input type="text" name="name_cc" id="name_cc" class="frm" size="35" value="<?=$name;?>">
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminEditCostCenter('<?=$id_center;?>');">Modificar</button> <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>

        <br>
		<div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function adminStatusCostCenter ()
{
	?>
    <h1>Estado de Centro de Costo</h1>
    <?
	$permission = permission ("cost_center");
	
	if($permission == "Y") {
		$id_center		= $_GET['id_center'];
		$access_token	= $_SESSION['access_token'];
        $url			= URL_WS."WSA-Telcel/api/center/$id_center?access_token=".$access_token;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$code			= $parseUrl['code'];
		$name			= $parseUrl['name'];
		$created_time	= $parseUrl['created_time'];
		$username		= $parseUrl['username'];
		$status			= $parseUrl['enabled'];
		
		$roles			= array(3 => 'Administrador', 4 => 'Operaci&oacute;n', 5 => 'Gesti&oacute;n');
		?>
        <div class="frm-label-big">
        	<label>Nombre de Lista de Env&iacute;o</label>
        	<?=$name;?>
        </div>
        <div class="frm-label-big">
        	<label>Estado</label>
        	<? selectStatus($status); ?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="adminStatusCostCenter('<?=$id_center;?>');">Modificar</button> <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>
        <br>
        <div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function singleMessage ()
{
	?>
    <h1>Env&iacute;o de Mensajer&iacute;a Individual</h1>
    <?
	$permission = permission ("single_msg");
	//$permission = "Y";
	if($permission == "Y") 
	{
		$access_token	= $_SESSION['access_token'];
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$data_coding	= arrayDataCoding();
		$countrys		= groupArray($paises, "code");

		?>
        
        <form method="POST">
        	

        	<div class="frm-label-medium">
            <label>Servicio</label> 
            	<? selectServiceActive(); ?>
                <div id="selectServiceCountry"></div>
            </div>
            <div class="frm-label-medium">
            <label>Pa&iacute;s</label>
                <select name="country" id="country" class="frm">
                    <option value="">Seleccionar Pa&iacute;s</option>
                    <?
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
							$id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$id?>" selected> <?=$ct?></option>
                                    <?
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?
                            }
                        }
                    }
                ?>
                </select>
            </div>
            <div class="frm-label-big">
            <label>Centro de Costo</label> 
            	<? selectCostCenter(); ?>
            </div>
            <div class="frm-label-big">
            	<label>Tipo de Env&iacute;o</label> 
            	<select name="type_send" id="type_send" onChange="selectTypeMessage(this.value);" class="frm">
                    <option value="">Seleccionar Tipo de Env&iacute;o</option>
                    <option value="1">Inmediato</option>
                    <option value="2">Agendado</option>
            	</select>
            </div>
            <div class="frm-label-big">
                <div id='typeSendTime' class="frm-label-small" style='display:none;'>
                	<label>&nbsp;</label> 
                    <input type="text" value="" readonly name="dispatchTime" id="dispatchTime" class="frm" size="16"> &nbsp; 
                    <img src="images/icons/calendar.png" id="lanzador1">
                    <script type="text/javascript">
                    var fecha = new Date();
                    var year=fecha.getFullYear();
                        Calendar.setup({ inputField  : "dispatchTime", range       :  [2015, 2999],
                        ifFormat    : "%d-%m-%Y %H:%M",
                        showsTime      :    true,
                        timeFormat     :    "24",
                        button      : "lanzador1"
                        }
                        )
                    </script>
                    
                </div>
            </div>
            <div class="frm-label-big">
            	<label>N&uacute;mero M&oacute;vil</label> 
            	<input type="text" name="msisdn" id="msisdn" class="frm">
            </div>
            <div class="frm-label-big">
            	<label>Data Coding</label> 
            	<select name="data_coding" id="data_coding" class="frm" onChange="typeDataCoding(this.value);">
                    <option value="">Seleccionar Data Coding</option>
                    <?
                    
                    foreach($data_coding as $data_c => $coding)
                    {
                        $id_coding		= $coding['id'];
                        $code_coding	= $coding['code'];
                        $description	= $coding['description'];
                        ?>
                        <option value="<?=$code_coding?>"> <?=$description?></option>
                        <?
                    }
                    ?>
				</select>
            </div>
            <div id="typeDataCoding">
                <div class="frm-label-big">
                    <label>Mensaje</label>
                    <textarea name="message" id="message" class="frm" cols="50" rows="6" onFocus="alert('Debe seleccionar un Data Coding');" readonly></textarea>       
                </div>
            	<div class="frm-label-big">
                    <label>&nbsp;</label> <input type="text" name="remLen" value="0" size="3" class="frm" readonly>                
                </div>
            </div>
            <div class="frm-label-big">
                <label>&nbsp;</label> <button type="button" class="frm-button" onClick="singleMessage();">Enviar</button> 
            </div>            
        </form>
        <br>

        <br>
		<div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function groupMessage ()
{
	?>
    <h1>Env&iacute;o de Mensajer&iacute;a por Lista de Env&iacute;o</h1>
    <?
	$permission = permission ("group_msg");
	//$permission = "Y";
	if($permission == "Y") {
		$type			= $_SESSION['type'];
		//$type			= "LOCAL";
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$data_coding	= arrayDataCoding();
		$services		= arrayService();
		$countrys		= groupArray($paises, "code");
		?>
        
        <form method="POST">
            <div class="frm-label-medium">
            	<label>Pa&iacute;s</label> 
                <select name="country" id="country" class="frm" onChange="selectServiceCountry(this.value);">
                    <option value="">Seleccionar</option>
                    <?
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
							$id			= $v['id'];
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$id?>"> <?=$ct?></option>
                                    <?
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$id?>"> <?=$ct?></option>
                                <?
                            }
                        }
                    }
                ?>
                </select>
            </div>
            <div class="frm-label-medium">
            <label>Servicio</label> 
                <select id="service" name="service" class="frm" onChange="selectSendListCountry();">
                	<option value="">Seleccionar</option>
                </select>
            </div>
            <div class="frm-label-big">
            	<label>Lista de Env&iacute;o</label> 
            	<select id="id_group" name="id_group" class="frm">
                	<option value="">Seleccionar</option>
                </select>
                <div id="selectSendListCountry"></div>
            </div>
            <div class="frm-label-big">
            	<label>Centro de Costo</label> 
            	<? selectCostCenter(); ?>
            </div>
            <div class="frm-label-big">
            	<label>Tipo de Env&iacute;o</label> 
            	<select name="type_send" id="type_send" onChange="selectTypeMessage(this.value);" class="frm">
                    <option value="">Seleccionar Tipo de Env&iacute;o</option>
                    <option value="1">Inmediato</option>
                    <option value="2">Agendado</option>
            	</select>
            </div>
             <div class="frm-label-big">
                <div id="typeSendTime" style="display:none;">
                	<label>&nbsp;</label> 
                    <input type="text" value="" readonly name="dispatchTime" id="dispatchTime" class="frm" size="16"> &nbsp;
                    <img src="images/icons/calendar.png" id="lanzador1">
                    <script type="text/javascript">
                    var fecha = new Date();
                    var year=fecha.getFullYear();
                        Calendar.setup({ inputField  : "dispatchTime", range       :  [2015, 2999],
                        ifFormat    : "%d-%m-%Y %H:%M",
                        showsTime      :    true,
                        timeFormat     :    "24",
                        button      : "lanzador1"
                        }
                        )
                    </script>
            	</div>
            </div>
            <div class="frm-label-big">
            	<label>Data Coding</label> 
            	<select name="data_coding" id="data_coding" class="frm" onChange="typeDataCoding(this.value);">
                    <option value="">Seleccionar Data Coding</option>
                    <?
                    
                    foreach($data_coding as $data_c => $coding)
                    {
                        $id_coding		= $coding['id'];
                        $code_coding	= $coding['code'];
                        $description	= $coding['description'];
                        ?>
                        <option value="<?=$code_coding?>"> <?=$description?></option>
                        <?
                    }
                    ?>
				</select>
            </div>
            <div id="typeDataCoding">
                <div class="frm-label-big">
                    <label>Mensaje</label>
                    <textarea name="message" id="message" class="frm" cols="50" rows="6" onFocus="alert('Debe seleccionar un Data Coding');" readonly></textarea>       
                </div>
            	<div class="frm-label-big">
                    <label>&nbsp;</label> <input type="text" name="remLen" value="0" size="3" class="frm" readonly>                
                </div>
            </div>
            <div class="frm-label-big">
                <label>&nbsp;</label> <button type="button" class="frm-button" onClick="groupMessage();">Enviar</button> 
            </div>
        </form>
        <br>
        <div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function fileMessage ()
{
	?>
    <h1>Env&iacute;o de Mensajer&iacute;a por Archivo</h1>
    <?
	$permission = permission ("file_msg");

	if($permission == "Y") {
		$type			= $_SESSION['type'];
		$code_c			= strtoupper($_SESSION['code']);
		$paises			= arrayCountry();
		$data_coding	= arrayDataCoding();
		$services		= arrayService();
		$countrys		= groupArray($paises, "code");
		?>
        
        <form method="POST" id="formulario" enctype="multipart/form-data">
        	<?
			if($type == "LOCAL")
			{
				?>
        	<div class="frm-label-medium">
            	<label>Servicio</label> 
            	<? selectServiceActive(); ?>
            </div>
            <div class="frm-label-medium">
            	<label>Pa&iacute;s</label> 
                <select name="country" id="country" class="frm">
                    <option value="">Seleccionar Pa&iacute;s</option>
                    <?
                    
                    foreach($countrys as $id_country => $country)
                    {
                        $a = array($country['groupeddata'][0]);
                        
                        foreach ($a as $k => $v) 
                        {
                            $code		= $v['code'];
                            $ct			= $v['country'];
                            $pref		= $v['pref'];
                            $max		= $v['max'];
                            
                            if($type == "LOCAL")
                            {
                                if($code_c == $code)
                                {
                                    ?>
                                    <option value="<?=$code?>" selected> <?=$ct?></option>
                                    <?
                                }
                            } else
                            {
                                ?>
                                <option value="<?=$code?>"> <?=$ct?></option>
                                <?
                            }
                        }
                    }
                ?>
                </select>
            </div>
            <? 
			}
			?>
            <div class="frm-label-big">
            	<label>Centro de Costo</label> 
            	<? selectCostCenter(); ?>
            </div>
            <div class="frm-label-big">
            	<label>Tipo de Env&iacute;o</label> 
            	<select name="type_send" id="type_send" onChange="selectTypeMessage(this.value);" class="frm">
                    <option value="">Seleccionar Tipo de Env&iacute;o</option>
                    <option value="1">Inmediato</option>
                    <option value="2">Agendado</option>
            	</select>
            </div>
             <div class="frm-label-big">
                <div id="typeSendTime" style="display:none;">
                	<label>&nbsp;</label> 
                    <input type="text" value="" readonly name="dispatchTime" id="dispatchTime" class="frm" size="16"> &nbsp;
                    <img src="images/icons/calendar.png" id="lanzador1">
                    <script type="text/javascript">
                    var fecha = new Date();
                    var year=fecha.getFullYear();
                        Calendar.setup({ inputField  : "dispatchTime", range       :  [2015, 2999],
                        ifFormat    : "%d-%m-%Y %H:%M",
                        showsTime      :    true,
                        timeFormat     :    "24",
                        button      : "lanzador1"
                        }
                        )
                    </script>
            	</div>
            </div>
            <div class="frm-label-big">
            	<label>Data Coding</label> 
            	<select name="data_coding" id="data_coding" class="frm">
                    <option value="">Seleccionar Data Coding</option>
                    <?
                    
                    foreach($data_coding as $data_c => $coding)
                    {
                        $id_coding		= $coding['id'];
                        $code_coding	= $coding['code'];
                        $description	= $coding['description'];
                        ?>
                        <option value="<?=$code_coding?>"> <?=$description?></option>
                        <?
                    }
                    ?>
				</select>
            </div>
            <div class="frm-label-big">
            	<label>Archivo </label>
            	<input type="file" id="phone_list" name="phone_list" class="frm">
            </div>
            <div class="frm-label-big">
            	<label> </label>
                <?
                if($type == "LOCAL")
				{
					echo " Formato: Número Móvil; Mensaje";
				} else {
					echo " Formato: Número Móvil; Servicio; Mensaje";
				}
				?> 
            </div>
            <div class="frm-label-big">
            	<label>&nbsp;</label>
            	<button type="button" class="frm-button" onClick="fileMessage('<?=$type;?>');">Enviar</button>
            </div>         
        </form>
        <br>
        <div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function cancelMessage ()
{
	?>
    <h1>Cancelar Env&iacute;os</h1>
    <?
	$permission = permission ("cancel_msg");
	if($permission == "Y") {
		$searchname		= $_GET['searchname'];
		?>
        <?
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		//$data			.= "&offset=".$offset;
		//$data			.= "&limit=".$limit;
		$data			.= "&id_country=".$id_country;
        //$url_base		= URL_WS."WSA-Telcel/api/process/cancel?access_token=".$access_token."&id_country=".$id_country;
		$url			= URL_WS."WSA-Telcel/api/process/cancel?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= count($parseUrl);
		$total_pages	= ceil($totalRows / $limit);
		$url_app		= "?module=cancelMessage&searchname=".$searchname;
		
		if($totalRows == 0)
		{
			?>
            <div class="show-message">
            <?
			showMessage (9);
			?>
            <?
		}
		else
		{
			//pag_pages ($page, $total_pages, $url_app);
				
			?>
			
			<table>
			<tr>
				<th>Nombre</th>
				<th>Tipo de Env&iacute;o</th>
				<th>Fecha</th>
				<th>Total</th>
				<th>Acci&oacute;n</th>
			</tr>
			<?
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$pid			= $parseUrl[$i]['pid'];
				$total			= $parseUrl[$i]['total'];
				$input_mode		= $parseUrl[$i]['input_mode'];
				$input_mode		= getInputModeName(substr($input_mode, 0, -4));
				$file_name		= $parseUrl[$i]['file_name'];
				$created_time	= returnFormatDate($parseUrl[$i]['created_time']);
				$canceleable	= $parseUrl[$i]['canceleable'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Desactivado";
				} else {
					$status = "Activado";
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$file_name;?></td>
					<td><?=$input_mode;?></td>
					<td><?=$created_time;?></td>
					<td><?=$total;?></td>
					<td width="150"><a href="?module=cancelStatusMessage&pid=<?=$pid;?>"><img src="images/icons/delete.png"></a></td>
				</tr>
				<?
			}
			?>
			</table>
			
			<?
			//pag_pages ($page, $total_pages, $url_app);
		}
	}
	else
	{
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function cancelStatusMessage ()
{
	?>
    <h1>Estado de Lista de Env&iacute;o</h1>
    <?
	$permission = permission ("cancel_msg");
	
	if($permission == "Y") {
		$pid			= $_GET['pid'];
		$id_country		= $_SESSION['id_country'];
		$access_token	= $_SESSION['access_token'];
		$data			.= "&id_country=".$id_country;
        $url			= URL_WS."WSA-Telcel/api/process/cancel/$pid?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$file_name		= $parseUrl['file_name'];

		?>
        <div class="frm-label-big">
        	<label>Nombre de Lista de Env&iacute;o</label>
        	<?=$file_name;?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label> ¿Est&aacute; seguro que desea cancelar este env&iacute;o?
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>
        	<button class="frm-button" onClick="cancelStatusMessage('<?=$pid;?>');">Si</button> <button class="frm-button" onClick="backUrl();">Volver</button>
        </div>
        <br>
        <div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function trafficMT ()
{
	?>
    <h1>Reporte de Mensajes Enviados por Canal Web</h1>
    <?
	$permission = permission ("traffic_mt");
	
	if($permission == "Y") 
	{
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
				
		$msisdn			= $_GET['msisdn'];
		$user			= $_GET['user'];
		$input_mode		= $_GET['input_mode'];
		$stage			= $_GET['stage'];
		$service_tag	= $_GET['service_tag'];
		$order			= $_GET['order'];
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($msisdn)) { $data_extra .= "&searchmsisdn=".$msisdn; }
		if(!empty($user)) { $data_extra .= "&searchuser=".$user; }
		if(!empty($input_mode)) { $data_extra .= "&searchtype=".$input_mode; }
		if(!empty($stage)) { $data_extra .= "&searchstage=".$stage; }
		if(!empty($service_tag)) { $data_extra .= "&searchsender=".$service_tag; }
		if(!empty($order)) { $data_extra .= "&order=".$order; }
		$url			= URL_WS."WSA-Telcel/api/message/consolidated?access_token=".$access_token.$data.$data_extra;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= count($parseUrl);
		
		if(!empty($start_date))
		{
			$dateFrom	= $start_date;
			$dateTo		= $end_date;
		} else {
			$dateFrom	= date('01-m-Y');
			$dateTo		= date('d-m-Y');
		}
		
		?>
        <div class="frm-label-big">
        	<label>Fecha de Inicio</label>
        	<input type="text" name="start_date" id="start_date" class="frm" value="<?=$dateFrom;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador1">
        </div>
        <div class="frm-label-big">
        	<label>Fecha de T&eacute;rmino</label>
        	<input type="text" name="end_date" id="end_date" class="frm" value="<?=$dateTo;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador2">
        </div>
        <div class="frm-label-big">
        	<label>N&uacute;mero M&oacute;vil</label>
        	<input type="text" name="msisdn" id="msisdn" class="frm" value="<?=$msisdn;?>">
        </div>    
        <div class="frm-label-big">
        	<label>Usuario</label>  <? selectUser($user); ?>
        </div>
        <div class="frm-label-big">
        	<label>Tipo de Ingreso</label>  <? selectInputMode($input_mode); ?>
        </div>
        <div class="frm-label-big">
        	<label>Estado</label> <? selectStage($stage); ?>
        </div>
        <div class="frm-label-big">
        	<label>N&uacute;mero Corto</label> <? selectServiceTag($service_tag);?>
        </div>
        <div class="frm-label-big">
        	<label>Orden de despligue</label> <? selectOrder($order); ?>       
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>  
	        <button class="frm-button" onClick="trafficMT();">Consultar</button> 
        </div>
        <br>
        <br>
		<script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "start_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador1"
            }
            )
        </script>
        
        <script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "end_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador2"
            }
            )
        </script>
        <?
        
		if(!empty($start_date))
		{
			if($totalRows == 0)
			{
				?>
                <div class="show-message">
                <?
                showMessage (9);
                ?>
                </div>
                <?
			}
			else
			{
				$urlRepTotal	= URL_WS."WSA-Telcel/api/message/detailed?access_token=".$access_token."&id_country=".$id_country."&start_date=".$start_date_get."&end_date=".$end_date_get.$data_extra;
				$fileRepTotal	= getFileReport($urlRepTotal);
				
				?>
				<div class="show-message">
                	Reporte Total <a href="ajaxFuncs/repTrafficTotalMT.php?report=<?=$fileRepTotal;?>"><img src="images/icons/excel.png"></a>
                </div>
				<table>
				<tr>
					<th>Fecha</th>
					<th>Enviados</th>
					<th>Confirmados</th>
					<th>Fallidos</th>
					<th>Rechazados</th>
					<th>Pendientes</th>
					<th>Cancelados</th>
					<th>Total</th>
                    <th>Descargar</th>
				</tr>
				<?
				
				for($i = 0; $i < count($parseUrl); $i++)
				{
					$report_date	= returnFormatDate($parseUrl[$i]['report_date']);
					$report_file	= $parseUrl[$i]['report_date'];
					$pushed			= $parseUrl[$i]['pushed'];
					$confirmed		= $parseUrl[$i]['confirmed'];
					$failed			= $parseUrl[$i]['failed'];
					$nopushed		= $parseUrl[$i]['nopushed'];
					$queued			= $parseUrl[$i]['queued'];
					$canceled		= $parseUrl[$i]['canceled'];
					$count			= $parseUrl[$i]['count'];
					$url_excel		= URL_WS."WSA-Telcel/api/message/detailed?access_token=".$access_token."&id_country=".$id_country."&start_date=".$report_file."&end_date=".$report_file.$data_extra;
					$fileReport		= getFileReport($url_excel);
					
					$tr_color = $i % 2;
					
					if($tr_color == 0)
					{
						$class	= ""; 
					} else {
						$class	= "tr-color"; 
					}
					
					if($statusCode == 0)
					{
						$status = "Desactivado";
					} else {
						$status = "Activado";
					}
					?>
					<tr class="<?=$class;?>">
						<td><a href="?module=trafficMTDetail&start_date=<?=$report_date;?>&end_date=<?=$report_date;?>&id_country=<?=$id_country;?>&msisdn=<?=$msisdn;?>&stage=<?=$stage;?>&input_mode=<?=$input_mode;?>&user=<?=$user;?>&service_tag=<?=$service_tag;?>&order=<?=$order;?>"><?=$report_date;?></a></td>
						<td><?=$pushed;?></td>
						<td><?=$confirmed;?></td>
						<td><?=$failed;?></td>
						<td><?=$nopushed;?></td>
						<td><?=$queued;?></td>
						<td><?=$canceled;?></td>
						<td><?=$count;?></td>
                        <td><a href="ajaxFuncs/repTrafficMT.php?report=<?=$fileReport;?>"><img src="images/icons/excel.png"></a>
                         </td>
					</tr>
					<?
				}
				?>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
                    <th></th>
				</tr>
				</table>
				<div id="download"></div>
				<?
			}
		}
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function trafficMTDetail ()
{
	?>
    <h1>Reporte de Mensajes Enviados por Canal Web </h1>
    <?
	$permission = permission ("traffic_mt");
	
	if($permission == "Y") 
	{
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$searchname		= $_GET['searchname'];
		$id_country		= $_GET['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
		
		$date			= returnFormatDate(substr($_GET['end_date'], 0,10));
		$msisdn			= $_GET['msisdn'];
		$user			= $_GET['user'];
		$input_mode		= $_GET['input_mode'];
		$stage			= $_GET['stage'];
		$service_tag	= $_GET['service_tag'];
		$order			= $_GET['order'];
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($msisdn)) { $data .= "&searchmsisdn=".$msisdn; }
		if(!empty($user)) { $data .= "&searchuser=".$user; }
		if(!empty($input_mode)) { $data .= "&searchtype=".$input_mode; }
		if(!empty($stage)) { $data .= "&searchstage=".$stage; }
		if(!empty($service_tag)) { $data .= "&searchsender=".$service_tag; }
		if(!empty($order)) { $data .= "&order=".$order; }
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$url			= URL_WS."WSA-Telcel/api/message/detailed/mt?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		
		?>
        <h2><?=$date;?></h2>
        <div class="frm-label-big">
        	<button type="button" class="frm-button" onClick="backUrl();">Volver</button>
        </div>
        <br>
        <br>
        <?
		
		
		
			//pag_pages ($page, $total_pages, $url_app);
				
			?>
			<div class="frame-scroll-table">
			<table class="scroll-table">
			<tr>
				<th>M&oacute;vil</th>
				<th>N&uacute;mero Corto</th>
				<th>Estado</th>
                <th>Resultado</th>
				<th>Fecha de Creaci&oacute;n</th>
                <th>Fecha de Agendamiento</th>
                <th>Fecha de Estado</th>
                <th>Tipo de Ingreso</th>
                <th>Tipo de Env&iacute;o</th>
                <th>Creado por</th>
                <th>Mensaje</th>
			</tr>
			<?
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$msisdn			= $parseUrl[$i]['msisdn'];
				$sender			= $parseUrl[$i]['sender'];
				$stage			= getStageName($parseUrl[$i]['stage']);
				$text			= $parseUrl[$i]['text'];
				$created_time	= returnFormatDate($parseUrl[$i]['created_time']);
				$dispatch_time	= returnFormatDate($parseUrl[$i]['dispatch_time']);
				$result_time	= returnFormatDate($parseUrl[$i]['result_time']);
				$content		= $parseUrl[$i]['content'];
				$inputmode		= explode("_", $parseUrl[$i]['input_mode']);
				$input_mode		= getInputModeName($inputmode[0]);
				$type_send		= getTypeSendName($inputmode[1]);
				$full_name		= $parseUrl[$i]['full_name'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Desactivado";
				} else {
					$status = "Activado";
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$msisdn;?></td>
					<td><?=$sender;?></td>
					<td><?=$stage;?> </td>
                    <td><?=$text;?></td>
					<td><?=$created_time;?></td>
                    <td><?=$dispatch_time;?></td>
                    <td><?=$result_time;?></td>
                    <td><?=$input_mode;?></td>
                    <td><?=$type_send;?></td>
                    <td><?=$full_name;?></td>
                    <td><?=$content;?></td>
				</tr>
				<?
			}
			?>
			</table>
			</div>
            <br>
			<?
			//pag_pages ($page, $total_pages, $url_app);
		
	}
	else
	{
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function trafficMO ()
{
	?>
    <h1>Reporte de Mensajes Recibidos por Canal Web</h1>
    <?
	$permission = permission ("traffic_mo");
	
	
	if($permission == "Y") {
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		$stage			= $_GET['stage'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}	
		
		$msisdn			= $_GET['msisdn'];
		$service_tag	= $_GET['service_tag'];
		$order			= $_GET['order'];
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($msisdn)) { $data_extra .= "&searchmsisdn=".$msisdn; }
		if(!empty($service_tag)) { $data_extra .= "&searchsender=".$service_tag; }
		if(!empty($stage)) { $data_extra .= "&searchstage=".$stage; }
		if(!empty($order)) { $data_extra .= "&order=".$order; }
		$url			= URL_WS."WSA-Telcel/api/message/consolidated/mo?access_token=".$access_token.$data.$data_extra;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= count($parseUrl);
		
		if(!empty($start_date))
		{
			$dateFrom	= $start_date;
			$dateTo		= $end_date;
		} else {
			$dateFrom	= date('01-m-Y');
			$dateTo		= date('d-m-Y');
		}
		
		?>
		<div class="frm-label-big">
        	<label>Fecha de Inicio</label>
        	<input type="text" name="start_date" id="start_date" class="frm" value="<?=$dateFrom;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador1">
        </div>
        <div class="frm-label-big">
        	<label>Fecha de T&eacute;rmino</label>
        	<input type="text" name="end_date" id="end_date" class="frm" value="<?=$dateTo;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador2">
        </div>
        <div class="frm-label-big">
        	<label>N&uacute;mero M&oacute;vil</label>
        	<input type="text" name="msisdn" id="msisdn" class="frm" value="<?=$msisdn;?>">
        </div>    
        <div class="frm-label-big">
        	<label>N&uacute;mero Corto</label> <? selectServiceTag($service_tag);?>
        </div>
        <div class="frm-label-big">
        	<label>Tipo</label> <? selectStageMo($stage);?>
        </div>
        <div class="frm-label-big">
        	<label>Order de Despliegue</label> <? selectOrder($order);?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>  
	        <button class="frm-button" onClick="trafficMO();">Consultar</button> 
        </div>
        <br>
        <br>
        <br>
        <br>
        <script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "start_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador1"
            }
            )
        </script>
        
        <script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "end_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador2"
            }
            )
        </script>
        <?
        
		if(!empty($start_date))
		{
			if($totalRows == 0)
			{
				?>
                <div class="show-message">
                <?
                showMessage (9);
                ?>
                </div>
                <?
			}
			else
			{
				$urlRepTotal	= URL_WS."WSA-Telcel/api/message/detailed/mo/?access_token=".$access_token."&id_country=".$id_country."&start_date=".$start_date_get."&end_date=".$start_date_get.$data_extra;
				$fileRepTotal	= getFileReport($urlRepTotal);
				
				?>
				<div class="show-message">
                	Reporte Total <a href="ajaxFuncs/repTrafficTotalMO.php?report=<?=$fileRepTotal;?>"><img src="images/icons/excel.png"></a>
                </div>
				
				
				<table>
				<tr>
					<th>Fecha</th>
					<th>Total</th>
                    <th>Descargar</th>
				</tr>
				<?
				
				for($i = 0; $i < count($parseUrl); $i++)
				{
					$report_date	= returnFormatDate($parseUrl[$i]['report_date']);
					$report_file	= $parseUrl[$i]['report_date'];
					$count			= $parseUrl[$i]['count'];
					$url_excel		= URL_WS."WSA-Telcel/api/message/detailed/mo/?access_token=".$access_token."&id_country=".$id_country."&start_date=".$report_file."&end_date=".$report_file.$data_extra;
					$fileReport		= getFileReport($url_excel);
					
					$tr_color = $i % 2;
					
					if($tr_color == 0)
					{
						$class	= ""; 
					} else {
						$class	= "tr-color"; 
					}
					
					if($statusCode == 0)
					{
						$status = "Desactivado";
					} else {
						$status = "Activado";
					}
					?>
					<tr class="<?=$class;?>">
						<td><a href="?module=trafficMODetail&start_date=<?=$report_date;?>&end_date=<?=$report_date;?>&id_country=<?=$id_country;?>&msisdn=<?=$msisdn;?>&service_tag=<?=$service_tag;?>&order=<?=$order;?>"><?=$report_date;?></a></td>
						<td><?=$count;?></td>
                        <td><a href="ajaxFuncs/repTrafficMO.php?report=<?=$fileReport;?>"><img src="images/icons/excel.png"></a></td>
					</tr>
					<?
				}
				?>
				<tr>
					<th></th>
					<th></th>
                    <th></th>
				</tr>
				</table>
				
				<?
			}
		}
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function trafficMODetail ()
{
	?>
    <h1>Reporte de Mensajes Recibidos por Canal Web </h1>
    <?
	$permission = permission ("traffic_mo");
	
	if($permission == "Y") 
	{
		if(!isset($_GET['p'])) {
			$page = 1;  
		} else {
			$page = $_GET['p'];  
		}
		
		$searchname		= $_GET['searchname'];
		$id_country		= $_GET['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
		
		$msisdn			= $_GET['msisdn'];
		$service_tag	= $_GET['service_tag'];
		$order			= $_GET['order'];
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($msisdn)) { $data .= "&searchmsisdn=".$msisdn; }
		if(!empty($service_tag)) { $data .= "&searchsender=".$service_tag; }
		if(!empty($order)) { $data .= "&order=".$order; }
		$limit			= 30;
		$offset			= (($page * $limit) - $limit);
		$access_token	= $_SESSION['access_token'];
		$url			= URL_WS."WSA-Telcel/api/message/detailed/mo/limit?access_token=".$access_token.$data;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		
		?>
        <h2><?=$start_date;?></h2>
        <div class="frm-label-big">
        	<button type="button" class="frm-button" onClick="backUrl();">Volver</button>
        </div>
        <br>
        <br>
        <?
		
		
		
			//pag_pages ($page, $total_pages, $url_app);
				
			?>
			<div class="frame-scroll-table">
			<table class="scroll-table">
			<tr>
				<th>M&oacute;vil</th>
				<th>N&uacute;mero Corto</th>
                <th>Fecha de Recepci&oacute;n</th>
                <th>Mensaje</th>
			</tr>
			<?
			
			for($i = 0; $i < count($parseUrl); $i++)
			{
				$msisdn			= $parseUrl[$i]['msisdn'];
				$sender			= $parseUrl[$i]['sender'];
				$created_time	= returnFormatDate($parseUrl[$i]['created_time']);
				$content		= $parseUrl[$i]['content'];
				
				$tr_color = $i % 2;
				
				if($tr_color == 0)
				{
					$class	= ""; 
				} else {
					$class	= "tr-color"; 
				}
				
				if($statusCode == 0)
				{
					$status = "Desactivado";
				} else {
					$status = "Activado";
				}
				?>
				<tr class="<?=$class;?>">
					<td><?=$msisdn;?></td>
					<td><?=$sender;?></td>
					<td><?=$created_time;?></td>
                    <td><?=$content;?></td>
				</tr>
				<?
			}
			?>
			</table>
			</div>
			<?
			//pag_pages ($page, $total_pages, $url_app);
		
	}
	else
	{
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function trafficFile ()
{
	?>
    <h1>Reporte de Mensajes por Archivo</h1>
    <?
	$permission = permission ("file_msg");

	if($permission == "Y") {
		$access_token	= $_SESSION['access_token'];
		$id_country		= $_SESSION['id_country'];
		$start_date		= $_GET['start_date'];
		$end_date		= $_GET['end_date'];
		$stage			= $_GET['stage'];
		
		if(!empty($start_date))
		{
			$start_date_get	= returnDateUrl($start_date);
		}
		
		if(!empty($end_date))
		{
			$end_date_get	= returnDateUrl($end_date);
		}
		
		$data			.= "&id_country=".$id_country;
		$data			.= "&start_date=".$start_date_get;
		$data			.= "&end_date=".$end_date_get;
		if(!empty($stage)) { $data_extra .= "&searchstage=".$stage; }
		$url			= URL_WS."WSA-Telcel/api/process/consolidated?access_token=".$access_token.$data.$data_extra;
		$iUrl			= curl_init($url);
		curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl			= curl_exec($iUrl);
		$parseUrl		= json_decode($pUrl,true);
		$totalRows		= count($parseUrl);
		
		if(!empty($start_date))
		{
			$dateFrom	= $start_date;
			$dateTo		= $end_date;
		} else {
			$dateFrom	= date('01-m-Y');
			$dateTo		= date('d-m-Y');
		}
		
		?>
		<div class="frm-label-big">
        	<label>Fecha de Inicio</label>
        	<input type="text" name="start_date" id="start_date" class="frm" value="<?=$dateFrom;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador1">
        </div>
        <div class="frm-label-big">
        	<label>Fecha de T&eacute;rmino</label>
        	<input type="text" name="end_date" id="end_date" class="frm" value="<?=$dateTo;?>" readonly> &nbsp;
            <img src="images/icons/calendar.png" id="lanzador2">
        </div>
        <div class="frm-label-big">
        	<label>Estado</label> <? selectStageFile($stage); ?>
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label>  
	        <button class="frm-button" onClick="trafficFile();">Consultar</button> 
        </div>
        <br>
        <br>
        <br>
        <br>
        <script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "start_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador1"
            }
            )
        </script>
        
        <script type="text/javascript">
        var fecha = new Date();
        var year=fecha.getFullYear();
            Calendar.setup({ inputField  : "end_date", range       :  [2015, 2999],
            ifFormat    : "%d-%m-%Y",
            showsTime      :    false,
            timeFormat     :    "24",
            button      : "lanzador2"
            }
            )
        </script>
        <?
        
		if(!empty($start_date))
		{
			if($totalRows == 0)
			{
				?>
                <div class="show-message">
                <?
                showMessage (9);
                ?>
                </div>
                <?
			}
			else
			{
				?>			
				<table>
				<tr>
					<th>Fecha</th>
                    <th>Archivo</th>
					<th>Total</th>
                    <th>Descargar</th>
				</tr>
				<?
				
				for($i = 0; $i < count($parseUrl); $i++)
				{
					$report_date	= returnFormatDate($parseUrl[$i]['created_time']);
					$report_file	= $parseUrl[$i]['created_time'];
					$filename		= $parseUrl[$i]['file_name'];
					$pid			= $parseUrl[$i]['pid'];
					$count			= $parseUrl[$i]['total'];
					$report_xls		= substr($report_file, 0,10);
					$url_excel		= URL_WS."WSA-Telcel/api/process/detailed?access_token=".$access_token."&id_country=".$id_country."&id_process=".$pid.$data_extra;
					$fileReport		= getFileReport($url_excel);
					
					$tr_color = $i % 2;
					
					if($tr_color == 0)
					{
						$class	= ""; 
					} else {
						$class	= "tr-color"; 
					}
					
					if($statusCode == 0)
					{
						$status = "Desactivado";
					} else {
						$status = "Activado";
					}
					?>
					<tr class="<?=$class;?>">
						<td><?=$report_date;?></td>
						<td><?=$filename;?></td>
						<td><?=$count;?></td>
                        <td><a href="ajaxFuncs/repTrafficFile.php?report=<?=$fileReport;?>"><img src="images/icons/excel.png"></a></td>
					</tr>
					<?
				}
				?>
				<tr>
					<th></th>
					<th></th>
                    <th></th>
                    <th></th>
				</tr>
				</table>
				
				<?
			}
		}
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function changePass ()
{
	?>
    <h1>Cambio de Clave</h1>
    <?
	$permission = permission ("pass_change");
	
	if($permission == "Y") {
		?>
        <div class="frm-label-big">
        	<label>Amtigua Clave</label> 
        	<input type="password" name="old_password" id="old_password" class="frm" placeholder="Antigua Clave">
        </div>
        <div class="frm-label-big">
        	<label>Nueva Clave</label> 
        	<input type="password" name="new_password" id="new_password" class="frm" placeholder="Nueva Clave">
        </div>
        <div class="frm-label-big">
        	<label>Repetir Nueva Clave</label> 
        	<input type="password" name="repeat_password" id="repeat_password" class="frm" placeholder="Repetir Nueva Clave">
        </div>
        <div class="frm-label-big">
        	<label>&nbsp;</label> 
        	<button class="frm-button" onClick="changePass();">Modificar</button>
        </div>
        <br>
        <div id="resultado"></div>
        <?
	} else {
		?>
        <div class="show-message">
        	<? showMessage (1); ?>
        </div>
        <?
	}
}

function logOut ()
{
	$access_token	= $_SESSION['access_token'];
	$url			= URL_WS."WSA-Telcel/api/oauth/revoke?access_token=".$access_token;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	$parseUrl		= json_decode($pUrl,true);
	
	if($statusCode == "401")
	{
		header("refresh: 2; url = index.php");
		?>
        <div id="logout">Sesi&oacute;n Finalizada</div>
        <?
	} else {
		header("refresh: 2; url = index.php");
		?>
        
        <div id="logout">Sesi&oacute;n Finalizada</div>
        <?
	}
	
	header("refresh: 2; url = index.php");
	?>
	
	<?
}

function permission ($module)
{
	
	$access_token	= $_SESSION['access_token'];
	$refresh_token	= $_SESSION['refresh_token'];
	$data			.= $module;
	$data			.= "?access_token=".$access_token;
	$url			= URL_WS."WSA-Telcel/api/user/permission/".$data;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	$parseUrl		= json_decode($pUrl,true);
	$response		= $parseUrl;
	

	if($statusCode == "401" || $response['error'] == "invalid_token")
	{
		$data1			.= "grant_type=refresh_token";
		$data1			.= "&client_id=trusted-client";
		$data1			.= "&refresh_token=".$refresh_token;
		$url2			= URL_WS."WSA-Telcel/oauth/token?".$data1;
		$iUrl1			= curl_init($url2);
		curl_setopt($iUrl1, CURLOPT_RETURNTRANSFER, TRUE);
		$pUrl1			= curl_exec($iUrl1);
		$pUrl2			= json_decode($pUrl1,true);
		
		$_SESSION['access_token'] = $pUrl2['access_token'];
		
		$url_dir = "http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
		
		header("location: ".$url_dir);
	
	} else {
		
		if($response == true)
		{
			$return = "Y";
		} elseif($response == false) 
		{
			$return = "N";
		} 		
	}
		
	return $return;
	
	
	
	curl_close($iUrl);	
}


function pag_pages ($page, $total_pages, $url)
{
	?>
    <div class="pagination-container">
	<?
	if($page > 1) {
		echo "<a title='Primera P&aacute;gina' href='$url&p=1'><img src='images/icons/pag_first.png'></a> "; 
	} else {
	}
	
	if ($page> 1) {
		echo "<a title='Anterior' href='$url&p=".($page-1)."'><img src='images/icons/pag_back.png'></a> "; 
	} else {
	}
	
    for($i = $page-3; $i < $page; $i++) {
		if($i < 1) {
		} else {
			if($i <= 9) { 
				$i = "0$i";
			}
			echo " <a title='P&aacute;gina $i' href='$url&p=$i' class='pag-off'>$i</a> ";
		}
	}
	
	if($i <= 9) {
		$i = "0$i";
	}
	
	echo "<b class='pag-on'>$i</b>";
	
	for($n = $page+1; $n < $page+6; $n++) {
		if($n > $total_pages) {
		} else {
			if($n <= 9) {
				$n = "0$n";
			}
			echo " <a title='P&aacute;gina $n' href='$url&p=$n' class='pag-off'>$n</a> ";
		}
	}
	
	if ($page < $total_pages) {
		echo "<a title='Siguiente' href='$url&p=".($page+1)."'><img src='images/icons/pag_next.png'></a> "; 
	} else {
	}
	
	if ($page < $total_pages) {
		echo " <a title='Última P&aacute;gina' href='$url&p=$total_pages'><img src='images/icons/pag_last.png'></a> "; 
	} else {
	}
	?> 
    </div>
    <br>
    <?
}
