

function download (report)
{
	report		= report;
	var parametros = {
		"report" : report
	};
	
	$.ajax({
		url:   'ajaxFuncs/download.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#download").html(data);
			window.location = "hola.csv";		
		},
        error: function(data) 
		{
			$("#download").html("Error");			
		},
	});
}

function login(id_company, gws_rd)
{
	grant_type		= "password";
	client_id		= "trusted-client";
	user_name		= document.getElementById("user").value;
	password		= document.getElementById("passwd").value;
	
	if(empty(id_company))
	{
		alert("Id company vacio");
		return false;

	}
	
	if(empty(gws_rd))
	{
		alert("Identificador de red no valido");
		return false;

	}
	
    var parametros = {
		"grant_type" : grant_type,
		"client_id" : client_id,
		"username" : user_name,
		"password" : password, 
		"id_company" : id_company
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/login.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			if(data == "Y")
			{
				
				document.location='webapp.php';	
			} else if(data == "E")
			{
				$("#resultado").html("El Usuario ingresado no es Administrador").addClass('show-message');	
			} else {
				$("#resultado").html("Usuario y/o Clave incorrecta").addClass('show-message');	
			}
			
			
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function AdminUserSearch ()
{	
	username		= document.getElementById("username").value;
	
	if(empty(username))
	{
		alert("Ingrese un usuario o nombre de usuario");
		document.getElementById("username").focus();
		return false;

	} else
	{
		document.location= "?module=adminUser&searchname="+username;
	}
}

function adminAddUser()
{	
	username		= document.getElementById("username").value;
	fullname		= document.getElementById("fullname").value;
	password		= document.getElementById("password").value;
	role			= document.getElementById("role").value;
	language		= document.getElementById("language").value;
	var regex 		= /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
	
	if(empty(username))
	{
		alert("Debe ingresar un Email");
		document.getElementById("username").focus();
		return false;
	}

    if (!regex.test($('#username').val().trim())) 
	{
		alert('El valor de usuario debe contener un e-mail v&aacute;lido');
		document.getElementById("username").focus();
		return false;
    }
	
	if(empty(fullname))
	{
		alert("Ingrese el nombre completo del usuario");
		document.getElementById("fullname").focus();
		return false;
	}
	
	if(empty(password))
	{
		alert("Ingrese una clave mayor o igual a 6 caracteres");
		document.getElementById("password").focus();
		return false;
	}
	
	if(password.length < 6)
	{
		alert("Ingrese una clave mayor o igual a 6 caracteres");
		document.getElementById("password").focus();
		return false;
	}
	
	if(empty(role))
	{
		alert("Seleccione un perfil de la lista desplegable");
		document.getElementById("role").focus();
		return false;
	}
	
	if(empty(language))
	{
		alert("Seleccione un lenguaje");
		document.getElementById("language").focus();
		return false;
	}
	
	
	
    var parametros = {
		"username" : username,
		"fullname" : fullname,
		"password" : password,
		"role" : role, 
		"language" : language
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminAddUser.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function adminEditUser(id_user)
{	
	username		= document.getElementById("username").value;
	fullname		= document.getElementById("fullname").value;
	password		= document.getElementById("password").value;
	role			= document.getElementById("role").value;
	language		= document.getElementById("language").value;
	
	if(empty(fullname))
	{
		alert("Ingrese el nombre completo del usuario");
		document.getElementById("fullname").focus();
		return false;
	}
	
	if(empty(role))
	{
		alert("Debe seleccionar un Role");
		document.getElementById("role").focus();
		return false;
	}
	
	if(!empty(password))
	{
		if(password.length < 6)
		{
			alert("La Clave debe contener al menos 6 caracteres");
			document.getElementById("password").focus();
			return false;
		}
	}
	
	if(empty(language))
	{
		alert("Seleccione un lenguaje");
		document.getElementById("language").focus();
		return false;
	}
	
    var parametros = {
		"id_user" : id_user,
		"username" : username,
		"fullname" : fullname,
		"password" : password,
		"role" : role,
		"language" : language
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminEditUser.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function adminStatusUser (id_user)
{
	status		= document.getElementById("status").value;
	
    var parametros = {
		"id_user" : id_user,
		"enabled" : status
	};
	
	$.ajax({
		url:   'ajaxFuncs/adminStatusUser.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}


function adminAddGroupList()
{
	country			= document.getElementById("country").value;
	name_group		= document.getElementById("name_group").value;
	
	if(empty(country))
	{
		alert("Seleccione un Pais");
		document.getElementById("country").focus();
		return false;
	}
	
	if(empty(name_group))
	{
		alert("Ingrese el nombre");
		document.getElementById("name_group").focus();
		return false;
	}
	
    var parametros = {
		"name_group" : name_group,
		"country" : country
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminAddGroupList.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function adminStatusGroupList (id_group)
{
	status			= document.getElementById("status").value;
    var parametros	= {
		"id_group" : id_group,
		"enabled" : status
	};
	
	$.ajax({
		url:   'ajaxFuncs/adminStatusGroupList.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');		
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function AdminSearchGroupList ()
{	
	name_group		= document.getElementById("name_group").value;
	
	if(empty(name_group))
	{
		alert("Ingrese el nombre");
		document.getElementById("name_group").focus();
		return false;

	} else
	{
		document.location= "?module=adminGroupList&searchname="+name_group;
	}
}

function adminEditGroupListSearch (id_group)
{	
	celular		= document.getElementById("celular").value;
	
	if(empty(celular))
	{
		alert("Ingrese un número móvil con formato internacional");
		document.getElementById("celular").focus();
		return false;

	} else
	{
		document.location= "?module=adminEditGroupList&id_group="+id_group+"&searchmsisdn="+celular;
	}
}

function adminDeletePhoneGroupList (id_group, id_mobile)
{
    var parametros = {
		"id_group" : id_group,
		"id_mobile" : id_mobile
	};
	
	$.ajax({
		url:   'ajaxFuncs/adminDeletePhoneGroupList.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#response"+id_mobile).html(data);		
		},
        error: function(data) 
		{
			$("#response"+id_mobile).html("Error");			
		},
	});
}

function adminAddPhoneGroupList(id_group)
{
	country			= document.getElementById("country").value;
	msisdn			= document.getElementById("msisdn").value;
	
	if(empty(msisdn))
	{
		alert("Ingrese un número móvil con formato internacional");
		document.getElementById("msisdn").focus();
		return false;
	}

    var parametros = {
		"id_group" : id_group,
		"msisdn" : msisdn,
		"country" : country,
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminAddPhoneGroupList.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}


function adminAddFileGroupList()
{
	country			= document.getElementById("country").value;
	phone_list		= document.getElementById("phone_list").value;
	id_group		= document.getElementById("id_group").value;
	
	if(country == "")
	{ 
		alert("Debe seleccionar un PaPa&iacute;s");
		document.getElementById("country").focus();
		return;
	}
	
	if(phone_list == "")
	{ 
		alert("Debe seleccionar un Archivo");
		document.getElementById("phone_list").focus();
		return;
	}
	
	var f = $(this);
	
	var file			= $("#phone_list")[0].files[0];
	var formData		= new FormData($("#formulario")[0], file, id_group, country);
	
	$.ajax({
		url:   'ajaxFuncs/adminAddFileGroupList.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});

}

function adminAddCostCenter ()
{	
	name_cc		= document.getElementById("name_cc").value;
	code		= document.getElementById("code").value;
	
	if(empty(name_cc))
	{
		alert("Ingrese el nombre");
		document.getElementById("name_cc").focus();
		return false;
	}
	
	if(empty(code))
	{
		alert("Debe ingresar un Centro de Costo");
		document.getElementById("code").focus();
		return false;
	}
	
    var parametros = {
		"name_cc" : name_cc,
		"code" : code
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminAddCostCenter.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function adminEditCostCenter (id_center)
{	
	name_cc		= document.getElementById("name_cc").value;
	code		= document.getElementById("code").value;
	
	if(empty(name_cc))
	{
		alert("Ingrese el nombre");
		document.getElementById("name_cc").focus();
		return false;
	}
	
	if(empty(code))
	{
		alert("Debe ingresar un Centro de Costo");
		document.getElementById("code").focus();
		return false;
	}
	
    var parametros = {
		"id_center": id_center,
		"name_cc" : name_cc,
		"code" : code
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/adminEditCostCenter.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function adminStatusCostCenter (id_center)
{
	status		= document.getElementById("status").value;
	
    var parametros = {
		"id_center" : id_center,
		"enabled" : status
	};
	
	$.ajax({
		url:   'ajaxFuncs/adminStatusCostCenter.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');			
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function AdminCostCenterSearch ()
{	
	username		= document.getElementById("username").value;
	
	if(empty(username))
	{
		alert("Debe ingresar un Usuario o Nombre");
		document.getElementById("username").focus();
		return false;

	} else
	{
		document.location= "?module=adminCostCenter&searchname="+username;
	}
}

function singleMessage()
{	
	country		= document.getElementById("country").value;
	msisdn		= document.getElementById("msisdn").value;
	cost_center	= document.getElementById("cost_center").value;
	message		= document.getElementById("message").value;
	type_send	= document.getElementById("type_send").value;
	data_coding	= document.getElementById("data_coding").value;
	service		= document.getElementById("service").value;
	
	if(empty(service))
	{
		alert("Debe seleccionar un Servicio");
		document.getElementById("service").focus();
		return false;
	}
		
	if(empty(country))
	{
		alert("Debe seleccionar un País");
		document.getElementById("country").focus();
		return false;
	}

	if(empty(cost_center))
	{
		alert("Debe seleccionar un Centro de Costo");
		document.getElementById("cost_center").focus();
		return false;
	}
	
	if(empty(type_send))
	{
		alert("Debe seleccionar un Tipo de Envío");
		document.getElementById("type_send").focus();
		return false;
	}
	
	if(type_send == 2)
	{
		dispatchTime = document.getElementById("dispatchTime").value;
	} else {
		dispatchTime = null;
	}
	
	if(empty(msisdn))
	{
		alert("Ingrese un número móvil con formato internacional");
		document.getElementById("msisdn").focus();
		return false;
	}
	
	if(empty(data_coding))
	{
		alert("Debe seleccionar un Data Coding");
		document.getElementById("data_coding").focus();
		return false;
	}
	
	if(empty(message))
	{
		alert("Debe ingresar un Mensaje");
		document.getElementById("message").focus();
		return false;
	}
		
	var parametros = {
		"country" : country,
		"msisdn" : msisdn,
		"message" : message, 
		"cost_center" : cost_center,
		"dispatchTime" : dispatchTime,
		"data_coding" : data_coding,
		"service" : service
	};
	
	$.ajax({
		url:   'ajaxFuncs/singleMessage.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function groupMessage()
{	
	country		= document.getElementById("country").value;
	id_group	= document.getElementById("id_group").value;
	cost_center	= document.getElementById("cost_center").value;
	message		= document.getElementById("message").value;
	type_send	= document.getElementById("type_send").value;
	data_coding	= document.getElementById("data_coding").value;
	service		= document.getElementById("service").value;
	
	if(empty(service))
	{
		alert("Debe seleccionar un Servicio");
		document.getElementById("service").focus();
		return false;
	}
	
	if(empty(country))
	{
		alert("Debe seleccionar un Pa&iacute;s");
		document.getElementById("country").focus();
		return false;
	}
	
	if(empty(cost_center))
	{
		alert("Debe seleccionar un Centro de Costo");
		document.getElementById("cost_center").focus();
		return false;
	}
	
	if(empty(type_send))
	{
		alert("Debe seleccionar un Tipo de Env&iacute;o");
		document.getElementById("type_send").focus();
		return false;
	}
	
	if(type_send == 2)
	{
		dispatchTime = document.getElementById("dispatchTime").value;
	} else {
		dispatchTime = null;
	}
	
	if(empty(id_group))
	{
		alert("Debe seleccionar una lista de Env&iacute;o");
		document.getElementById("id_group").focus();
		return false;
	}
	
	if(empty(data_coding))
	{
		alert("Debe seleccionar un Data Coding");
		document.getElementById("data_coding").focus();
		return false;
	}
	
	if(empty(message))
	{
		alert("Debe ingresar un Mensaje");
		document.getElementById("message").focus();
		return false;
	}
	
    var parametros = {
		//"country" : country,
		"id_group" : id_group,
		"cost_center" : cost_center,
		"message" : message,
		"dispatchTime" : dispatchTime,
		"data_coding" : data_coding,
		"service" : service
	};
	
	$.ajax({
		url:   'ajaxFuncs/groupMessage.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function fileMessage(type)
{
	
	phone_list	= document.getElementById("phone_list").value;
	cost_center	= document.getElementById("cost_center").value;
	type_send	= document.getElementById("type_send").value;
	data_coding	= document.getElementById("data_coding").value;
	
	if(type == "LOCAL")
	{
		service		= document.getElementById("service").value;
		country		= document.getElementById("country").value;
	
		if(empty(service))
		{
			alert("Debe seleccionar un Servicio");
			document.getElementById("service").focus();
			return false;
		}
		
		if(empty(country))
		{
			alert("Debe seleccionar un País");
			document.getElementById("country").focus();
			return false;
		}
	}
	
	if(empty(cost_center))
	{
		alert("Debe seleccionar un Centro de Costo");
		document.getElementById("cost_center").focus();
		return false;
	}
	
	if(empty(type_send))
	{
		alert("Debe seleccionar un Tipo de Env&iacute;o");
		document.getElementById("type_send").focus();
		return false;
	}
	
	if(type_send == 2)
	{
		dispatchTime = document.getElementById("dispatchTime").value;
		
		if(empty(dispatchTime))
		{
			alert("Debe seleccionar una Fecha y Hora de Env&iacute;vio");
			document.getElementById("dispatchTime").focus();
			return false;
		}
	
	} else {
		dispatchTime = null;
	}
	
	if(empty(data_coding))
	{
		alert("Debe seleccionar un Data Coding");
		document.getElementById("data_coding").focus();
		return false;
	}
	
	if(empty(phone_list))
	{
		alert("Debe seleccionar un Archivo");
		document.getElementById("phone_list").focus();
		return false;
	}
	
	phone_list	= document.getElementById("phone_list").files[0];
	
	formData 		= new FormData();
	
	if(type == "LOCAL")
	{
		formData.append('country', country);
		formData.append('service', service);
	}
	
	formData.append('phone_list', phone_list);
	formData.append('cost_center', cost_center);
	formData.append('data_coding',data_coding);
	formData.append('type_send', type_send);
	formData.append('dispatchTime', dispatchTime);
	
	$("#resultado").html('<p style="text-align:left;"><img src="images/waiting.gif"></p>').removeClass('show-message');
	
	$.ajax({
		url:   'ajaxFuncs/fileMessage.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function cancelStatusMessage (pid)
{
    var parametros = {
		"pid" : pid
	};
	
	$.ajax({
		url:   'ajaxFuncs/cancelStatusMessage.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function selectServiceCountry (country)
{
	//document.getElementById("id_group").disabled= true;
	
	document.getElementById("id_group").value = "";
	$("#id_group").html("<option value=''>Seleccionar</option>");
	
	formData 		= new FormData();
	
	formData.append('country', country);
	
	$.ajax({
		url:   'ajaxFuncs/selectServiceCountry.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#service").html(data);
		},
        error: function(data) 
		{
			$("#selectServiceCountry").html("Error");			
		},
	});
	
	
	
	
}

function selectSendListCountry ()
{
	country		= document.getElementById("country").value;
	
	formData 	= new FormData();
	
	formData.append('country', country);
	
	$.ajax({
		url:   'ajaxFuncs/selectSendListCountry.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#id_group").html(data);	
		},
        error: function(data) 
		{
			$("#selectServiceCountry").html("Error");			
		},
	});
}



function typeDataCoding (id_coding)
{
	formData 		= new FormData();
	
	formData.append('id_coding', id_coding);
	
	$.ajax({
		url:   'ajaxFuncs/typeDataCoding.php',
		data: formData,
		type:  'POST', 
		cache: false,
		contentType: false,
		processData: false,
		success: function(data) 
		{
			$("#typeDataCoding").html(data);	
		},
        error: function(data) 
		{
			$("#typeDataCoding").html("Error");			
		},
	});
}

function contador (campo, cuentacampo, limite) {
                   
        if (campo.value.length > limite) {
        campo.value = campo.value.substring(0, limite);
        } else {
        cuentacampo.value = limite - campo.value.length;
        }
}

function selectTypeMessage (value)
{
	if(value == 2)
	{
		document.getElementById('typeSendTime').style.display = 'block';
	} else {
		document.getElementById('typeSendTime').style.display = 'none';
	}
}

function trafficMT ()
{	
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	msisdn			= document.getElementById("msisdn").value;
	input_mode		= document.getElementById("input_mode").value;
	user			= document.getElementById("user").value;
	stage			= document.getElementById("stage").value;
	service_tag		= document.getElementById("service_tag").value;
	order			= $('input:radio[name=order]:checked').val();
	
	if (typeof order == "undefined")
	{
		order = "ASC";
	}
		
	document.location= "?module=trafficMT&start_date="+start_date+"&end_date="+end_date+"&msisdn="+msisdn+"&input_mode="+input_mode+"&user="+user+"&stage="+stage+"&service_tag="+service_tag+"&order="+order;

}

function trafficMO ()
{	
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	msisdn			= document.getElementById("msisdn").value;
	service_tag		= document.getElementById("service_tag").value;
	stage			= document.getElementById("stage").value;
	order			= $('input:radio[name=order]:checked').val();
	
	if (typeof order == "undefined")
	{
		order = "ASC";
	}
	
	document.location= "?module=trafficMO&start_date="+start_date+"&end_date="+end_date+"&msisdn="+msisdn+"&service_tag="+service_tag+"&stage="+stage+"&order="+order;

}

function trafficFile ()
{	
	start_date		= document.getElementById("start_date").value;
	end_date		= document.getElementById("end_date").value;
	stage			= document.getElementById("stage").value;

	document.location= "?module=trafficFile&start_date="+start_date+"&end_date="+end_date+"&stage="+stage;

}

function changePass ()
{	
	old_password	= document.getElementById("old_password").value;
	new_password	= document.getElementById("new_password").value;
	repeat_password	= document.getElementById("repeat_password").value;
	
	if(empty(old_password))
	{
		alert("Debe ingresar la Clave Antigua");
		document.getElementById("old_password").focus();
		return false;
	}
	
	if(empty(new_password))
	{
		alert("Debe ingresar la nueva Clave");
		document.getElementById("new_password").focus();
		return false;
	}
	
	if(new_password.length < 6)
	{
		alert("La nueva Clave debe contener al menos 6 caracteres");
		document.getElementById("new_password").focus();
		return false;
	}
	
	if(empty(repeat_password))
	{
		alert("Debe repetir la nueva Clave");
		document.getElementById("repeat_password").focus();
		return false;
	}
	
	if(repeat_password.length < 6)
	{
		alert("La nueva Clave debe contener al menos 6 caracteres");
		document.getElementById("repeat_password").focus();
		return false;
	}
	
	if(new_password != repeat_password)
	{
		alert("La nueva Clave no coincide");
		document.getElementById("repeat_password").focus();
		return false;
	}
	
    var parametros = {
		"old_password" : old_password,
		"new_password" : new_password,
		"repeat_password" : repeat_password
	};
	
	$.ajax({
		//url:   'http://devfenix.mobid.cl:9090/WSA-Telcel/oauth/token',
		url:   'ajaxFuncs/changePass.php',
		data: parametros,
		type:  'POST', 
		success: function(data) 
		{
			$("#resultado").html(data).addClass('show-message');	
		},
        error: function(data) 
		{
			$("#resultado").html("Error").addClass('show-message');			
		},
	});
}

function backUrl ()
{
	history.back();
}

function goUrl (url)
{
	document.location= url;
}

function empty(text)
{
	if(text == "")
	{
		return true;
	} else {
		return false;
	}
	
}



