<?
include('../config.php');

function showMessage ($value)
{
	$alert		= '<img src="images/icons/alert.png"> ';
	$delete		= '<img src="images/icons/delete.png"> ';
	$confirm	= '<img src="images/icons/confirm.png"> ';
	
	switch($value)
	{
		case 1: echo $alert."Usted no tiene permisos de acceso a esta opci&oacute;n"; break;
		case 2: echo $confirm."El usuario ha sido agregado correctamente con &eacute;xito"; break;
		case 3: echo $alert."El usuario que ha ingresado ya existe"; break;
		case 4: echo $confirm."El usuario ha sido modificado correctamente"; break;
		case 5: echo $alert."No se han podido modificar los datos"; break;
		case 6: echo $confirm."Desactivado."; break;
		case 7: echo $confirm."Activado."; break;
		case 8: echo $alert."No se ha podido desactivar al usuario"; break;
		case 9: echo $alert."No existen registros asociados"; break;
		case 10: echo $confirm."El n&uacute;mero m&oacute;vil fue asociado correctamente a la lista de env&iacute;o"; break;
		case 11: echo $alert."El n&uacute;mero m&oacute;vil ya existe en esta lista de env&iacute;o"; break;
		case 12: echo $alert."No se ha podido agregar el n&uacute;mero m&oacute;vil"; break;
		case 13: echo $alert."No se ha podido crear la carpeta"; break;
		case 14: echo $alert."No se ha podido obtener el archivo. Valide los pemisos de la carpeta"; break;
		case 15: echo $alert."El archivo debe tener extensi&oacute;n .csv o .txt"; break;
		case 16: echo $confirm."El centro de costo ha sido creado correctamente"; break;
		case 17: echo $confirm."El centro de costo ha sido modificado correctamente"; break;
		case 18: echo $confirm."El archivo ha sido cargado correctamente para ser despachado"; break;
		case 19: echo $confirm."La lista de env&iacute;o ha sido agregada correctamente"; break;
		case 20: echo $alert."El archivo no debe superar el límite de 5.000 registros para listas de env&iacute;o"; break;
		case 21: echo $alert."El prefijo del n&uacute;mero m&oacute;vil es incorrecto"; break;
		case 22: echo $alert."El largo del n&uacute;mero m&oacute;vil es incorrecto"; break;
		case 23: echo $alert."El largo del mensaje supera el m&aacute;ximo permitido"; break;
		case 24: echo $alert."El formato de los registros del archivo es incorrecto"; break;
		case 25: echo $alert."El archivo no debe superar el límite de 50.000 registros"; break;
		case 26: echo $confirm."La clave ha sido modificada correctamente"; break;
		case 27: echo $alert."La lista de env&iacute;o ingresada ya existe"; break;
		case 28: echo $alert."P&eacute;rdida de conexi&oacute;n. Por favor intente nuevamente"; break;
		case 29: echo $alert."No se ha podido desactivar el centro de costo"; break;
		case 30: echo $alert."La hora agendada debe ser mayor a 30 minutos respecto de la hora actual"; break;
		case 31: echo $alert."No se ha podido modificar la clave"; break;
		case 32: echo $alert."No se ha podido agregar el centro de costo"; break;
		case 33: echo $alert."No se ha podido cargar la lista de envío"; break;
		case 34: echo $alert."La hora agendada debe ser mayor a 30 minutos respecto de la hora actual"; break;
		case 35: echo $confirm."El mensaje ha sido cargado correctamente para ser despachado"; break;
		case 36: echo $confirm."El mensaje ha sido cargado correctamente para ser despachado"; break;
		case 37: echo $alert."El n&uacute;mero m&oacute;vil s&oacute;lo debe contener d&iacute;gitos"; break;
		case 38: echo $alert."El centro de costo que ha ingresado ya existe"; break;
		case 39: echo $confirm."El archivo ha sido asociado correctamente a la lista de env&iacute;o"; break;
		case 40: echo $confirm."El usuario ha sido activado correctamente"; break;
		case 41: echo $confirm."El usuario ha sido desactivado correctamente"; break;
		case 42: echo $confirm."La Lista de Env&iacute;o ha sido activada correctamente"; break;
		case 43: echo $confirm."La Lista de Env&iacute;o ha sido desactivada correctamente"; break;
		case 44: echo $confirm."El Centro de Costo ha sido activado correctamente"; break;
		case 45: echo $confirm."El Centro de Costo ha sido desactivado correctamente"; break;
		case 46: echo $confirm."El env&iacute;o ha sido cancelado correctamente"; break;
	}
}

function urlTotalRows ($url)
{
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	$parseUrl		= json_decode($pUrl,true);
	
	if($statusCode == "401")
	{
		refresh_token();
	} else {
		return $parseUrl;
	}	
	
	curl_close($iUrl);
}

function parseUrl ($url)
{
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$statusCode		= curl_getinfo($iUrl, CURLINFO_HTTP_CODE);
	$parseUrl		= json_decode($pUrl,true);
	
	if($statusCode == "401")
	{
		refresh_token();
	} else {
		return $parseUrl;
	}	
	
	curl_close($iUrl);
}

function getFileReport ($url)
{
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	
	return $pUrl;	
	
	curl_close($iUrl);
}

function refresh_token ()
{
	$refresh_token	= $_SESSION['refresh_token'];
	$data			.= "grant_type=refresh_token";
	$data			.= "&client_id=trusted-client";
	$data			.= "&refresh_token=".$refresh_token;
	$url			= URL_WS."WSA-Telcel/oauth/token?".$data;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$parseUrl		= json_decode($pUrl,true);
		
	$_SESSION['access_token']	= $parseUrl['access_token'];

	curl_close($iUrl);
}

function arrayCountry ()
{
	$countrys = array (
					array ("id" => 1, "code" => "MX", "country" => "Mexico", "pref" => "52", "max" => "12"), 
					array ("id" => 2, "code" => "BR", "country" => "Brasil", "pref" => "55", "max" => "13"),
					array ("id" => 3, "code" => "AR", "country" => "Argertina", "pref" => "54", "max" => "13"),
					array ("id" => 4, "code" => "CO", "country" => "Colombia", "pref" => "57", "max" => "12"),
					array ("id" => 5, "code" => "CL", "country" => "Chile", "pref" => "56", "max" => "11"),
					array ("id" => 6, "code" => "PE", "country" => "Peru", "pref" => "51", "max" => "11"),
					array ("id" => 7, "code" => "HN", "country" => "Honduras", "pref" => "504", "max" => "11"),
					array ("id" => 8, "code" => "UY", "country" => "Uruguay", "pref" => "598", "max" => "11"),
					array ("id" => 9, "code" => "PY", "country" => "Paraguay", "pref" => "595", "max" => "12"),
					array ("id" => 10, "code" => "EC", "country" => "Ecuador", "pref" => "593", "max" => "12"),
					array ("id" => 11, "code" => "NI", "country" => "Nicaragua", "pref" => "505", "max" => "11"),
					array ("id" => 12, "code" => "DO", "country" => "Republica Dominicana", "pref" => "1829", "max" => "11"),
					array ("id" => 12, "code" => "DO", "country" => "Republica Dominicana", "pref" => "1809", "max" => "11"),
					array ("id" => 12, "code" => "DO", "country" => "Republica Dominicana", "pref" => "1849", "max" => "11"),
					array ("id" => 13, "code" => "PA", "country" => "Panama", "pref" => "507", "max" => "11"),
					array ("id" => 14, "code" => "SV", "country" => "El Salvador", "pref" => "503", "max" => "11"),
					array ("id" => 15, "code" => "GT", "country" => "Guatemala", "pref" => "502", "max" => "11"),
					array ("id" => 16, "code" => "CR", "country" => "Costa Rica", "pref" => "506", "max" => "11"),
					array ("id" => 17, "code" => "PR", "country" => "Puerto Rico", "pref" => "1787", "max" => "11"),
					array ("id" => 17, "code" => "PR", "country" => "Puerto Rico", "pref" => "1939", "max" => "11")
				);
	
	return $countrys;
}

function groupArray($array,$groupkey)
{
 if (count($array)>0)
 {
 	$keys = array_keys($array[0]);
 	$removekey = array_search($groupkey, $keys);		
	if ($removekey===false)
 		return array("Clave \"$groupkey\" no existe");
 	else

 	$groupcriteria = array();
 	$return=array();
 	foreach($array as $value)
 	{
 		$item=null;
 		foreach ($keys as $key)
 		{
 			$item[$key] = $value[$key];
 		}
 	 	$busca = array_search($value[$groupkey], $groupcriteria);
 		if ($busca === false)
 		{
 			$groupcriteria[]=$value[$groupkey];
 			$return[]=array($groupkey=>$value[$groupkey],'groupeddata'=>array());
 			$busca=count($return)-1;
 		}
 		$return[$busca]['groupeddata'][]=$item;
 	}
 	return $return;
 }
 else
 	return array();
}

function admAddFileGroupListValueLen ($array, $phone, $code) 
{
	foreach ($array as $k => $v) 
	{
		$id			= trim($v['id']);
		$max		= trim($v['max']);
		$code_c		= trim($v['code']);
		$pref		= trim($v['pref']);
		$len_pref	= trim(strlen($v['pref']));
		$phone_pref	= trim(substr($phone, 0, $len_pref));
		
		if($code == $id)
		{
			if($phone_pref == $pref)
			{
				if(strlen($phone) != $max)
				{
					$r = "Y";
				}
				break;
				
			} else 
			{
				$r = "N";
			}
		}
	}
	return $r;
}

function admAddFileGroupListValuePref ($array, $phone, $code) 
{
	foreach ($array as $k => $v) 
	{
		$id			= trim($v['id']);
		$code_c		= trim($v['code']);
		$max		= trim($v['max']);
		$pref		= trim($v['pref']);
		$len_pref	= trim(strlen($v['pref']));
		$phone_pref	= trim(substr($phone, 0, $len_pref));
		
		if($code == $id)
		{
			if($phone_pref == $pref)
			{
				$r = "Y";
				break;
				
			} else 
			{
				$r = "N";
			}
		}
	}
	return $r;
}

function arrayDataCoding ()
{
	$array = array (
					array ("id" => 3, "code" => "0", "description" => "GSM-7  (Sin acento)")
					//array ("id" => 1, "code" => "8", "description" => "UTF-8 (Con acento)")
				);
	
	return $array;
}

function arrayService ()
{
	$array = array (
					array ("id" => 1, "code" => "utf-8", "description" => "UTF-8 (Con acento)"), 
					array ("id" => 3, "code" => "gsm-7", "description" => "GSM-7  (Sin acento)")
				);
	
	return $array;
}

function arrayInputMode ()
{
	$array = array (
					array ("id" => 1, "code" => "SNG", "description" => "Individual"), 
					array ("id" => 2, "code" => "GRP", "description" => "Lista de Env&iacute;o"),
					array ("id" => 3, "code" => "FILE", "description" => "Archivo")
				);
	
	return $array;
}

function arrayInputModeName ()
{
	$array = array (
					array ("id" => 1, "code" => "SNG", "description" => "Individual"), 
					array ("id" => 2, "code" => "GRP", "description" => "Lista de Env&iacute;o"),
					array ("id" => 3, "code" => "FILE", "description" => "Archivo")
				);
	
	return $array;
}

function arrayTypeSend ()
{
	$array = array (
					array ("id" => 1, "code" => "IMM", "description" => "Inmediato"), 
					array ("id" => 2, "code" => "SCH", "description" => "Agendado")
				);
	
	return $array;
}

function arrayOrder ()
{
	$array = array (
					array ("id" => 1, "code" => "ASC", "description" => "Ascendente"), 
					array ("id" => 2, "code" => "DESC", "description" => "Descendente")
				);
	
	return $array;
}

function arrayLanguage ()
{
	$array = array (
					array ("id" => 1, "code" => "ES", "description" => "Espa&ntilde;ol"), 
					array ("id" => 2, "code" => "EN", "description" => "Ingl&eacute;s"),
					array ("id" => 3, "code" => "PT", "description" => "Portugu&eacute;s")
				);
	
	return $array;
}

function arrayStatus ()
{
	$array = array (
					array ("id" => 1, "code" => "1", "description" => "Activado"), 
					array ("id" => 2, "code" => "0", "description" => "Desactivado")
				);
	
	return $array;
}


function arrayStage ()
{
	$array = array (
					array ("id" => 1, "code" => "QUEUED", "description" => "Pendiente"), 
					array ("id" => 2, "code" => "PUSHED", "description" => "Despachado"),
					array ("id" => 3, "code" => "FAILED", "description" => "Fallido"), 
					array ("id" => 4, "code" => "CONFIRMED", "description" => "Confirmado"),
					array ("id" => 5, "code" => "REJECTED", "description" => "Rechazado")
				);
	
	return $array;
}

function arrayStageFile ()
{
	$array = array (
					array ("id" => 1, "code" => "QUEUED", "description" => "Pendiente"), 
					array ("id" => 2, "code" => "PUSHED", "description" => "Despachado"),
					array ("id" => 3, "code" => "FAILED", "description" => "Fallido"), 
					array ("id" => 4, "code" => "CONFIRMED", "description" => "Confirmado"),
					array ("id" => 5, "code" => "REJECTED", "description" => "Rechazado"),
					array ("id" => 5, "code" => "NOUPLOAD", "description" => "Error de Carga")
				);
	
	return $array;
}

function arrayStageMo ()
{
	$array = array (
					array ("id" => 1, "code" => "ARRIVED", "description" => "MO"), 
					array ("id" => 2, "code" => "DELIVERED", "description" => "DR")
				);
	
	return $array;
}

function getStageName ($value) 
{
	$array = arrayStage();
	
	foreach($array as $mode)
	{
		if($value == $mode['code'])
		{
			$return = $mode['description'];
		}
	}
	
	return $return;
}

function getStageFileName ($value) 
{
	$array = arrayStageFile();
	
	foreach($array as $mode)
	{
		if($value == $mode['code'])
		{
			$return = $mode['description'];
		}
	}
	
	return $return;
}

function getStageMoName ($value) 
{
	$array = arrayStageMo();
	
	foreach($array as $mode)
	{
		if($value == $mode['code'])
		{
			$return = $mode['description'];
		}
	}
	
	return $return;
}

function getStatusName ($value) 
{
	$array = arrayStatus();
	
	foreach($array as $mode)
	{
		if($value == $mode['code'])
		{
			$return = $mode['description'];
		}
	}
	
	return $return;
}

function getInputModeName ($value) 
{
	$array = arrayInputModeName();
	
	foreach($array as $mode)
	{
		if($value == $mode['code'])
		{
			$return = $mode['description'];
		}
	}
	
	return $return;
}

function getLanguageName ($value) 
{
	$array = arrayLanguage();
	
	foreach($array as $mode)
	{
		if($value == $mode['code'])
		{
			$return = $mode['description'];
		}
	}
	
	return $return;
}

function getTypeSendName ($value) 
{
	$array = arrayTypeSend();
	
	foreach($array as $mode)
	{
		if($value == $mode['code'])
		{
			$return = $mode['description'];
		}
	}
	
	return $return;
}

function selectUser ($value) 
{
	$access_token	= $_SESSION['access_token'];
	$data			.= "&offset=0";
	$data			.= "&limit=100";
	$url			= URL_WS."WSA-Telcel/api/user?access_token=".$access_token.$data;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$parseUrl		= json_decode($pUrl,true);
	?>
    <select name="user" id="user" class="frm">
    	<option value="">Todos</option>
        <?

		for($i = 0; $i < count($parseUrl); $i++)
		{
			$id_user	= $parseUrl[$i]['id_user'];
			$full_name	= $parseUrl[$i]['full_name'];
			
			if($id_user == $value)
			{
				$selected = "selected";
			} else {
				$selected = "";
			}
			?>
            <option value="<?=$id_user;?>" <?=$selected;?>><?=$full_name;?></option>
            <?
		}
		?>
    </select>
    <?
	curl_close($iUrl);
}

function selectOrder ($value) 
{
	$array = arrayOrder();

	foreach($array as $mode)
	{
		if($mode['code'] == $value)
		{
			$selected = "checked";
		} else {
			$selected = "";
		}
		?>
		<input type="radio" name="order" id="order" value="<?=$mode['code']?>" <?=$selected;?>><?=$mode['description']?>
		<?
	}
}

function selectLanguage ($value) 
{
	$array = arrayLanguage();
	?>
    <select name="language" id="language" class="frm">
    	<option value="">Seleccionar</option>
        <?
		foreach($array as $mode)
		{
			if($mode['code'] == $value)
			{
				$selected = "selected";
			} else {
				$selected = "";
			}
			?>
            <option value="<?=$mode['code']?>" <?=$selected;?>><?=$mode['description']?></option>
            <?
		}
		?>
    </select>
    <?
}

function selectStageFile ($value) 
{
	$array = arrayStageFile();
	?>
    <select name="stage" id="stage" class="frm">
    	<option value="">Todos</option>
        <?
		foreach($array as $mode)
		{
			if($mode['code'] == $value)
			{
				$selected = "selected";
			} else {
				$selected = "";
			}
			?>
            <option value="<?=$mode['code']?>" <?=$selected;?>><?=$mode['description']?></option>
            <?
		}
		?>
    </select>
    <?
}

function selectStageMo ($value) 
{
	$array = arrayStageMo();
	?>
    <select name="stage" id="stage" class="frm">
    	<option value="">Todos</option>
        <?
		foreach($array as $mode)
		{
			if($mode['code'] == $value)
			{
				$selected = "selected";
			} else {
				$selected = "";
			}
			?>
            <option value="<?=$mode['code']?>" <?=$selected;?>><?=$mode['description']?></option>
            <?
		}
		?>
    </select>
    <?
}

function selectStatus ($value) 
{
	$array = arrayStatus();
	?>
    <select name="status" id="status" class="frm">
    	<option value="">Seleccionar</option>
        <?
		foreach($array as $mode)
		{
			if($mode['code'] == $value)
			{
				$selected = "selected";
			} else {
				$selected = "";
			}
			?>
            <option value="<?=$mode['code']?>" <?=$selected;?>><?=$mode['description']?></option>
            <?
		}
		?>
    </select>
    <?
}


function selectInputMode ($value) 
{
	$array = arrayInputMode();
	?>
    <select name="input_mode" id="input_mode" class="frm">
    	<option value="">Todos</option>
        <?
		foreach($array as $mode)
		{
			if($mode['code'] == $value)
			{
				$selected = "selected";
			} else {
				$selected = "";
			}
			?>
            <option value="<?=$mode['code']?>" <?=$selected;?>><?=$mode['description']?></option>
            <?
		}
		?>
    </select>
    <?
}

function selectStage ($value) 
{
	$array = arrayStage();
	?>
    <select name="stage" id="stage" class="frm">
    	<option value="">Todos</option>
        <?
		foreach($array as $mode)
		{
			if($mode['code'] == $value)
			{
				$selected = "selected";
			} else {
				$selected = "";
			}
			?>
            <option value="<?=$mode['code']?>" <?=$selected;?>><?=$mode['description']?></option>
            <?
		}
		?>
    </select>
    <?
}

function selectGroup () 
{
	$access_token	= $_SESSION['access_token'];
	$data			.= "&offset=0";
	$data			.= "&limit=100";
	$url			= URL_WS."WSA-Telcel/api/group?access_token=".$access_token.$data;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$parseUrl		= json_decode($pUrl,true);
	?>
    <select name="id_group" id="id_group" class="frm">
    	<option value="">Seleccionar</option>
        <?

		for($i = 0; $i < count($parseUrl); $i++)
		{
			$id_group		= $parseUrl[$i]['id_group'];
			$name			= $parseUrl[$i]['name'];
			$created_time	= $parseUrl[$i]['created_time'];
			$created_by		= $parseUrl[$i]['created_by'];
			$statusCode		= $parseUrl[$i]['enabled'];
			?>
            <option value="<?=$id_group;?>"><?=$name;?></option>
            <?
		}
		?>
    </select>
    <?
	curl_close($iUrl);
}

function selectCostCenter ()
{
	$access_token	= $_SESSION['access_token'];
	$url			= URL_WS."WSA-Telcel/api/center?access_token=".$access_token."&offset=0&limit=100";
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$parseUrl		= json_decode($pUrl,true);
	?>
    <select name="cost_center" id="cost_center" class="frm">
    	<option value="">Seleccionar</option>
        <?

		for($i = 0; $i < count($parseUrl); $i++)
		{
			$id_center		= $parseUrl[$i]['id_center'];
			$code			= $parseUrl[$i]['code'];
			$name			= $parseUrl[$i]['name'];
			$created_time	= $parseUrl[$i]['created_time'];
			$username		= $parseUrl[$i]['username'];
			$enabled		= $parseUrl[$i]['enabled'];
			
			if($enabled == "1")
			{
				?>
                <option value="<?=$id_center;?>"><?=$name;?></option>
                <?
			}
		}
		?>
    </select>
    <?
	curl_close($iUrl);
}

function selectService () 
{
	$access_token	= $_SESSION['access_token'];
	$url			= URL_WS."WSA-Telcel/api/tgg/service?access_token=".$access_token;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$parseUrl		= json_decode($pUrl,true);
	?>
    <select name="service" id="service" class="frm">
    	<option value="">Seleccionar</option>
        <?

		for($i = 0; $i < count($parseUrl); $i++)
		{
			$id_service		= $parseUrl[$i]['id_service'];
			$service_name	= $parseUrl[$i]['service_name'];
			$service_tag	= $parseUrl[$i]['service_tag'];
			?>
            <option value="<?=$service_tag;?>"><?=$service_tag;?> - <?=$service_name;?></option>
            <?
		}
		?>
    </select>
    <?
	curl_close($iUrl);
}

function selectServiceActive () 
{
	$access_token	= $_SESSION['access_token'];
	$url			= URL_WS."WSA-Telcel/api/tgg/service?access_token=".$access_token;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$parseUrl		= json_decode($pUrl,true);
	?>
    <select name="service" id="service" class="frm">
    	<option value="">Seleccionar</option>
        <?

		for($i = 0; $i < count($parseUrl); $i++)
		{
			$id_service		= $parseUrl[$i]['id_service'];
			$service_name	= $parseUrl[$i]['service_name'];
			$status			= $parseUrl[$i]['service_status'];
			$service_tag	= $parseUrl[$i]['service_tag'];
			$enable_sms_web	= $parseUrl[$i]['enable_sms_web'];
			$ask_for_ack	= $parseUrl[$i]['ask_for_ack'];

			if($status == "ACTIVE" && $enable_sms_web == "Y")
			{
				?>
                <option value="<?=$service_tag;?>,<?=$ask_for_ack;?>"><?=$service_tag;?> - <?=$service_name;?></option>
                <?
			}
		}
		?>
    </select>
    <?
	curl_close($iUrl);
}

function selectServiceTag ($value) 
{
	$access_token	= $_SESSION['access_token'];
	$url			= URL_WS."WSA-Telcel/api/tgg/service?access_token=".$access_token;
	$iUrl			= curl_init($url);
	curl_setopt($iUrl, CURLOPT_RETURNTRANSFER, TRUE);
	$pUrl			= curl_exec($iUrl);
	$parseUrl		= json_decode($pUrl,true);
	?>
    <select name="service_tag" id="service_tag" class="frm">
    	<option value="">Todos</option>
        <?

		for($i = 0; $i < count($parseUrl); $i++)
		{
			$service_tag	= $parseUrl[$i]['service_tag'];
			
			if($service_tag == $value)
			{
				$selected = "selected";
			} else {
				$selected = "";
			}

			?>
            <option value="<?=$service_tag;?>" <?=$selected;?>><?=$service_tag;?></option>
            <?
		}
		?>
    </select>
    <?
	curl_close($iUrl);
}

function returnFormatDate($string)
{
	if(!empty($string))
	{
		$date	= substr($string, 0, 10);
		$date	= $date;
		$date	= date("d-m-Y",strtotime($date));
		$hour	= substr($string, 11, 8);
		
		if(!empty($hour))
		{
			$hour	= " ".substr($string, 11, 8);
		}
		
		return $date."".$hour;
	}
}

function returnDateUrl($string)
{
	if(!empty($string))
	{
		$d	= substr($string, 0, 2);
		$m	= substr($string, 3, 2);
		$y	= substr($string, 6, 4);
		
		return $y."-".$m."-".$d;
	}
}
?>