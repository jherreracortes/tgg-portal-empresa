package cl.mobid.fileprocessor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UTFDataFormatException;
import java.util.ArrayList;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import cl.mobid.Apps.Main;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        String file,ruta;
        file = "latin1_texto.csv";
        ruta ="/home/dpichinil/Escritorio";
        isfileGroup(null,ruta,file);
    }
    
    public static void isfileGroup(String ip, String ruta, String file) {
//		JSch jsch = new JSch();
//		Session session = null;
		FileReader reader = null;
		BufferedReader buffer = null;
		ArrayList<String> data = new ArrayList<String>();
		try {
//			Main.toLog("[" + Main.fecha_log(false) + "] Group EMPIEZA CONEXION");
//			session = jsch.getSession("procfile", ip);
//			session = jsch.getSession("appmobid", ip);
//			java.util.Properties config = new java.util.Properties();
//			config.put("StrictHostKeyChecking", "no");
//			session.setConfig(config);
//			if (ip.equals("")) {
//
//			} else {
//			}
//			session.setPassword("proc_tgg-file");
//			session.setPassword("app_fen-mobid+");
//			session.connect();
//			Channel channel = session.openChannel("sftp");
//			channel.connect();
//			ChannelSftp sftpChannel = (ChannelSftp) channel;
//			Main.toLog("Is connected to IP:" + channel.isConnected());
//			@SuppressWarnings("rawtypes")
//			Vector ls = sftpChannel.ls(ruta);
//			for (int i = 0; i < ls.size(); i++) {
//				Main.toLog("[" + Main.fecha_log(false) + "] Group Ls:" + ls.get(i));
//			}
//			sftpChannel.cd(ruta);
//			InputStream is = sftpChannel.get(file);
			FileInputStream is = new FileInputStream(new File(ruta+"/"+file)) ;
			buffer = new BufferedReader(new InputStreamReader(is,"ISO-8859-1"));
//			buffer = new BufferedReader(new InputStreamReader(is,"UTF-8"));
			String getLine = "";
			while ((getLine = buffer.readLine()) != null) {
//				System.out.println("text latin: "+getLine);
				byte[] latin1arr=getLine.getBytes("ISO-8859-1");
//				imprimirByteToHexa("hexa latin: ",latin1arr);
				byte[] utf8arr=new String(latin1arr,"ISO-8859-1").getBytes("UTF-8");
				String textUtf8 = new String(utf8arr, "UTF-8");
//				System.out.println("text utf8: "+textUtf8);
//				imprimirByteToHexa("hexa utf8: ",utf8arr);
				data.add(textUtf8);
			}
//			sftpChannel.exit();
//			session.disconnect();
//			Main.toLog("[" + Main.fecha_log(false) + "] Group Line: " + data.size());
//		} catch (JSchException e) {
//			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null)
					reader.close();
				if (buffer != null)
					buffer.close();
			} catch (Exception e) {
//				e.printStackTrace();
//				Main.toLog("Exception:" + e);
			}
		}
//		System.out.println(data);
//		return data;
	}

	private static void imprimirByteToHexa(String texto, byte[] areglo) {
		StringBuilder sb = new StringBuilder(); 
		
//		for (int i = 0; i < areglo.length; i++) {
//			text += " "+javax.xml.bind.DatatypeConverter.printByte(areglo[i]);
//		}
		for(byte b: areglo)
		sb.append(String.format(" %02x", b));
		System.out.println(texto+sb.toString());
	}
    
    
}
