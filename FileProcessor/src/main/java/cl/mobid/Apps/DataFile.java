package cl.mobid.Apps;

public class DataFile {
	  private String msisdn;
	  private String texto;
	  
	  public DataFile() {}
	  
	  public String getMsisdn() {
	    return msisdn;
	  }
	  
	  public void setMsisdn(String msisdn) {
	    this.msisdn = msisdn;
	  }
	  
	  public String getTexto() {
	    return texto;
	  }
	  
	  public void setTexto(String texto) {
	    this.texto = texto;
	  }
	}
