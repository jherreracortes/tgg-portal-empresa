package cl.mobid.Apps;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Vector;

public class GetFile {
	public GetFile() {
	}

	public static ArrayList<DataFile> isfile(String ip, String ruta, String file) {
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader buffer = null;
		ArrayList<DataFile> data = new ArrayList<DataFile>();
		try {
			Main.toLog("[" + Main.fecha_log(false) + "] EMPIEZA CONEXION " + ip + " " + ruta + file);
			session = jsch.getSession("procfile", ip);

			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword("proc_tgg-file");

			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			Main.toLog("Is connected to IP:" + channel.isConnected());

//			Vector<?> ls = sftpChannel.ls(ruta);
//			for (int i = 0; i < ls.size(); i++) {
//				Main.toLog("[" + Main.fecha_log(false) + "] Ls:" + ls.get(i));
//			}

			sftpChannel.cd(ruta);
			Main.toLog("revisando file: " + file);
			
			String[] fileArr = file.split("_");
			String charset = null;
			if (fileArr[0].toUpperCase().equals("UTF-8")) {
				charset = "UTF-8";
			} else if (fileArr[0].toUpperCase().equals("LATIN1")) {
				charset = "ISO-8859-1";
			} else if (fileArr[0].toUpperCase().equals("ASCII")) {
				charset = "ASCII";
			} else {
				charset = "otro";
			}
			if (!charset.equals("otro")) {
				InputStream is = sftpChannel.get(file);
				InputStreamReader isr = new InputStreamReader(is, charset);
				buffer = new BufferedReader(isr);
				String getLine = "";

				while ((getLine = buffer.readLine()) != null) {
					// se transforma a utf8 los textos
					if (charset.equals("ascii")||charset.equals("ASCII")) {
						byte[] latin1arr = getLine.getBytes(charset);
						byte[] utf8arr = new String(latin1arr, charset).getBytes("UTF-8");
						String textUtf8 = new String(utf8arr, "UTF-8");
						getLine = textUtf8;
					} else if (!charset.equals("UTF-8")) {
						byte[] latin1arr = getLine.getBytes(charset);
						byte[] utf8arr = new String(latin1arr, charset).getBytes("UTF-8");
						String textUtf8 = new String(utf8arr, "UTF-8");
						getLine = textUtf8;
					}
					DataFile df = new DataFile();
					String[] split = getLine.split(";", 2);
					df.setMsisdn(split[0]);
					df.setTexto(split[1]);
					data.add(df);
				}
				sftpChannel.exit();
				session.disconnect();
			} else {
				Main.toLog("[" + Main.fecha_log(false) + "] Line: archivo sin encoding conocido");
			}
			
			Main.toLog("[" + Main.fecha_log(false) + "] Line: " + data.size());
			try {
				if (buffer != null)
					buffer.close();
			} catch (Exception e) {
				StringBuilder sb = Main.stackTraceToString(e);
				Main.toLog(sb.toString());
			}

			Main.toLog("OUT");
		} catch (Exception e) {
			StringBuilder sb = Main.stackTraceToString(e);
			Main.toLog(sb.toString());
		} finally {
			try {
				if (buffer != null)
					buffer.close();
			} catch (Exception e) {
				StringBuilder sb = Main.stackTraceToString(e);
				Main.toLog(sb.toString());
				Main.toLog("Exception:" + e);
			}
		}

		return data;
	}

	public static ArrayList<String> isfileGroup(String ip, String ruta, String file) {
		JSch jsch = new JSch();
		Session session = null;
		FileReader reader = null;
		BufferedReader buffer = null;
		ArrayList<String> data = new ArrayList<String>();
		try {
			Main.toLog("[" + Main.fecha_log(false) + "] Group EMPIEZA CONEXION");
			session = jsch.getSession("procfile", ip);
			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			if (ip.equals("")) {
			}
			session.setPassword("proc_tgg-file");
			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			Main.toLog("Is connected to IP:" + channel.isConnected());
			/*
			Vector<?> ls = sftpChannel.ls(ruta);
			for (int i = 0; i < ls.size(); i++) {
				Main.toLog("[" + Main.fecha_log(false) + "] Group Ls:" + ls.get(i));
			}
			*/
			sftpChannel.cd(ruta);
			InputStream is = sftpChannel.get(file);

			buffer = new BufferedReader(new InputStreamReader(is));
			String getLine = "";

			while ((getLine = buffer.readLine()) != null) {
				data.add(getLine);
			}
			sftpChannel.exit();
			session.disconnect();

			Main.toLog("[" + Main.fecha_log(false) + "] Group Line: " + data.size());

		} catch (Exception e) {
			data = null;
			StringBuilder sb = Main.stackTraceToString(e);
			Main.toLog(sb.toString());
		} finally {
			try {
				if (reader != null)
					reader.close();
				if (buffer != null)
					buffer.close();
			} catch (Exception e) {
				StringBuilder sb = Main.stackTraceToString(e);
				Main.toLog(sb.toString());
			}
		}
		return data;
	}
}
