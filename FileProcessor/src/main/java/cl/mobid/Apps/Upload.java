package cl.mobid.Apps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Upload implements Runnable
{
  GetData s = null;
  
  GetDataGroup r = null;
  
  public Upload(GetData s) {
    this.s = s;
  }
  
  public Upload(GetDataGroup r) {
    this.r = r;
  }
  
  public void run()
  {
    try {
    	DBAccess db = new DBAccess();
      Main.toLog("[" + Main.fecha_log(false) + "] EMPIEZA EL THREAD");
      
      if (s != null) {
    	
  		
        HashMap<String, String> d = db.getPaisData(s.getId_pais());
        ArrayList<DataFile> data = new ArrayList<DataFile>();
        data = DataUpload.data;
        for (Iterator<DataFile> iterator = data.iterator(); iterator.hasNext();) {
          DataFile dataFile = (DataFile)iterator.next();
          
          if (Main.validateMSISDN(dataFile.getMsisdn(), (String)d.get("length"))) {
            s.setStage("QUEUED");
          } else {
            s.setStage("FAILED");
            s.setResponse_code("401");
          }
          if ((s.getDispatch_time() == null) || (s.getDispatch_time().length() <= 0))
          {
            s.setDispatch_time("");
          }
          
          if ((dataFile.getTexto().length() > 0) && (dataFile.getTexto().length() < 161))
          {

            s.setStage("QUEUED");
          } else {
            s.setStage("FAILED");
            s.setResponse_code("400");
          }
          
          db.uploadMessage(s, dataFile);
        }
        
        db.updateprocess(s.getId(), "file_process", "FINISH");
      }
      else
      {
        ArrayList<String> data = new ArrayList<String>();
        data = DataUpload.dataGroup;
        Main.toLog("[" + Main.fecha_log(false) + "] EMPIEZA la subida de grupo " + data.size());
        for (int i = 0; i < data.size(); i++) {
          String msisdn = (String)data.get(i);
          Main.toLog("[" + Main.fecha_log(false) + "] TELEFONOS " + msisdn);
          if (msisdn != null) {
            db.uploadMsisdn(msisdn, r.getId_group(), r.getCreated_by());
          }
        }
        db.updateprocess(r.getIdgroup_process(), "group_process", "FINISH");
      }
      
      Main.toLog("[" + Main.fecha_log(false) + "] TERMINA EL THREAD");
    }
    catch (Exception e) {
    	StringBuilder sb = Main.stackTraceToString(e);
		Main.toLog(sb.toString());
    }
  }
}
