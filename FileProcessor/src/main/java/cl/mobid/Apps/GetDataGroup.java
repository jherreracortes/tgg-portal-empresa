package cl.mobid.Apps;

import java.util.HashMap;

public class GetDataGroup {

	private String idgroup_process;
	private String ip_server;
	private String file_path;
	private String file_name;
	private String stage;
	private String created_time;
	private String id_group;
	private String created_by;
	private boolean data;

	public GetDataGroup() {
		DBAccess db = new DBAccess();
		HashMap<String, String> hm = db.getDataGroup();

		if (hm != null) {
			idgroup_process = ((String) hm.get("id_process"));
			ip_server = ((String) hm.get("ip_server"));
			file_path = ((String) hm.get("file_path"));
			file_name = ((String) hm.get("file_name"));
			stage = ((String) hm.get("stage"));
			created_time = ((String) hm.get("created_time"));
			id_group = ((String) hm.get("id_group"));
			created_by = ((String) hm.get("created_by"));
			data = true;
		} else {
			data = false;
		}
	}

	public String getIdgroup_process() {
		return idgroup_process;
	}

	public String getIp_server() {
		return ip_server;
	}

	public String getFile_path() {
		return file_path;
	}

	public String getFile_name() {
		return file_name;
	}

	public String getStage() {
		return stage;
	}

	public String getCreated_time() {
		return created_time;
	}

	public String getId_group() {
		return id_group;
	}

	public boolean isData() {
		return data;
	}

	public String getCreated_by() {
		return created_by;
	}
}
