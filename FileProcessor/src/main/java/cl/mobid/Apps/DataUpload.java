package cl.mobid.Apps;

import java.util.ArrayList;

public class DataUpload {
	  public static ArrayList<DataFile> data;
	  public static ArrayList<String> dataGroup;
	  
	  public DataUpload() {}
	  
	  public static void setDataFile(ArrayList<DataFile> df) {
	    data = new ArrayList<DataFile>();
	    data = df;
	  }
	  
	  public static void setDataGroup(ArrayList<String> df) {
	    dataGroup = new ArrayList<String>();
	    dataGroup = df;
	  }
	  
	  public static void subir(GetData g)
	  {
	    Upload r = new Upload(g);
	    r.run();
	  }
	  
	  public static void subirGroup(GetDataGroup gdg) {
	    Upload r = new Upload(gdg);
	    r.run();
	  }
	}
