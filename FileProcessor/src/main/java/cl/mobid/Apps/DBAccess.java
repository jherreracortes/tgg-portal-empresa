package cl.mobid.Apps;

import java.sql.CallableStatement;
import com.google.gson.JsonObject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sql.DataSource;

public class DBAccess {
	public DBAccess() {
	}

	private ArrayList<HashMap<String, String>> getMysqlResponse(String sql, boolean data) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(Main.DATA_BASE);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			if (!data) {
				while (rs.next()) {
					HashMap<String, String> hm = new HashMap<String, String>();

					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						String colname = rsmd.getColumnName(i);
						hm.put(colname, rs.getString(colname));
					}
					arrayList.add(hm);
				}
			}else {
				HashMap<String, String> hm = new HashMap<String, String>();
				while (rs.next()) {
					System.out.println("rs.next");
					hm.put(rs.getString(rsmd.getColumnName(2)), rs.getString(rsmd.getColumnName(1)));
				}
				arrayList.add(hm);
			}
			return arrayList;
		} catch (SQLException e) {
			StringBuilder sb = Main.stackTraceToString(e);
			Main.toLog(sb.toString());
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = Main.stackTraceToString(e);
				Main.toLog(sb.toString());
				return null;
			}
		}
	}

	private void exec(String sql) {
		Connection con = null;
		Statement stmt = null;
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(Main.DATA_BASE);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			stmt.execute(sql);
			return;
		} catch (SQLException e) {
			StringBuilder sb = Main.stackTraceToString(e);
			Main.toLog(sb.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = Main.stackTraceToString(e);
				Main.toLog(sb.toString());
			}
		}
	}

	public HashMap<String, String> getDataFile() {
		ArrayList<HashMap<String, String>> bal = getMysqlResponse(
				"SELECT  * FROM file_process WHERE stage='QUEUED' AND (dispatch_time < now() OR dispatch_time is NULL) ORDER BY created_time DESC LIMIT 1;",
				false);
		if (bal.size() > 0) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}

	public HashMap<String, String> getDataGroup() {
		ArrayList<HashMap<String, String>> bal = getMysqlResponse(
				"SELECT  * FROM group_process WHERE stage='QUEUED'  ORDER BY created_time DESC LIMIT 1;", false);

		if (bal.size() == 1) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}

	public void updateprocess(String id, String table, String s) {
		String sql = "UPDATE " + table + " SET stage = '" + s + "' WHERE id_process=" + id;

		Main.toLog("[" + Main.fecha_log(false) + "] SQL  " + sql);
		exec(sql);
	}

	public void uploadMessageMYSQL(GetData g, DataFile f) {
		String id_process = g.getId_process();
		String id_pais = g.getId_pais();
		String stage = g.getStage();
		String delivery_receipt = g.getDelivery_receipt();
		String data_coding = g.getData_coding();
		String id_company = g.getId_company();
		String sender = g.getSender();
		String texto = f.getTexto();
		String numero = f.getMsisdn();
		String agendado = g.getDispatch_time();

		Connection con = null;
		CallableStatement cs = null;
		try {
			String sqlStore;
			if (agendado.length() > 0) {
				sqlStore = "{call createMessage('" + numero + "', '" + sender + "', '" + stage + "', '"
						+ Integer.parseInt(delivery_receipt) + "', '" + data_coding + "', '"
						+ Integer.parseInt(id_company) + "', '" + Integer.parseInt(id_pais) + "', '"
						+ Integer.parseInt(id_process) + "', '" + agendado + "', '" + texto + "')}";

			} else {

				sqlStore = "{call createMessage('" + numero + "', '" + sender + "', '" + stage + "', '"
						+ Integer.parseInt(delivery_receipt) + "', '" + data_coding + "', '"
						+ Integer.parseInt(id_company) + "', '" + Integer.parseInt(id_pais) + "', '"
						+ Integer.parseInt(id_process) + "', null, '" + texto + "')}";
			}

			PoolConexionConfig pool = PoolConexionConfig.getInstancia(Main.DATA_BASE);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			cs = con.prepareCall(sqlStore);
			cs.execute();
			return;
		} catch (SQLException e) {
			StringBuilder sb = Main.stackTraceToString(e);
			Main.toLog(sb.toString());
		} finally {
			try {
				if (con != null) {
					con.close();
				}

				if (cs != null) {
					cs.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = Main.stackTraceToString(e);
				Main.toLog(sb.toString());
			}
		}
	}
	
	public void uploadMessageRedis(GetData g, DataFile f) {
	
		
		JsonObject message = new JsonObject();
		message.addProperty("id_process", g.getId_process());
		message.addProperty("id_pais", g.getId_pais());
		message.addProperty("stage", g.getStage());
		message.addProperty("delivery_receipt", g.getDelivery_receipt());
		message.addProperty("data_coding", g.getData_coding());
		message.addProperty("id_company", g.getId_company());
		message.addProperty("sender", g.getSender());
		message.addProperty("texto", f.getTexto());
		message.addProperty("numero", f.getMsisdn());
		message.addProperty("agendado", g.getDispatch_time());
		
		String json = message.toString();
		
		RedisAccess.addToQueue("traffic_mt",json);

		
	}

	public void uploadMsisdn(String g, String f, String d) {
		Connection con = null;
		CallableStatement cs = null;
		ResultSet rs = null;
		try {
			String sqlStore = "{call AssignToGroup ('" + g + "', " + f + "," + d + ")};";
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(Main.DATA_BASE);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			cs = con.prepareCall(sqlStore);
			rs = cs.executeQuery();

			if (rs.next()) {
				String codigo = rs.getString(1);
				Main.toLog("CODIGO " + codigo);
			}
			return;
		} catch (SQLException e) {
			StringBuilder sb = Main.stackTraceToString(e);
			Main.toLog(sb.toString());
		} finally {
			try {
				if (con != null) {
					con.close();
				}

				if (cs != null) {
					cs.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = Main.stackTraceToString(e);
				Main.toLog(sb.toString());
			}
		}
	}

	public HashMap<String, String> getPaisData(String id_pais) {
		String sql = "SELECT   * FROM   tgg_country  WHERE   id_country =" + id_pais + ";";

		ArrayList<HashMap<String, String>> resp = getMysqlResponse(sql, false);
		if (resp.size() == 1) {
			return (HashMap<String, String>) resp.get(0);
		}
		return null;
	}

}
