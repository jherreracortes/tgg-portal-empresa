package cl.mobid.Apps;

import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

public class Main
{
  public static String DATA_BASE = "wsa_telcel_la";
  
  public static void main(String[] args)
  {
    DATA_BASE = "wsa_telcel_la";
    int inter = Integer.parseInt(args[0]);
    RedisAccess.connect("127.0.0.1", 6379);
    try
    {
      Timer timer = new Timer();
      timer.schedule(new TimerTask()
      {
        public void run()
        {
          Main.get();
          Main.getGroup();
        }
      }, 0L, inter);
    }
    catch (Exception e)
    {
      StringBuilder sb = stackTraceToString(e);
      toLog(sb.toString());
    }
  }
  
  public static void get()
  {
    try
    {
      DBAccess db = new DBAccess();
      GetData g = new GetData();
      if (g.isData())
      {
        toLog("Start Ciclo");
        db.updateprocess(g.getId(), "file_process", "PROCESSING");
        DataUpload.setDataFile(GetFile.isfile(g.getIp_server(), g.getRuta_archivo(), g.getFile()));
        if (DataUpload.data.size() > 0) {
          DataUpload.subir(g);
        } else {
          db.updateprocess(g.getId(), "file_process", "FAILED");
        }
      }
      else
      {
        toLog("Sin Archivos para cargar");
      }
    }
    catch (Exception e)
    {
      StringBuilder sb = stackTraceToString(e);
      toLog(sb.toString());
    }
  }
  
  public static void getGroup()
  {
    try
    {
      DBAccess db = new DBAccess();
      GetDataGroup gdg = new GetDataGroup();
      if (gdg.isData())
      {
        db.updateprocess(gdg.getIdgroup_process(), "group_process", "PROCESSING");
        
        DataUpload.setDataGroup(GetFile.isfileGroup(gdg.getIp_server(), gdg.getFile_path(), gdg.getFile_name()));
        if (DataUpload.dataGroup.size() > 0)
        {
          toLog("COMENZO A SUBIR ");
          DataUpload.subirGroup(gdg);
          db.updateprocess(gdg.getIdgroup_process(), "group_process", "FINISH");
        }
        else
        {
          db.updateprocess(gdg.getIdgroup_process(), "group_process", "FAILED");
        }
      }
    }
    catch (Exception e)
    {
      StringBuilder sb = stackTraceToString(e);
      toLog(sb.toString());
    }
  }
  
  public static String fecha_log(boolean d)
  {
    Date date = new Date();
    String finalDate = new String();
    Calendar calendario = Calendar.getInstance();
    calendario.setTime(date);
    calendario.add(5, 0);
    String formatFecha;

    if (d) {
      formatFecha = "yyyyMMdd";
    } else {
      formatFecha = "yyyy-MM-dd HH:mm:ss.SSS";
    }
    SimpleDateFormat sdf = new SimpleDateFormat(formatFecha);
    finalDate = sdf.format(calendario.getTime());
    
    return finalDate;
  }
  
  public static boolean validateMSISDN(String msisdn, String len)
  {
    boolean resp = false;
    if (msisdn.length() == Integer.parseInt(len)) {
      resp = true;
    } else {
      resp = false;
    }
    return resp;
  }
  
  public static String changeHour(String fecha, String time_offset)
  {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String fecha_respuesta = null;
    
    Date d = new Date();
    try
    {
      d = df.parse(fecha);
      
      Calendar gc = new GregorianCalendar();
      gc.setTime(d);
      gc.add(10, Integer.parseInt(time_offset));
      Date d2 = gc.getTime();
      fecha_respuesta = df.format(d2);
    }
    catch (ParseException e)
    {
      StringBuilder sb = stackTraceToString(e);
      toLog(sb.toString());
    }
    return fecha_respuesta;
  }
  
  public static StringBuilder stackTraceToString(Exception e)
  {
    StringBuilder sb = new StringBuilder();
    StackTraceElement[] listStackTrace = e.getStackTrace();
    for (StackTraceElement element : listStackTrace)
    {
      sb.append("ERROR: ");
      sb.append(element.toString());
      sb.append("\n");
    }
    return sb;
  }
  
  public static synchronized void toLog(String mess)
  {
    try
    {
      String timestamp = fecha_log(false);
      String eol = System.getProperty("line.separator");
      String date = fecha_log(true);
      FileWriter out = new FileWriter("/home/appmobid/Apps/Logs/fileprocess." + date + ".log", true);
      out.write("[" + timestamp + "] " + mess + eol);
      out.close();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}
