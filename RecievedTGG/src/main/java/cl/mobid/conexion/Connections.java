package cl.mobid.conexion;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import cl.mobid.recieved.Main_Recieved_TGG;
import cl.mobid.recieved.TGG_Country;

public class Connections implements Runnable {

	public static String response;
	private String idService;
	private String company;
	private String USER;
	private String PASS;
	private String URL;
	private String LOG;
	private String TAG_LOG;
	private boolean finish;
	private Pattern pattern;
	ArrayList<TGG_Country> tgg;

	public Connections(String USER, String PASS, String URL, String idService, String company, String LOG,
			String TAG_LOG) {
		this.idService = idService;
		this.company = company;
		this.USER = USER;
		this.PASS = PASS;
		this.URL = URL;
		this.LOG = LOG;
		this.TAG_LOG = TAG_LOG;
		pattern = Pattern.compile(
				"^(?=.*id:(.+?)\\s)(?=.*submit date:(.+?)\\s)(?=.*done date:(.+?)\\s)(?=.*stat:(.+?)\\s)(?=.*err:(.+?)\\s?)(?=.*text:(.+)?)");
		tgg = new ArrayList<TGG_Country>();

		ArrayList<HashMap<String, String>> d = DBAccess.getPaisData();
		for (HashMap<String, String> hm : d) {
			TGG_Country tc = new TGG_Country();
			tc.setId_country((String) hm.get("id_country"));
			tc.setLargo((String) hm.get("length"));
			tc.setTime_gmt((String) hm.get("time_gmt"));
			tc.setName((String) hm.get("name"));
			tc.setCode((String) hm.get("code"));
			tc.setTime_offset((String) hm.get("time_offset"));
		}
		setFinish(false);
	}

	@Override
	public void run() {
		HttpURLConnection connection = null;
		HashMap<String, String> soapRespuesta = null;

		int code = 200;
		byte[] authEncBytes;
		String authStringEnc;
		DataOutputStream wr;
		try {
			String envio = hashToSoap(idService, company);
			URL siteURL = new URL(URL);

			connection = (HttpURLConnection) siteURL.openConnection();

			String authString = USER + ":" + PASS;
			Base64 base64 = new Base64();
			authEncBytes = base64.encode(authString.getBytes());
			authStringEnc = new String(authEncBytes);
			connection.setRequestProperty("Authorization", "Basic " + authStringEnc);
			connection.setRequestProperty("SOAPAction",
					"\"http://ws.tiaxa.net/tggDataSoapService/getReceivedMessageComp\"");
			connection.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Length", String.valueOf(envio.length()));
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.connect();
			wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(envio);
			wr.flush();
			wr.close();

			code = connection.getResponseCode();
			Main_Recieved_TGG.to_Log(LOG, TAG_LOG, "CONNECTIONS CODE " + code);
			if (code == 200) {
				InputStream is = connection.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				StringBuilder result = new StringBuilder();
				String line;
				while ((line = reader.readLine()) != null) {
					result.append(line);
				}
				soapRespuesta = sopatohash(result.toString(), company);
			}
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Recieved_TGG.exceptionToString(e.getStackTrace());
			Main_Recieved_TGG.to_Log(LOG, TAG_LOG, sb.toString());
		}
		try {
			Main_Recieved_TGG.to_Log(LOG, TAG_LOG, "CONNECTIONS CERRANDO ");
			connection.disconnect();

			try {
				finalize();
			} catch (Throwable e) {
				StringBuilder sb = new StringBuilder();
				sb = Main_Recieved_TGG.exceptionToString(e.getStackTrace());
				Main_Recieved_TGG.to_Log(LOG, TAG_LOG, sb.toString());
			}
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Recieved_TGG.exceptionToString(e.getStackTrace());
			Main_Recieved_TGG.to_Log(LOG, TAG_LOG, sb.toString());
		}
		// esto es para usar el soap de la respuesta
		Main_Recieved_TGG.to_Log(LOG, TAG_LOG, soapRespuesta.toString());
		setFinish(true);
	}

	private String hashToSoap(String id, String com) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		String strMsg = "";
		try {
			MessageFactory mf = MessageFactory.newInstance();
			SOAPMessage sm = mf.createMessage();

			SOAPPart soapPart = sm.getSOAPPart();

			SOAPEnvelope envelope = soapPart.getEnvelope();
			envelope.addNamespaceDeclaration("tgg", "http://ws.tiaxa.net/tggDataSoapService/");
			SOAPBody soapBody = sm.getSOAPBody();
			QName bodyName = new QName("", "getReceivedMessageCompRequest", "tgg");
			SOAPBodyElement bodyElement = soapBody.addBodyElement(bodyName);
			QName subscriber = new QName("idService");
			QName sender = new QName("company");
			QName requestId = new QName("requestId");

			SOAPElement quotationsubscriber = bodyElement.addChildElement(subscriber);
			if (!id.equals("0")) {
				quotationsubscriber.addTextNode(id);
			}
			SOAPElement quotationsender = bodyElement.addChildElement(sender);
			quotationsender.addTextNode(com);
			String da = Main_Recieved_TGG.fecha_log(false);
			da = da.substring(4);
			da = da.replace(":", "");
			da = da.replace("-", "");
			da = da.replace(" ", "");
			String req = da;

			SOAPElement quotationrequestId = bodyElement.addChildElement(requestId);
			quotationrequestId.addTextNode(req);
			sm.writeTo(os);
			strMsg = new String(os.toByteArray());
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Recieved_TGG.exceptionToString(e.getStackTrace());
			Main_Recieved_TGG.to_Log(LOG, TAG_LOG, sb.toString());
		}
		strMsg = strMsg.replace("SOAP-ENV", "soapenv");
		Main_Recieved_TGG.to_Log(LOG, TAG_LOG, "CONNECTIONS STRING DE SALIDA hashToSoap " + strMsg);
		return strMsg;
	}

	private HashMap<String, String> sopatohash(String input, String company) {
		Main_Recieved_TGG.to_Log(LOG, TAG_LOG, "CONNECTIONS STRING DE ENTRADA sopatohash " + input);

		input = input.replace('\n', ' ');
		input = input.replace('\t', ' ');
		input = input.replaceAll("&lt;", "<");
		input = input.replaceAll("&gt;", ">");
		input = input.replaceAll("&quot;", "\"");
		input = input.replaceAll("  ", "");
		input = input.replaceAll("> <", "><");
		input = input.trim();

		Main_Recieved_TGG.to_Log(LOG, TAG_LOG, "CONNECTIONS STRING DE ENTRADA LIMPIO= " + input);
		HashMap<String, String> fte = new HashMap<String, String>();
		try {
			InputStream is = new ByteArrayInputStream(input.getBytes());
			SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
			SOAPBody sb = request.getSOAPBody();

			NodeList returnList = sb.getElementsByTagName("ns2:getReceivedMessageCompResponse");

			for (int k = 0; k < returnList.getLength(); k++) {
				NodeList innerResultList = returnList.item(k).getChildNodes();

				for (int l = 0; l < innerResultList.getLength(); l++) {
					if (innerResultList.item(l).getNodeName().equalsIgnoreCase("resultCode")) {
						fte.put("resultCode", innerResultList.item(l).getTextContent());
					}

					if (innerResultList.item(l).getNodeName().equalsIgnoreCase("aditionalMessages")) {
						fte.put("aditionalMessages", innerResultList.item(l).getTextContent());
					}

					if (innerResultList.item(l).getNodeName().equalsIgnoreCase("requestId")) {
						fte.put("requestId", innerResultList.item(l).getTextContent());
					}
					if (innerResultList.item(l).getNodeName().equalsIgnoreCase("rowsList")) {
						String service = null;
						String receivedTime = null;
						String receivedTimeGMT = null;
						String country = null;
						String sender = null;
						String type = "";
						String message = null;
						NodeList nl = innerResultList.item(l).getChildNodes();
						for (int i = 0; i < nl.getLength(); i++) {
							if (nl.item(i).getNodeName().equalsIgnoreCase("rowMessage")) {

								Node nodo = nl.item(i);

								if (nodo.getChildNodes().item(0).getNodeName().equalsIgnoreCase("service")) {
									Main_Recieved_TGG.to_Log(LOG, TAG_LOG,
											" service " + nodo.getChildNodes().item(0).getTextContent());

									service = nodo.getChildNodes().item(0).getTextContent();
								}
								if (nodo.getChildNodes().item(1).getNodeName().equalsIgnoreCase("receivedTime")) {
									Main_Recieved_TGG.to_Log(LOG, TAG_LOG,
											" receivedTime " + nodo.getChildNodes().item(1).getTextContent());
									receivedTime = nodo.getChildNodes().item(2).getTextContent();
									receivedTime = receivedTime.replace(".", "");
								}

								if (nodo.getChildNodes().item(2).getNodeName().equalsIgnoreCase("receivedTimeGMT")) {
									Main_Recieved_TGG.to_Log(LOG, TAG_LOG,
											" receivedTimeGMT " + nodo.getChildNodes().item(2).getTextContent());

									receivedTimeGMT = nodo.getChildNodes().item(2).getTextContent();
									receivedTimeGMT = receivedTimeGMT.replace(".", "");
								}
								if (nodo.getChildNodes().item(3).getNodeName().equalsIgnoreCase("country")) {
									Main_Recieved_TGG.to_Log(LOG, TAG_LOG,
											" country " + nodo.getChildNodes().item(3).getTextContent());

									country = nodo.getChildNodes().item(3).getTextContent();
								}
								if (nodo.getChildNodes().item(4).getNodeName().equalsIgnoreCase("sender")) {
									Main_Recieved_TGG.to_Log(LOG, TAG_LOG,
											" sender " + nodo.getChildNodes().item(4).getTextContent());

									sender = nodo.getChildNodes().item(4).getTextContent();
								}
								if (nodo.getChildNodes().item(5).getNodeName().equalsIgnoreCase("type")) {
									Main_Recieved_TGG.to_Log(LOG, TAG_LOG,
											" type " + nodo.getChildNodes().item(5).getTextContent());

									type = nodo.getChildNodes().item(5).getTextContent();
								}
								if (nodo.getChildNodes().item(6).getNodeName().equalsIgnoreCase("message")) {
									Main_Recieved_TGG.to_Log(LOG, TAG_LOG,
											" message " + nodo.getChildNodes().item(6).getTextContent());

									message = nodo.getChildNodes().item(6).getTextContent();
								}
								if (nodo.getChildNodes().item(7).getNodeName().equalsIgnoreCase("operationId")) {
									Main_Recieved_TGG.to_Log(LOG, TAG_LOG,
											" operationId " + nodo.getChildNodes().item(7).getTextContent());
								}

								receivedTimeGMT = changeGMT(receivedTimeGMT);

								if (type.equals("MO")) {
									DBAccess.insertMO(sender, service, company, country, receivedTimeGMT, message);
								} else if (type.equals("DR")) {
									Matcher matcher = pattern.matcher(message);
									matcher.find();
									String id = matcher.group(1).trim();
									String stat = matcher.group(4).trim();
									String text = matcher.group(6).trim();
									String stage;
									if (stat.contains("DELIVRD")) {
										stage = "CONFIRMED";
									} else {
										if ((stat.contains("UNDELIV")) && (text.startsWith("tgg"))) {
											stage = "FAILED";
										} else
											stage = "PUSHED";
									}
									DBAccess.insertMODR(sender, service, company, country, receivedTimeGMT, message);
									DBAccess.updateMT(stage, id, receivedTimeGMT, stat);
								}
							}
						}
					}
				}
			}
		} catch (IOException | SOAPException e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Recieved_TGG.exceptionToString(e.getStackTrace());
			Main_Recieved_TGG.to_Log(LOG, TAG_LOG, sb.toString());
		}

		return fte;
	}

	public boolean isFinish() {
		return finish;
	}

	public void setFinish(boolean finish) {
		this.finish = finish;
	}

	private String changeGMT(String Local) {
		Date localTime = null;
		SimpleDateFormat sdf = null;
		try {
			String format = "yyyy-MM-dd HH:mm:ss";
			sdf = new SimpleDateFormat(format);
			localTime = sdf.parse(Local);
			TimeZone gmtTime = TimeZone.getTimeZone("GMT");
			sdf.setTimeZone(gmtTime);
			Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, "Local:" + localTime.toString()
					+ "," + localTime.getTime() + " --> UTC time:" + sdf.format(localTime));
		} catch (ParseException e) {
			StringBuilder sb = new StringBuilder();
			Main_Recieved_TGG.exceptionToString(e.getStackTrace());
			Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, sb.toString());
		}
		return sdf.format(localTime);
	}

}
