package cl.mobid.conexion;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sql.DataSource;

import cl.mobid.recieved.Main_Recieved_TGG;

public class DBAccess {

	public DBAccess() {
	}

	private static ArrayList<HashMap<String, String>> getMysqlResponse(String sql, boolean data) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(Main_Recieved_TGG.BASE_DATOS);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			if (!data) {
				while (rs.next()) {
					HashMap<String, String> hm = new HashMap<String, String>();

					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						String colname = rsmd.getColumnName(i);
						hm.put(colname, rs.getString(colname));
					}
					arrayList.add(hm);
				}
			} else {
				HashMap<String, String> hm = new HashMap<String, String>();
				while (rs.next()) {
					hm.put(rs.getString(rsmd.getColumnName(2)), rs.getString(rsmd.getColumnName(1)));
				}
				arrayList.add(hm);
			}
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			Main_Recieved_TGG.exceptionToString(e.getStackTrace());
			Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, sb.toString());
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = new StringBuilder();
				Main_Recieved_TGG.exceptionToString(e.getStackTrace());
				Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, sb.toString());

				return null;
			}
		}
		return arrayList;
	}

	private static void exec(String sql) {
		Connection con = null;
		Statement stmt = null;
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(Main_Recieved_TGG.BASE_DATOS);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			stmt.execute(sql);
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			Main_Recieved_TGG.exceptionToString(e.getStackTrace());
			Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, sb.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = new StringBuilder();
				Main_Recieved_TGG.exceptionToString(e.getStackTrace());
				Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, sb.toString());

			}
		}
	}

	public HashMap<String, String> getConf() {
		ArrayList<HashMap<String, String>> bal = getMysqlResponse("SELECT  proc, id FROM proc WHERE proc='Received';",
				true);
		if (bal.size() == 1) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}

	public HashMap<String, String> getProcConf(int id) {
		String sql = "SELECT   param,tag_param FROM proc_params WHERE id_proc = '" + id + "'; ";

		ArrayList<HashMap<String, String>> bal = getMysqlResponse(sql, true);
		if (bal.size() == 1) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}

	public static ArrayList<HashMap<String, String>> getService(String Zona) {
		ArrayList<HashMap<String, String>> respuesta = new ArrayList<HashMap<String, String>>();

		String sql = "SELECT * FROM serviceACK WHERE stage ='MORE' AND zona= '" + Zona + "' ;";
		respuesta = getMysqlResponse(sql, false);
		return respuesta;
	}

	public static void insertMO(String msisdn, String sender, String idCompany, String idCountry, String time,
			String mensaje) {
		String sql = "  INSERT INTO mo_report (`msisdn`,`sender`,`stage`,`id_company`,`id_country`,`created_time`,`content`) VALUES('"
				+

				msisdn + "','" + sender + "','" + "ARRIVED'," + idCompany + "," + idCountry + ",'" + time + "','"
				+ mensaje + "');";
		Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, "SQL MO " + sql);

		exec(sql);
	}

	public static void insertMODR(String msisdn, String sender, String idCompany, String idCountry, String time,
			String mensaje) {
		String sql = "  INSERT INTO mo_report (`msisdn`,`sender`,`stage`,`id_company`,`id_country`,`created_time`,`content`) VALUES('"
				+

				msisdn + "','" + sender + "','" + "DELIVERED'," + idCompany + "," + idCountry + ",'" + time + "','"
				+ mensaje + "');";
		Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, "SQL DR " + sql);

		exec(sql);
	}

	public static void updateMT(String stage, String operationid, String time, String stat) {
		String sql = "UPDATE mt_report SET stage ='" + stage + "',result_text='" + stat + "' ,result_time ='" + time
				+ "' WHERE operation_id='" + operationid + "';";
		Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, "SQL " + sql);
		exec(sql);
	}

	public static ArrayList<HashMap<String, String>> getPaisData() {
		String sql = "SELECT * FROM tgg_country WHERE id_country > 0";

		ArrayList<HashMap<String, String>> resp = getMysqlResponse(sql, false);

		return resp;
	}
}
