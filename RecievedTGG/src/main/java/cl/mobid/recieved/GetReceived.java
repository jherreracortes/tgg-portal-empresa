package cl.mobid.recieved;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cl.mobid.conexion.Connections;
import cl.mobid.conexion.DBAccess;

public class GetReceived {

	public static String xml;
	private static int id = 0;
	public static HashMap<String, String> aver;

	public int getId() {
		return id;
	}

	public GetReceived(final String USER, final String PASS, final String URL, final String ZONA, String INTERVALO,
			final String id, final String LOG, final String TAG_LOG) {
		try {
			int inter = Integer.parseInt(INTERVALO);

			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				public void run() {
					GetReceived.this.doMaintenance(USER, PASS, URL, ZONA, id, LOG, TAG_LOG);
				}
			}, 0L, inter);
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Recieved_TGG.exceptionToString(e.getStackTrace());
			Main_Recieved_TGG.to_Log(LOG, TAG_LOG, sb.toString());
		}
	}

	private void doMaintenance(String USER, String PASS, String URL, String ZONA, String id, String LOG,
			String TAG_LOG) {
		ArrayList<HashMap<String, String>> servicios = DBAccess.getService(ZONA);

		Connections worker = null;
		ExecutorService es = Executors.newFixedThreadPool(2);

		for (int i = 0; i < servicios.size(); i++) {
			HashMap<String, String> servicio = (HashMap<String, String>) servicios.get(i);
			worker = new Connections(USER, PASS, URL, (String) servicio.get("id_service"),
					(String) servicio.get("id_company"), LOG, TAG_LOG);
			es.execute(worker);
		}
		es.shutdown();
		while (!es.isTerminated()) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				StringBuilder sb = null;
				sb = Main_Recieved_TGG.exceptionToString(e.getStackTrace());
				Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, sb.toString());
			}
		}
		Main_Recieved_TGG.to_Log(LOG, TAG_LOG, "NO hay servicios ");

		return;

	}

}
