package cl.mobid.recieved;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import cl.mobid.conexion.DBAccess;

public class Main_Recieved_TGG {
	public static final String BASE_DATOS = "wsa_telcel_la";
	static HashMap<String, GetReceived> dis_val;
	static HashMap<String, String> proc;
	public static DBAccess db = new DBAccess();
	static String USER;
	static String PASS;
	static String URL;
	static String ZONA;
	public static String LOG;
	static String INTERVALO;
	public static String TAG_LOG;
	public static HashMap<String, String> configuracion;

	public Main_Recieved_TGG() {
	}

	public static void main(String[] args) {
		try {
			proc = new HashMap<String, String>();
			dis_val = new HashMap<String, GetReceived>();
			proc = db.getConf();
			if (proc != null) {
				Set<String> s = proc.keySet();
				Iterator<String> iterator = s.iterator();
				while (iterator.hasNext()) {
					String proc_init = (String) iterator.next();
					configuracion = new HashMap<String, String>();
					configuracion = db.getProcConf(Integer.parseInt(proc_init));
					Set<String> values = configuracion.keySet();
					Iterator<String> iterator2 = values.iterator();
					while (iterator2.hasNext()) {
						String value = (String) iterator2.next();
						USER = (String) configuracion.get("USER");
						PASS = (String) configuracion.get("PASS");
						URL = (String) configuracion.get("URL");
						ZONA = (String) configuracion.get("ZONA");
						LOG = (String) configuracion.get("LOG");
						TAG_LOG = (String) configuracion.get("TAG_LOG");
						Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG,
								"[" + fecha_log(false) + "] " + TAG_LOG + " INICIO : Parametros tag = " + value
										+ " para el value " + (String) configuracion.get(value));

						INTERVALO = (String) configuracion.get("INTERVALO");
					}

					GetReceived d = new GetReceived(USER, PASS, URL, ZONA, INTERVALO, proc_init, LOG, TAG_LOG);
					dis_val.put(proc_init, d);
				}

				int inter = Integer.parseInt("60000");
				try {
					Timer timer = new Timer();
					timer.schedule(new TimerTask() {

						public void run() {
						}

					}, 0L, inter);
				} catch (Exception e) {
					StringBuilder sb = new StringBuilder();
					Main_Recieved_TGG.exceptionToString(e.getStackTrace());
					Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, sb.toString());
				}
			} else {
				Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG,
						"[" + fecha_log(false) + "] Maim_TGG: NO HAY PROCESOS PARA LEVANTAR");
			}
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			Main_Recieved_TGG.exceptionToString(e.getStackTrace());
			Main_Recieved_TGG.to_Log(Main_Recieved_TGG.LOG, Main_Recieved_TGG.TAG_LOG, sb.toString());
		}
	}

	public static String fecha_log(boolean d) {
		Date date = new Date();
		String finalDate = new String();

		Calendar calendario = Calendar.getInstance();
		calendario.setTime(date);
		calendario.add(5, 0);
		String formatFecha;
		if (d) {
			formatFecha = "yyyyMMdd";
		} else {
			formatFecha = "yyyy-MM-dd HH:mm:ss";
		}

		SimpleDateFormat sdf = new SimpleDateFormat(formatFecha);
		finalDate = sdf.format(calendario.getTime());

		return finalDate;
	}

	public static void to_Log(String log, String taglog, String f) {
		try {
			String eol = System.getProperty("line.separator");
			String date = Main_Recieved_TGG.fecha_log(true);
			FileWriter out = new FileWriter(log + "/" + taglog.replaceAll("\\.log", "") + "." + date + ".log", true);
			String timestamp = Main_Recieved_TGG.fecha_log(false);
			out.write("[" + timestamp + "] " + f + eol);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static StringBuilder exceptionToString(StackTraceElement[] listStackTrace) {
		StringBuilder sb = new StringBuilder();
		for (StackTraceElement element : listStackTrace) {
			sb.append("ERROR: ");
			sb.append(element.toString());
			sb.append("\n");
		}
		return sb;
	}

}
