package cl.mobid.recieved;

public class TGG_Country {
	String id_country;
	String name;
	String code;
	String time_offset;
	String time_gmt;
	String prefix;
	String largo;

	public TGG_Country() {
	}

	public String getId_country() {
		return id_country;
	}

	public void setId_country(String id_country) {
		this.id_country = id_country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getTime_offset() {
		return time_offset;
	}

	public void setTime_offset(String time_offset) {
		this.time_offset = time_offset;
	}

	public String getTime_gmt() {
		return time_gmt;
	}

	public void setTime_gmt(String time_gmt) {
		this.time_gmt = time_gmt;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getLargo() {
		return largo;
	}

	public void setLargo(String largo) {
		this.largo = largo;
	}
}
