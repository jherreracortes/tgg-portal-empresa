package cl.mobid;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sql.DataSource;

public class DBAccess {

	public DBAccess() {

	}

	private static ArrayList<HashMap<String, String>> getMysqlResponse(String sql) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap <String, String>>();
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(Main_Group.BASE_DATOS);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			while (rs.next()) {
				HashMap<String, String> hm = new HashMap<String, String>();

				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					String colname = rsmd.getColumnName(i);
					hm.put(colname, rs.getString(colname));
				}
				arrayList.add(hm);
				stmt.clearBatch();
				exec("UPDATE group_message SET STAGE='PROCESANDO' WHERE id_process =" + (String) hm.get("id_process"));
			}
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Group.exceptionToString(e);
			Main_Group.toLog(sb.toString());
			return null;
		} finally {
			cerrarConexiones(con, stmt, rs);
		}
		return arrayList;
	}

	private static ArrayList<String> getMysqlSimpleResponse(String sql) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<String> arrayList = new ArrayList<String>();
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(Main_Group.BASE_DATOS);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);

			while (rs.next()) {

				arrayList.add(rs.getString("numero"));
			}
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Group.exceptionToString(e);
			Main_Group.toLog(sb.toString());
			return null;
		} finally {
			cerrarConexiones(con, stmt, rs);
		}
		return arrayList;
	}

	public static void exec(String sql) {
		Connection con = null;
		Statement stmt = null;
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(Main_Group.BASE_DATOS);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			stmt.execute(sql);
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Group.exceptionToString(e);
			Main_Group.toLog(sb.toString());
		} finally {
			cerrarConexiones(con, stmt, null);
		}
	}

	public static ArrayList<HashMap<String, String>> getGroupMessage() {
		String sql = "SELECT  * FROM group_message WHERE stage='QUEUED' AND (dispatch_time < now() OR dispatch_time is NULL) ORDER BY created_time DESC LIMIT 1;";
		return getMysqlResponse(sql);
	}

	public static ArrayList<String> getListGrupo(String id) {
		String sql = "SELECT pm.msisdn as numero from prov_mobile pm , prov_group_mobile pgm WHERE pgm.id_group =" + id
				+ " AND pgm.id_mobile = pm.id_mobile ;";
		return getMysqlSimpleResponse(sql);
	}

	public static void setMessage(String id_process, String id_pais, String stage, String delivery_receipt,
			String data_coding, String id_company, String sender, String texto, String numero, String agendado) {
		Connection con = null;
		CallableStatement cs = null;
		try {
			String sqlStore;
			if (agendado.length() > 0) {
				sqlStore =

						"{call createMessage('" + numero + "', '" + sender + "', '" + stage + "', '"
								+ Integer.parseInt(delivery_receipt) + "', '" + data_coding + "', '"
								+ Integer.parseInt(id_company) + "', '" + Integer.parseInt(id_pais) + "', '"
								+ Integer.parseInt(id_process) + "', '" + agendado + "', '" + texto + "')}";
			} else {
				sqlStore =

						"{call createMessage('" + numero + "', '" + sender + "', '" + stage + "', '"
								+ Integer.parseInt(delivery_receipt) + "', '" + data_coding + "', '"
								+ Integer.parseInt(id_company) + "', '" + Integer.parseInt(id_pais) + "', '"
								+ Integer.parseInt(id_process) + "', null, '" + texto + "')}";
			}

			PoolConexionConfig pool = PoolConexionConfig.getInstancia(Main_Group.BASE_DATOS);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			cs = con.prepareCall(sqlStore);
			cs.execute();
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Group.exceptionToString(e);
			Main_Group.toLog(sb.toString());
		} finally {
			cerrarConexiones(con,cs,null);
		}
	}

	private static void cerrarConexiones(Connection con, Statement stmt,ResultSet rs) {
		try {
			if(rs!=null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Group.exceptionToString(e);
			Main_Group.toLog(sb.toString());
		}
	}

	public static void updateprocess(String id, String table, String s) {
		String sql = "UPDATE " + table + " SET stage = '" + s + "' WHERE id_process=" + id;
		exec(sql);
	}

}
