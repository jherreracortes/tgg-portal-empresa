package cl.mobid;

import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Timer;

public class Main_Group {
	public static String BASE_DATOS="wsa_telcel_la";
	
	public static void main(String[] args) {
		int inter = Integer.parseInt(args[0]);
		try {
			Timer timer = new Timer();
			timer.schedule(new java.util.TimerTask() {

				public void run() {
				}
			}, 0L, inter);
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Group.exceptionToString(e);
			Main_Group.toLog(sb.toString());
		}
	}

	public static void procesar() {
		ArrayList<HashMap<String, String>> data = DBAccess.getGroupMessage();
		toLog("Cantidad de datos " + data.size());
		for (HashMap<String, String> hm : data) {
			toLog("PROCESANDO DATOS " + hm.toString());
			String id = (String) hm.get("id_process");
			DBAccess.exec("UPDATE group_message SET STAGE='PROCESANDO' WHERE id_process =" + id);
			Group_Processor gp = new Group_Processor(hm);
			gp.run();
		}
	}

	public static String fecha_log(boolean d) {
		Date date = new Date();
		String finalDate = new String();
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(date);
		calendario.add(5, 0);
		String formatFecha;
		if (d) {
			formatFecha = "yyyyMMdd";
		} else {
			formatFecha = "yyyy-MM-dd HH:mm:ss.SSS";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formatFecha);
		finalDate = sdf.format(calendario.getTime());
		return finalDate;
	}

	public static boolean validateMSISDN(String msisdn, String len) {
		boolean resp = false;
		if (msisdn.length() == Integer.parseInt(len)) {
			resp = true;
		} else {
			resp = false;
		}
		return resp;
	}

	public static String changeHour(String fecha, String time_offset) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String fecha_respuesta = null;
		Date d = new Date();
		try {
			d = df.parse(fecha);
			Calendar gc = new GregorianCalendar();
			gc.setTime(d);
			gc.add(10, Integer.parseInt(time_offset));
			Date d2 = gc.getTime();
			fecha_respuesta = df.format(d2);
		} catch (ParseException e) {
			StringBuilder sb = new StringBuilder();
			sb = Main_Group.exceptionToString(e);
			Main_Group.toLog(sb.toString());
		}
		toLog("hora inicial " + fecha);
		toLog("hora final " + fecha_respuesta);
		Date d3 = new Date();
		toLog("hora now server " + df.format(d3));
		return fecha_respuesta;
	}

	public static synchronized void toLog(String mess) {
		try {
			String eol = System.getProperty("line.separator");
			String date = fecha_log(true);
			FileWriter out = new FileWriter("/home/appmobid/Apps/Logs/groupprocess." + date + ".log", true);
			String timestamp = fecha_log(false);
			out.write("[" + timestamp + "] " + mess + eol);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static StringBuilder exceptionToString(Exception e) {
		StringBuilder sb = new StringBuilder();
		StackTraceElement[] listStackTrace = e.getStackTrace(); 
		for (StackTraceElement element : listStackTrace) {
			sb.append("ERROR: ");
			sb.append(element.toString());
			sb.append("\n");
		}
		return sb; 
	}
	
}
