package cl.mobid;

public class Group_Message {
	String id_process;
	String id_group;
	String sender;
	String stage;
	String delivery_receipt;
	String data_coding;
	String id_company;
	String id_country;
	String input_process;
	String created_time;
	String dispatch_time;
	String content;
	String total;

	public Group_Message() {
	}

	public String getId_process() {
		return id_process;
	}

	public void setId_process(String id_process) {
		this.id_process = id_process;
	}

	public String getId_group() {
		return id_group;
	}

	public void setId_group(String id_group) {
		this.id_group = id_group;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getDelivery_receipt() {
		return delivery_receipt;
	}

	public void setDelivery_receipt(String delivery_receipt) {
		this.delivery_receipt = delivery_receipt;
	}

	public String getData_coding() {
		return data_coding;
	}

	public void setData_coding(String data_coding) {
		this.data_coding = data_coding;
	}

	public String getId_company() {
		return id_company;
	}

	public void setId_company(String id_company) {
		this.id_company = id_company;
	}

	public String getId_country() {
		return id_country;
	}

	public void setId_country(String id_country) {
		this.id_country = id_country;
	}

	public String getInput_process() {
		return input_process;
	}

	public void setInput_process(String input_process) {
		this.input_process = input_process;
	}

	public String getCreated_time() {
		return created_time;
	}

	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}

	public String getDispatch_time() {
		return dispatch_time;
	}

	public void setDispatch_time(String dispatch_time) {
		this.dispatch_time = dispatch_time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

}
