package cl.mobid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Group_Processor implements Runnable {

	Group_Message gm = new Group_Message();

	public Group_Processor(HashMap<String, String> hm) {
		gm.setId_process((String) hm.get("id_process"));
		gm.setId_group((String) hm.get("id_group"));
		gm.setSender((String) hm.get("sender"));
		gm.setStage((String) hm.get("stage"));
		gm.setDelivery_receipt((String) hm.get("delivery_receipt"));
		gm.setData_coding((String) hm.get("data_coding"));
		gm.setId_company((String) hm.get("id_company"));
		gm.setId_country((String) hm.get("id_country"));
		gm.setInput_process((String) hm.get("input_process"));
		gm.setCreated_time((String) hm.get("created_time"));
		gm.setDispatch_time((String) hm.get("dispatch_time"));
		gm.setContent((String) hm.get("content"));
		gm.setTotal((String) hm.get("total"));
	}

	@Override
	public void run() {
		String idgrupos = gm.getId_group();
		String totalGrupos = gm.getTotal();
		idgrupos = idgrupos.replace("[", "");
		idgrupos = idgrupos.replace("]", "");
		idgrupos = idgrupos.replace(" ", "");
		totalGrupos = totalGrupos.replace("[", "");
		totalGrupos = totalGrupos.replace("]", "");
		totalGrupos = totalGrupos.replace(" ", "");
		String[] ides = idgrupos.split(",");
		String[] gropusde = totalGrupos.split(",");
		for (int i = 0; i < ides.length; i++) {
			Main_Group.toLog("ides " + ides[i]);
			Main_Group.toLog("gropusde " + gropusde[i]);
			ArrayList<String> s = DBAccess.getListGrupo(ides[i]);
			if (s.size() == Integer.parseInt(gropusde[i])) {
				for (Iterator<String> iterator = s.iterator(); iterator.hasNext();) {
					String numero = (String) iterator.next();
					String agendado = "";
					if (gm.getDispatch_time() != null) {
						agendado = gm.getDispatch_time();
					}
					DBAccess.setMessage(gm.getInput_process(), gm.getId_country(), "QUEUED", gm.getDelivery_receipt(),
							gm.getData_coding(), gm.getId_company(), gm.getSender(), gm.getContent(), numero, agendado);
				}
			}
			DBAccess.updateprocess(gm.getId_process(), "group_message", "FINALIZADO");
		}
	}
}
