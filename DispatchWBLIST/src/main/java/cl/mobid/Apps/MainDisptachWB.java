package cl.mobid.Apps;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;

import cl.mobid.conexiones.DBAccess;

public class MainDisptachWB {
	public static String USER = null;
	public static String PASS = null;
	public static String URL = null;
	public static String ZONA = null;
	public static String CANT = null;
	public static String MOD = null;
	public static String DIV = null;
	public static String LOG;
	public static String INTERVALO = null;
	public static String TAG_LOG;
	public static HashMap<String, String> proc;
	static HashMap<String, Dispatch> dis_val;
	public static String DATA_BASE = "wsa_telcel_la";
	
	public MainDisptachWB() {
	}

	public static void main(String[] args) {
		dis_val = new HashMap<String, Dispatch>();
		proc = new HashMap<String, String>();
		proc = DBAccess.getConf();
		if (proc != null) {
			Set<String> s = proc.keySet();
			Iterator<String> iterator = s.iterator();
			while (iterator.hasNext()) {
				String proc_init = (String) iterator.next();

				HashMap<String, String> configuracion = new HashMap<String, String>();
				configuracion = DBAccess.getProcConf(Integer.parseInt(proc_init));
				Set<String> values = configuracion.keySet();
				for (Iterator<String> iterator2 = values.iterator(); iterator2.hasNext();) {
					String value = (String) iterator2.next();
					USER = (String) configuracion.get("USER");
					PASS = (String) configuracion.get("PASS");
					URL = (String) configuracion.get("URL");
					ZONA = (String) configuracion.get("ZONA");
					CANT = (String) configuracion.get("CANT");
					LOG = (String) configuracion.get("LOG");
					MOD = (String) configuracion.get("MOD");
					DIV = (String) configuracion.get("DIV");
					TAG_LOG = (String) configuracion.get("TAG_LOG");
					INTERVALO = (String) configuracion.get("INTERVALO");
					String MENSAJE_LOG = "[" + fecha_log(false) + "] " + TAG_LOG + " INICIO : Parametros tag = " + value
							+ " para el value " + (String) configuracion.get(value);
					MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, MENSAJE_LOG);
				}
				Dispatch d = new Dispatch(USER, PASS, URL, ZONA, CANT, INTERVALO, proc_init, LOG, TAG_LOG, MOD, DIV);
				dis_val.put(proc_init, d);
			}
		}

		int inter = Integer.parseInt("60000");
		try {
			Timer timer = new Timer();
			timer.schedule(new java.util.TimerTask() {

				public void run() {
				}

			}, 0L, inter);
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = MainDisptachWB.exceptionToString(e);
			MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
		}
	}

	public static String fecha_log(boolean d) {
		Date date = new Date();
		String finalDate = new String();
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(date);
		calendario.add(5, 0);
		String formatFecha;
		if (d) {
			formatFecha = "yyyyMMdd";
		} else {
			formatFecha = "yyyy-MM-dd HH:mm:ss.SSS";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formatFecha);
		finalDate = sdf.format(calendario.getTime());

		return finalDate;
	}

	public static void toLog(String log, String taglog, String f) {
		try {
			String eol = System.getProperty("line.separator");
			String date = fecha_log(true);
			FileWriter out = new FileWriter(log + "/" + taglog.replaceAll("\\.log", "") + "." + date + ".log", true);
			String timestamp = fecha_log(false);
			out.write("[" + timestamp + "] " + f + eol);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static StringBuilder exceptionToString(Exception e) {
		StringBuilder sb = new StringBuilder();
		StackTraceElement[] listaExcepciones = e.getStackTrace();
		for (StackTraceElement element : listaExcepciones) {
			sb.append("ERROR:");
			sb.append(element.toString());
			sb.append("\n");
		}
		return sb;
	}

}
