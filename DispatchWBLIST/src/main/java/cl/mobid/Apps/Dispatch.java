package cl.mobid.Apps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cl.mobid.conexiones.DBAccess;
import cl.mobid.conexiones.SendData;
import cl.mobid.pogo.GetData;
import cl.mobid.pogo.SetFile;

public class Dispatch {

	public static HashMap<String, String> aver;

	public Dispatch(final String USER, final String PASS, final String URL, final String ZONA, final String CANT,
			String INTERVALO, final String id, final String LOG, final String TAG_LOG, final String MOD,
			final String DIV) {
		try {
			int inter = Integer.parseInt(INTERVALO);
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				public void run() {
					doMaintenance(USER, PASS, URL, ZONA, CANT, id, LOG, TAG_LOG, MOD, DIV);
					MainDisptachWB.toLog(LOG, TAG_LOG, "DISPATCH HEART BEAT" + ZONA);
				}
			}, 0L, inter);
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = MainDisptachWB.exceptionToString(e);
			MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
		}
	}

	public void doMaintenance(String USER, String PASS, String URL, String ZONA, String CANT, String id, String LOG,
			String TAG_LOG, String MOD, String DIV) {
		GetData gd = new GetData(ZONA);
		MainDisptachWB.toLog(LOG, TAG_LOG, "DISPATCH PROSESANDO ZONA" + ZONA);
		if (gd.isData()) {
			DBAccess.updateprocess(gd.getId(), "PROCESSING");
			ArrayList<String> mt = SetFile.isfile(gd.getIp_server(), gd.getFile_path(), gd.getFile_name());
			String sender = gd.getSender();
			ExecutorService es = Executors.newFixedThreadPool(Integer.parseInt(CANT));
			for (int i = 0; i < mt.size(); i++) {
				String msisdn = (String) mt.get(i);
				MainDisptachWB.toLog(LOG, TAG_LOG, "DISPATCH PROSESANDO WORKER" + msisdn);
				SendData worker = new SendData(USER, PASS, URL, sender, msisdn, LOG, TAG_LOG, gd);
				es.execute(worker);
			}
			while (!es.isTerminated()) {
			}
			DBAccess.updateprocess(gd.getId(), "FINISH");
		} else {
			MainDisptachWB.toLog(LOG, TAG_LOG, "DISPATCH PROSESANDO ID_LAP" + id);
		}
	}
}
