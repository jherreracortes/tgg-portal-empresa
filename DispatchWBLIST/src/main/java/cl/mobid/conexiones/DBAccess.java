package cl.mobid.conexiones;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sql.DataSource;

import cl.mobid.Apps.MainDisptachWB;

public class DBAccess {

	public DBAccess() {
		
	}

	private static ArrayList<HashMap<String, String>> getMysqlResponse(String sql, boolean data) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(MainDisptachWB.DATA_BASE);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			if (!data) {
				while (rs.next()) {
					HashMap<String, String> hm = new HashMap<String, String>();
					for (int i = 1; i <= rsmd.getColumnCount(); i++) {
						String colname = rsmd.getColumnName(i);
						hm.put(colname, rs.getString(colname));
					}
					arrayList.add(hm);
				}
			} else {
				HashMap<String, String> hm = new HashMap<String, String>();
				while (rs.next()) {
					hm.put(rs.getString(rsmd.getColumnName(2)), rs.getString(rsmd.getColumnName(1)));
				}
				arrayList.add(hm);
			}
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			sb = MainDisptachWB.exceptionToString(e);
			MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = new StringBuilder();
				sb = MainDisptachWB.exceptionToString(e);
				MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());

				return null;
			}
		}
		return arrayList;
	}

	private static void exec(String sql) {
		Connection con = null;
		Statement stmt = null;
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(MainDisptachWB.DATA_BASE);
			DataSource ds = pool.getDsMySql();
			con = ds.getConnection();
			stmt = con.createStatement();
			stmt.execute(sql);
		} catch (SQLException e) {
			StringBuilder sb = new StringBuilder();
			sb = MainDisptachWB.exceptionToString(e);
			MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				StringBuilder sb = new StringBuilder();
				sb = MainDisptachWB.exceptionToString(e);
				MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
			}
		}
	}

	public static HashMap<String, String> getDataFile(String ZONA) {
		ArrayList<HashMap<String, String>> bal = getMysqlResponse(
				"SELECT  * FROM file_wb_process JOIN (tgg_region) USING (id_country)WHERE file_wb_process.stage='QUEUED' AND tgg_region.name='"
						+ ZONA + "' ORDER BY file_wb_process.created_time DESC LIMIT 1;",
				false);
		if (bal.size() == 1) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}

	public static void updateprocess(String id, String s) {
		String sql = "UPDATE file_wb_process SET stage = '" + s + "' WHERE id=" + id;
		exec(sql);
	}

	public static void putError(String retcode, String msisdn, String input_process, String type) {
		String sql = "INSERT INTO wb_error_report (retcode,msisdn,input_process,WB_TYPE) VALUE ('" + retcode + "', '"
				+ msisdn + "','" + input_process + "','" + type + "' )";
		exec(sql);
	}

	public static HashMap<String, String> getConf() {
		ArrayList<HashMap<String, String>> bal = getMysqlResponse("SELECT  proc, id FROM proc WHERE proc='DispatchWB';",
				true);
		if (bal.size() == 1) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}

	public static HashMap<String, String> getProcConf(int id) {
		String sql = "SELECT param,tag_param FROM proc_params WHERE id_proc = '" + id + "'; ";
		ArrayList<HashMap<String, String>> bal = getMysqlResponse(sql, true);
		if (bal.size() == 1) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}
}
