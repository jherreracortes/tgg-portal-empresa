package cl.mobid.conexiones;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.NodeList;

import cl.mobid.Apps.MainDisptachWB;
import cl.mobid.pogo.GetData;

public class SendData implements Runnable {

	GetData gd = null;
	private String msisdn = null;
	private String USER;
	private String PASS;
	private String URL;
	private String LOG;
	private String TAG_LOG;

	public SendData(String USER, String PASS, String URL, String sender, String msisdn, String LOG, String TAG_LOG,
			GetData gd) {
		this.msisdn = msisdn;
		this.USER = USER;
		this.URL = URL;
		this.PASS = PASS;
		this.LOG = LOG;
		this.TAG_LOG = TAG_LOG;
		this.gd = gd;
	}

	@Override
	public void run() {
		HttpURLConnection connection = null;
		int code = 200;
		HashMap<String, String> soapRespuesta = null;

		String addurl = "";
		byte[] authEncBytes;
		String authStringEnc;
		DataOutputStream wr;
		try {
			String soapaction;
			String tgg;
			String tgg2;
			if (gd.getType().equals("WL")) {
				soapaction = "\"http://ws.tiaxa.net/tggProvSoapService/addToWListService\"";
				addurl = "addToWListService";
				tgg = "addToWListServiceRequest";
				tgg2 = "addToWListServiceResponse";
			} else {
				if (gd.getType().equals("BL")) {
					soapaction = "\"http://ws.tiaxa.net/tggProvSoapService/addToServRestricted\"";
					addurl = "addToServRestricted";
					tgg = "addToServRestrictedRequest";
					tgg2 = "addToServRestrictedResponse";
				} else {
					if (gd.getType().equals("DWL")) {
						soapaction = "\"http://ws.tiaxa.net/tggProvSoapService/delFromWListService\"";
						addurl = "delFromWListService";
						tgg = "delFromWListServiceRequest";
						tgg2 = "addToServRestrictedResponse";
					} else {
						soapaction = "\"http://ws.tiaxa.net/tggProvSoapService/delFromServRestricted\"";
						addurl = "delFromServRestricted";
						tgg = "delFromServRestrictedRequest";
						tgg2 = "delFromServRestrictedResponse";
					}
				}
			}
			String envio = hashToSoap(msisdn, gd.getSender(), tgg);
			soapRespuesta = new HashMap<String, String>();
			URL siteURL = new URL(URL + "/" + addurl);
			MainDisptachWB.toLog(LOG, TAG_LOG, "  CONNECTIONS URL " + URL + "/" + addurl);
			connection = (HttpURLConnection) siteURL.openConnection();
			String authString = USER + ":" + PASS;
			Base64 base64 = new Base64();
			authEncBytes = base64.encode(authString.getBytes());
			authStringEnc = new String(authEncBytes);
			connection.setRequestProperty("Authorization", "Basic " + authStringEnc);
			connection.setRequestProperty("SOAPAction", soapaction);
			connection.setRequestProperty("Content-Type", "text/xml;charset=ISO-8859-1");
			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Length", String.valueOf(envio.length()));
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.connect();
			wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(envio);
			wr.flush();
			wr.close();
			code = connection.getResponseCode();
			if (code == 200) {
				InputStream is = connection.getInputStream();
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				StringBuilder result = new StringBuilder();
				String line;
				while ((line = reader.readLine()) != null) {
					result.append(line);
				}
				soapRespuesta = sopatohash(result.toString(), tgg2);
			} else {
				soapRespuesta.put("resultCode", "400");
			}
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = MainDisptachWB.exceptionToString(e);
			MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
		}
		try {
			connection.disconnect();
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = MainDisptachWB.exceptionToString(e);
			MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
		}
		if ((soapRespuesta != null) && (!((String) soapRespuesta.get("retCode")).equals("0"))) {
			DBAccess.putError((String) soapRespuesta.get("retCode"), msisdn, gd.getInput_process(), addurl);
		}
	}

	private String hashToSoap(String msisdn, String sender, String tgg) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		String strMsg = "";
		SOAPEnvelope envelope;
		SOAPBody soapBody;
		try {
			MessageFactory mf = MessageFactory.newInstance();
			SOAPMessage sm = mf.createMessage();
			SOAPPart soapPart = sm.getSOAPPart();
			envelope = soapPart.getEnvelope();
			envelope.addNamespaceDeclaration("tgg", "http://ws.tiaxa.net/tggProvSoapService/");
			soapBody = sm.getSOAPBody();
			QName bodyName = new QName("", tgg, "tgg");
			SOAPBodyElement bodyElement = soapBody.addBodyElement(bodyName);
			QName subscriber = new QName("subscriber");
			QName serve = new QName("sender");
			SOAPElement quotationsubscriber = bodyElement.addChildElement(subscriber);
			quotationsubscriber.addTextNode(msisdn);
			SOAPElement quotationsender = bodyElement.addChildElement(serve);
			quotationsender.addTextNode(sender);
			sm.writeTo(os);
			strMsg = new String(os.toByteArray());
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = MainDisptachWB.exceptionToString(e);
			MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
		}
		strMsg = strMsg.replace("SOAP-ENV", "soapenv");
		MainDisptachWB.toLog(LOG, TAG_LOG, "INPUT :" + strMsg);
		return strMsg;
	}

	
	private HashMap<String, String> sopatohash(String input, String tgg) {
		MainDisptachWB.toLog(LOG, TAG_LOG, "OUTPUT :" + input);
		HashMap<String, String> fte = new HashMap<String, String>();
		NodeList returnList;
		int k;
		NodeList innerResultList;
		try {
			InputStream is = new ByteArrayInputStream(input.getBytes());
			SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
			SOAPBody soapBody = request.getSOAPBody();
			returnList = soapBody.getElementsByTagName("ns2:" + tgg);
			for (k = 0; k < returnList.getLength(); k++) {
				innerResultList = returnList.item(k).getChildNodes();
				for (int l = 0; l < innerResultList.getLength(); l++) {
					if (innerResultList.item(l).getNodeName().equalsIgnoreCase("retCode")) {
						fte.put("retCode", innerResultList.item(l).getTextContent());
					}
				}
			}
		} catch (IOException | SOAPException e) {
			StringBuilder sb = new StringBuilder();
			sb = MainDisptachWB.exceptionToString(e);
			MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
			return null;
		}
		return fte;
	}

}
