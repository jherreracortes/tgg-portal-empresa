package cl.mobid.conexiones;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

public class PoolConexionConfig {
	private static PoolConexionConfig instancia = null;

	private DataSource dsMySql = null;

	public DataSource getDsMySql() {
		return dsMySql;
	}

	private PoolConexionConfig(String DB) {
		generateDsMySql(DB);
	}

	private void generateDsMySql(String DB) {
		BasicDataSource basicDataSource = new BasicDataSource();
		basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
		basicDataSource.setUrl("jdbc:mysql://127.0.0.1:3306/" + DB);
		basicDataSource.setUsername("appmobid");
		basicDataSource.setPassword("mobid123");
		basicDataSource.setInitialSize(1);
		basicDataSource.setMaxIdle(1);
		basicDataSource.setMaxTotal(5);
		basicDataSource.setValidationQuery("SELECT 1");
		dsMySql = basicDataSource;
	}

	public static PoolConexionConfig getInstancia(String DB) {
		if (instancia == null) {
			instancia = new PoolConexionConfig(DB);
		}
		return instancia;
	}
}
