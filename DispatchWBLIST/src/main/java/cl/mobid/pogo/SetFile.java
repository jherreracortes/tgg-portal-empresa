package cl.mobid.pogo;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Properties;
//import java.util.Vector;

import com.jcraft.jsch.Session;

import cl.mobid.Apps.MainDisptachWB;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;

public class SetFile {
	
	public SetFile() {
	}

	public static ArrayList<String> isfile(String ip, String ruta, String file) {
		JSch jsch = new JSch();
		Session session = null;
		BufferedReader buffer = null;
		ArrayList<String> data = new ArrayList<String>();

		try {
			session = jsch.getSession("procfile", ip);

			Properties config = new Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword("proc_tgg-file");
			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();
			ChannelSftp sftpChannel = (ChannelSftp) channel;
			sftpChannel.cd(ruta);
			InputStream is = sftpChannel.get(file);
			buffer = new BufferedReader(new InputStreamReader(is));
			String getLine = "";
			while ((getLine = buffer.readLine()) != null) {
				data.add(getLine);
			}
			sftpChannel.exit();
			session.disconnect();
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb = MainDisptachWB.exceptionToString(e);
			MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
		} finally {
			try {
				if (buffer != null)
					buffer.close();
			} catch (Exception e) {
				StringBuilder sb = new StringBuilder();
				sb = MainDisptachWB.exceptionToString(e);
				MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, sb.toString());
			}
		}
		return data;
	}

}
