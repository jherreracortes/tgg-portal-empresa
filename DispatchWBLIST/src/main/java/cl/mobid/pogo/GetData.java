package cl.mobid.pogo;

import java.util.HashMap;

import cl.mobid.Apps.MainDisptachWB;
import cl.mobid.conexiones.DBAccess;

public class GetData {

	private String id;
	private String ip_server;
	private String file_path;
	private String file_name;
	private String sender;
	private String stage;
	private String input_process;
	private String type;
	private String total;
	private String created_time;
	private String id_country;
	private boolean data;

	public GetData(String ZONA) {
		HashMap<String, String> hm = DBAccess.getDataFile(ZONA);

		if (hm != null) {
			id = ((String) hm.get("id"));
			ip_server = ((String) hm.get("ip_server"));
			file_path = ((String) hm.get("file_path"));
			file_name = ((String) hm.get("file_name"));
			sender = ((String) hm.get("sender"));
			stage = ((String) hm.get("stage"));
			input_process = ((String) hm.get("input_process"));
			type = ((String) hm.get("type"));
			total = ((String) hm.get("total"));
			created_time = ((String) hm.get("created_time"));
			id_country = ((String) hm.get("id_country"));
			MainDisptachWB.toLog(MainDisptachWB.LOG, MainDisptachWB.TAG_LOG, hm.toString());
			
			data = true;
		} else {
			data = false;
		}
	}

	public String getId_country() {
		return id_country;
	}

	public void setId_country(String id_country) {
		this.id_country = id_country;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIp_server() {
		return ip_server;
	}

	public void setIp_server(String ip_server) {
		this.ip_server = ip_server;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getInput_process() {
		return input_process;
	}

	public void setInput_process(String input_process) {
		this.input_process = input_process;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getCreated_time() {
		return created_time;
	}

	public void setCreated_time(String created_time) {
		this.created_time = created_time;
	}

	public boolean isData() {
		return data;
	}

	public void setData(boolean data) {
		this.data = data;
	}

}
