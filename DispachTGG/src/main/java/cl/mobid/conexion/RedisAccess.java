package cl.mobid.conexion;

import java.util.List;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;


public class RedisAccess {
	
	private static JedisPool pool;
	

	public static void connect(String host, int port) {
        pool = new JedisPool(host, port);
    }

    public static void close() {
	    pool.close();
    }

    public static JedisPool getPool() {
        return pool;
    }

    public static Jedis getResource() {
        return pool.getResource();
    }
    
    public static void addToQueue(String tag, String objeto) {
        try (Jedis jedis = pool.getResource()) {
            jedis.lpush(tag, objeto);
        }
    }

    public static String removeFromQueue(String tag) {
        try (Jedis jedis = pool.getResource()) {
            List<String> item = jedis.brpop(5, tag);
            if (item.size() > 0) {
                return item.get(1);
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }


    
}
