package cl.mobid.conexion;

import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.sql.DataSource;


import cl.mobid.dispatch.Main_TGG;

public class DBAccess {

	private static String dataBase;
	
	public DBAccess(String DB) {
		dataBase = DB;
	}

	private static ArrayList<HashMap<String, String>> getMysqlResponse(String sql, boolean data) {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(dataBase);
			DataSource connectionPool = pool.getDsMySql();
			con = connectionPool.getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			if (!data) {
				while (rs.next()) {
					HashMap<String, String> hm = new HashMap<String, String>();
					for (int col = 1; col <= rsmd.getColumnCount(); col++) {
						String colname = rsmd.getColumnName(col);
						String coment = null;
						if( rsmd.getColumnTypeName(col).endsWith("BLOB") ) {
							byte[] array = rs.getBytes(col);
							if(array!=null) {
								coment = new String(array,"UTF8");
							}else {
								coment=null;
							}
						}else {
							coment = rs.getString(col);
						}
						hm.put(colname, coment);     
					}
					arrayList.add(hm);
				}
			} else {
				HashMap<String, String> hm = new HashMap<String, String>();
				while (rs.next()) {
					hm.put(rs.getString(rsmd.getColumnName(2)), rs.getString(rsmd.getColumnName(1)));
				}
				arrayList.add(hm);
			}
		} catch (SQLException | UnsupportedEncodingException e) {
			StringBuilder sb = null;
			sb = Main_TGG.stackTraceToString(e.getStackTrace());
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, sb.toString());
			return null;
		} finally {
			cerrarObjectSql(con, stmt, rs);
		}
		return arrayList;
	}
	
	private static ArrayList<HashMap<String, String>> executeSpMysqlResponse(String sql, boolean returnData) {
		Connection con = null;
		CallableStatement stmt = null;
		ResultSet rs = null;
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String, String>>();
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(dataBase);
			DataSource connectionPool = pool.getDsMySql();
			con = connectionPool.getConnection();
			stmt = con.prepareCall(sql);
			boolean resp = stmt.execute();
			if(resp) {
				if(returnData) {
					rs = stmt.getResultSet();
					ResultSetMetaData rsmd = rs.getMetaData();
					while (rs.next()) {
						HashMap<String, String> row = new HashMap<String, String>();
						for (int col = 1; col <= rsmd.getColumnCount(); col++) {
							String colname = rsmd.getColumnName(col);
							String coment = null;
							if( rsmd.getColumnTypeName(col).endsWith("BLOB") ) {
								byte[] array = rs.getBytes(col);
								if(array!=null) {
									coment = new String(array,"UTF8");
								}else {
									coment=null;
								}
							}else {
								coment = rs.getString(col);
							}
							row.put(colname, coment);
						}
						arrayList.add(row);
				    } 
				}
			}
		} catch (SQLException | UnsupportedEncodingException e) {
			StringBuilder sb = Main_TGG.stackTraceToString(e.getStackTrace());
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, sb.toString());
			return null;
		} finally {
			cerrarObjectSql(con, stmt, rs);
		}
		return arrayList;
	}

	private static void cerrarObjectSql(Connection con, Statement stmt, ResultSet rs) {
		try {
			if (rs != null) {
				rs.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			StringBuilder sb = Main_TGG.stackTraceToString(e.getStackTrace());
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, sb.toString());
		}
	}
	
	private static void exec(String sql) {
		Connection con = null;
		Statement stmt = null;
		
		try {
			PoolConexionConfig pool = PoolConexionConfig.getInstancia(dataBase);
			DataSource connectionPool = pool.getDsMySql();
			con = connectionPool.getConnection();
			stmt = con.createStatement();
			stmt.execute(sql);
		} catch (SQLException e) {
			StringBuilder sb=null; 
			sb = Main_TGG.stackTraceToString(e.getStackTrace());
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, sb.toString());
		} finally {
			cerrarObjectSql(con, stmt,null);
		}
	}

	public HashMap<String, String> getConf() {
		ArrayList<HashMap<String, String>> bal = getMysqlResponse("SELECT  proc, id FROM proc WHERE proc='Dispatch';",
				true);
		if (bal.size() == 1) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}

	public HashMap<String, String> getProcConf(int id) {
		String sql = "SELECT param,tag_param FROM proc_params WHERE id_proc = '" + id + "'; ";

		ArrayList<HashMap<String, String>> bal = getMysqlResponse(sql, true);
		if (bal.size() == 1) {
			return (HashMap<String, String>) bal.get(0);
		}
		return null;
	}
	
	public HashMap<String, HashMap<String, String>> getProcConfWS() {
		String sql = "SELECT id_country, ws_user, ws_password, ws_url FROM wsa_telcel_la.dispatch_ws_param";
		HashMap<String, HashMap<String, String>> ws = new HashMap<String, HashMap<String, String>>();
		ArrayList<HashMap<String, String>> bal = getMysqlResponse(sql, true);
		for(HashMap<String, String> country : bal )
		{
			
			ws.put(country.get("id_country"),country);
			
		}
		return ws;
		
	}
	
	

	public ArrayList<HashMap<String, String>> getMessagesMT(String cant, String zona, String mod, String div) {
//		String sql = "SELECT * FROM mt_traffic  JOIN (tgg_region) USING (id_country) WHERE mt_traffic.stage='QUEUED' AND (mt_traffic.dispatch_time < now() OR mt_traffic.dispatch_time is NULL)AND tgg_region.name='"
//				+
//				zona + "' AND MOD(mt_traffic.id_mt, " + div + ") = " + mod
//				+ "  ORDER BY mt_traffic.dispatch_time LIMIT " + cant + "; ";
//		return getMysqlResponse(sql, false);
		String sql = null;
		sql = "{ CALL sp_get_messages_mt('"+zona+"', "+div+", "+mod+", "+cant+") }";
		boolean retornaData = true;
		return executeSpMysqlResponse(sql,retornaData);
		
	}

	public void updateProcessing(ArrayList<HashMap<String, String>> mt, String stage) {
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < mt.size(); i++) {
			if (i < mt.size() - 1) {
				sb.append((String) ((HashMap<?, ?>) mt.get(i)).get("id_mt") + ",");
			} else {
				sb.append((String) ((HashMap<?, ?>) mt.get(i)).get("id_mt"));
			}
		}
		String sql = "UPDATE mt_traffic set stage='" + stage + "' WHERE id_mt IN (" + sb.toString() + ");";
		exec(sql);
	}

	public void updatePushed(HashMap<String, String> mt) {
		String id_ciclo = (String) mt.get("requestId");
		String resultCode = (String) mt.get("resultCode");
		String operationId = (String) mt.get("operationId");
		String delivery_count = (String) mt.get("Delivery");
		
		String LOG_MESSAGE="Datos id_ciclo=" + id_ciclo + " resultCode=" + resultCode + " operationId=" + operationId
				+ " delivery_count=" + delivery_count;
		Main_TGG.to_Log(Main_TGG.PATH ,Main_TGG.TAG_LOG ,LOG_MESSAGE);
		
		if(id_ciclo!=null&& id_ciclo.length()>0) {
			String[] s = id_ciclo.split("_");
			
			String textCode = getCode(resultCode);
			if (resultCode.equals("500")) {
				String sql = "UPDATE mt_traffic set stage='QUEUED', delivery_count=" + delivery_count + " WHERE id_mt = "
						+ s[0] + " AND lap =" + s[1] + " ;";
				exec(sql);
			} else if (resultCode.equals("0")) {
				String sql = "UPDATE mt_traffic set stage='PUSHED' WHERE id_mt = " + s[0] + " AND lap =" + s[1] + " ;";
				exec(sql);

				String sqlReportes = "UPDATE mt_report set stage='PUSHED',operation_id=" + operationId + ",result_code="
						+ resultCode + ",result_text='" + textCode + "', result_time=now()  WHERE id_mt = " + s[0]
						+ " AND lap =" + s[1] + " ;";
				exec(sqlReportes);
			} else if (resultCode.equals("999")) {
				String sql = "UPDATE mt_traffic set stage='FAILED' WHERE id_mt = " + s[0] + " AND lap =" + s[1] + " ;";
				exec(sql);

				String sqlReportes = "UPDATE mt_report set stage='NOPUSHED',operation_id=" + operationId + ",result_code="
						+ resultCode + ",result_text='" + textCode + "', result_time=now()  WHERE id_mt = " + s[0]
						+ " AND lap =" + s[1] + " ;";
				exec(sqlReportes);
			} else {
				String sql = "UPDATE mt_traffic set stage='REJECTED' WHERE id_mt = " + s[0] + " AND lap =" + s[1] + " ;";
				exec(sql);

				if (operationId.length() == 0) {
					operationId = "0";
				}

				String sqlReportes = "UPDATE mt_report set stage='REJECTED',operation_id=" + operationId + ",result_code="
						+ resultCode + ",result_text='" + textCode + "', result_time=now() WHERE id_mt = " + s[0]
						+ " AND lap =" + s[1] + " ;";
				exec(sqlReportes);
			}
			deletePUSHED_FAILED();
		}else {
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG,"ERROR: cl.mobid.conexion.DBAccess.updatePushed(requestId no enviado o con valor null)");
		}
	}

	private void deletePUSHED_FAILED() {
		String sql = "Delete from mt_traffic WHERE stage IN ('PUSHED', 'REJECTED' );";
		exec(sql);
	}

	private String getCode(String code) {
		String sql = "SELECT text FROM Code_response where id= " + code + " ;";
		ArrayList<HashMap<String, String>> r = getMysqlResponse(sql, false);

		if (r.size() == 1) {
			HashMap<String, String> h = (HashMap<String, String>) r.get(0);
			return (String) h.get("text");
		}

		return "Error interno. Ref: [errorCode]";
	}

	public String getText(String id_mt, String lap) {
		String sql = "SELECT content FROM mt_content WHERE id_mt = " + id_mt + " AND lap =" + lap + ";";
		ArrayList<HashMap<String, String>> r = getMysqlResponse(sql, false);

		if (r.size() == 1) {
			HashMap<String, String> h = (HashMap<String, String>) r.get(0);
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, h.toString());
			String content = (String) h.get("content"); 
			return content;
		}
		return "NO";
	}
	
	
	
}
