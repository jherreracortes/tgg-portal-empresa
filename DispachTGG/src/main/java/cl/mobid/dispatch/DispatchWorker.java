package cl.mobid.dispatch;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;

import cl.mobid.conexion.RedisAccess;

public class DispatchWorker implements Runnable {

	int idThread = 0;
	int expiration_time = 0;
	HashMap<String, HashMap<String, String>> ws_config=null;

	public DispatchWorker(int idThread,HashMap<String, HashMap<String, String>> ws_config) {
		this.idThread = idThread;
		this.ws_config = ws_config;
	}

	@Override
	public void run() {
		try {
			while (true) {
				doProcess();
			}

		} catch (Exception e) {
			Main_TGG.to_Log("Error", "Main_TGG.main.catch.exception", "run : InterruptedException");
			System.exit(-1);
		}
	}

	public void doProcess() {
		String message = getMessage();
		if (message == null) {
			// LogGK.log(getHeader() + "No message found");
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, "  No message found " );
		} else {
			processMessage(message);
		}
	}

	public String getMessage() {
		String newMessage = RedisAccess.removeFromQueue("traffic_mt");
		return newMessage;
	}

	public void processMessage(String json) {
		Gson gson = new Gson();
		HashMap<String,String> mt = new HashMap<String,String>();
		mt = (HashMap<String,String>) gson.fromJson(json, mt.getClass());
		HttpURLConnection connection = null;
		int code = 200;
		String URL=(this.ws_config.get(mt.get("id_country"))).get("ws_url");
		String USER=(this.ws_config.get(mt.get("id_country"))).get("ws_user");
		String PASS=(this.ws_config.get(mt.get("id_country"))).get("ws_password");

		HashMap<String, String> soapRespuesta = null;
		byte[] authEncBytes;
		String authStringEncT;
		OutputStreamWriter wr;
		try {
			if (mt.size() > 0) {
				String envio = hashToSoap(mt);
				soapRespuesta = new HashMap<String, String>();

				URL siteURL = new URL(URL);
				Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, "  CONNECTIONS URL " + URL);
				connection = (HttpURLConnection) siteURL.openConnection();

				String authString = USER + ":" + PASS;
				Base64 base64 = new Base64();
				authEncBytes =    base64.encode(authString.getBytes());
				authStringEncT = new String(authEncBytes);
				connection.setRequestProperty("Authorization", "Basic " + authStringEncT);
				connection.setRequestProperty("SOAPAction",
						"\"http://ws.tiaxa.net/tggDataSoapService/sendMessageComp\"");
				connection.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");

				connection.setRequestProperty("Connection", "Keep-Alive");
				connection.setRequestProperty("Content-Length", String.valueOf( envio.length() ));

				connection.setRequestProperty("Accept-Encoding", "gzip,deflate");
				connection.setRequestMethod("POST");
				connection.setDoOutput(true);
				connection.setDoInput(true);
				connection.connect();
				wr = new OutputStreamWriter(connection.getOutputStream());
				wr.write(envio);
				wr.flush();
				wr.close();

				code = connection.getResponseCode();

				Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, "CONNECTIONS COMPANY_ID= " + (String) mt.get("id_company") + " PAIS_ID= "
						+ (String) mt.get("id_country") + " CODE " + code);
				if (code == 200) {
					InputStream is = connection.getInputStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(is));
					StringBuilder result = new StringBuilder();
					String line;
					while ((line = reader.readLine()) != null) {
						result.append(line);
					}
					soapRespuesta = sopatohash(result.toString(),mt);
					String LOG_MESSAGE="CONNECTIONS COMPANY_ID= " + (String) mt.get("id_company")
					+ " PAIS_ID= " + (String) mt.get("id_country") + " RESPUESTA " + result.toString();
					Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, LOG_MESSAGE);
				} else {
					int count = Integer.parseInt((String) mt.get("delivery_count"));
					if (count <= 3) {
						count++;
						soapRespuesta.put("Delivery", String.valueOf(count));
						soapRespuesta.put("resultCode", "500");
						soapRespuesta.put("requestId", "id ciclo cambiar");
					} else {
						soapRespuesta.put("Delivery", String.valueOf(count));
						soapRespuesta.put("resultCode", "400");
						soapRespuesta.put("requestId", "id ciclo cambiar");
					}
				}
			}
		} catch (Exception e) {
			StringBuilder sb = null;
			sb = Main_TGG.stackTraceToString(e.getStackTrace());
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, sb.toString());
			
			HashMap<String, String> h = new HashMap<String, String>();
			h.put("Delivery", "0");
			h.put("resultCode", "999");
			h.put("requestId", "id ciclo cambiar");
			//Main_TGG.db.updatePushed(h);

			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, sb.toString());
		}
		try {
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, "CONNECTIONS CERRANDO " + "id ciclo cambiar");
			connection.disconnect();
		} catch (Throwable e) {
			StringBuilder sb = null;
			sb = Main_TGG.stackTraceToString(e.getStackTrace());
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, sb.toString());
		}
		//Main_TGG.db.updatePushed(soapRespuesta);
	}
	
	private String hashToSoap(HashMap<String, String> mt) {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		String strMsg = "";

		try {
			MessageFactory mf = MessageFactory.newInstance();
			SOAPMessage sm = mf.createMessage();
			sm.setProperty("javax.xml.soap.character-set-encoding", "UTF-8");
			SOAPPart soapPart = sm.getSOAPPart();

			SOAPEnvelope envelope = soapPart.getEnvelope();
			envelope.addNamespaceDeclaration("tgg", "http://ws.tiaxa.net/tggDataSoapService/");
			SOAPBody sb = sm.getSOAPBody();

			QName bodyName = new QName("", "sendMessageCompRequest", "tgg");
			SOAPBodyElement bodyElement = sb.addBodyElement(bodyName);
			QName subscriber = new QName("subscriber");
			QName sender = new QName("sender");
			QName receiptRequest = new QName("receiptRequest");
			QName dataCoding = new QName("dataCoding");
			QName company = new QName("companyId");
			QName message = new QName("message");
			QName requestID = new QName("requestId");
			SOAPElement quotationsubscriber = bodyElement.addChildElement(subscriber);
			quotationsubscriber.addTextNode((String) mt.get("msisdn"));

			SOAPElement quotationsender = bodyElement.addChildElement(sender);
			quotationsender.addTextNode((String) mt.get("sender"));

			SOAPElement quotationreceiptRequest = bodyElement.addChildElement(receiptRequest);
			quotationreceiptRequest.addTextNode((String) mt.get("delivery_receipt"));

			SOAPElement quotationdataCoding = bodyElement.addChildElement(dataCoding);
			quotationdataCoding.addTextNode((String) mt.get("data_coding"));

			SOAPElement quotationcompany = bodyElement.addChildElement(company);
			quotationcompany.addTextNode((String) mt.get("id_company"));

			SOAPElement quotationmessage = bodyElement.addChildElement(message);
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, "mensage " + mt.get("texto"));
			quotationmessage.addTextNode(mt.get("texto"));
			
			SOAPElement quotationrequestID = bodyElement.addChildElement(requestID);
			quotationrequestID.addTextNode((String) mt.get("id_mt") + "_" + (String) mt.get("lap"));
			//ID_CICLO = (String) mt.get("id_mt") + "_" + (String) mt.get("lap");
			sm.writeTo(os);

			strMsg = new String(os.toByteArray(), "UTF-8");
		} catch (Exception e) {
			StringBuilder sb = null;
			sb = Main_TGG.stackTraceToString(e.getStackTrace());
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, sb.toString());
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, sb.toString());
		}

		strMsg = strMsg.replace("SOAP-ENV", "soapenv");
		Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, "CONNECTIONS COMPANY_ID= " + (String) mt.get("id_company") + " PAIS_ID= "
				+ (String) mt.get("id_country") + " STRING DE SALIDA hashToSoap " + strMsg);
		return strMsg;
	}


	private static HashMap<String, String> sopatohash(String input,HashMap<String,String> mt) {
		Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, "CONNECTIONS COMPANY_ID= " + (String) mt.get("id_company") + " PAIS_ID="
				+ (String) mt.get("id_country") + " STRING DE ENTRADA sopatohash " + input);

		HashMap<String, String> fte = new HashMap<String, String>();
		try {
			InputStream is = new ByteArrayInputStream(input.getBytes());
			SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
			SOAPBody sb = request.getSOAPBody();

			NodeList returnList = sb.getElementsByTagName("ns2:sendMessageCompResponse");

			for (int k = 0; k < returnList.getLength(); k++) {
				NodeList innerResultList = returnList.item(k).getChildNodes();
				for (int l = 0; l < innerResultList.getLength(); l++) {
					if (innerResultList.item(l).getNodeName().equalsIgnoreCase("resultCode")) {
						fte.put("resultCode", innerResultList.item(l).getTextContent());
					}

					if (innerResultList.item(l).getNodeName().equalsIgnoreCase("operationId")) {
						fte.put("operationId", innerResultList.item(l).getTextContent());
					}

					if (innerResultList.item(l).getNodeName().equalsIgnoreCase("requestId")) {
						fte.put("requestId", innerResultList.item(l).getTextContent());
					}
				}
			}
		} catch (IOException | SOAPException e) {
			StringBuilder sb = null;
			sb = Main_TGG.stackTraceToString(e.getStackTrace());
			Main_TGG.to_Log(Main_TGG.PATH, Main_TGG.TAG_LOG, sb.toString());
		}
		return fte;
	}
}