package cl.mobid.dispatch;

import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import cl.mobid.conexion.RedisAccess;
import cl.mobid.conexion.DBAccess;

public class Main_TGG {
	public static String PATH;
	public static String TAG_LOG;
	static HashMap<String, String> proc;
	public static DBAccess db = new DBAccess("wsa_telcel_la");

	public static void main(String[] args) {
		try {
			proc = db.getConf();
			TAG_LOG = "Dispatcher";
			PATH = "/home/appmobid/Apps/Logs";
			RedisAccess.connect("127.0.0.1", 6379);
			HashMap<String, HashMap<String, String>> ws_config =db.getProcConfWS(); 
			int NUM_THREADS = 10;
			int i = 0;
			Thread subThr[] = new Thread[NUM_THREADS];
			for (i = 0; i < NUM_THREADS; i++) {
				subThr[i] = new Thread(new DispatchWorker(i,ws_config), "dsp_"+i);
				subThr[i].start();
			}

			for (i = 0; i < NUM_THREADS; i++) {
				subThr[i].join();
			}

		} catch (Exception e) {
			StringBuilder sb = null;
			sb = Main_TGG.stackTraceToString(e.getStackTrace());
			Main_TGG.to_Log("ERROR", "Main_TGG.main.catch.exception", sb.toString());
		}
	}

	public static String fecha_log(boolean d) {
		Date date = new Date();
		String finalDate = new String();

		Calendar calendario = Calendar.getInstance();

		calendario.setTime(date);
		calendario.add(5, 0);
		String formatFecha;
		if (d) {
			formatFecha = "yyyyMMdd";
		} else {
			formatFecha = "yyyy-MM-dd HH:mm:ss";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(formatFecha);
		sdf.setTimeZone(java.util.TimeZone.getTimeZone("GMT"));
		finalDate = sdf.format(calendario.getTime());
		return finalDate;
	}
	
	public static void to_Log(String log, String taglog, String f) {
		try {
			String eol = System.getProperty("line.separator");
			String date = Main_TGG.fecha_log(true);
			//System.out.println(log + "/" + taglog.replaceAll("\\.log", "") + "." + date + ".log");
			FileWriter out = new FileWriter(log + "/" + taglog.replaceAll("\\.log", "") + "." + date + ".log", true);
			String timestamp = Main_TGG.fecha_log(false);
			out.write("[" + timestamp + "] " + f + eol);
			out.close();
		} catch (Exception e) {
			// esto es por si falla escritura en log 
			e.getMessage();
			e.printStackTrace();
		}
	}
	
	public static StringBuilder stackTraceToString(StackTraceElement[] stackTrace) {
		StringBuilder sb = new StringBuilder();
		for (StackTraceElement element : stackTrace) {
			sb.append("ERROR: ");
			sb.append(element.toString());
			sb.append("\n");
		}
		return sb;
	}

}
